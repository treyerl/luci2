/** MIT License
 * @author Lukas Treyer
 * @date Feb 2, 2016
 * */
package luci.connect;

import org.json.JSONObject;

/**Base class of {@link luci.connect.JSONGeometry} and {@link luci.connect.Attachment}. Visibility 
 * restricted to luci.connect package. Both of them consist of a JSONObject holding a required 
 * "format"-value, an optional "name"-, "crs"-, "attributeMap"-value. Wraps JSONObject. 
 * 
 * @author Lukas Treyer
 *
 */
public class JSONFormat extends JSONObject {
	
	public final String UnknownFormat = "unknown";
	String format, name;
	
	/** the default constructor (to be explicitly called by no-args constructors of subclasses)
	 */
	JSONFormat(){
		super();
	}
	
	/**Creates a new JSONFormat object based on the information of the given JSONObject object.
	 * @param name to be taken if the JSONObject does not hold a name.
	 * @param j JSONObject holding relevant JSONFormat information such as "format", "name", "crs","attributeMap" 
	 */
	JSONFormat(String name, JSONObject j) {
		setFormat(j.getString("format"));
		setName(j.optString("name", name));
		copyFrom(j);
	}
	
	/**Copies values from a foreign JSONObject in case the keys are not equal to "format" or "name"
	 * @param j other JSONObject
	 */
	public void copyFrom(JSONObject j){
		for (String k: j.keySet()){
			if (k.equals("format") || k.equals("name")) continue;
			put(k, j.get(k));
		}
	}
	
	/**Creates a new JSONFormat object based on name and format.
	 * @param name String
	 * @param format String
	 */
	JSONFormat(String name, String format){
		setName(name);
		setFormat(format);
	}
	
	/**private method = to be used in the constructor
	 * @param format String
	 */
	void setFormat(String format){
		put("format", (this.format = format));
	}
	
	void setName(String name){
		putOpt("name", (this.name = name));
	}
	
	/**Returns the name associated with this Attachment or JSONGeometry.
	 * @return the name String
	 */
	public String getName(){
		return name;
	}
	
	/**Returns the format string this Attachment or JSONGeometry has been initialized with 
	 * @return the format String
	 */
	public String getFormat() {
		return format;
	}
}
