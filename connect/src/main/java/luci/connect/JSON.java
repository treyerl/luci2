/** MIT License
 * @author Lukas Treyer
 * @date Jun 14, 2016
 * */
package luci.connect;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JSON {
	public static JSONObject fromFile(File f) throws JSONException{
		try {
			return new JSONObject(new JSONTokener(new FileInputStream(f)));
		} catch (FileNotFoundException e) {
			return new JSONObject();
		}
	}
	
	public static void toFile(JSONObject j, File f) throws IOException{
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
			f.createNewFile();
			fos = new FileOutputStream(f);
		}
		fos.write(j.toString(4).getBytes("UTF-8"));
		fos.close();
		
	}
	
	/**
	 * @param j JSONArray
	 * @return a Set of Integers the size of which equals the amount of objects in j that are
	 * convertible into Integers
	 */
	public static Set<Integer> ArrayToIntSet(JSONArray j){
		Set<Integer> ints = null;
		if (j != null) {
			ints = new HashSet<>();
			for (Object o: j){
				try {
					ints.add((Integer)o);
				} catch (ClassCastException e){
					try {
						ints.add(Integer.valueOf(o.toString()));
					} catch(NumberFormatException e1){
						// silently continue
					}
				}
				
			}
		}
		return ints;
	}
	
	/**
	 * @param j JSONArray
	 * @return a Set of Integers the size of which equals the amount of objects in j that are
	 * convertible into Integers
	 */
	public static Set<Long> ArrayToLongSet(JSONArray j){
		Set<Long> longs = null;
		if (j != null) {
			longs = new HashSet<>();
			for (Object o: j){
				try {
					longs.add((Long)o);
				} catch (ClassCastException e){
					try {
						longs.add(Long.valueOf(o.toString()));
					} catch(NumberFormatException e1){
						// silently continue
					}
				}
			}
		}
		return longs;
	}
	
	public static Set<Double> ArrayToDoubleSet(JSONArray j){
		Set<Double> doubles = null;
		if (j != null) {
			doubles = new HashSet<>();
			for (Object o: j){
				try {
					doubles.add(Double.valueOf(o.toString()));
				} catch(NumberFormatException e){
					// silently continue
				}
			}
		}
		return doubles;
	}
	
	public static Set<String> ArrayToStringSet(JSONArray j){
		Set<String> strings = null;
		if (j != null) {
			strings = new HashSet<>();
			for (Object o: j){
				if (o instanceof String) strings.add((String)o);
			}
		}
		return strings;
	}
	
	public static List<Integer> ArrayToIntList(JSONArray j){
		List<Integer> ints = null;
		if (j != null) {
			ints = new ArrayList<>();
			for (Object o: j){
				if (o instanceof Integer) ints.add((Integer)o);
			}
		}
		return ints;
	}
	
	public static List<Long> ArrayToLongList(JSONArray j){
		List<Long> longs = null;
		if (j != null) {
			longs = new ArrayList<>();
			for (Object o: j){
				if (o instanceof Long) longs.add((Long)o);
			}
		}
		return longs;
	}
	
	public static List<Double> ArrayToDoubleList(JSONArray j){
		List<Double> doubles = null;
		if (j != null) {
			doubles = new ArrayList<>();
			for (Object o: j){
				if (o instanceof Double) doubles.add((Double)o);
			}
		}
		return doubles;
	}
	
	public static List<String> ArrayToStringList(JSONArray j){
		List<String> strings = null;
		if (j != null) {
			strings = new ArrayList<>();
			for (Object o: j){
				if (o instanceof String) strings.add((String)o);
			}
		}
		return strings;
	}
	
	public static Set<String> ObjectValuesToStringSet(JSONObject j){
		Set<String> strings = null;
		if (j != null) {
			strings = new HashSet<>();
			for (String key: j.keySet()){
				if (j.get(key) instanceof String) strings.add(j.getString(key));
			}
		}
		return strings;
	}
	
	public static Set<Integer> ObjectValuesToIntSet(JSONObject j){
		Set<Integer> ints = null;
		if (j != null) {
			ints = new HashSet<>();
			for (String key: j.keySet()){
				if (j.get(key) instanceof Integer) ints.add(j.getInt(key));
			}
		}
		return ints;
	}

	/**
	 * @param arrayToIntSet
	 * @return
	 */
	public static Set<Integer> notNullInteger(Set<Integer> set) {
		if (set == null) return new HashSet<>();
		return set;
	}
	
	public static Set<Double> notNullDouble(Set<Double> set) {
		if (set == null) return new HashSet<>();
		return set;
	}
	
	public static Set<String> notNullString(Set<String> set){
		if (set == null) return new HashSet<>();
		return set;
	}
}
