/** MIT License
 * Copyright (c) 2015 Lukas Treyer, ETH Zurich
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * */
package luci.connect;
import static com.esotericsoftware.minlog.Log.debug;
import static com.esotericsoftware.minlog.Log.error;
import static com.esotericsoftware.minlog.Log.trace;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.esotericsoftware.minlog.Log;

import luci.connect.Attachment.AttachmentReferenceException;
import luci.connect.Attachment.ChecksumException;


/**TCPSocketHandler is intended to be used as a base class when implementing clients.
 */
public abstract class TcpSocketHandler implements SocketHandler {
	/**Interface for selection threads to provide information about the status of the selection loop.
	 * In case of changing interests value of SelectionKeys potentially needs the selector to be
	 * woken up, which aborts all currently selected keys.
	 */
	public interface SelectorLoop {
		/**Indicates whether the selector is currently waiting or processing a set of selected keys.
		 * @return true or false
		 */
		public boolean isInLoop();
		/**Returns the selector to which the established connections are registered.
		 * @return Selector
		 */
		public Selector getSelector();
	}
	
	public static class CallException extends Exception {
		private static final long serialVersionUID = 1L;
		private long callID;
		public CallException(Throwable cause, long callID){
			super(cause);
			this.callID = callID;
		}
		public CallException(String cause, long callID){
			super(cause);
			this.callID = callID;
		}
		public long getCallID(){
			return callID;
		}
	}
	
	public static class PanicException extends CallException {
		private static final long serialVersionUID = 1L;
		public PanicException(String string, long callID){
			super(string, callID);
		}
	}
	
	public static class DeadLockException extends IOException {
		private static final long serialVersionUID = 1L;
	}
	
	private static class Pair<A,B> {
		public final A a;
		public final B b;
		Pair(A a, B b){
			this.a = a;
			this.b = b;
		}
	}
	
	/**ForwardTimeout: how long to wait before aborting attachment forwarding
	 */
	public final static int ForwardTimeout = 2;
	
	public final static int ForwardUnlockTimeout = 1000;
	/**ForwardTimeoutUnit set to seconds.
	 */
	public final static TimeUnit ForwardTimeoutUnit = TimeUnit.SECONDS;
	
	/**How many milliseconds to wait before continuing reading, writing, forwarding upon having
	 * read/written/forwarded 0 bytes 
	 */
	public int UnderflowTimeout = 250;
	
	// with an unterflow timeout of 250 we're trying to resend/reread 4 times a second. 
	// with an underflow count of 240 this means we're trying to resend/reread for 60 seconds
	public final static int UnderflowCount = 240;

	public static final int tooShortHeaderTimeout = 10000;
	
	/**Static method to encode a number of type long into big endian bytes.
	 * @param w WritableByteChannel
	 * @param number Long
	 * @throws IOException
	 */
	private static void writeLong(WritableByteChannel w, long number) throws IOException {
		ByteBuffer bb = ByteBuffer.allocate(8);
		bb.putLong(number);
		bb.rewind();
		int numWritten = 0;
		while (numWritten < 8)
			numWritten += w.write(bb);
	}

	protected Selector selector;
	protected SocketChannel soc;
	protected SelectionKey key;
	private SelectorLoop selectorLoop;
	private LinkedList<Pair<Message, Function<JSONObject, JSONObject>>> writeQueue = new LinkedList<>();
	public String PRE = ".--";
	
	// state variables: to be reset after each completely read/written message 
	protected Message 
			currentMsgToRead, 
			currentMsgToWrite;
	boolean 
			isHeaderRead = false, 
			isHeaderWritten = false, 
			skipRead = false, 
			skipWrite = false,
			startedToWriteAttachments = false,
			deadlockchecking = false,
			downloadIfForwardFails = true,
			hasBlindAttachments = false,
			isForwarding = false;
	Boolean awaitsPanic = false;
	long	
			// read
			numHeaderBytesToRead = -1,
			numAttachmentBytesToRead = -1,
			amountAttachmentBytesRead = 8,
			numReadTotal, 
			
			// write
			numAttachmentBytesToWrite = 0,
			amountAttachmentBytesWritten = 0,
			amountCurrentAttachmentBytesWritten = 0,
			numWrittenTotal,
			numForwardedTotal;
	int 
			numAttachmentsToRead = -1, 
			numReadBB8 = 0, 
			numReadCurrentAttachment = 0,
			attachmentType = Attachment.DOWNLOAD,
			currentReadInterest = SelectionKey.OP_READ,
			currentWriteInterest = 0,
			positionOfCurrentAttmntToRead = 0,
			positionOfCurrentAttmntToWrite = 0,
			readLoop = 0, 
			zeroWritten = 0,
			readSuppressID = 1,
			readSuppressControlID = 1,
			forwardDeadLockID = 0;
	ByteBuffer 
			bb, 
			bbWrite, 
			bb8 = ByteBuffer.allocate(8), 
			panicKey, 
			skipBuffer = ByteBuffer.allocateDirect(16*1024);
	Map<Integer, Attachment> attsToRead;
	Attachment[] 
			attsToWrite;
	Attachment 
			currentAttmntToRead,
			currentAttmntToWrite;
	TimerTask 
			readingBreak, 
			writingBreak,
			readInterestDeadLockChecker,
			forwardDeadLockChecker,
			tooShortHeaderBreak;
	
	/**Creates a new TCPSocketHandler based on SelectionKey, SelectorLoop, and set of TCPSocketHandlers
	 * to which one needs to subscribe in case one wants to write / send 
	 * @param soc SocketChannel
	 * @param sl SelectorLoop
	 * @param needsToWrite Set of TcpSocketHandlers
	 */
	protected TcpSocketHandler(SelectionKey key, SelectorLoop sl) {
		this.key = key;
		this.selectorLoop = sl;
		this.soc = (SocketChannel) key.channel();
		selector = sl.getSelector();
	}

	/**For switching socket handlers upon remote service registration
	 * @param sh TcpSocketHandler
	 */
	protected TcpSocketHandler(TcpSocketHandler sh) {
		key = sh.key;
		soc = sh.soc;
		selectorLoop = sh.selectorLoop;
		selector = sh.selector;
		writeQueue = sh.writeQueue;
	}
	
	void dprint(Object... o){
		String[] s = new String[o.length];
		for (int i = 0; i < o.length; i++) s[i] = String.valueOf(o[i]);
		dprint(s);
	}
	
	void dprint(String... s){
		System.out.println(PRE+" "+LcString.join(s, ", "));
	}
	
	void dprint(int n){
		for (int i = 0; i < n; i++) System.out.println(PRE);
	}
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// SELECTION INTERESTS
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	/**Sets the interest in selection events to 0 / none
	 * and stores the current read interest in this class.
	 */
	public void suppressReadInterest(){
		currentReadInterest = 0;
		key.interestOps(key.interestOps() & ~SelectionKey.OP_READ);
		if (deadlockchecking){
			synchronized(this){
				readSuppressControlID = readSuppressID++;
			}
			if (readInterestDeadLockChecker != null) readInterestDeadLockChecker.cancel();
			timer.schedule((readInterestDeadLockChecker = new TimerTask(){
				public void run() {
					synchronized(this){
						if (readSuppressControlID < readSuppressID) {
							System.err.println("suppressed read interest not restored; "
									+readSuppressControlID+" "+readSuppressID);
	//						restoreReadInterest();
						}
					}
				}}), 5000);
		}
		
//		print(PRE+" suppressing read; "+currentReadInterest+" "+key.interestOps());
	}
	
	/**Sets the interest in selection events to SelectionKey.OP_READ
	 * and stores the current 
	 */
	public void restoreReadInterest(){
		synchronized(key) {
			key.interestOps(key.interestOps() | (currentReadInterest = SelectionKey.OP_READ));
		}
		if (!selectorLoop.isInLoop()) selector.wakeup();
		synchronized(this){
			readSuppressControlID = readSuppressID;
		}
//		print(PRE+" restoring read; "+currentReadInterest+" "+key.interestOps());
	}
	
	public void suppressWriteInterest(){
		currentWriteInterest = 0;
		key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE);
//		dprint("suppressing write; "+currentWriteInterest+" "+key.interestOps()+" "+Thread.currentThread().getStackTrace()[2]+" "+(currentMsgToWrite != null ? currentMsgToWrite.getHeader().keySet():""));
	}
	
	public void restoreWriteInterest(){
		synchronized(key) {
			key.interestOps(key.interestOps() | (currentWriteInterest = SelectionKey.OP_WRITE));
		}
		if (!selectorLoop.isInLoop()) selector.wakeup();
//		dprint("restoring write; "+currentWriteInterest+" "+key.interestOps()+" "+Thread.currentThread().getStackTrace()[2]+" "+(currentMsgToWrite != null ? currentMsgToWrite.getHeader().keySet():""));
	}
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// READING
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	/* (non-Javadoc)
	 * @see luci.connect.SocketHandler#read(luci.connect.SocketHandler.HeaderProcessor, luci.connect.SocketHandler.ExceptionProcessor)
	 */
	public void read(SocketHandler processor) throws IOException {
		int nextAttemptIn_x_Millis = 5;
		synchronized(this){
			if (readSuppressControlID != readSuppressID){
				readSuppressControlID = readSuppressID;
			}
		}
		if (readingBreak != null) readingBreak.cancel();
		if (readLoop++ == UnderflowCount) {
			bb = ByteBuffer.allocate(32);
			soc.read(bb);
			error("read() at "+readLoop+" empty iterations; closing connection!");
			error("trying to read 32 bytes (all bytes = 0 means no remainging bytes) : "+java.util.Arrays.toString(bb.array()));
			key.cancel();
			soc.close();
			return;
		}
		
		if (skipRead){
			int numRead = 0;
			long read_msgLength = numHeaderBytesToRead + numAttachmentBytesToRead;
			if(numReadTotal < read_msgLength){
				skipBuffer.position(0).limit((int) Math.min(skipBuffer.capacity(), read_msgLength - numReadTotal));				
				numReadTotal += (numRead = soc.read(skipBuffer));
			}
			if (numReadTotal == read_msgLength) {
				resetRead();
				if (currentMsgToRead != null && currentMsgToRead.anyNetworkAttachment()) 
					currentMsgToRead.setWritten(numWrittenTotal);
				trace(PRE+"finished skipping "+numReadTotal);
			}
			if (numRead > 0) readLoop = 0;
			return;
		}
		
		synchronized(awaitsPanic){
			if (awaitsPanic) {
				readPanic();
				return;
			}
		}
		
		int numRead = 0;
		if (numHeaderBytesToRead == -1){
			numReadBB8 += numRead = soc.read(bb8);
			if (hasReadNothing(numRead, nextAttemptIn_x_Millis)) return;
			if (numReadBB8 == 8) {
				numHeaderBytesToRead = bb8.getLong(0);
				numReadBB8 = 0;
				bb8.rewind();
			} else return;
		}
		if (numAttachmentBytesToRead == -1){
			numReadBB8 += numRead = soc.read(bb8);
			if (hasReadNothing(numRead, nextAttemptIn_x_Millis)) return;
			if (numReadBB8 == 8) {
				numAttachmentBytesToRead = bb8.getLong(0);
				bb = ByteBuffer.allocate((int) numHeaderBytesToRead);
				numReadBB8 = 0;
				bb8.rewind();
				
			} else return;
		}
		try {
			// read headerbytes
			if (!isHeaderRead) {
				numReadTotal += numRead = soc.read(bb);
				if (numRead == 0){
					// too short header break:
					if (tooShortHeaderBreak == null) {
					SocketHandler.timer.schedule(tooShortHeaderBreak = new TimerTask(){
						public void run() {
							writeError(new TimeoutException("Waited for header for "+(tooShortHeaderTimeout / 1000)+" seconds. Aborting!"));
							byte[] tmp = new byte[100];
							int p = bb.position();
							bb.position(0);
							bb.get(tmp, 0, Math.min(p, 100));
							trace("Aborting on too short header timeout. First 100 bytes of received header: "+new String(tmp));
							resetRead();
						}}, 
						tooShortHeaderTimeout);
					}
				} else if (tooShortHeaderBreak != null){
					tooShortHeaderBreak.cancel();
					tooShortHeaderBreak = null;
				}
				if (hasReadNothing(numRead, nextAttemptIn_x_Millis)) return;
				isHeaderRead = numReadTotal == numHeaderBytesToRead;
			}
			
			// read numAttachments
			if (isHeaderRead &&numAttachmentsToRead == -1){
				numReadBB8 += numRead = soc.read(bb8);
				if (hasReadNothing(numRead, nextAttemptIn_x_Millis)) return;
				if (numReadBB8 == 8) {
					numAttachmentsToRead = (int) bb8.getLong(0);
					numReadBB8 = 0;
					bb8.rewind();
					numReadTotal += 8;
				} else return;
			}
			
			
			// read header json
			if (isHeaderRead && currentMsgToRead == null) {
				if (tooShortHeaderBreak != null) {
					tooShortHeaderBreak.cancel();
					tooShortHeaderBreak = null;
				}
				bb.rewind();
//				dprint(numHeaderBytesToRead, numAttachmentBytesToRead, new String(bb.array()));
				JSONObject h = new JSONObject(new JSONTokener(new ByteArrayInputStream(bb.array())));
				currentMsgToRead = Message.withPositionCheck(h)
						.setSourceSocketHandler(this)
						.setSrcChannelOfAttachmentsInNetwork(soc);
				bb = null;
				if (h.has("panic")) 
					sendPanic(h.getString("panic"));
				else 
					processor.processHeader(currentMsgToRead);
			}
			
			if (numAttachmentsToRead != -1 && readAttachments()) {
//				dprint(currentMsgToRead);
				validateAndResetRead();
				// indicate the separation between the print statements of two messages:
				
//				dprint(3);
				return;
			}
			
		} catch (IOException e){
			if (processor.processIOException(e)){
				resetRead();
				throw e;
			}
		} catch (PanicException e){
			processor.processCallException(e);
			panic(e);
			resetRead();
		} catch (Exception | Error e) {
			if (e instanceof CallException) 
				processor.processCallException((CallException) e);
			if (isSendingErrors()) writeError(e);
			if (numAttachmentsToRead == 0) resetRead();
			else if (e instanceof ChecksumException) {
				try {
					validateAndResetRead();
					resetWrite();
				} catch (PanicException e1){
					processor.processCallException(e1);
					panic(e1);
					resetRead();
				}
			}
			else if (numHeaderBytesToRead > 0 && numAttachmentBytesToRead > 8) skipRead();
			if (Log.TRACE) e.printStackTrace();
		}
	}
	
	/**reads 8 bytes and compares the resulting long to the given length
	 * @param length
	 * @return true if everything is ok or false if numRead is 0 (=read operation should be aborted 
	 * until the SelectionKey gets selected again) 
	 * @throws IOException if numRead is -1
	 * @throws IllegalStateException if the read and the given long are not equal
	 */
	private boolean read8(Attachment a) throws IOException, PanicException{
		int numRead;
		numReadBB8 += numRead = soc.read(bb8);
		if (hasReadNothing(numRead, 5)) return false;
		if (numReadBB8 == 8){
			try {
				a.setLength(bb8.getLong(0));
			} catch (AttachmentReferenceException e){
				throw new PanicException(e.getMessage(), currentMsgToRead.getHeader().optLong("callID", 0));
			}
			bb8.rewind();
			numReadBB8 = 0;
		}
		return true;
	}
	
	/**Checks if the amount of read bytes is equal to zero and throws an EOFException if numRead is
	 * equal to -1.
	 * @param numRead amount of bytes having been read
	 * @return true or false
	 * @throws EOFException
	 */
	private boolean hasReadNothing(int numRead, int timeoutMillis) throws EOFException{
		if (numRead == -1) throw new EOFException();
		if (numRead == 0) {
			if (timeoutMillis > 0){
				suppressReadInterest();
				if (readingBreak != null) readingBreak.cancel();
				timer.schedule((readingBreak = new TimerTask(){
					public void run(){restoreReadInterest();}
				}), timeoutMillis); 
			}
			return true;
		}
		readLoop = 0;
		return false;
	}
	
	private void nextAttachmentToRead(){
		positionOfCurrentAttmntToRead++;
		currentAttmntToRead = attsToRead.get(positionOfCurrentAttmntToRead);
		if (currentAttmntToRead == null){
			if (positionOfCurrentAttmntToRead > numAttachmentsToRead) throw new IllegalStateException();
			if (!hasBlindAttachments) 
				throw new IllegalStateException("attachment map does not have an attachment at position "
						+ positionOfCurrentAttmntToRead+" yet the header references as much attachments "
						+ "as the binary number of attachments indicates. Something wrong with attachment "
						+ "positions in the header?");
			currentAttmntToRead = Attachment.newBlind(currentMsgToRead.getAttachmentType())
					.setSrcSH(this)
					.setSrcChannel(soc)
					.setPosition(positionOfCurrentAttmntToRead);
			currentMsgToRead.addBlindAttachment(currentAttmntToRead);
		}
	}
	
	/**Reads as much of all attachments as possible / availble. If this.attachmentType is set to
	 * NETWORK we register for Attachment.onWritable to start forwarding the attachment once a 
	 * writable destination is availble and the Message.setAttachmentsReady() is being called in
	 * order to start any remote service, i.e. its input transmission (ClientHandler.processHeader
	 * waits for attachments to be ready before starting services in case they are being called
	 * without instance id). If in doubt clients should not set this.attachmentType to NETWORK. It's
	 * meant for forwarding messages.
	 * @param m Message
	 * @return true or false
	 * @throws IOException
	 */
	private boolean readAttachments() throws IOException, PanicException{
		int numRead = 0;
		long callID = currentMsgToRead != null ? currentMsgToRead.getCallID() : 0;

		if (numAttachmentsToRead == 0){
			if (numAttachmentBytesToRead != 8){
				throw new PanicException("#attachments == 0, hence all attachment bytes should not exceed 8; "
						+ "#allAttachmentBytes = "+numAttachmentBytesToRead, callID);
			}
			currentMsgToRead.setAttachmentsReady(numReadTotal);
			return true;
		} else if (numAttachmentBytesToRead == 8) {
			throw new PanicException("#attachments > 0 but size of all attachment bytes == 8", callID);
		}
		
		// initialize attachments array
		if (attsToRead == null) {
			currentMsgToRead.setAttachmentType(getAttachmentType());
			attsToRead = currentMsgToRead.getAttachmentMap();
		}
		
		if (numAttachmentsToRead != attsToRead.size()){
			if (!Attachment.allowBlindAttachments)
				throw new PanicException("binary number of attachments ("+numAttachmentsToRead
						+") != number of attachments in header ("+attsToRead.size()+")", callID);
			else {
				hasBlindAttachments = true;
				// Add an attribute "blind" to attachments to indicate that they are not referenced 
				// in the header (aka blind passanger) and omit format and checksum checks if an 
				// attachment is blind.
			}
		}

		if (currentMsgToRead.anyNetworkAttachment() && !currentMsgToRead.allNetworkAttachment()){
			throw new IllegalArgumentException("Internal Error: Cannot read message with network and "
					+ "non-network attachments being mixed.");
		}
		
		synchronized(currentMsgToRead){
			if (currentAttmntToRead == null || currentAttmntToRead.didReadCompletely()) 
				nextAttachmentToRead();
			boolean isLastAttachment = currentAttmntToRead.getPosition() == numAttachmentsToRead;
		
			if (currentAttmntToRead instanceof AttachmentInNetwork ){
				AttachmentInNetwork an  = (AttachmentInNetwork) currentAttmntToRead;
				WritableByteChannel dst = an.getWritableDestination();
				if (dst == null) {
					// if attachments are all network attachments then upon the first
					// mute the selector for reading. only continue once it becomes writable
					an.onWritable((d) -> {
						restoreReadInterest();
					});
					suppressReadInterest();
					// pretend attachments would be ready so that services start to run
					currentMsgToRead.setAttachmentsReady(numReadTotal);

					if (downloadIfForwardFails ){
						// schedule a task that downloads the attachment if the
						// network attachment does not become writable after *x* seconds
						// This is crucial since network attachments block the client 
						// connection in case the destination is not available
						currentMsgToRead.setID(++forwardDeadLockID);
						if (forwardDeadLockChecker != null) forwardDeadLockChecker.cancel();
						timer.schedule(forwardDeadLockChecker = new TimerTask(){

							@Override
							public void run() {
								if (currentMsgToRead != null 
										&& currentMsgToRead.getID() == forwardDeadLockID 
										&& !an.startedToRead()){
									synchronized(currentMsgToRead){
										// call a callback that can be registered to this event
										// --> for remote factories to cancel ticket reservation and re-book
										//     the service once the attachments are ready
										forwardUnlock();
									}
								}
							}

							}, ForwardUnlockTimeout);
					}

				} else {
					if (!currentAttmntToRead.startedToRead()) {
						if (!read8(currentAttmntToRead)) return false;
//						dprint("started to read: "+currentAttmntToRead.getLength());
						writeLong(dst, currentAttmntToRead.getLength());
						numReadTotal += 8;
						amountAttachmentBytesRead += 8;
						currentAttmntToRead.startedToRead(true);
					}
					checkBoundaries(currentAttmntToRead);
					try {
						numReadTotal += numRead = (int) currentAttmntToRead.write(dst);
					} catch (IOException e){
						// in case the IOException occurred on the write channel we still might want
						// to add numRead to numReadTotal since this is needed for skipping!
						if (an.read > 0) numReadTotal += an.read;
						throw e;
					}
					numReadCurrentAttachment += numRead;
					amountAttachmentBytesRead += numRead;
					if (currentAttmntToRead.getLength() > 0) {
						// UnderflowTimeout * 3: needs to be slower than clients using non-forwarding attachments (code below)
						if (hasReadNothing(numRead, UnderflowTimeout * 3)){
//							dprint(currentAttmntToRead.getClass().getSimpleName()+" read nothing");
						}
					}
					if (currentAttmntToRead.writtenCompletely(numReadCurrentAttachment)){
						numReadCurrentAttachment = 0;
						numForwardedTotal += numReadCurrentAttachment;
						currentAttmntToRead.checkTheSum();
						if (isLastAttachment){
							currentMsgToRead.setWritten(numForwardedTotal);
							return true;
						}
					}
				}
			} else {
				if (!currentAttmntToRead.startedToRead()) {
					if (!read8(currentAttmntToRead)) return false;
					numReadTotal += 8;
					amountAttachmentBytesRead += 8;
					currentAttmntToRead.startedToRead(true);
				}
				checkBoundaries(currentAttmntToRead);
				numReadTotal += numRead = currentAttmntToRead.read();
				numReadCurrentAttachment += numRead;
				amountAttachmentBytesRead += numRead;
				
				if (currentAttmntToRead.getLength() > 0 ){
					// needs to be faster than forwarding, slower than writing
					if (hasReadNothing(numRead, UnderflowTimeout * 2)){
//						dprint(currentAttmntToRead.getClass().getSimpleName()+" read nothing");
					}
				}
				if (currentAttmntToRead.didReadCompletely(numReadCurrentAttachment)){
					numReadCurrentAttachment = 0;
					currentAttmntToRead.checkTheSum();
					if (isLastAttachment){
						currentMsgToRead.setAttachmentsReady(numReadTotal);
						return true;
					}
				}
			}
			return false;
		}
	}
	
	public void forwardUnlock() {
		dprint("FORWARD UNLOCK");
		currentMsgToRead.callForwardUnlocks(ForwardUnlockTimeout)
						.setAttachmentType(Attachment.DOWNLOAD);
		positionOfCurrentAttmntToRead = 0;
		currentAttmntToRead = null;
		restoreReadInterest();
	}
	
	private void checkBoundaries(Attachment nextAttachment) throws PanicException{
//		print("throwing PanicException "+(numReadTotal + nextAttachment.getLength() - numReadCurrentAttachment > read_headerLength + read_attachmentsLength));
		if (numReadTotal + nextAttachment.getLength() - numReadCurrentAttachment > numHeaderBytesToRead + numAttachmentBytesToRead)
			throw new PanicException("reading the next attachment would exceed the number of expected "
			+ "bytes and perhaps render luci non-responsive: "
			+ numReadTotal + " + "+ nextAttachment.getLength() +" - "+numReadCurrentAttachment+" > " + numHeaderBytesToRead +" + "+numAttachmentBytesToRead, 
			currentMsgToRead.getHeader().optLong("callID", 0));
	}
	
	private void validateAndResetRead() throws IOException, PanicException{
		long callID = currentMsgToRead.getHeader().optLong("callID", 0);
		long real_attachmentsLength = currentMsgToRead.getSumAttachmentsLength() + 8*(currentMsgToRead.getNumAttachments()+1);
		if (real_attachmentsLength != numAttachmentBytesToRead)
			throw new PanicException("Sum of attachment length ("+real_attachmentsLength+
					") != indicated attachment length ("+numAttachmentBytesToRead+")", callID);
		if (numReadTotal < numHeaderBytesToRead + numAttachmentBytesToRead){

			throw new PanicException("numReadTotal ("+numReadTotal+") != read_headerLength "
				+ "("+numHeaderBytesToRead+") + read_attachmentsLength ("+numAttachmentBytesToRead+")",callID);
		} else {
			resetRead();
		}
	}
	
	/**After a message has been read, all variables are being reset.
	 * @param key SelectionKey
	 */
	private void resetRead() {
		numHeaderBytesToRead = -1;
		numAttachmentBytesToRead = -1;
		currentAttmntToRead = null;
		numAttachmentsToRead = -1;
		// we still might be interested in writing; just had an intersecting read operation
		// such as a progress notification or "newCallID"
		// same key.interestOps as in restoreReadInterest() just without waking up any selector 
		key.interestOps(key.interestOps() | (currentReadInterest = SelectionKey.OP_READ));
		currentMsgToRead = null;
		attsToRead = null;
		positionOfCurrentAttmntToRead = 0;
		bb8.rewind();
		numReadBB8 = 0;
		numReadTotal = 0;
		numReadCurrentAttachment = 0;
		isHeaderRead = false;
		readLoop = 0;
		numForwardedTotal = 0;
		amountAttachmentBytesRead = 8;
		hasBlindAttachments = false;
		skipRead = false;
		if (readInterestDeadLockChecker != null) readInterestDeadLockChecker.cancel();
	}
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// WRITING
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	public final void write(Message m){
		write(m, msg -> msg);
	}
	
	public final void write(Message m, Function<JSONObject, JSONObject> modifier) {
//		dprint("WRITE MESSAGE: add to queue "+m);
		if (key.isValid()){
			synchronized(writeQueue){
				writeQueue.add(new Pair<>(m, modifier));
			}
			if (!isForwarding) restoreWriteInterest();
		}
	}
	
	/**Write error String.
	 * @param e String
	 */
	private void writeError(Throwable e){
		JSONObject h = new JSONObject();
		String error = (e.getMessage() != null) ? e.getMessage() : e.toString();
		h.put("error", error);
		if (e instanceof CallException) h.put("callID", ((CallException) e).callID);
		write(new Message(h));
		trace(error);
	}
	
	public void write() throws IOException {
		synchronized(key){
			// writing only if interested; read might have changed the interests
			if (isHeaderWritten && (key.interestOps() & SelectionKey.OP_WRITE) == 0) return;
		}
		if (skipWrite && currentMsgToWrite != null){
			long msgLength = numAttachmentBytesToWrite + bbWrite.capacity();
			skipBuffer.position(0).limit((int) Math.min(bbWrite.capacity(), msgLength - numWrittenTotal));
			numWrittenTotal += soc.write(skipBuffer);
			if (numWrittenTotal == msgLength) {
				writeMore();
				resetWrite();
			}
			return;
		}
		currentWriteInterest = SelectionKey.OP_WRITE;
		if (writingBreak != null) writingBreak.cancel();
		try {
			synchronized(awaitsPanic){
				if (panicKey != null && !awaitsPanic) {
					int numWritten = 0;
					while((numWritten += soc.write(panicKey)) < 32);
					panicKey = null;
					synchronized(writeQueue){
						if (writeQueue.size() == 0){
							key.interestOps(currentReadInterest);
							return;
						}
					}
				}
			}
			if (currentMsgToWrite == null) {
				numAttachmentBytesToWrite = prepareMessageToBeWritten();
				writeLong(soc, bbWrite.capacity());
				writeLong(soc, numAttachmentBytesToWrite);
				numWrittenTotal = 16;
			}
			if (!isHeaderWritten){
				numWrittenTotal += soc.write(bbWrite);
				isHeaderWritten = !bbWrite.hasRemaining();
				// ARTEM PROPOSAL TEST PANIC:
//				soc.write(ByteBuffer.wrap(new byte[]{0,1,2,3,4,5,6,7,8,9}));
			}
			if (isHeaderWritten && writeAttachments()){
//				JSONObject h = currentMsgToWrite.getHeader();
				writeMore();
				resetWrite();
//				String what = h.optString("serviceName", h.optString("run", h.optString("result", h.optString("error", "_"))));
//				long callID = h.optLong("callID", h.optLong("referenceID", 0));
//				String r = h.has("referenceID") ? "R" : "";
//				dprint("written ["+r+callID+"] "+what+" to "+getRemoteSocketAddress().getPort());
			}
		} catch (Exception e){
			if (key.isValid()) key.interestOps(currentReadInterest);
			throw e;
		}
	}
	
	private void resetWrite(){
		isHeaderWritten = false;
		currentMsgToWrite = null;
		bbWrite = null;
		attsToWrite = null;
		currentAttmntToWrite = null;
		positionOfCurrentAttmntToWrite = 0;
		startedToWriteAttachments = false;
		numWrittenTotal = 0;
		zeroWritten = 0;
		skipWrite = false;
		isForwarding = false;
		amountCurrentAttachmentBytesWritten = 0;
	}
	
	private long prepareMessageToBeWritten(){
		numWrittenTotal = 0;
		currentAttmntToWrite = null;
		isHeaderWritten = false;
		startedToWriteAttachments = false;
		Pair<Message, Function<JSONObject, JSONObject>> p;
		
		synchronized (writeQueue) {
			p = writeQueue.pollFirst();
			currentMsgToWrite = p.a;
		}
		attsToWrite = currentMsgToWrite.getAttachmentsSortedByPosition();
		long attachmentsLength;
		if (currentMsgToWrite == currentMsgToRead){
			attachmentsLength = numAttachmentBytesToRead;
		} else {
			attachmentsLength = 8 + 8*attsToWrite.length;
			int i = 1;
			for (Attachment a: attsToWrite) {
				attachmentsLength += a.getLength();
				a.setPosition(i++);
			}
		}
		
		bbWrite = ByteBuffer.wrap(p.b.apply(currentMsgToWrite.getHeader()).toString().getBytes(Charset.forName("UTF8")));
		if (currentMsgToWrite.anyNetworkAttachment()){
			numForwardedTotal = numWrittenTotal;
		}
		return attachmentsLength;
	}
	
	/**perform the write operation for attachments
	 * @param m Message
	 * @throws IOException
	 */
	private boolean writeAttachments() throws IOException {
		if(!startedToWriteAttachments) {
			writeLong(soc, attsToWrite.length);
			startedToWriteAttachments = true;
			amountAttachmentBytesWritten = 8;
		}
		if (attsToWrite.length == 0) {
			currentMsgToWrite.setWritten(numWrittenTotal);
			return true;
		}
		if (currentAttmntToWrite == null) currentAttmntToWrite = attsToWrite[0];
		else if (currentAttmntToWrite.writtenCompletely()) {
			currentAttmntToWrite.resetForRewrite();
			// remember attachment positions start at 1; 0 means the position is not set
			currentAttmntToWrite = attsToWrite[currentAttmntToWrite.getPosition()];
			amountCurrentAttachmentBytesWritten = 0;
		}
		boolean isLastAttachment = currentAttmntToWrite.getPosition() == attsToWrite.length;
		int numWritten = 0;
		if (currentAttmntToWrite instanceof AttachmentInNetwork){
			((AttachmentInNetwork) currentAttmntToWrite).setWritable(soc);
			if (isLastAttachment){
				currentMsgToWrite.onWritten((l)-> {
					resetWrite();
					writeMore();
				});
				// turn off write interests and turn them on again only after all attachments 
				// have been forwarded and more messages need to be written
//				dprint("FORWARDING");
				isForwarding = true;
				suppressWriteInterest();
			} else {
				// remember attachment positions start at 1; 0 means the position is not set
				currentAttmntToWrite = attsToWrite[currentAttmntToWrite.getPosition()];
			}
		} else {
			if (!currentAttmntToWrite.startedToWrite()) {
				writeLong(soc, currentAttmntToWrite.getLength());
				amountAttachmentBytesWritten += 8;
				currentAttmntToWrite.startedToWrite(true);
			}
			if(currentAttmntToWrite.writtenCompletely(
					amountCurrentAttachmentBytesWritten += numWritten = currentAttmntToWrite.write(soc))){
				amountAttachmentBytesWritten += amountCurrentAttachmentBytesWritten;
				numWrittenTotal += amountAttachmentBytesWritten;
				currentMsgToWrite.setWritten(numWrittenTotal);
				if (isLastAttachment) currentAttmntToWrite.resetForRewrite();
				return isLastAttachment;
			} else if (numWritten == 0){
				if (zeroWritten++ > UnderflowCount) {
					error(PRE+UnderflowCount+" times zero bytes written. Closing socket!");
					soc.close();
					throw new EOFException();
				}
				suppressWriteInterest();
				if (writingBreak != null) writingBreak.cancel();
				timer.schedule((writingBreak = new TimerTask(){
						public void run(){restoreWriteInterest();}
					}), 
					// needs to be faster than reading and forwarding
					(int) UnderflowTimeout);
			}
		}
		return false;
	}

	protected void writeMore(){
		synchronized (writeQueue) {
			if (writeQueue.size() > 0) {
				restoreWriteInterest();
			} else {
				suppressWriteInterest();
			}
		}
	}
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// SKIPPING
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	public TcpSocketHandler skipRead(){
		trace(PRE+" skipping read at "+numReadTotal+" of "+(numHeaderBytesToRead + numAttachmentBytesToRead));
		skipRead = true;
		if (currentAttmntToRead instanceof AttachmentInNetwork){
			((AttachmentInNetwork) currentAttmntToRead).stopWriteContinuation();
		}
		// make sure we can read
		key.interestOps(key.interestOps() | SelectionKey.OP_READ);
		return this;
	}
	
	public TcpSocketHandler skipWrite(){
		if (currentMsgToWrite != null){
			debug(PRE+"skipping write");
			skipWrite = true;
			if (currentAttmntToWrite instanceof AttachmentInNetwork){
				((AttachmentInNetwork) currentAttmntToWrite).stopWriteContinuation();
			}
			// make sure we can write
			key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
		}
		return this;
	}
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// PANIC
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	private void readPanic() throws IOException{
		// bb.capacity() == 32; set in panic(PanicException e)
		int nextAttemptIn_x_Millis = 5;
		int p = bb.position();
		int numRead = soc.read(bb);
		boolean matches, matchesSoFar = false;
		bb.position(p);
		hasReadNothing(numRead, nextAttemptIn_x_Millis);
		
		while(bb.position() < p+numRead){
			matches = true;
			int i = p;
			for (; i < 32 && bb.position() < p+numRead && matches; i++){
				if (panicKey.get(i) != bb.get()) matches = false;
			}
			if (matches) {
				if (i == 32) {
//					dprint("got panic key (1) needs to match (2)");
//					dprint(Arrays.toString(bb.array()), new String(bb.array()));
//					dprint(Arrays.toString(panicKey.array()), new String(panicKey.array()));
					synchronized(awaitsPanic){
						awaitsPanic = false;
					}
					panicKey = null;
				}
				else {
					bb.position(numRead - i);
					bb.limit(numRead);
					bb.compact();
					matchesSoFar = true;
				}
				break;
			}
		}
		if (!matchesSoFar) bb.rewind();
	}
	
	private ByteBuffer newPanicKey(){
		byte[] b = new byte[32];
		new Random().nextBytes(b);
		return ByteBuffer.wrap(b);
	}
	
	protected void panic(PanicException e){
		synchronized(awaitsPanic){
			awaitsPanic = true;
		}
		panicKey = newPanicKey();
		if (readingBreak != null) readingBreak.cancel();
		bb = ByteBuffer.allocate(32);
		JSONObject j = new JSONObject()
				.put("panic", new String(Base64.getEncoder().encode(panicKey).array(), Charset.forName("UTF8")))
				.put("reason", e.getMessage())
				.put("callID", e.getCallID());
		write(new Message(j));
		
		// if we're trying for more than 'sec' seconds, close the connection
		int sec = 20;
		timer.schedule(new TimerTask(){
		@Override
		public void run() {
			synchronized(awaitsPanic){
				
			}
			if (awaitsPanic == true){
				try {
					key.cancel();
					soc.close();
					Log.error("Closing socket after reading panic sequence for "+sec+" seconds");
				} catch (IOException e) {
					Log.error("ERROR upon closing socket after trying to read panic sequence for "+sec+" seconds");
					e.printStackTrace();
				}
			}
		}}, sec*1000);
	}
	
	private void sendPanic(String panicKeyString) throws UnsupportedEncodingException{
		if (currentMsgToWrite != null) {
			if (currentMsgToWrite.anyNetworkAttachment()) skipRead();
			resetWrite();
		}
		panicKey = ByteBuffer.wrap(Base64.getDecoder().decode(panicKeyString.getBytes("UTF8")));
		key.interestOps(currentReadInterest | SelectionKey.OP_WRITE);
	}
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// GETTERS
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	/**Returns the SelectionKey associated with this TCPSocketHandler
	 * @return SelectionKey 
	 */
	public SelectionKey getSelectionKey(){
		return key;
	}
	
	public boolean isSendingErrors(){
		return false;
	}
	
	/**Returns the type of how this TcpSocketHandler is handling attachments.<br>
	 * 1 = ARRAY, 2 = FILE, 3 = NETWORK, 4 = DOWNLOAD
	 * @return Integer
	 */
	public int getAttachmentType() {
		return attachmentType;
	}

	/**Sets the type of how this TcpSocketHandler handles attachments.<br>
	 * 1 = ARRAY, 2 = FILE, 3 = NETWORK, 4 = DOWNLOAD
	 * @param attachmentType Integer
	 */
	public void setAttachmentType(int attachmentType) {
		this.attachmentType = attachmentType;
	}
	
	public InetSocketAddress getRemoteSocketAddress(){
		return (InetSocketAddress) soc.socket().getRemoteSocketAddress();
	}
	
	public InetSocketAddress getLocalSocketAddress(){
		return (InetSocketAddress) soc.socket().getLocalSocketAddress();
	}
	
	public void close() throws IOException{
		soc.close();
	}
	
	public long getNumAttachmentBytesToRead(){
		return numAttachmentBytesToRead;
	}
	
	public long getNumAttachmentBytesToWrite(){
		return numAttachmentBytesToWrite;
	}
	
	public long getAmountAttachmentBytesRead(){
		return amountAttachmentBytesRead;
	}
	
	public long getAmountAttachmentBytesWritten(){
		return amountAttachmentBytesWritten;
	}
	
	public Message getCurrentMessageToWrite() {
		return currentMsgToWrite;
	}

}