/** MIT License
 * @author Lukas Treyer
 * @date Feb 5, 2016
 * */
package luci.connect;

import static com.esotericsoftware.minlog.Log.error;
import static com.esotericsoftware.minlog.Log.warn;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.json.JSONObject;

import com.esotericsoftware.minlog.Log;

import luci.connect.LcRemoteService.RemoteServiceResponseHandler;

/**A class that guides the creation of Luci clients in Java and implements all low level 
 * functionality a typical Luci user doesn't want to care about: </br>
 * - define a ResponseHandler interface to handle "result", "progress", "error", "run", "cancel" events </br>
 * - associate a ResponseHandler with only one call / callID </br>
 * - provide a standard TcpSocketHandler implementation to send data
 * @author Lukas Treyer
 *
 */
public abstract class LcClient extends SelectionLoop {
	
	/**Defines how responses are to be handled.  
	 */
	public abstract static class ResponseHandler{
		long wid, callID;
		/**Handles any response. Presumably by calling one of the other three methods.
		 * @param m Message
		 */
		public void processResponse(Message m) {
			JSONObject header = m.getHeader();
			if (header.has("result")) processResult(m);
			else if (header.has("progress")) processProgress(m);
			else if (header.has("error")) processError(m);
			
			else throw new IllegalArgumentException("invalid message "+m);
		}

		/**Handles errors
		 * @param m Message
		 */
		public void processError(Message m) {
			System.err.println(m.getHeader().getString("error"));
		}
		/**Handles progress notifications
		 * @param m Message
		 */
		public void processProgress(Message m) {
			
		}
		/**Handles results
		 * @param m Message
		 */
		public abstract void processResult(Message m);
		
		public long associatedWriteID(){
			return wid;
		}
		
		public long associatedWriteID(long writeID){
			return (wid = writeID);
		}

		/**
		 * @param newCallID
		 */
		public ResponseHandler setCallID(long newCallID) {
			callID = newCallID;
			return this;
		}
		
		public long getCallID(){
			return callID;
		}
	}
	
	/**Private implementation of a TcpSocketHandler for LcClient.
	 */
	public class ResponseProcessor extends TcpSocketHandler{
		
		public void read(SocketHandler processor) throws IOException {
			super.read(processor);
			triggerPostRead();
		}
		
		public void write() throws IOException {
			super.write();
			triggerPostWrite();
		}
		
		private void triggerPostRead(){
			for (Consumer<TcpSocketHandler> sle: postReads) sle.accept(this);
			for (Consumer<TcpSocketHandler> sle: postReadOnce) sle.accept(this);
			postReadOnce.clear();
		}
		
		private void triggerPostWrite(){
			for (Consumer<TcpSocketHandler> sle: postWrites) sle.accept(this);
			for (Consumer<TcpSocketHandler> sle: postWriteOnce) sle.accept(this);
			postWriteOnce.clear();
		}
		
		private void triggerEarlyResult(){
			for (Consumer<TcpSocketHandler> sle: onEarlyResults) sle.accept(this);
			for (Consumer<TcpSocketHandler> sle: onEarlyResultOnce) sle.accept(this);
			onEarlyResultOnce.clear();
		}
		
		/**Instantiates the LcClient's implementation of TcpSocketHandler and sets the type of how
		 * attachments should be handled.
		 * @param key SelectionKey
		 * @param sl SelectorLoop
		 */
		public ResponseProcessor(SelectionKey key, SelectorLoop sl) {
			super(key, sl);
			setAttachmentType(LcClient.this.howToHandleAttachments());
			deadlockchecking = true;
			downloadIfForwardFails = !Log.TRACE && !Log.DEBUG;
			PRE = "CLI";
		}
		
		public boolean processIOException(IOException e) {
			// nothing to do; 
			return true;
		}

		
		public void processHeader(Message m) throws CallException {
			final JSONObject header = m.getHeader();
			if (header.has("newCallID")){
//				System.out.println("newCallID"+header.getLong("newCallID"));
				if (responseHandlerQueue.size() > 0){
					long newCallID = header.getLong("newCallID");
					responseHandlers.put(newCallID, 
							responseHandlerQueue.remove(0).setCallID(newCallID));
				}
			} else if (header.has("result") && header.has("callID")){
//				System.out.println("got result");
				// if we're still writing the message we're getting a result for already:
				// call registered events
				Message currentMsgToWrite = getCurrentMessageToWrite();
				if (currentMsgToWrite != null) {
					ResponseHandler rh = responseHandlers.get(header.getLong("callID"));
					if (rh.associatedWriteID() == currentMsgToWrite.getID()){
						triggerEarlyResult();
					}
				}
			}
//			System.out.println(header);
			m.onAttachmentsReady((length) -> {
				if (header.has("callID")) {
					long callID = header.getLong("callID");
					m.setID(callID);
					ResponseHandler callHandler = responseHandlers.get(callID);
					if (callHandler != null) {
						if (header.has("result")) {
							callHandler.processResult(m);
							// remove it only upon final messages not upon progress messages
							responseHandlers.remove(callID);
						} else if (header.has("error")) {
							callHandler.processError(m);
							// remove it only upon final messages not upon progress messages
							responseHandlers.remove(callID);
						} else if (header.has("progress")) 
							callHandler.processProgress(m);
						return;
					} else if (header.has("run") || header.has("cancel")){
						if (!(stdResponseHandler instanceof RemoteServiceResponseHandler)){
							warn("to process run and cancel messages newResponseHandler() must return a "
									+ "RemoteServiceResponseHandler object");
						} else {
							RemoteServiceResponseHandler rsrh = (RemoteServiceResponseHandler) stdResponseHandler;
							if (header.has("run")) 
								rsrh.processRun(m);
							else if (header.has("cancel")) {
								m.setID(header.getLong("cancel"));
								rsrh.processCancel(m);
							}
							return;
						}
					}
				}
				if (!header.has("newCallID"))
					stdResponseHandler.processResponse(m);
			});
		}

		@Override
		public void processCallException(CallException e) {
			ResponseHandler callHandler = responseHandlers.remove(e.getCallID());
			if (callHandler != null){
				callHandler.processError(new Message(new JSONObject()
						.put("error", "Panic: "+e.getMessage())));
			}
		}
	}
	
	TcpSocketHandler tcpSocket;
	ResponseHandler stdResponseHandler;
	List<ResponseHandler> responseHandlerQueue = new LinkedList<>();
	Map<Long, ResponseHandler> responseHandlers = new HashMap<>();
	private long writeID = 0;
	private long callID = 0;
	protected boolean generatesID = true;
	private Set<Consumer<TcpSocketHandler>> postReads = new HashSet<>();
	private Set<Consumer<TcpSocketHandler>> postReadOnce = new HashSet<>();
	private Set<Consumer<TcpSocketHandler>> postWrites = new HashSet<>();
	private Set<Consumer<TcpSocketHandler>> postWriteOnce = new HashSet<>();
	private Set<Consumer<TcpSocketHandler>> onEarlyResults = new HashSet<>();
	private Set<Consumer<TcpSocketHandler>> onEarlyResultOnce = new HashSet<>();
	

	protected void postRead(Consumer<TcpSocketHandler> sle){
		postReads.add(sle);
	}
	
	protected void postReadOnce(Consumer<TcpSocketHandler> sle){
		postReadOnce.add(sle);
	}
	
	protected void notPostRead(Consumer<TcpSocketHandler> sle){
		postReads.remove(sle);
	}
	
	protected void notPostReadOnce(Consumer<TcpSocketHandler> sle){
		postReadOnce.remove(sle);
	}
	
	protected void postWrite(Consumer<TcpSocketHandler> sle){
		postWrites.add(sle);
	}
	
	protected void postWriteOnce(Consumer<TcpSocketHandler> sle){
		postWriteOnce.add(sle);
	}
	
	protected void notPostWrite(Consumer<TcpSocketHandler> sle){
		postWrites.remove(sle);
	}
	
	protected void notPostWriteOnce(Consumer<TcpSocketHandler> sle){
		postWriteOnce.remove(sle);
	}
	
	protected void onEarlyResult(Consumer<TcpSocketHandler> sle){
		onEarlyResults.add(sle);
	}
	
	protected void onEarlyResultOnce(Consumer<TcpSocketHandler> sle){
		onEarlyResultOnce.add(sle);
	}
	
	protected void notOnEarlyResult(Consumer<TcpSocketHandler> sle){
		onEarlyResults.remove(sle);
	}
	
	protected void notOnEarlyResultOnce(Consumer<TcpSocketHandler> sle){
		onEarlyResultOnce.remove(sle);
	}
	
	/**@see <a href="http://softwareengineering.stackexchange.com/questions/
	 * 276859/what-is-the-name-of-a-function-that-takes-no-argument-and-
	 * returns-nothing}">Runnable as a Functional Interface</a>
	 * @param serviceName
	 * @param onExists
	 */
	public void runIfExists(String serviceName, Runnable onExists){
		sendAndReceive(
			new Message(new JSONObject()
				.put("run", "Exists").put("serviceName", serviceName)
			), 
			new ResponseHandler() {
				@Override
				public void processResult(Message m) {
//					System.out.println("expecting result of exists: "+m);
					if (m.getHeader().getJSONObject("result").getBoolean("exists")){
						onExists.run();
					}
				}
			});
	}
	
	@Override
	protected TcpSocketHandler confirmConnection(SelectionKey key) throws IOException{
		stdResponseHandler = newStdResponseHandler();
		return (tcpSocket = super.confirmConnection(key));
	}
	
	/**Specifies how attachments should be handled:
	 * 1 = ARRAY, 2 = FILE, 3 = NETWORK, 4 = DOWNLOAD
	 * @return attachment type constant (int)
	 */
	public int howToHandleAttachments() {
		return Attachment.DOWNLOAD;
	}

	/**Allows clients to implement their own response handlers
	 * @return the response handler associated with this client
	 */
	protected abstract ResponseHandler newResponseHandler();
	
	protected ResponseHandler newStdResponseHandler(){
		return newResponseHandler();
	}
	
	public final ResponseHandler getStdResponseHandler(){
		return stdResponseHandler;
	}
	
	/**Sends a given Message m using the TcpSocketHandler provided by LcClient
	 * @param m Message
	 */
	public void send(Message m) {
		sendAndReceive(m, newResponseHandler());
	}
	
	/**Sends a given Message m using the TcpSocketHandler provided by LcClient and registers the 
	 * given ResponseHandler to handle this specific call.
	 * @param m Message
	 * @param rh ResponseHandler
	 */
	public void sendAndReceive(Message m, ResponseHandler rh) {
		if (!isConnected()) throw new IllegalStateException("not connected!");
		writeID++;
		m.setID(writeID);
		rh.associatedWriteID(writeID);
		if (generatesID){
			long callID = ++this.callID;
			m.getHeader().put("callID", callID);
			responseHandlers.put(callID, rh);
		} else {
			responseHandlerQueue.add(rh);
		}
//		System.out.println(m+" - "+responseHandlerQueue.size());
		tcpSocket.write(m);
	}
	
	@Override
	protected TcpSocketHandler newSocketHandler(SelectionKey key) {
		return new ResponseProcessor(key, this);
	}

	@Override
	protected boolean processIOException(SelectionKey key, IOException e) {
		key.cancel();
		System.out.println("LcClient.processIOException()");
		error(e.toString());
		return true;
	}
	
	protected boolean processException(Exception e){
		System.out.println("LcClient.processException()");
		if (Log.DEBUG || Log.TRACE) e.printStackTrace();
		else error(e.getMessage());
		return true;
	}
}