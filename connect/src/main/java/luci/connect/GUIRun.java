/** MIT License
 * @author Lukas Treyer
 * @date Apr 21, 2016
 * */
package luci.connect;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GUIRun {
	public final static int NIX = 0;
	public final static int OSX = 1;
	public final static int WIN = 2;
	public final static int UNK = 3;
	public final static int OS = getOSType();
	
	public final static int getOSType(){
		String osname = System.getProperty("os.name").toLowerCase();
		return (new LcString(osname).containsAny("nix", "nux", "aix")) ? NIX : 
			(osname.startsWith("mac") ? OSX : (osname.startsWith("win") ? WIN : UNK));
	}
	
	/**
	 * @return
	 * @throws IOException 
	 */
	public static void guiRun(String javaCommand) throws IOException {
		
		String cwd = Attachment.cwd.getAbsolutePath();

		switch (OS){
		case OSX:
			createOSXBashFile(javaCommand);
			Runtime.getRuntime().exec("open -a Terminal "+cwd+"/run.sh");
			break;
		case WIN:
			Runtime.getRuntime().exec("cmd.exe /c start cmd.exe /k \"cd "+cwd+" & "
					+javaCommand.replace("\"", "\"\"")+"\"");
			break;
		case NIX:
			String[] split = javaCommand.split("\\s");
			String[] nix = new String[split.length+2];
			System.arraycopy(split, 0, nix, 2, split.length);
			try {
				nix[0] = "gnome-terminal";
				nix[1] = "-e";
				Runtime.getRuntime().exec(nix);
			} catch (IOException e) {
				try {
					nix[0] = "konsole";
					nix[1] = "-e";
					Runtime.getRuntime().exec(nix);
				} catch (IOException e1) {
					try{
						String[] xterm = new String[split.length+1];
						System.arraycopy(split, 0, xterm, 1, split.length);
						xterm[0] = "xterm";
						Runtime.getRuntime().exec(xterm);
					} catch (IOException e2){
						throw new UnsupportedOperationException("Unsupported Unix Window Manager");
					}
				}
			}
			
			break;
		default:
			throw new UnsupportedOperationException("Unsupported OS "+System.getProperty("os.name"));
		}
	}
	
	public static List<String> getPATH() throws IOException{
		switch(OS){
		case OSX:
		case NIX:
			return GUIRun.getOutput("echo $PATH");
		case WIN:
			return GUIRun.getOutput("echo %PATH%");
		default:
			return Arrays.asList(new String[]{"unsupported OS"});
		}
	}
	
	private static void createOSXBashFile(String command) throws IOException{
		byte[] bash = ("#!/bin/bash\n"
				
				// for when run from jar?
				+ "DIR=$( cd \"$( dirname \"${BASH_SOURCE[0]}\" )\" && pwd )\n"
				+ "cd $DIR\n"
				
				+ "PATH=/usr/local/bin:$PATH\n"
				+ command).getBytes();
		File bashFile = new File(Attachment.cwd, "run.sh");
		bashFile.delete();
		bashFile.createNewFile();
		bashFile.setExecutable(true);
		FileOutputStream fos = new FileOutputStream(bashFile);
		fos.write(bash, 0, bash.length);
		fos.close();
	}
	
	public static List<String> getOutput(String command) throws IOException {
		return getOutput(command, 0, TimeUnit.SECONDS, null);
	}
	
	public static List<String> getOutput(String command, int timeout, TimeUnit unit, List<String> defaultValue) throws IOException {
		Runtime rt = Runtime.getRuntime();
		List<String> result = new ArrayList<>();
		String line;
		Process p;
		switch(OS){
		case OSX:
			createOSXBashFile(command);
			p = rt.exec("./run.sh");
			break;
		case WIN:
			p = rt.exec(""+command);
			break;
		case NIX:
			p = rt.exec(command);
			break;
		default:
			return result;
		}
		
		
		BufferedReader reader = new BufferedReader (new InputStreamReader(p.getInputStream()));
		try {
			if (timeout > 0  &&  !p.waitFor(timeout, unit)) return defaultValue;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		while ((line = reader.readLine ()) != null) {
			result.add(line);
		}
		if (result.size() == 0){
			reader = new BufferedReader (new InputStreamReader(p.getErrorStream()));
			while ((line = reader.readLine ()) != null) {
				result.add(line);
			}
			if (result.size() > 0) throw new IOException(result.get(0));
			else throw new IOException("no output, no error");
		}
		reader.close();
		return result;
	}
	
	public static void openURLWithoutAWT(String url){
		Runtime rt = Runtime.getRuntime();
		try {
			switch(OS){
			case WIN:
				rt.exec( "rundll32 url.dll,FileProtocolHandler " + url);
				break;
			case OSX:
				rt.exec( "open " + url);
				break;
			case NIX:
				String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
                        "netscape","opera","links","lynx"};

				StringBuffer cmd = new StringBuffer();
				for (int i=0; i<browsers.length; i++)
				cmd.append( (i==0  ? "" : " || " ) + browsers[i] +" \"" + url + "\" ");
				
				rt.exec(new String[] { "sh", "-c", cmd.toString() });
				break;
			}
		} catch (IOException e){
			e.printStackTrace();
		}
	}
}
