/** MIT License
 * @author Lukas Treyer
 * @date Aug 20, 2016
 * */
package luci.connect;

public interface On {
	@FunctionalInterface
	public interface OnVoid{
		public void react();
	}
	
	@FunctionalInterface
	public interface OnLong{
		public void react(long number);
	}
	
	@FunctionalInterface
	public interface OnInt{
		public void react(int number);
	}
}
