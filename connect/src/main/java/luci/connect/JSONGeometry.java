/** MIT License
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * */
package luci.connect;

import org.json.JSONObject;

/**JSONGeometry extends JSONFormat extends JSONObject and therefore is a JSONObject holding all
 * relevant information of a JSONGeometry such as a GeoJSON formated JSONObject. As it extends 
 * JSONFormat a JSONGeometry also has format, name, crs, attributeMap fields, where format would
 * hold values such as "GeoJSON" or "TopoJSON" etc. 
 * 
 * @author Lukas Treyer
 *
 */
public class JSONGeometry extends JSONFormat {
	private JSONObject geometry;
	
	/**Creates a JSONGeometry object based on the information provided by jsonObject. Name indicates
	 * the name that should be used if jsonObject does not contain a name.
	 * @param name
	 * @param jsonObject
	 */
	public JSONGeometry(String name, JSONObject jsonObject) {
		super(name, jsonObject);
		setGeometry(jsonObject.optJSONObject("geometry"));
	}
		
	/**Sets the geometry JSONObject
	 * @param geometry JSONObject
	 */
	public void setGeometry(JSONObject geometry){
		putOpt("geometry", (this.geometry = geometry));
	}

	/**Returns the geometry JSONObject
	 * @return the geometry
	 */
	public JSONObject getGeometry() {
		return geometry;
	}
}
