/** MIT License
 * @author Lukas Treyer
 * @date Feb 2, 2016
 * */
package luci.connect;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Objects;
import java.util.Properties;

import org.json.JSONObject;

/**Basic abstract class for attachments. Extends JSONFormat and therefore JSONObject in order to
 * be easily embeddable into JSONObjects and not being encoded as a String in JSONObjects.
 * Besides being a JSONObject it holds all relevant information of an attachment and provides
 * essential methods to handle attachments such as downloading or forwarding an attachment or
 * converting it into another attachment. For array and file attachments also a getInputStream()
 * method is available if the attachment has been downloaded. 
 * 
 * @author Lukas Treyer
 *
 */
public abstract class Attachment extends JSONFormat {
	
	/**Exception being thrown when the length of an attachment in the json header and the binary
	 * notion do not correspond
	 */
	static class AttachmentReferenceException extends RuntimeException{
		private static final long serialVersionUID = 1L;
		public AttachmentReferenceException(String msg){
			super(msg);
		}
	}
	
	class ChecksumException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public ChecksumException(String message){
			super(message);
		}
	}
	
	static File cwd = new File(".");
	private static boolean cwdHasBeenSet = false;
	private static boolean sizeHasBeenRead = false;
	
	public static void setCwd(File c){
		if (cwdHasBeenSet) throw new SecurityException("Allowed to set cwd only once!");
		cwd = c;
		cwdHasBeenSet = true;
		if (!sizeHasBeenRead) toFileIfLargerThan = getSize();
	}
	
	public static File getCwd() {
		return cwd;
	}
	
	/**Wheter or not an attachment must be referenced in the message header; for debug purposes
	 *
	 */
	public static boolean allowBlindAttachments = false;
	
	/**Defines the threshold up to which an attachment is being stored in memory (byte array) and
	 * from which on an attachment is being stored to file.
	 */
	private static int toFileIfLargerThan = getSize();
	
	/**ARRAY = 1, the type of attachments that are being backed by an array
	 */
	public final static int ARRAY = 1;
	/**FILE = 2, the type of attachments that are being backed by a file
	 */
	public final static int FILE = 2;
	/**NETWORK = 3, the type of attachments that are being forwarded to a network destination directly
	 */
	public final static int NETWORK = 3;
	/**DOWNLOAD = 4, the actual type of attachments is being determined by its length. Default threshold is 10MB:
	 * Attachments smaller than 10MB get downloaded to memory (array), larger than 10MB get downloaded to file.
	 */
	public final static int DOWNLOAD = 4; // = ARRAY OR FILE BASED ON getLength() / size of the attachment
	
	/**
	 * @return Integer: the limit up to which an attachment should be stored in memory / from on 
	 * which an attachment should be stored to file. This method expects a Luci.conf file to be
	 * present in the working directory. If the config file does not specify a "toFileIfLargerThan"
	 * value, by default 10MB is returns as the limit.
	 */
	private static int getSize() {
		int MB10 = 10 * 1024*1024; //10MB;
		try {
			Properties config = new Properties();
			config.load(new FileInputStream(new File(cwd, "Luci.conf")));
			if (!config.containsKey("toFileIfLargerThan")) config.put("toFileIfLargerThan", ""+MB10);
			sizeHasBeenRead = true;
			return Integer.parseInt(config.getProperty("toFileIfLargerThan", ""+MB10)); 
		} catch (IOException | NumberFormatException e) {
			return MB10;
		}
	}
	
	/**checks whether a given attachment size is too large for memory and should be stored as a file
	 * @param length long
	 * @return
	 */
	public static boolean tooLargeForMemory(long length){
		return length == 0 || length > toFileIfLargerThan;
	}
	
	/**Makes an Attachment available as InputStream. This applies only for array and file attachments.
	 * @param a Attachment
	 * @return InputStream
	 * @throws FileNotFoundException
	 * @throws IllegalStateException in case the Attachment is of type AttachmentInNetwork
	 */
	public static InputStream getInputStream(Attachment a) throws FileNotFoundException{
		if (a instanceof AttachmentAsArray) return getInputStream((AttachmentAsArray) a);
		if (a instanceof AttachmentAsFile) return getInputStream((AttachmentAsFile) a);
		throw new IllegalStateException("InputStreams are restricted to array and file attachments");
	}
	
	/**Makes an AttachmentAsArray available as InputStream using java's ByteArrayInputStream class.
	 * @param a AttachmentAsArray
	 * @return InputStream
	 */
	public static InputStream getInputStream(AttachmentAsArray a) {
		return new ByteArrayInputStream(a.bb.array());
	}
	
	/**Makes an AttachmentAsFile available as InputStream using the FileInputStream class.
	 * @param a AttachmentAsFile 
	 * @return InputStream
	 * @throws FileNotFoundException
	 */
	public static InputStream getInputStream(AttachmentAsFile a) throws FileNotFoundException {
		return new FileInputStream(a.file);
	}

	/**Based on the value of {@code toFileIfLargerThan} this method converts an AttachmentInNetwork
	 * to array or file attachment and then gets the InputStream of this attachment - which will 
	 * fail / be useless since those arrays/files will be empty; in case of the array filled with
	 * zeros. Therefore the visibility of this method is restricted to package visibility and is 
	 * being kept here fore completeness.
	 * @param a AttachmentInNetwork
	 * @return InputStream
	 * @throws FileNotFoundException
	 */
	static InputStream getInputStream(AttachmentInNetwork a) throws FileNotFoundException {
		if (a.tooLargeForMemory()){
			return getInputStream((AttachmentAsFile) a.convertTo(FILE));
		} else {
			return getInputStream((AttachmentAsArray) a.convertTo(ARRAY));
		}
	}
	
	public static Attachment newBlind(int attachmentType){
		switch(attachmentType){
		case ARRAY:
			return new AttachmentAsArray();
		case DOWNLOAD:
		case FILE:
			return new AttachmentAsFile();
		case NETWORK:
			return new AttachmentInNetwork();
		default:
			return null;
		}
	}
	
	public static Attachment newWeb(int attachmentType, String name, JSONObject j){
		Attachment a = null;
		switch(attachmentType){
		case ARRAY:
			a = new AttachmentAsArray(name, j, true);
			break;
		case DOWNLOAD:
		case FILE:
			a = new AttachmentAsFile(name, j, true);
			break;
		case NETWORK:
			a = new AttachmentInNetwork(name, j, true);
			break;
		}
		a.isBlind = true;
		return a;
	}
	
	/**Converts an attachment from one type to another
	 * @param type int: 1 = ARRAY, 2 = FILE, 3 = NETWORK, 4 = DOWNLOAD (ARRAY OR FILE)
	 * @return Attachment - the converted attachment
	 */
	public Attachment convertTo(int type){
		if (this instanceof AttachmentAsArray) {
			if (type == FILE || (type == DOWNLOAD && this.tooLargeForMemory()) ) {
				AttachmentAsFile af = new AttachmentAsFile(this);
				try {
					FileOutputStream fos = new FileOutputStream(af.getFile());
					FileChannel fchan = fos.getChannel();
					ByteBuffer bb = ((AttachmentAsArray) this).getByteBuffer();
					int numReadTotal = 0;
					while((numReadTotal += fchan.write(bb)) < getLength());
					fos.close();
					return af;
				} catch (IOException e) {
					throw new IllegalArgumentException(e);
				}
			}
			if (type == Attachment.NETWORK) return new AttachmentInNetwork(this);
		} else if (this instanceof AttachmentAsFile) {
			if (type == ARRAY || (type == DOWNLOAD && !this.tooLargeForMemory())) {
				if (getLength() > toFileIfLargerThan)
					throw new IllegalArgumentException(
							"File too large to be converted to array!");
				File f = ((AttachmentAsFile) this).getFile();
				try {
					FileInputStream fis = new FileInputStream(f);
					FileChannel fchan = fis.getChannel();
					ByteBuffer bb = ByteBuffer.allocate((int) getLength());
					@SuppressWarnings("unused")
					int numReadTotal = 0, numRead = 0;
					while ((numReadTotal += numRead = fchan.read(bb)) < getLength());
					fis.close();
					return new AttachmentAsArray(bb, this);
				} catch (IOException e) {
					throw new IllegalArgumentException(e);
				}
			}
			if (type == NETWORK) return new AttachmentInNetwork(this);
		} else if (this instanceof AttachmentInNetwork) {
			if (type == ARRAY || (type == DOWNLOAD && !this.tooLargeForMemory())) 
				return new AttachmentAsArray(this);
			if (type == FILE || (type == DOWNLOAD && this.tooLargeForMemory())) 
				return new AttachmentAsFile(this);
		}
		return this;
	}
	
	JSONObject descr;
	int position;
	long length;
	String checksum;
	SocketHandler srcSH;
	ReadableByteChannel src;
	boolean shouldDownloadToFile = false;
	boolean didRead = false, startedToRead = false, didWrite = false, startedToWrite, isBlind = false;
	
	/** the default constructor (to be explicitly called by no-args constructors of subclasses)
	 */
	Attachment(){
		isBlind = true;
		put("attachment", (descr = new JSONObject()));
		setFormat(UnknownFormat);
	}
	
	/**Creates a new Attachment object based on a name string and a json object.
	 * @param name String - name to be used if the json object (second parameter) does not contain a name
	 * @param j JSONObject containing all relevant attachment information formatted as defined in {@link luci.core.LcMetaJson}
	 */
	public Attachment(String name, JSONObject j) {
		this(name, j, false);
	}
	
	protected Attachment(String name, JSONObject j, boolean isPrivate) {
		super(name, j);
		put("attachment", (descr = new JSONObject()));
		if (j != null && j.has("attachment")){
			JSONObject a = j.getJSONObject("attachment");
			setPosition(a.optInt("position", 0));
			setLength(a.optLong("length", 0));
			if (isPrivate) descr.put("checksum", (checksum = a.optString("checksum", null)));
			else setChecksum(a.optString("checksum", null));
		}
	}
	
	/**Create a new Attachment based on name and format; all necessary attachment data needs to be 
	 * added subsequentially.
	 * @param nameFormat String[] like String[]{name, format}
	 */
	Attachment(String[] nameFormat){
		super(nameFormat[0], nameFormat[1]);
		put("attachment", (descr = new JSONObject()));
	}
	
	/**Creates a new Attachment based on the information of another attachment. 
	 * Since Attachment extends JSONObject the 
	 * {@link luci.connect.Attachment#Attachment(String, JSONObject)} 
	 * constructor can be reused for this.
	 * @param attachment Attachment
	 */
	public Attachment(Attachment attachment){
		this(attachment.name, attachment, true);
		this.src = attachment.getSrcByteChannel();
		this.srcSH = attachment.getSourceSocketHandler();
		this.isBlind = attachment.isBlind;
	}
	
	public void resetForRewrite(){
		didWrite = false;
		startedToWrite = false;
	}
	
	public void resetForReread(){
		didRead = false;
		startedToRead = false;
	}
	
	/**Sets the position of this attachment.
	 * @param pos int 
	 * @return Attachment
	 */
	public Attachment setPosition(int pos){
		descr.put("position", (position = pos));
		return this;
	}
	
	/**Sets the length of this attachment.
	 * @param length long
	 * @return Attachment
	 */
	public Attachment setLength(long length){
		if(this.length > 0 && this.length != length)
			throw new AttachmentReferenceException("setting an attachment length that has been set "
				+ "before with new length ("+length+") != old length ("+this.length+"); most likely "
				+ "tcp attachment length != json header attachment length");
		descr.put("length", (this.length = length));
		return this;
	}
	
	/**Sets the checksum of this attachment.
	 * @param chck String hexadecimal representation of the MD5 hash of the attachment.
	 * @return Attachment
	 */
	public Attachment setChecksum(String chck){
		Objects.requireNonNull(chck, "Attachments are identified by its checksum, therefore it "
				+ "cannot be 'null'!");
		descr.put("checksum", (checksum = chck));
		return this;
	}
	
	/**Sets the source SocketHandler, the SocketHandler that handles the connection where the 
	 * attachment originates from.
	 * @param srcSH the source SocketHandler to set
	 * @return Attachment
	 */
	public Attachment setSrcSH(SocketHandler srcSH) {
		this.srcSH = srcSH;
		return this;
	}

	/**Sets the source channel = the connection where the attachment originates from.
	 * @param src the src to set
	 * @return Attachment
	 */
	public Attachment setSrcChannel(ReadableByteChannel src) {
		this.src = src;
		return this;
	}
	
	/**Returns the current position of this attachment. Relevant for TCP communication since
	 * attachments are sent sequentially after a json header. Also indicated by the first 16 bytes
	 * of a TCP message (8 bytes for headerlength, 8 bytes for attachments length).
	 * @return the position as int
	 */
	public int getPosition() {
		return position;
	}

	/**Returns number of bytes of an attachment.
	 * @return the length as long
	 */
	public long getLength() {
		return length;
	}
	/**Returns the MD5 hash of the attachment bytes as a hexadecimal string.
	 * @return the checksum as String
	 */
	public String getChecksum() {
		return checksum;
	}
	
	/**Returns the source SocketHandler that handles the connection from which this attachment originates.
	 * @return the srcSH SocketHandler
	 */
	public SocketHandler getSourceSocketHandler() {
		return srcSH;
	}
	
	/**Checks if two attachments are the same by comparing their checksums. If the other object is
	 * not an Attachment {@code false} is being returned.
	 */
	public boolean equals(Object other){
		if (other instanceof Attachment)
			return checksum.equalsIgnoreCase(((Attachment) other).getChecksum());
		return false;
	}
	
	/**Returns true if the attachment should be stored to a file instead of memory space.
	 * @return boolean
	 */
	public boolean tooLargeForMemory(){
		return tooLargeForMemory(getLength());
	}
	
	/**returns true if the attachment has been read (or written/forwarded in the case of a network attachment)
	 * @return boolean
	 */
	public boolean didReadCompletely(){
		return didRead;
	}
	
	/**Returns true if numRead == amount of bytes of this attachment
	 * @param numRead int the amount of bytes of this attachment that have been read (so far).
	 * @return boolean 
	 */
	public boolean didReadCompletely(int numRead){
		return (didRead = getLength() == numRead);
	}
	
	/**Returns true if the reading process started
	 * @return boolean
	 */
	public boolean startedToRead() {
		return startedToRead;
	}
	
	public void startedToRead(boolean b){
		startedToRead = b;
	}
	
	public boolean startedToWrite(){
		return startedToWrite;
	}
	
	public void startedToWrite(boolean b){
		startedToWrite = b;
	}
	
	/**Returns true if the attachment has been written
	 * @return boolean
	 */
	public boolean writtenCompletely(){
		return didWrite;
	}
	
	/**Returns true if numWritten == amount of bytes in this attachment
	 * @param numWritten the abount of bytes of this attachment that have been written (so far).
	 * @return boolean
	 */
	public boolean writtenCompletely(long numWritten){
		return (didWrite = getLength() == numWritten);
	}
	
	/**Similar to {@link luci.connect.Attachment#tooLargeForMemory()} checks the size limit for 
	 * attachments to be hold in memory.
	 * @return int - attachment ARRAY or FILE that should be used for downloading this attachment.
	 */
	public int getLocalStorageTypeForConversion(){
		if (getLength() > toFileIfLargerThan) return FILE;
		else return ARRAY;
	}
	
	/**
	 * Returns the source byte channel where this attachment originates from. If an array or file 
	 * attachment has been downloaded already in case of an array attachment the array is being
	 * wrapped into a channel. In case of a file an exception is being thrown, since retrieving 
	 * a channel from a file would leave the FileInputStream as unclosed resource.
	 * @return source byte channel - ReadableByteChannel
	 */
	public abstract ReadableByteChannel getSrcByteChannel();

	/**Read the attachment from ReadableByteChannel src. The read operation may not be complete and
	 * will continue once the selector indicates that there is more data available from the socket.
	 * Dont' forget to set the source channel before with 
	 * {@link Attachment#setSrcChannel(ReadableByteChannel src)}
	 * @return number of bytes read
	 * @throws IOException
	 * @throws NullPointerException if the ReadableByteChannel src has not been set before with 
	 * {@link Attachment#setSrcChannel(ReadableByteChannel src)}
	 */
	public abstract int read() throws IOException;
	
	/**writes attachment to WriteableByteChannel dst
	 * @param src WritableByteChannel
	 * @return number of bytes written
	 * @throws IOException
	 */
	public abstract int write(WritableByteChannel dst) throws IOException;
	
	/**calculates the checksum of the attachment
	 * @return hexadecimal representation of the checksum
	 * @throws IllegalStateException if the attachment has not been read yet or the file is empty
	 */
	public abstract String calcChecksum() throws IllegalStateException;
	
	/**
	 * Checks if the the result of {@link luci.connect.Attachment#calcChecksum()} matches the value
	 * returned by {@link luci.connect.Attachment#getChecksum()} and throws an exception if they don't.
	 */
	public void checkTheSum(){
		String newChck = calcChecksum();
		String oldChck = getChecksum();
		if (oldChck == null) setChecksum(newChck);
		else if (!oldChck.equalsIgnoreCase(newChck))
			throw new ChecksumException(String.format(getClass()+": Checksums don't match: should: %s, calc: %s", 
					getChecksum(), newChck));
	}

	public void copyFrom(JSONObject j) {
		for (String k: j.keySet()){
			if (k.equals("format") || k.equals("name") || k.equals("attachment")) continue;
			put(k, j.get(k));
		}

	}
	
	public boolean isBlind(){
		return isBlind;
	}
}
