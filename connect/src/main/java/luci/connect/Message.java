/** MIT License
 * @author Lukas Treyer
 * @date Dec 3, 2015
 * */
package luci.connect;


import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import luci.connect.Attachment.AttachmentReferenceException;
import luci.connect.AttachmentSpot.JSONObjectArray;
import luci.connect.On.OnLong;


/**
 * The heart of Luci. A message contains a json formatted header and an arbitrary amount of 
 * attributes. </br>
 * In TCP context a message is preceded by 16 bytes that split up into 8 bytes 
 * representing a long number indicating the length of the header and 8 bytes indicating the
 * length of all attachments. After the header 8 bytes follow indicating the number of 
 * attachments followed by another 8 bytes preceding each attachment and describing the length
 * of the following attachment. In the HTTP/Websocket context only text-encoded headers are 
 * being sent through websockets while attachments are being uploaded with the regular HTTP 
 * POST-method or downloaded with the regular HTTP GET-method. </br>
 * While it is the SocketHandler taking care of downloading attachments in case of the
 * TCPSocketHandler the socket handler first instantiates the requested service in order to
 * "ask" it how to handle attachments. WebSocketHandler downloads all attachments regardless
 * of the service's indication because web connections are assumed to be slower / too slow
 * to claim for valueable time of a remote service to which a potentially slow connection 
 * forwards its attachments. WebSocketHandler rather downloads all attachments and calls the
 * remote service once all attachments are available. TCPSocketHandler on the other hand is 
 * able to forward attachments directly to remote services.</br>
 * Both WebSocketHandler and TCPSocketHandler use {@link luci.connect.Message#setAttachmentsReady()}
 * to notify potential listeners that all the attachments are ready.
 */
public class Message {
	/**Represents the JSON header of a message holding the JSONObject = the actual header
	 * and a list of all AttachmentSpots representing the spot in the json header where
	 * attachments appear. Upon construction of a Header the given JSONObject is being
	 * recursively scanned for attachments, that is if a JSONObject holds a "format"-key
	 * and an "attachment"-key its being recognized as an attachment and replaced with an
	 * AttachmentInNetwork object.
	 */
	private class Header {		
		JSONObject json;
		List<AttachmentSpot> spotList = new ArrayList<>();
		private boolean strict;
		
		/**Creates a new Header object by recursively scanning the given JSONObject for
		 * attachments.
		 * @param headerJSON
		 */
		Header(JSONObject headerJSON){
			this(headerJSON, false);
		}
		
		/**Creates a new Header object by recursively scanning the given JSONObject for
		 * attachments.
		 * @param headerJSON
		 * @param strict Boolean that indicates whether to check for position in attachments
		 */
		Header(JSONObject headerJSON, boolean strict){
			this.strict = strict;
			this.json = headerJSON;
			rcrsvReplaceAttachments(headerJSON);
		}
		
		/**Checks whether another Header object contains the same attachments.
		 * @param other Header object
		 * @return true or false
		 */
		boolean hasEqualAttachments(Header other){
			return spotList.equals(other.spotList);
		}
		
		/**Computes the length of all attachments.
		 * @return the length of all attachments
		 */
		
		long sumAttachmentsLengthFromSpots(){
			long l = 0;
			for (AttachmentSpot as: spotList) l += attMap.get(as.getChecksum()).getLength();
//			trace("sumAttachmentsLength: "+l);
			return l;
		}
		
		/**Scans the given JSONObject "json" for any occurrence of a JSONObject containing "format" and
		 * "attachment" (which is then being transformed into an AttachmentInNetwork) or for Attachment
		 * objects. JSONObjects in JSONArrays are also being processed.
		 * @param json JSONObject to be processed
		 * @param insert boolean whether or not a JSONObject streaminfo should be replaced with an 
		 * Attachment object
		 * @return Map&lt;Integer, Attachment&gt; with Integer keys corresponding with the order the 
		 * attachments are being sent
		 */
		private void rcrsvReplaceAttachments(JSONObject json){
			for (String key: json.keySet()){
				Object o = json.get(key);
				if (o instanceof Attachment){
					Attachment a = (Attachment) o;
					verifyAttachment(a);
					spotList.add(new AttachmentSpot(key, new JSONObjectArray(json), a.getChecksum()));
					json.put(key, a);
				} else if (o instanceof JSONObject){
					JSONFormat f = rcrsvProcessJSONObject((JSONObject)o, key);
					if (f != null) {
						if (f instanceof Attachment){
							Attachment a = (Attachment) f;
							json.put(key, a);
							spotList.add(new AttachmentSpot(key, new JSONObjectArray(json), a.getChecksum()));
						} else {
							json.put(key, (JSONGeometry) f);
						}
					}
				} else if (o instanceof JSONArray){
					JSONArray array = (JSONArray) o;
					for(int i = 0; i < array.length(); i++){
						Object oa = array.get(i);
						if (oa instanceof Attachment){
							Attachment a = (Attachment) oa;
							verifyAttachment(a);
							spotList.add(new AttachmentSpot(i, new JSONObjectArray(array), a.getChecksum()));
							array.put(i, a);
						}
						if (oa instanceof JSONObject){
							JSONFormat f = rcrsvProcessJSONObject((JSONObject)oa, String.valueOf(i));
							if (f != null) {
								if (f instanceof Attachment){
									Attachment a = (Attachment) f;
									array.put(i, a);
									spotList.add(new AttachmentSpot(i, new JSONObjectArray(array), a.getChecksum()));
								} else {
									array.put(i, (JSONGeometry) f);
								}
							}
						}
					}
				}  
			}
		}
		
		private Attachment verifyAttachment(JSONObject json){
			JSONObject attachment = json.getJSONObject("attachment");
			String checksum = attachment.getString("checksum");
			Object p = strict ? attachment.get("position") : attachment.opt("position");
			if (p == null) p = 0;
			// check for LcMetaJSON: upon service registration an attachment input might be 
			// described with 'position':'integer' in which case p is a String
			if (p instanceof Integer) {
				int position = (int) p;
				Attachment a;
				if (position == 0) {
					a = getAttachmentByChecksum(checksum);
					if (a == null) position = getMaxPosition()+1;
					else return a;
				}
				if (!attMap.containsKey(position)) {
					if (json instanceof Attachment) a = (Attachment) json;
					else a = new AttachmentInNetwork(json.optString("name", checksum), json);
					a.setSrcSH(sh);
					a.setPosition(position);
					attMap.put(position, a);
				} else a = attMap.get(position);
				return a;
			}
			return null;
		}

		/**Checks whether a JSONObject has the keys "format" AND "attachment". If it does the value
		 * of checksum is being checked: Service-Input and -Output specifications may contain also
		 * "format" AND "attachment" keys but its values correspond to the regular JSON-types - in 
		 * case of format or checksum this is "string". So if checksum is equal to "string" this 
		 * json object is being ignored. Else a new AttachmentInNetwork is being created using the
		 * information provided by the json object. The resulting attachment object is also being 
		 * put into the attMap hash map. </br>
		 * If the JSONObject does not have the keys "format" AND "attachment" the JSONObject is 
		 * recursively being scanned with {@link: recursivelyReplaceAttachments(JSONObject json)}.
		 * 
		 * @param json JSONObject to be scanned
		 * @return the Attachment object into which the JSONObject has been transformed
		 * @throws JSONException
		 */
		private JSONFormat rcrsvProcessJSONObject(JSONObject json, String key) 
				throws JSONException {
			if (json.has("format") && json.has("attachment")){
				return verifyAttachment(json);
			} else if (json.has("format") && json.has("geometry")){
				Object geometry = json.get("geometry");
				if (geometry instanceof JSONObject){
					return new JSONGeometry(key, json);
				}
			} else {
				rcrsvReplaceAttachments(json);
			}
			return null;
		}
	}
	
	boolean isAttachmentsReady = false;
	private TreeMap<Integer, Attachment> attMap = new TreeMap<>();
	private Header header;
	private SocketHandler sh;
	Set<OnLong> attsReady = new HashSet<>();
	Set<OnLong> onWritten = new HashSet<>();
	Set<OnLong> forwardUnlocks = new HashSet<>();
	private Object affiliation;
	private long ID = 0, callID = 0;
	private boolean isWritten = false;
	private int attachmentType = 0;

	/**
	 * Used to create a new message from a JSON header. Binary attachments can be included as 
	 * {@link Attachment} objects in the JSON object.
	 * 
	 * @param headerJSON JSONObject
	 */
	public Message(JSONObject headerJSON){
		this(headerJSON, false);
	}
	
	private Message(JSONObject headerJSON, boolean strict){
		header = new Header(headerJSON, strict);
		setAttachmentPositions();
		isAttachmentsReady = attMap.size() == 0 || !anyNetworkAttachment();
	}
	
	public static Message withPositionCheck(JSONObject headerJSON){
		return new Message(headerJSON, true);
	}
	
	public Message affiliate(Object o){
		affiliation = o;
		return this;
	}
	
	public Object unaffiliate(){
		Object a = affiliation;
		affiliation = null;
		return a;
	}
	
	public long setID(long id){
		return (ID = id);
	}
	
	public long getID(){
		return ID;
	}
	
	public Message setCallID(long callID){
		this.callID = callID;
		return this;
	}
	
	public long getCallID(){
		return callID;
	}
	
	/**Returns true if attMap.size() is greater than 0
	 * @return boolean
	 */
	public boolean hasAttachments(){
		return attMap.size() > 0;
	}
	
	/**Returns the SocketHandler set as source of this message
	 * @return SocketHandler
	 */
	public SocketHandler getSourceSocketHandler() {
		return sh;
	}
	
	/**Sets the source SocketHandler from which this message originates.
	 * @param sh SocketHandler to be set
	 * @return this message
	 */
	public Message setSourceSocketHandler(SocketHandler sh) {
		this.sh = sh;
		return this;
	}
	
	/**Returns the byte-length of all attachments
	 * @return long
	 */
	public long getSumAttachmentsLength(){
		return attMap.values().stream().mapToLong((a) -> a.getLength()).sum();
	}
	
	public long getSumAttachmentSpotsLength(){
		return header.sumAttachmentsLengthFromSpots();
	}
	
	public int getNumAttachments(){
		return attMap.size();
	}

	/**Returns the header JSONObject as given at initialization of this message but with embedded attachments.
	 * @return JSONObject
	 */
	public JSONObject getHeader(){
		return header.json;
	}
	
	/**Returns a list of AttachmentSpots
	 * @return List&lt;AttachmentSpot&gt;
	 */
	public List<AttachmentSpot> getAttachmentSpots(){
		return header.spotList;
	}
	
	/**Returns an attachments specified by checksum
	 * @param checksum hexadecimal String of MD5 checksum
	 * @return Attachment
	 */
	public Attachment getAttachmentByChecksum(String checksum){
		return attMap.values().stream().filter((a)->a.getChecksum().equals(checksum)).findFirst().orElse(null);
	}
	
	/**Allows to acces attachments as if they were in a list; notice that the index parameter is not equal
	 * to the position key by which the attachment is stored internally
	 * @param index starting from 0
	 * @return
	 */
	public Attachment getAttachment(int index){
		return attMap.get(index+1);
	}
	
	public Attachment getAttachmentByPosition(int position){
		return attMap.get(position);
	}
	
	public int getMaxPosition(){
//		return attMap.keySet().stream().max(Comparator.naturalOrder()).orElse(0);
		Integer max = attMap.floorKey(Integer.MAX_VALUE);
		if (max == null) return 0;
		return max;
	}
	
	/**Adds the AttachmentSpot to the list of attachment spots, replaces the object at the attachment
	 * spot with the attachment, adds the attachment to the attachments map of this message and sets
	 * the position of this attachment to 0 in order to undetermine the attachment sequence (which
	 * must be restored by calling {@link Message#resetAttachmentPositions}
	 * @param a Attachment
	 * @param as AttachmentSpot
	 * @return
	 */
	public Message setAttachment(Attachment a, AttachmentSpot as){
		Object previousValue = as.getParent().get(as.getName());
		if (previousValue instanceof Attachment){
			Attachment previousAttachment = (Attachment) previousValue;
			if (!previousAttachment.getChecksum().equals(a.getChecksum()))
				removeAttachment(previousAttachment);
		}
		Attachment existing = getAttachmentByChecksum(a.getChecksum());
		if (existing == null) attMap.put(getMaxPosition()+1, a);
		else a = existing;
		if (!header.spotList.contains(as)) header.spotList.add(as);
		as.getParent().put(as.getName(), a);
		return this;
	}
	
	public Message removeAttachment(Attachment a) {
		int p = a.getPosition();
		if (attMap.remove(p) != null){
			SortedMap<Integer, Attachment> remaining = attMap.tailMap(p+1);
			if (remaining.size() > 0){
				Iterator<Attachment> rit = remaining.values().iterator();
				while(rit.hasNext()){
					attMap.put(p++, rit.next());
				}
				attMap.pollLastEntry();
			}
		}
		
		return this;
	}
	
	public Message addBlindAttachment(Attachment a){
		if (a.getPosition() == 0) throw new AttachmentReferenceException("Cannot add an attachment with position == 0");
		attMap.put(a.getPosition(), a);
		return this;
	}
	
	/**Checks if the attachment identified by the checksum is present in the attachment map of this
	 * message.
	 * @param checksum hexadecimal string representing the MD5 checksum of the attachment
	 * @return true or false
	 */
	public boolean hasAttachment(String checksum){
		return attMap.values().stream().anyMatch((a)->a.getChecksum().equals(checksum));
	}
	
	/**
	 * @return Map&lt;String, Attachment&gt; with String being the checksum of the attachment
	 */
	public Map<Integer, Attachment> getAttachmentMap(){
		return attMap;
	}
	
	/**returns values of the attribute map of this message and sorts them by position
	 * @return Attachment[]
	 */
	public Attachment[] getAttachmentsSortedByPosition() {
		return attMap.values().toArray(new Attachment[attMap.size()]);
		
	}
	
	/**returns network attachtments of the attribute map of this message and sorts them by position
	 * @return Attachment[]
	 */
	public Collection<Attachment> getNetworkAttachmentsSortedByPosition() {
		return attMap.values().stream()
				.filter((a) -> a instanceof AttachmentInNetwork)
				.sorted((ja1, ja2) -> new Integer(ja1.getPosition()).compareTo(ja2.getPosition()))
				.collect(Collectors.toList());
	}
	
	/**Sets a new json header. Used for {@link luci.service.test.FileEcho FileEcho} service for instance.
	 * @param h JSONObject header
	 * @return JSONObject old header
	 * @throws IllegalStateException
	 */
	public JSONObject setHeader(JSONObject h) throws IllegalStateException{
		Header newHeader = new Header(h);
		if (!header.hasEqualAttachments(newHeader)) {
			for (AttachmentSpot as: newHeader.spotList){
				if (!attMap.containsKey(as.getChecksum())) 
					throw new IllegalStateException("Missing attachment: "
							+ "When setting a new Header the attachment must be included.");
			}
		}
		Header oldHeader = header;
		header = newHeader;
		return oldHeader.json;
	}
	
	/**Sets the source byte channel of all attachments to the given source channel.
	 * Visibility: package: only used by TcpSocketHandler
	 * @param src ReadableByteChannel
	 */
	Message setSrcChannelOfAttachmentsInNetwork(ReadableByteChannel src){
		for (Attachment a: attMap.values()){
			if (a instanceof AttachmentInNetwork) a.setSrcChannel(src);
		}
		return this;
	}
	
	/**Registers the given listener for the event of all attachments becoming ready.
	 * @param ar OnAttachmentsReady listener
	 */
	public void onAttachmentsReady(OnLong ar){
		synchronized(attsReady){
			attsReady.add(ar);
		}
	}
	
	/**Triggers the event of all attachments becoming ready.
	 * 
	 */
	public Message setAttachmentsReady(long length){
		// registered attachments-ready-listeners are called only once
		// and get removed after being called. In case a responder adds
		// another responder the new one will not be deleted but is 
		// being called only the next time the attachments of this 
		// message are being set ready
		
		synchronized(attsReady){
			Set<OnLong> currentArs = new HashSet<>(attsReady);
			for (OnLong ar: currentArs) {
				ar.react(length);
				attsReady.remove(ar);
			}
		}
		return this;
	}
	
	public Message setAttachmentType(int type){
		attachmentType = type;
		Map<String, Attachment> changed = new HashMap<>();
		for (Integer key: attMap.keySet()){
			Attachment a1 = attMap.get(key);
			Attachment a2 = a1.convertTo(type);
			if (a1 != a2) {
				attMap.put(key, a2);
				changed.put(a2.checksum, a2);
			}
		}
		if (changed.size() > 0){
			for (AttachmentSpot as: header.spotList){
				if (changed.containsKey(as.checksum)) 
					as.parent.put(as.name, changed.get(as.checksum));
			}
		}
		return this;
	}
	
	public int getAttachmentType(){
		return attachmentType;
	}
	
	public void onWritten(OnLong ow){
		synchronized(onWritten){
			onWritten.add(ow);
		}
	}
	
	public void cancelOnWritten(OnLong ow){
		synchronized (onWritten) {
			onWritten.remove(ow);
		}
	}
	
	public Message setWritten(long length){
//		System.out.println("called by "+Thread.currentThread().getStackTrace()[2]);
		isWritten = true;
		synchronized(onWritten){
			Set<OnLong> currentArs = new HashSet<>(onWritten);
			for (OnLong ow: currentArs) {
				ow.react(length);
				onWritten.remove(ow);
			}
		}
		return this;
	}
	
	public Message onForwardUnlock(OnLong on){
		synchronized(forwardUnlocks){
			forwardUnlocks.add(on);
		}
		return this;
	}
	
	public Message callForwardUnlocks(long waitedForMillis){
		synchronized(forwardUnlocks){
			for (OnLong on: forwardUnlocks) on.react(waitedForMillis);
			forwardUnlocks.clear();
		}
		return this;
	}
	
	public Message cancelForwardUnlocks(){
		synchronized(forwardUnlocks){
			forwardUnlocks.clear();
		}
		return this;
	}
	
	/**Returns the header JSON string
	 */
	@Override
	public String toString(){
		return header.json.toString();
	}
	
	/**checks if any of the attachments stored in attachment map has a position of zero.
	 * @return true or false
	 */
	private boolean anyZeroPosition(){
		for (Attachment a: attMap.values()){
			if (a.getPosition() == 0) return true;
		}
		return false;
	}
	
	/**Checks if any of the attachments stored in attachment map is a AttachmentInNetwork
	 * @return true or false
	 */
	public boolean anyNetworkAttachment(){
		return attMap.values().stream().anyMatch(a-> (a instanceof AttachmentInNetwork));
	}
	
	public boolean allNetworkAttachment(){
		return attMap.values().stream().allMatch(a-> (a instanceof AttachmentInNetwork));
	}
	
	/**Init attachments by resetting their position if any of them has a position equal to 0.
	 * 
	 */
	public void setAttachmentPositions(){
		if (anyZeroPosition()){
			if (!anyNetworkAttachment()){
				int i = 1;
				for (Attachment a: attMap.values()) a.setPosition(i++);
			} else {
				System.out.println(header.json);
				throw new IllegalStateException("Download all attachments before adding (other) attachments with position equal to zero!");
			}
		}
	}

	/**
	 * @return
	 */
	public boolean writtenOnce() {
		return isWritten;
	}
}
