/** MIT License
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * */
package luci.connect;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONObject;

/**Implementation of Attachment using a ByteBuffer to store all bytes of an attachment.
 * 
 * @author Lukas Treyer
 *
 */
public class AttachmentAsArray extends Attachment{
	ByteBuffer bb;

	AttachmentAsArray(){}
	
	/**Creates a new AttachmentAsArray using an existing ByteBuffer
	 * @param format String
	 * @param name String
	 * @param bb ByteBuffer
	 */
	public AttachmentAsArray(String format, String name, ByteBuffer bb){
		super(name, new JSONObject().put("format", format));
		init(bb);
	}
	
	/**Creates a new AttachmentAsArray using an existing ByteBuffer and the information of 
	 * an existing Attachment.
	 * @param bb ByteBuffer
	 * @param attachment Attachment
	 */
	public AttachmentAsArray(ByteBuffer bb, Attachment attachment) {
		super(attachment);
		init(bb);
	}
	
	/**Create a new AttachmentAsArray using an the information of an existing Attachment and allocates
	 * a new ByteBuffer in memory using the length specified in the given attachment.
	 * @param d Attachment
	 */
	public AttachmentAsArray(Attachment d){
		super(d);
		bb = ByteBuffer.allocate((int) getLength());
	}
	
	/**Create a new AttachmentAsArray using an the information stored in the given JSONObject. The
	 * name parameter is being used in case the JSONObject does not contain a name. A new ByteBuffer
	 * is being created based on the length specified in the given JSONObject.
	 * @param name String
	 * @param json JSONObject
	 */
	public AttachmentAsArray(String name, JSONObject json){
		this(name, json, false);
	}
	
	AttachmentAsArray(String name, JSONObject json, boolean nullableChecksum){
		super(name, json, nullableChecksum);
		bb = ByteBuffer.allocate((int) getLength());
	}
	
	/**Sets the given ByteBuffer as the ByteBuffer backing this attachment, sets didRead to true, 
	 * sets the length of the attachment and calculates the checksum of the bytes contained in the
	 * given ByteBuffer. 
	 * @param bb ByteBuffer
	 */
	private void init(ByteBuffer bb){
		this.bb = bb;
		didRead = true;
		setChecksum(calcChecksum());
		setLength(bb.capacity());
	}
	
	@Override
	public Attachment setLength(long length){
		super.setLength(length);
		if (bb == null) bb = ByteBuffer.allocate((int) length);
		return this;
	}
	
	/**
	 * @return ByteBuffer backing this attachment.
	 */
	public ByteBuffer getByteBuffer() {
		return bb;
	}
	
	/** If didRead is {@code true} the backing ByteBuffer is being rewinded.
	 */
	public boolean didReadCompletely(int numRead){
		didRead = getLength() == numRead;
		if (didRead) bb.rewind();
		return didRead;
	}
	
	public boolean writtenCompletely(long numWritten){
		didWrite = getLength() == numWritten;
		// for writing a message multiple times
		if (didWrite) bb.rewind();
		return didWrite;
	}

	/**Reads bytes from a byte channel to a ByteBuffer.
	 * @param source ReadableByteChannel
	 * @return number of bytes read
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	
	public int read() throws IOException {
		int numRead = -1;
		startedToRead = true;
		numRead = src.read(bb);
		if (numRead < 0) throw new EOFException();
		return numRead;
	}
	
	/**Writes bytes from a ByteBuffer to a WritableByteChannel
	 * @param dest WritableByteChannel
	 * @return number of written bytes
	 * @throws IOException 
	 */
	public int write(WritableByteChannel dest) throws IOException{
		startedToWrite = true;
		return dest.write(bb);
	}

	/* (non-Javadoc)
	 * @see luci.connect.Attachment#calcChecksum()
	 */
	public String calcChecksum() {
		if (!didRead) throw new IllegalStateException("ByteBuffer not ready / filled");
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			int buffersize = 8196;
			int capacity = bb.capacity();
			byte[] b = bb.array();
			for(int i = 0; i < capacity; i+=buffersize ){
				digest.update(b, i, Math.min(buffersize, capacity - i));
			}
			return LcString.getHexFromBytes(digest.digest());
		} catch (NoSuchAlgorithmException e) {
			// never happens, and if it does, we need to debug anyway
			e.printStackTrace();
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see luci.connect.Attachment#getSrcByteChannel()
	 */
	@Override
	public ReadableByteChannel getSrcByteChannel() {
		if (didReadCompletely()) return Channels.newChannel(new ByteArrayInputStream(bb.array()));
		else return src;
	}

	
}
