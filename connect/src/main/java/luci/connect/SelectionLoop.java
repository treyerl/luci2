/** MIT License
 * @author Lukas Treyer
 * @date Feb 5, 2016
 * */
package luci.connect;

import static com.esotericsoftware.minlog.Log.error;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.CountDownLatch;

import luci.connect.TcpSocketHandler.SelectorLoop;

public abstract class SelectionLoop implements Runnable, SelectorLoop{
	
	private static InetAddress resolve(String hostname) {
		try {
			return (hostname.equalsIgnoreCase("localhost")) ? 
					InetAddress.getLocalHost() : InetAddress.getByName(hostname);
		} catch (UnknownHostException e) {
			error(e.toString());
			return null;
		}
	}
	
	protected Selector selector;
	private boolean isInLoop = false;
	private CountDownLatch cdl = new CountDownLatch(1);
	private SocketChannel channel;
	private ServerSocketChannel serverChannel;
	private InetSocketAddress isa;
	protected boolean debug = false;
	
	/* (non-Javadoc)
	 * @see luci.connect.TcpSocketHandler.SelectorLoop#isInLoop()
	 */
	@Override
	public boolean isInLoop() {
		synchronized(this){
			return isInLoop;
		}
	}
	
	public InetSocketAddress getIsa(){
		return isa;
	}

	/* (non-Javadoc)
	 * @see luci.connect.TcpSocketHandler.SelectorLoop#getSelector()
	 */
	@Override
	public Selector getSelector() {
		return selector;
	}
	
	/**Initiate the connection process with hostname and port.
	 * @param hostname String
	 * @param port int
	 * @return the InetSocketAddress object representing hostname and port
	 */
	public InetSocketAddress connect(String hostname, int port){
		isa = new InetSocketAddress(resolve(hostname), port);
		connect(isa);
		return isa;
	}
	
	/**Initiate the connection process with an InetSocketAddress object representing hostname and port.
	 * @param isa InetSocketAddress object
	 */
	public void connect(InetSocketAddress isa){
		try {
			selector = Selector.open();
			channel = SocketChannel.open();
			channel.configureBlocking(false);
			channel.register(selector, SelectionKey.OP_CONNECT);
			channel.connect(isa);
			cdl.countDown();
			this.isa = isa;
		} catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**Called when the key of a connection request has been selected. Used to confirm / complete 
	 * dial-up.
	 * @param key SelectionKey: the key used for connecting. Will be replaced with the new key 
	 * that represents the established connection.
	 * @return the newly constructed TcpSocketHandler that handles this connection and is being
	 * attached to the new selection key.
	 * @throws IOException
	 */
	protected TcpSocketHandler confirmConnection(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();
        if (socketChannel.isConnectionPending()) 
        	socketChannel.finishConnect();
        socketChannel.configureBlocking(false);
        SelectionKey newKey = socketChannel.register(selector, SelectionKey.OP_READ);
        TcpSocketHandler sh = newSocketHandler(newKey);
        newKey.attach(sh);
        return sh;
	}
	
	public boolean isConnected(){
		return channel != null && channel.isConnected();
	}
	
	/**Binds an address to the selector of this selection loop.
	 * @param hostname String
	 * @param port int
	 * @return the InetSocketAddress object created out of hostname and port
	 * @throws IOException
	 */
	public InetSocketAddress bind(String hostname, int port) throws IOException{
		selector = SelectorProvider.provider().openSelector();

		serverChannel = ServerSocketChannel.open();
		serverChannel.configureBlocking(false);

		InetAddress iAddr = resolve(hostname);
		InetSocketAddress isa = new InetSocketAddress(iAddr, port);
		serverChannel.socket().bind(isa);
		serverChannel.register(selector, SelectionKey.OP_ACCEPT);
		cdl.countDown();
		this.isa = isa;
		return isa;
	}
	
	/**
	 * @param key the temporary acception key that is going to be replaced by a new
	 * key in this method, that represents the established connection. A newly 
	 * created TcpSocketHandler is being attached to the key.
	 * @throws IOException
	 */
	private void accept(SelectionKey key) throws IOException {
		ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
		SocketChannel socketChannel = serverSocketChannel.accept();
		socketChannel.configureBlocking(false);
		SelectionKey newKey = socketChannel.register(selector, SelectionKey.OP_READ);
		newKey.attach(newSocketHandler(newKey));
	}
	
	/**Creates a new TcpSocketHandler based on the implementation of the selection loop.
	 * @param key the SelectionKey representing the established connection.
	 * @param needsToWrite the set of TcpSocketHandlers that are currently interested in writing
	 * and to which a TcpSocketHandler must add itself if it wants to write.
	 * @return the newly created TcpSocketHandler
	 */
	protected abstract TcpSocketHandler newSocketHandler(SelectionKey key);
	
	/**Processes an exception that occurred in the selection loop.
	 * @param e Exception that was thrown
	 * @return true or false: whether the selection loop should be exited or not
	 */
	protected abstract boolean processException(Exception e);
	/**Processes an IOException that might occur on an individual SelectionKey. Typically the 
	 * processIOException method implements the cancellation of such a key.
	 * @param key SelectionKey
	 * @param e Exception
	 */
	protected abstract boolean processIOException(SelectionKey key, IOException e);
	
	@Override
	public void run() {
//		int i = 0, j = 0;
		main:
		while (!Thread.interrupted()) {
//			j++;
			try {
				cdl.await();
				selector.select();
				java.util.Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
				synchronized(this){
					isInLoop = true;
				}
				while (selectedKeys.hasNext()) {	
					SelectionKey key = (SelectionKey) selectedKeys.next();
					selectedKeys.remove();
//					i++;
					try {
						if (!key.isValid()) {
							System.out.println("invalid key");
							continue;
						}
						
						if (key.isConnectable()){
	                        confirmConnection(key);
	                    }
						if (key.isAcceptable()) {
							accept(key);
						}
						if (key.isReadable()) {
//							if (debug) System.out.println("+ + readable "+i+" "+j+" "+key.hashCode()+" "+key.interestOps()+" "+System.currentTimeMillis());
							SocketHandler sh = (SocketHandler) key.attachment();
							sh.read(sh);
						}
						if (key.isWritable()) {
//							if (debug) System.out.println("+ + writable "+i+" "+j+" interests "+key.interestOps()+" "+key.hashCode());
							SocketHandler sh = (SocketHandler) key.attachment();
							sh.write();
						}
					} catch (IOException e) {
						SocketHandler sh = (SocketHandler) key.attachment();
						if (sh != null) sh.processIOException(e);
						if (processIOException(key, e)) break main;
					}
				}
				synchronized(this) {
					isInLoop = false;
				}
			} catch (Exception e) {
				if (processException(e)) break;
			} 
		}
		try {
			selector.close();
			if (channel != null) channel.close();
			if (serverChannel != null) serverChannel.close();
//			channel.socket().close();
			selector = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
