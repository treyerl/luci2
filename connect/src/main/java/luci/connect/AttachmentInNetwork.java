/** MIT License
 * @author Lukas Treyer
 * @date Dec 21, 2015
 * */
package luci.connect;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.json.JSONObject;

/**Implements Attachment with only source and destination byte channels which is being used for
 * forwarding an attachment or cleaning / emtpying a socket in case of an error.
 * 
 * @author Lukas Treyer
 *
 */
class AttachmentInNetwork extends Attachment{
	
	/**Used to notify listeners (SocketHandlers most likely) if an 
	 * AttachmentInNetwork as become writable, that is if its destination channel has been set.
	 * 
	 */
	@FunctionalInterface
	public interface OnWritable {
		/**Executed once an AttachmentInNetwork becomes writable, that is if its destination channel has been set.
		 * @param dst WritableByteChannel
		 */
		public void becomesWritable(WritableByteChannel dst);
	}
	
	String chck;
	int remaining = -1;
	int read = 0, written = 0, w = 0;
	private TimerTask continuation;
	MessageDigest digest;
	private List<OnWritable> onWritables = new ArrayList<>();
	final ByteBuffer bb = ByteBuffer.allocateDirect(16 * 1024);
	private WritableByteChannel dst;
	
	AttachmentInNetwork(){
		initDigest();
	}
	
	/**Inits an AttachmentInNetwork based on the information of another attachment.
	 * @param attachment
	 */
	AttachmentInNetwork(Attachment attachment){
		super(attachment);
		initDigest();
	}
	
	/**Inits an AttachmentInNetwork based on the information of the given JSONObject. Parameter "name"
	 * used in case the JSONObject does not hold a name.
	 * @param name
	 * @param j JSONObject
	 */
	AttachmentInNetwork(String name, JSONObject j){
		this(name, j, false);
	}
	
	AttachmentInNetwork(String name, JSONObject j, boolean nullableChecksum){
		super(name, j, nullableChecksum);
		initDigest();
	}
	
	private void initDigest(){
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	/**Registers a listener for the becoming writable event.
	 * @param w OnWritable(FunctionalInterface) = function to be executed once the attachment becomes
	 * writable.
	 */
	public void onWritable(OnWritable w){
		onWritables.add(w);
	}
	
	public int getNumWritten(){
		return written;
	}
	
	/**
	 * @return WritableByteChannel
	 */
	public WritableByteChannel getWritableDestination(){
		return dst;
	}
	
	/**Set the destination channel of this attachment and notifies all listeners that the 
	 * attachment has become writable.
	 * @param dst WritableByteChannel
	 */
	public void setWritable(WritableByteChannel dst){
		this.dst = dst;
		for (OnWritable w: onWritables) w.becomesWritable(dst);
	}

	@Override
	public int read() throws IOException {
		int numReadTotal = 0, numRead = 1;
		startedToRead = true;
		if (remaining < 0) remaining = (int) getLength();
		while(numRead > 0){
			bb.limit(Math.min(bb.capacity(), remaining));
			remaining -= numRead = src.read(bb);
			if (numRead < 0) throw new EOFException();
			if (numRead > 0) {
				digest.update(bb);
				bb.rewind();
				numReadTotal += numRead;
			} 
		}
//		didRead = numReadTotal == getLength();
		didRead = remaining == 0;
		if (didRead) chck = LcString.getHexFromBytes(digest.digest());
		return numReadTotal;
	}
	
	@Override
	public int write(final WritableByteChannel dst) throws IOException {
		startedToWrite = true;
		startedToRead = true;
		if (remaining < 0) 
			remaining = (int) getLength();
		if (written == read && remaining > 0) {
			written = 0;
			bb.position(0).limit(Math.min(bb.capacity(), remaining));
			remaining -= read = src.read(bb);
			bb.flip();
			digest.update(bb);
			bb.flip();
			if (read == -1) throw new EOFException();
		}
		if (written < read){
			written += w = dst.write(bb);
		}
		if (written == read && remaining == 0){
			didRead = didWrite = true;
			chck = LcString.getHexFromBytes(digest.digest());
		}
		return w;
	}
	
	public boolean stopWriteContinuation(){
		if (continuation != null) return continuation.cancel();
		return false;
	}
	
	@Override
	public String calcChecksum() {
		if (!didRead) throw new IllegalStateException("Checksum only available once the attachment "
				+ "has been read or forwarded (written)");
		return chck;
	}

	@Override
	public ReadableByteChannel getSrcByteChannel() {
		return src;
	}
}
