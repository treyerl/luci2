/** MIT License
 * @author Lukas Treyer
 * @date Jan 5, 2016
 * */
package luci.connect;

import java.sql.SQLException;

/**Interface for a TileReader: allows a) multiple implementations of a TileReader (as opposed to 
 * only a reader for mbtiles) and b) is necessary to avoid cyclic maven dependencies: core has a 
 * maven profile to be built with the mbTileReader sub-module, yet the mbTileReader module itself
 * would rely on the core module if this interface would not reside in the connect module.
 * 
 * @author Lukas Treyer
 *
 */
public interface TileReader {

	/**Tile objects hold the tile data as a byte array and provide a format string representing the 
	 * file ending, e.g. "jpg" or "png".
	 */
	public interface Tile {
		/**Returns String representing the file ending such as "jpg" or "png"
		 * @return String 
		 */
		public String getFormat();
		/**Returns the image data as a byte array
		 * @return byte[]
		 */
		public byte[] getData();
		
		/**Returns the mime type for serving the tile
		 * @return String
		 */
		public String getMimeType();
	}
	
	/**reads the tile from the tile repository based on zoom level, x- and y-coordinate.
	 * @param zoom int - zoom level
	 * @param x int - x-coordinate
	 * @param y int - y-coordinate
	 * @param format String the file format of the tile
	 * @param scheme String tms or xyz
	 * @return a Tile object containing the tile data as a byte array
	 * @throws SQLException
	 */
	public Tile read(int zoom, int x, int y, String format, String scheme) throws SQLException;
	
	/**
	 * @return String a file path or URL to the tile source
	 */
	public String getTileSource();
}
