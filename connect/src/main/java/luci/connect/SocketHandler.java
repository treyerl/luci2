/** MIT License
 * @author Lukas Treyer
 * @date Dec 18, 2015
 * */
package luci.connect;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Timer;
import java.util.function.Function;

import org.json.JSONObject;

import luci.connect.TcpSocketHandler.CallException;

/**Used to allow different SocketHandler implementations; mainly to deal with the difference between
 * TCPSocketHandler and WebSocketHandler but also to provide a different socket handler for 
 * registered services (Factory.Node).
 * 
 * @author Lukas Treyer
 *
 */
public interface SocketHandler {
	
    /**Indicates whether the timer runs as a deamon process. Non-deamon threads prevent
     * termination of VM
     */
    final boolean isDaemon = true;
    /** static Timer object used to schedule tasks.
     */
    public final Timer timer = new Timer(isDaemon);
	
//	/**Processes the Exception thrown during read operation or message processing
//	 */
//	@FunctionalInterface
//	public interface ExceptionProcessor{
//		public void processIOException(IOException e);
//	}
//	/**Processes the header of a given message m.
//	 */
//	@FunctionalInterface
//	public interface HeaderProcessor{
//		public void processHeader(Message m) throws CallException;
//	}
	
	// a SocketHandler is also a ExceptionProcessor
	/**Despite using FunctionalInterfaces for header and exception processing a socket handler must
	 * provide this functionality. The functional interfaces are used to coordinate the different
	 * socket handler among each other and call each others processing functions appropriately.
	 * @param e Exception
	 */
	public boolean processIOException(IOException e);
	
	public void processCallException(CallException e);
	
	// a SocketHandler is also a HeaderProcessor
	/**Despite using FunctionalInterfaces for header and exception processing a socket handler must
	 * provide this functionality. The functional interfaces are used to coordinate the different
	 * socket handler among each other and call each others processing functions appropriately.
	 * @param m Message
	 * @throws Exception
	 */
	public void processHeader(Message m) throws CallException;
	
	/**Performs the read operation once the selector has a key ready for reading.
	 * @param hp {@link HeaderProcessor} FunctionalInterface
	 * @param ep {@link ExceptionProcessor} FunctionalInterface
	 * @throws IOException
	 */
	public void read(SocketHandler sh) throws IOException;
	
	/**Write a message (json header and attachments[Array/File/InNetwork])
	 * @param m Message
	 */
	public void write(Message m);
	
	/**When sending the same message multiple times and subtle changes to the header are necessary
	 * use the modifier function. Does not allow modification of attachments on purpose!
	 * @param m
	 * @param modifier
	 */
	public void write(Message m, Function<JSONObject, JSONObject> modifier);
	
	/**Perform the write operation when the selector has a key ready for writing.
	 * @throws IOException
	 */
	public void write() throws IOException;
	
//	/**Returns the name of the item handled by this SocketHandler
//	 * @return String
//	 */
//	public String handles();
	
	public InetSocketAddress getRemoteSocketAddress();
	
	public InetSocketAddress getLocalSocketAddress();
	
	public void close() throws IOException;

}
