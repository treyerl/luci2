/** MIT License
 * @author Lukas Treyer
 * @date 2014 - 2016
 * */

package luci.connect;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;

/**Methods to simplify string operations used in Luci's context.
 */
public class LcString {
	public final static int HEX = 1;
	public final static int ALPHANUMERIC = 2;
	private String s;
	
	/**Create a LcString using the given string as a delegate.
	 * @param s String
	 */
	public LcString(String s){
		this.s = s;
	}
	
	/**Creates a LcString converting the given byte-array to a hexadecimal string and using it as a
	 * delegate
	 * @param toHex byte[]
	 */
	public LcString(byte[] toHex){
		this(getHexFromBytes(toHex));
	}
	
	/**Concatenates <code>'what'</code> x times. <br>
	 * LcString.multiply(5, "x ") --> "x x x x x "
	 * @param what String
	 * @param times int
	 * @return multiplied String
	 */
	public static String multiply(String what, int times){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < times; i++) sb.append(what);
		return sb.toString();
	}
	/**Calls the static multiply method using the delegate String object.
	 * @see luci.connect.LcString#multiply(java.lang.String, java.lang.Integer)
	 * @param times Integer
	 * @return String
	 */
	public String multiply(int times){return multiply(s, times);}
	
	/**Creates a string that multiplies <code>'with'</code> x times so that the length of the 
	 * resulting string will be equal or less than <code>'length'</code> 
	 * @param with String
	 * @param length int
	 * @return String
	 */
	public static String fill(String with, int length) {
	    StringBuilder sb = new StringBuilder(length);
	    int len = with.length();
	    while (sb.length() + len <= length) sb.append(with);
	    return sb.toString();
	}
	/**Calls the static fill method using the delegate String object.
	 * @see luci.connect.LcString#fill(java.lang.String, java.lang.Integer)
	 * @param length Integer
	 * @return String
	 */
	public String fill(int length){return fill(s, length);}
	
	/**Trims a string as indicated by <code>i</code> on both ends.
	 * @param str String
	 * @param i int
	 * @return trimmed string
	 */
	public static String trim(String str, int i){
		return str.substring(i, str.length() - i);
	}
	/**Calls the static trim method using the delegate String object.
	 * @see luci.connect.LcString#trim(java.lang.String, java.lang.Integer)
	 * @param charCount Integer
	 * @return String
	 */
	public String trim(int charCount) {return trim(s, charCount);} 
	
	/**Checks whether all characters in a given string are upper case.
	 * @param r String
	 * @return boolean
	 */
	public static boolean isAllUpperCase(String r) {
		boolean is = true;
		for (int i = 0; i < r.length(); i++){
			char ch = r.charAt(i);
			if (Character.isWhitespace(ch))
				continue;
			if (ch == '_') 
				continue;
			if (!Character.isUpperCase(ch))
				is = false;
		}
		return is;
	}
	/**Calls the static isAllUpperCase method using the delegate String object.
	 * @see luci.connect.LcString#isAllUpperCase(java.lang.String)
	 * @return boolean
	 */
	public boolean isAllUpperCase(){return isAllUpperCase(s);}
	
	/**Concatenates Strings using a separation string. 
	 * @param strings Strings to be joined
	 * @param sep String; separation char/sequence
	 * @return
	 */
	public static String join(Collection<String> strings, String sep){
		if (strings.size() > 0){
			StringBuilder sb = new StringBuilder();
			String sepa = "";
			for (String s: strings){
				sb.append(sepa+s);
				sepa = sep;
			}
			return sb.toString();
		}
		return ""; 
	}
	
	public static String join(String[] strings, String sep){
		if (strings.length > 0){
			StringBuilder sb = new StringBuilder();
			String sepa = "";
			for (String s: strings){
				sb.append(sepa+s);
				sepa = sep;
			}
			return sb.toString();
		}
		return ""; 
	}
	
	/**Calls the static join method using the delegate String object as joining element.
	 * @see luci.connect.LcString#join(java.util.Collection, java.lang.String)
	 * @param strings java.util.Collection&lt;String&gt;
	 * @return String
	 */
	public String join(Collection<String> strings) {return join(strings, s);}
	
	/**Indentation of texts using \n as line breaks
	 * @param str String to scan for \n
	 * @param by int; Indentation by how many whitespaces
	 * @return indented string
	 */
	public static String indent(String str, int by){
		String BY = multiply(" ", by);
		return str.replace("\n", "\n"+BY);
	}
	/**Calls the static indent method using the delegate String object.
	 * @see luci.connect.LcString#trim(java.lang.String, java.lang.Integer)
	 * @param by Integer
	 * @return String
	 */
	public String indent(int by){return indent(s, by);}
	
	/**limit string in length
	 * @param src String
	 * @param limit int
	 * @return "shortened string that is shorte..."
	 */
	public static String limit(String src, int limit){
		if (src.length() > limit - 3)
			return src.substring(0, limit - 3) + "...";
		return src;
	}
	/**Calls the static limit method using the delegate String object.
	 * @see luci.connect.LcString#limit(java.lang.String, java.lang.Integer)
	 * @param limit Integer
	 * @return String
	 */
	public String limit(int limit) {return limit(s, limit);}
	
	/** similar to substring but also takes negative amount that is being cut away from the end
	 * @param src String
	 * @param fromEnd int number of characters from the end if negative, else from beginning
	 * @return cropped string
	 */
	public static String upto(String src, int fromEnd){
		return src.substring(0, fromEnd < 0 ? Math.max(src.length() + fromEnd, 0) : fromEnd);
	}
	/**Calls the static upto method using the delegate String object.
	 * @see luci.connect.LcString#upto(java.lang.String, java.lang.Integer)
	 * @param to Integer
	 * @return String
	 */
	public String upto(int to) {return upto(s, to);}
	
	/**returns true if this string contains any of the given strings
	 * @see luci.connect.LcString#containsAny(java.lang.String, java.lang.String[])
	 * @return boolean
	 */
	public boolean containsAny(String...strings){
		for (String c: strings) if (s.contains(c)) return true;
		return false;
	}
	
	/**Returns true if the given source string 'src' contains any character of the specified type
	 * @param src String to be tested
	 * @param type Integer: either LcString.ALPHANUMERIC or LcString.HEX
	 * @return true if the given source string 'src' contains any character of the specified type
	 */
	public static boolean containsAny(String src, int type){
		char[] chars = src.toCharArray();
		switch(type){
		case ALPHANUMERIC:
			for (char ch: chars) {
				int c = (int) ch;
				// 0-9					A-Z					a-z
				if ((c>=48 && c<=57) || (c>=65 && c<=90) || (c>=97&&c<=122)) return true;
			}
			return false;
		case HEX:
			for (char ch: chars){
				int c = (int) ch;
				// 0-9					A-F					a-f
				if ((c>=48 && c<=57) || (c>=65 && c<=70) || (c>=97&&c<=102)) return true;
			}
			return false;
		}
		throw new IllegalArgumentException("Unkown type "+type);
	}
	/**Calls the static containsAny method using the delegate String object.
	 * @see luci.connect.LcString#containsAny(java.lang.String,java.lang.Integer)
	 * @return boolean
	 */
	public boolean containsAny(int type){return containsAny(s, type);}
	
	/**tests a given string whether it contains all of the other given strings
	 * @param src String to be tested
	 * @param cc String: variable number of strings
	 * @return true if a given string contains all of the other given strings
	 */
	public static boolean containsAll(String src, String... cc){
		for (String c: cc) if (!src.contains(c)) return false;
		return true;
	}
	/**Calls the static containsAll method using the delegate String object.
	 * @see luci.connect.LcString#containsAll(java.lang.String, java.lang.String[])
	 * @return boolean
	 */
	public boolean containsAll(String... strings){return containsAll(s, strings);}
	
	/**tests whether a given string is restricted to only the indicated type of characters
	 * @param src String to be tested
	 * @param type Integer: either LcString.ALPHANUMERIC or LcString.HEX
	 * @return true if the given string only contains character of the given type
	 */
	public static boolean containsOnly(String src, int type){
		char[] chars = src.toCharArray();
		switch(type){
		case ALPHANUMERIC:
			for (char ch: chars) {
				int c = (int) ch;
				//  0-9					  A-Z					a-z
				if (!(c>=48 && c<=57) || !(c>=65 && c<=90) || !(c>=97&&c<=122)) return false;
			}
			return true;
		case HEX:
			for (char ch: chars){
				int c = (int) ch;
				//  0-9					  A-F					a-f
				if (!(c>=48 && c<=57) || !(c>=65 && c<=70) || !(c>=97&&c<=102)) return false;
			}
			return true;
		}
		throw new IllegalArgumentException("Unkown type "+type);
	}
	/**Calls the static containsOnly method using the delegate String object.
	 * @see luci.connect.LcString#containsOnly(java.lang.String, java.lang.Integer)
	 * @return boolean
	 */
	public boolean containsOnly(int type){return containsOnly(s, type);}
	
	/**Converts a byte array to a hexadecimal string representation (hexadecimal values being padded
	 * with zeros).
	 * @param bytes byte[] array
	 * @return String in hexadecimal format
	 */
	public static String getHexFromBytes(byte[] bytes){
		StringBuilder sb = new StringBuilder();
		for (byte b: bytes){
			sb.append(String.format("%02X", b));
		}
		return sb.toString();
	}
	
	/**Converts a hexadecimal string representation to a byte[] array.
	 * @param s hexadecimal String representation of a byte[] array 
	 * @return the byte[] array representation of the given string
	 */
	public static byte[] getBytesFromHex(String s) {
		if (s == null) return null;
		if (!containsOnly(s,HEX)) 
			throw new IllegalArgumentException("Given string '"+s+"' contains nonhexadecimal characters");
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
		    data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
		                         + Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}
	/**
	 * see {@link luci.connect.LcString#getBytesFromHex(java.lang.String)}
	 */
	public byte[] getBytesFromHex(){ return getBytesFromHex(s);}
	
	
	public static boolean isHex(String s){
		return s.matches("^[0-9a-fA-F]+$");
	}
	
	/**Transforms an InetAddress into a String and in case it's a wildcard address "0:0:0:0:1" tries
	 * to get the IP of localhost
	 * @param ia InetAddress
	 * @return IP as a String
	 * @throws UnknownHostException
	 */
	public static String IPfromInetAddress(InetAddress ia) {
		String ip = ia.getHostAddress();
		if (ip.startsWith("0:"))
			try {
				ip = InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				// ip remains unchanged
			}
		return ip;
	}
}
