/** MIT License
 * @author Lukas Treyer
 * @date Dec 4, 2015
 * */
package luci.connect;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.json.JSONObject;

/**Implements an attachment with the attachment bytes being stored in (written to / read from) a file.
 * 
 * @author Lukas Treyer
 *
 */
public class AttachmentAsFile extends Attachment {
	
	/**
	 * Indicates whether files should be named by name or checksum of the attachment.
	 */
	public static boolean filenameFromChecksum = false;
	/**
	 * Represents the directory to which attachment files should be stored.
	 */
	public static File parentDirectory = new File(".");
	
	File file;
	int transferredTotal = 0, d = 0;
	
	private RandomAccessFile aFile;
	private FileChannel fchan;
	private boolean deleteOnceWritten = false;
	
//	AttachmentAsFile(String checksumOrUUID){
//		file = new File(parentDirectory, checksumOrUUID);
//		if (file.exists() && !file.isDirectory()) file.delete();
//		file.getParentFile().mkdirs();
//	}
	
	AttachmentAsFile(){
		createFile();
	}
	
	/**Creates a new AttachmentAsFile based on the information of another attachment. The backing 
	 * file is being created either based on the attachment name or its checksum. See
	 * {@link luci.connect.AttachmentAsFile#filenameFromChecksum}
	 * @param a Attachment
	 */
	public AttachmentAsFile(Attachment a) {
		super(a);
		createFile();
	}
	
	/**Creates a new AttachmentAsFile based on the information provided by the JSONObject.
	 * The backing file is being created either based on the attachment name or its checksum. See
	 * {@link luci.connect.AttachmentAsFile#filenameFromChecksum}
	 * @param name to be used if the JSONObject parameter does not contain a name
	 * @param json JSONObject
	 */
	public AttachmentAsFile(String name, JSONObject json){
		this(name, json, false);
	}
	
	AttachmentAsFile(String name, JSONObject json, boolean nullableChecksum){
		super(name, json, nullableChecksum);
		createFile();
	}
	
	/**Creates the backing file either based on the attachment name or its checksum. See
	 * {@link luci.connect.AttachmentAsFile#filenameFromChecksum}
	 */
	private void createFile(){
		String format = (UnknownFormat.equals(getFormat()) ? "" : "."+getFormat());
		if (filenameFromChecksum) {
			String name = (getChecksum() == null) ? UUID.randomUUID().toString() : getChecksum();
			file = new File(parentDirectory, name+format);
		} else file = new File(parentDirectory, getName()+format);
		if (file.exists() && !file.isDirectory()) file.delete();
		file.getParentFile().mkdirs();
	}

	
	/**Creates a new AttachmentAsFile based on the given file extracting name and format from the 
	 * filename (pre- and suffix). Also calculates the checksum using {@link luci.connect.AttachmentAsFile#calcChecksum()}.
	 * @param file that backs the attachment
	 */
	public AttachmentAsFile(File file) {
		super(fromNameAndFormat(file));
		this.file = file;
		super.setChecksum(calcChecksum());
		setLength(file.length());
	}
	
	public Attachment setChecksum(String chck){
		super.setChecksum(chck);
		if (filenameFromChecksum && file != null){
			File renamed = new File(parentDirectory, chck+"."+getFormat());
			file.renameTo(renamed);
			file = renamed;
		}
		return this;
	}
	
	
	/**Extracts name and format from filename.
	 * @param file
	 * @return String[]{name, format}
	 */
	private static String[] fromNameAndFormat(File file){
		String n = file.getName();
		int dot = n.lastIndexOf(".");
		if (dot > 0)
			return new String[]{n.substring(0, dot), n.substring(dot+1)};
		return new String[]{n,null};
	}
	
	public static AttachmentAsFile withChecksum(File file, String checksum){
		return new AttachmentAsFile(file, checksum);
	}
	
	private AttachmentAsFile(File file, String checksum){
		super(fromNameAndFormat(file));
		this.file = file;
		setChecksum(checksum);
		setLength(file.length());
	}
	
	/**
	 * @return File backing this attachment
	 */
	public File getFile() {
		return file;
	}
	
	public int read() throws IOException{
		int transferred = 0;
		didRead = false;
		startedToRead = true;
		long count = getLength() - transferredTotal;
		if (!file.exists()) file.createNewFile();
		if (aFile == null){
			aFile = new RandomAccessFile(file, "rw");
			fchan = aFile.getChannel();
		}
		transferredTotal += transferred = (int) fchan.transferFrom(src, transferredTotal, count);
		if (transferred < 0) {
			aFile.close();
			throw new EOFException();
		}
		if (transferredTotal == getLength()) {
			transferredTotal = 0;
			aFile.close();
			aFile = null;
			didRead = true;
		}
//		System.out.println(getName()+".read(): transferred "+transferred+" "+transferredTotal);
		return transferred;
	}
	
	public int write(WritableByteChannel dst) throws IOException{
		startedToWrite = true;
		int transferred = 0;
		long count = getLength() - transferredTotal;
		
		if (aFile == null){
			aFile = new RandomAccessFile(file, "r");
			fchan = aFile.getChannel();
		}
		transferredTotal += transferred = (int) fchan.transferTo(transferredTotal, count, dst);
		if(transferredTotal == getLength()) {
			aFile.close();
			aFile = null;
			fchan = null;
		}
//		System.out.println(getName()+".write(): transferred "+transferred+" "+transferredTotal);
		return transferred;
	}
	
	@Override
	public boolean writtenCompletely(long numWritten){
		if (super.writtenCompletely(transferredTotal)) {
			transferredTotal = 0;
			if (deleteOnceWritten) file.delete();
			return true;
		}
		return false;
	}
	
	public String calcChecksum() {
//		if (file.length() == 0) throw new IllegalStateException("File is empty!");
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			int buffersize = 8196;
			ByteBuffer b = ByteBuffer.allocateDirect(buffersize);
			long fileLength = file.length();
			
			RandomAccessFile aFile = new RandomAccessFile(file, "r");
			FileChannel inChannel = aFile.getChannel();
			int numRead = 0;
			for(int i = 0; i < fileLength; i+=numRead ){
				numRead = inChannel.read(b);
//				if ( i > 50000) System.out.println(numRead+" "+Arrays.toString(b.array()));
				b.flip();
				digest.update(b);
				b.flip();
			}
			aFile.close();
			return LcString.getHexFromBytes(digest.digest());
		} catch (NoSuchAlgorithmException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void checkTheSum(){
		super.checkTheSum();
		if (isBlind()) file.renameTo(new File(parentDirectory, getChecksum()));
	}


	@Override
	public ReadableByteChannel getSrcByteChannel() {
		if (didReadCompletely()){
			throw new IllegalStateException("Cannot get source byte channel of file that has already been read "
					+ "(getting the channel from a file leads to file channels remaining open)");
		} else {
			return src;
		}
	}
	
	public AttachmentAsFile deleteOnceWritten(){
		deleteOnceWritten = true;
		return this;
	}

}
