/** MIT License
 * @author Lukas Treyer
 * @date Feb 5, 2016
 * */
package luci.connect;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.util.Arrays;
import java.util.List;
import java.util.TimerTask;

import org.json.JSONObject;

import com.esotericsoftware.minlog.Log;

/**Abstract class guiding the development of remote services in Java. Directly registers
 * upon connection has been established.
 * 
 * @author Lukas Treyer
 */
public abstract class LcRemoteService extends LcClient {
	InetSocketAddress isa;
	private int numAttemptsReconnect = 0;
	
	
	/**Class to parse command line arguments passed over by console.
	 * Checks for <pre> -host &lt;hostname&gt; -port &lt;port&gt; -id &lt;persistenceID&gt; -io </pre>. 
	 */
	public static class DefaultArgsProcessor{
		String hostname;
		int port, i;
		Integer id;
		boolean io;
		
		public DefaultArgsProcessor(){
			this(new String[]{});
		}
		
		public DefaultArgsProcessor(String[] args){
			List<String> largs = Arrays.asList(args);
			hostname = ((i = largs.indexOf("-host")) >= 0) ? largs.get(i+1) :  "localhost";
			port = ((i = largs.indexOf("-port")) >= 0) ? Integer.valueOf(largs.get(i+1)) : 7654;
			id = ((i = largs.indexOf("-id")) >= 0) ? Integer.valueOf(largs.get(i+1)) : null;
			io = largs.contains("-io");
		}
		
		public String getHostname(){
			return hostname;
		}
		
		public int getPort(){
			return port;
		}
		
		public Integer getID(){
			return id;
		}
		
		public boolean isIO(){
			return io;
		}
		
		public String toString(){
			return "-host "+hostname+" -port "+port+((id!=null)?" -id "+id.toString():"")+((io)?" -io":"");
		}
	}
	
	/**Defines how remote service calls are to be handled.
	 */
	public abstract class RemoteServiceResponseHandler extends ResponseHandler {
		/**Processes the "run" instruction.
		 * @param m Message
		 */
		public void processRun(Message m){
			long callID = m.getHeader().getLong("callID");
			try {
				Message response = implementation(m);
				response.getHeader().put("callID", callID);
				sendBack(response);
			} catch (Exception e){
				if (Log.DEBUG) e.printStackTrace();
				sendBack(new Message(new JSONObject().put("error", e.toString()).put("callID", callID)));
			}
			
		}
		
		public abstract Message implementation(Message input);
		
		/**Processes the "cancel" instruction.
		 * @param m Message
		 */
		public void processCancel(Message m) {
			this.processError(new Message(new JSONObject().put("error", "cancelled "+m.getID())));
		}
	}
	
	/**sends a message without putting anything into the response handler queue since Luci will never
	 * answer to this message with a `newCallID` or anything similar
	 * @param m Message
	 */
	public void sendBack(Message m){
		tcpSocket.write(m);
	}
	
	public class RemoteResponseProcessor extends ResponseProcessor {

		public RemoteResponseProcessor(SelectionKey key, SelectorLoop sl) {
			super(key, sl);
			PRE = "REM";
		}
		
		public boolean isSendingErrors(){
			return true;
		}
		
	}
	
	DefaultArgsProcessor ap;
	TimerTask reconnect;
	
	public LcRemoteService(DefaultArgsProcessor ap){
		this.ap = ap;
	}
	
	private TimerTask getNewReconnector(){
		return new TimerTask(){
			public void run() {
				if (++numAttemptsReconnect > 3) this.cancel();
				if (isa != null) connect(isa);
			}
		};
	}
	public TcpSocketHandler confirmConnection(SelectionKey key) throws IOException{
		numAttemptsReconnect = 0;
		if (reconnect != null) {
			reconnect.cancel();
			reconnect = null;
		}
		
		super.confirmConnection(key);
		sendAndReceive(new Message(new JSONObject()
				.put("run","RemoteRegister")
				.put("serviceName", specifyInputs().getString("run"))
				.put("description", getDescription())
				.put("exampleCall", exampleCall())
				.putOpt("inputs", specifyInputs())
				.putOpt("outputs", specifyOutputs())
				.putOpt("id", ap.id)
				), newResponseHandler());
		return tcpSocket;
	}
	
	/**Returns the canonical name of the service without a leading "luci.service." if present.
	 * @return String 
	 */
	public String getName(){
		return getClass().getName().replace("luci.service.", "");
	}
	
	/**returns a description of the remote service
	 * @return String
	 */
	public abstract String getDescription();
	
	/**Returns a JSONObject that specifies the required inputs.
	 * @return JSONObject 
	 */
	protected abstract JSONObject specifyInputs();
	/**Returns a JSONObject that specifies the deliverd outputs.
	 * @return JSONObject 
	 */
	protected abstract JSONObject specifyOutputs();
	/**An example call must be indicated to facilitate reading of the input 
	 * specification and avoid misunderstandings.
	 * @return JSONObject representing an example call 
	 */
	protected abstract JSONObject exampleCall();
	
	public void run(){
		if (ap.io){
			System.out.println(new JSONObject()
					.put("inputs", specifyInputs())
					.put("outputs", specifyOutputs())
					.put("description", getDescription())
					.put("exampleCall", exampleCall()));
		} else {
			super.run();
		}
	}
	
	public InetSocketAddress connect(String hostname, int port) {
		return (isa = super.connect(hostname, port));
	}
	
	@Override
	protected boolean processIOException(SelectionKey key, IOException e) {
		key.cancel();
		if (reconnect != null) reconnect.cancel();
		// try to reconnect after 0.5 sec and then every 10 sec for two times
		SocketHandler.timer.schedule((reconnect = getNewReconnector()), 500, 10000);
		return false;
	}
	
	@Override
	protected TcpSocketHandler newSocketHandler(SelectionKey key) {
		return new RemoteResponseProcessor(key, this);
	}
}
