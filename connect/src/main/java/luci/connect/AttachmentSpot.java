/** MIT License
 * @author Lukas Treyer
 * @date Feb 3, 2016
 * */
package luci.connect;

import org.json.JSONArray;
import org.json.JSONObject;

/**Simple class representing the appearence of an attachment in the header json object allowing to 
 * modify / replace the object at that specific spot in the json header. In the Message.Header class
 * a list of AttachmenSpots is being kept to access all spots in a linear way instead of recursively
 * scanning the json object.
 *  
 * @author Lukas Treyer
 *
 */
public class AttachmentSpot{
	
	/**Abstraction of JSONObject and JSONArray to handle them both equally. As far AttachmentSpot
	 * is concerned only the put method of the two is used anyway. Static so it is available without
	 * an enclosing instance.
	 */
	public static class JSONObjectArray{
		private JSONObject json;
		private JSONArray array;
		/**Creates a JSONObjectArray object based on a JSONObject
		 */
		public JSONObjectArray(JSONObject json){
			this.json = json;
		}
		/**Creates a JSONObjectArray object based on a JSONArray
		 */
		public JSONObjectArray(JSONArray array){
			this.array = array;
		}
		
		public JSONObjectArray(Object o){
			if (o instanceof JSONObject) this.json = (JSONObject) o;
			else if (o instanceof JSONArray) this.array = (JSONArray) o;
		}
		
		/**Puts an object either into the JSONObject using a string key or into a JSONArray using an
		 * integer index.
		 * @param key Object
		 * @param value Object
		 */
		public void put(Object key, Object value){
			if (json != null) json.put(String.valueOf(key), value);
			else if (array != null) array.put((Integer) key, value);
		}
		/**Retrieves an object associated with either a string key or an integer index. Returns 
		 * <code>null</code> if no such key exists.
		 * @param key Object
		 * @return
		 */
		public Object get(Object key){
			if (json != null) return json.opt((String) key);
			else return array.opt((Integer) key);
		}
		
		/**Returns the delegate object (JSONObject or JSONArray)
		 * @return Object - either JSONObject or JSONArray
		 */
		public Object getDelegate(){
			if (json != null) return json;
			else return array;
		}
		
		/** checks if the JSONObjectArray.getDelegate() is the same by reference (==) as this.getDelegate()
		 */
		public boolean equals(Object other){
			if (other instanceof JSONObjectArray){
				return getDelegate() == ((JSONObjectArray) other).getDelegate();
			}
			return false;
		}
	}
	Object name;
	JSONObjectArray parent;
	String checksum;
	/**Creates an AttachmentSpot based on parent JSONObject, keyname and hexadecimal checksum String
	 * @param name String
	 * @param parent JSONObject
	 * @param checksum String identifying the attachment in the Message's attMap
	 */
	public AttachmentSpot(Object name, JSONObjectArray parent, String checksum){
		this.name = name;
		this.parent = parent;
		this.checksum = checksum;
	}
	/**Returns name / key of how the attachment is referenced in the parent JSONObject accessible
	 * through {@link luci.connect.AttachmentSpot#getParent()}.
	 * @return Object 
	 */
	public Object getName(){
		return name;
	}
	/**Returns the parent JSONObject in which the attachment referenced by this spot is being 
	 * accessible with the key / name provided by {@link luci.connect.AttachmentSpot#getName()}
	 * @return JSONObjectArray
	 */
	public JSONObjectArray getParent(){
		return parent;
	}
	
	/**Returns a hexadecimal string representing the checksum of the referenced attachment
	 * @return String
	 */
	public String getChecksum(){
		return checksum;
	}
	/**Sets the hexadecimal string representing the checksum of the referenced attachment
	 * @param checksum hexadecimal String
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	
	/** Compares name and parent (with parents checking whether their delegate is the same object)
	 */
	public boolean equals(Object other){
		if (other instanceof AttachmentSpot){
			AttachmentSpot as = (AttachmentSpot) other;
			return name.equals(as.getName()) && parent.equals(as.getParent());
		}
		return false;
	}
}
