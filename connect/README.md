#**Luci Connect**
The luci.connect package implements the Luci protocol, a JSON-RPC alike protocol allowing to mix
human readable text-based headers with binary attachments. Refer to the Protocol section for more 
information. 

This package is intended for any Java project that needs to be connected to Luci as a client or a 
remote service. As such it contains two classes `LcClient` and `LcRemoteService` that likely represent
the main classes of this package for developers using this package for Luci-Clients. Also to mention
`Message`, perhaps `Attachment` and `JSONGeometry` that might be used as in- and output types. 

The `luci.core` package, Luci's main implementation, uses this package to handle TCP sockets. 

`JSON`, `GUIRun`, `LcString` are helper classes.  

#**LcClient**
A class that guides the creation of Luci clients in Java and implements all low level functionality 
a typical Luci user/client developer doesn't want to care about. You need to

* implement a `ResponseHandler` which is abstractly defined in `LcClient`
	* implement a method `processResult(Message m)`
* implement a method `newResponseHandler` that return your ResponseHandler
* use `send(Message m)` to send Messages (refer to Protocol for how they need to look). LcClient will
create a new ResponseHandler using `newResponseHandler()` to handle the response of your request.
* use `sendAndReceive(Message m, ResponseHandler rh)` if you have your ResponseHandler defined already.

#**LcRemoteService**
Additionally to the `ResponseHandler` of a normal LcClient a `RemoteServiceResponseHandler` must 
implement a method `processRun(Message m)`. And in addition to a `LcClient` a `LcRemoteService`
requires 4 abstract methods to be implemented:

* `String getDescription()`
* `JSONObject exampleCall()`
* `JSONObject specifyInputs()`
* `JSONObject specifyOutputs()`

Those methods correspond to the methods a local / built-in service (e.g. [ServiceList](https://bitbucket.org/treyerl/luci2/src/master/core/src/main/java/luci/service/ServiceList.java?at=master&fileviewer=file-view-default)) needs to implement as well.

#**Protocol**
###Not (only) text-based
Even though Luci's protocol is very similar to JSON-RPC it allows binary attachments, which in essence
turns the protocol into a binary, not anymore purely text-based protocol. This is true at least for 
the TCP-version of the protocol. Luci also supports json-based calls through websockets that share
the same JSON headers as its TCP equivalent. The attachments nontheless are handled differently.
Websockets are not covered by the `luci.connect` package.
 
###Asynchronous Calls (aka: Push Notifications)
Similar to JSON-RPC the response to a request is identified by an ID, the `callID`. In general the 
(TCP-)connection remains open until the client gets closed (or all network operations have completed). 
In contrast to JSON-RPC the callID is a simple number. It is optional in a request and if omitted 
Luci will generate one and send it back to the client in a message like `{'newCallID':1}`. 
All responses (`error, result, progress`) must contain the `callID`. 
Luci handles a socket connection sequentially. Therefore all generated
callIDs return in the same order as requests where sent off. The service results and/or progress
notifications then return asynchronously. (Don't misconcept Luci's sequential socket handling with
blocking I/O, Luci is implemented using Java's NIO package in asynchronous / non-blocking mode. Yet 
that does not mean, that the data in a TCP socket would not arrive in the order as it was sent off.)

###Request
Luci's services can be run by sending a json request to Luci:

	{
		'run':'ServiceName',
		'param1':'any json type'
	}
	
If you are calling a task that was created earlier your request might look like:

	{
		'run':5,
		'param1':'any json type'
	}
	
or if you want to call multiple tasks at once:

	{
		'run':[1,2,3]
	}
	
Note that everything else than the `run` key is handled as optional input parameters. They are not
required in a request. A Task is a predefined virtual instance of a service call. It holds all input
parameters that were set upon task creation. This comes in handy if you have much input data and/or
call the service many times with the same input parameters. The input data is sent to Luci only once.
Tasks are identified by a `taskID`. It's this very id you provide when calling tasks. Clients/Users
not using tasks can just send `{'run':'ServiceName'}` without bothering about tasks at all.

If you want to cancel a service, send:

	{
		'cancel':1,
		'callID':1
	}

with `1` being the callID you received with `{'newCallID':1}`.

###Responses

The first response was introduced already: `{'newCallID':1}`.

**Result messages**:

	{
		'result':{},
		'callID':1, (optional)
		'serviceName':'myService', (optional)
		'duration':1 (in milliseconds), (optional)
		'taskID':0 (optional)
	}
	
Result messages may include a taskID, which is 0 if the service was run "directly" with a service
name. The duration of a service call refers to the amount of time a service requires to run in Luci.
For remote services (services connected to Luci from other machines) this also includes the time spent
for networking. While Luci always adds `callID`, `serviceName`, `duration`, `taskID` (meaning as a
client you can expect those keys, a remote service does not need to include them when sending back
a result. 

**Progress Messages**:

	{
		'progress':45,
		'intermediateResult':{}, (optional)
		'callID':1, (optional)
		'serviceName':'myService', (optional)
		'duration':650, (optional)
		'taskID':0 (optional)
	}
	
Progress messages contain a `progress` key with a progress being indicated with a number from 0 - 100.
`intermediateResult` contains an intermediate result with same structure/type as defined for result.

**Error Messages**:

	{
		'error':'error message string',
		'callID':1, (optional)
		'serviceName':'myService', (optional)
		'duration':650, (optional)
		'taskID':0 (optional)
	}
	
If the origin of an error can be associated with a service call, the error message contains `callID`, 
`serviceName`, `duration`, `taskID`. Otherwise it doesn't. Error messages are always of "string" type:
The `error` key cannot contain a JSONArray or a JSONObject.

### Key summary:
A Luci message must contain one of the 6 keys `run`, `cancel`, `result`, `progress`, `error`, `newCallID`. 
Luci validates all service calls and returns an error if a message is not valid.

###JSON types
Luci specifies 2 json structures that are associated with a type name: `attachment`, `jsongeometry` 

`attachment`:

	{
		'format':'jpg',
		'attachment':{
			'length':123456789,
			'checksum':'md5AsHexString',
			'position':1 (0 = unknown)
		},
		'OPT name':'string',
    	'ANY key':'any'
	} 
	
`jsongeometry`:

	{
		'format':'geojson',
		'geometry':{
			'type':'Point',
			'coordinates':[1,2,3]
		},
		'OPT name':'string',
    	'ANY key':'any'
	}
	
	
These types are used when defining the structure and/or type of inputs and outputs of a service. 
Please refer to `luci.core.validation`](https://bitbucket.org/treyerl/luci2/raw/master/core/validation/)
for more information.

###Attachments
Luci messages can contain binary data. It is attached to messages as byte arrays. While every message 
must contain a JSON "header", attachments are optional. And while the UTF8 encoded JSON header is 
human-readable, a complete luci message consists not only of a json string but also of a thin binary 
wrapper around the json header: 16 big endian bytes before the json header at the very beginning of 
every Luci message and 8 big endian bytes after the json header.

![binmsg.png](binmsg.png)

The first 8 bytes encode the byte length of the header, the second 8 bytes encode the length of the 
rest, the attachment part. The encoded number must not be lower than 8 since the attachment part 
itself starts with 8 bytes encoding the amount of attachments and those 8 bytes are added even if 
the message does not contain any attachments. The following attachment byte arrays are preceded by 
additional 8 bytes encoding the length of the attachment. Attachments can be referenced in the json 
header multiple times by an attachment json structure defined in the JSON Types section.

###Self-Recovery Procedure (PANIC Messages)
The 8 bytes big-endian numbers are control numbers that allow for binary validation checks. The length 
of the individual attachments + (number of attachments + 1) * 8 must be equal as the second 8-bytes
number preceding the JSON header. Also if the total number of attachment bytes is only 8 but the 
number of attachments encoded in the first 8 attachment bytes is larger than 0, binary validation fails
as well. If so, the party that notices a validation failure, sends a json message like 
`{'panic':'base64encoded 32 random bytes'}`. The bytes represented in the json header must be sent 
back by the receiver of a panic message byte by byte in decoded form. The sender keeps searching
for those 32 bytes on the socket until all 32 bytes have arrived. This serves as a synchronizing 
mechanism once something's got mixed up with binary attachments.
