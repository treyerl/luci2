package luci.service;

import static com.esotericsoftware.minlog.Log.trace;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.esotericsoftware.minlog.Log;

import luci.connect.Attachment;
import luci.connect.AttachmentAsFile;
import luci.connect.GUIRun;
import luci.connect.LcRemoteService;
import luci.connect.Message;

public class RemoteServiceControl extends LcRemoteService{
	private final static File dir = new File(".");
	private final static Properties config = new Properties();
	private final static File configFile = new File("start.conf");
	static {
		Log.set(Log.LEVEL_TRACE);
		try {
			if (!configFile.exists()) configFile.createNewFile();
			config.load(new FileInputStream(configFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public RemoteServiceControl(DefaultArgsProcessor as) {
		super(as);
	}
	
	@Override
	protected JSONObject specifyInputs() {
		return new JSONObject()
				.put("run", "ServiceControl")
				.put("XOR listServices", "boolean")
				.put("XOR start", new JSONObject("{'ANY serviceName':['number',{'ANY id':'string'}]}"))
				.put("XOR systemInfo","boolean")
				.put("XOR install", new JSONArray().put("attachment"))
				.put("XOR remove", new JSONArray().put("string"))
				.put("OPT silent", "boolean");
	}

	@Override
	protected JSONObject specifyOutputs() {
		return new JSONObject("{'XOR result':"
				+ "{'XOR started':{'ANY serviceName':'number'},"
				+ " 'XOR removed':['string'],"
				+ " 'XOR installedObjects':{'ANY serviceName':{'exec':'string','numRequestedCPUCores':'number','io':'json'}},"
				+ " 'XOR system':{"
				+ "			'CPU':{'numCores':'number','archNumBits':'number','endian':'string'},"
				+ "			'osname':'string',"
				+ "			'osversion':'string',"
				+ "			'name':'string'"
				+ "		},"
				+ " 'OPT errors':'list'"
				+ "},"
				+ "'XOR error':'string'}");
	}

	@Override
	protected JSONObject exampleCall() {
		return new JSONObject("{'run':'ServicControl','listFiles':'jar'}");
	}
	
	public int howToHandleAttachments() {
		return Attachment.FILE;
	}
	
	@Override
	protected ResponseHandler newResponseHandler() {
		return new RemoteServiceResponseHandler(){
			public void processResult(Message m) {
				System.out.println(m);
			}
			public JSONObject getInstalledObjects(){
				JSONObject installed = new JSONObject();
				for (Entry<Object, Object> en: config.entrySet()){
					String key = en.getKey().toString();
					String subkey = "exec";
					Object val = en.getValue();
					if (key.endsWith("_numCores")) {
						key = key.substring(0, key.length()-9);
						subkey = "numRequestedCPUCores";
					} else if (key.endsWith("_io")){
						key = key.substring(0, key.length()-3);
						subkey = "io";
						String sval = (String) val;
						if (sval.equals("unsupported")) val = JSONObject.NULL;
						else val = new JSONObject(sval);
					}
					JSONObject info = installed.optJSONObject(key);
					if (info == null) installed.put(key, (info = new JSONObject()));
					info.put(subkey, val);
				}
				for (String k: installed.keySet()){
					JSONObject info = installed.getJSONObject(k);
					if (!info.has("numRequestedCPUCores")) info.put("numRequestedCPUCores", 1);
				}
				return installed;
			}
			
			public Message implementation(Message m) {
				JSONObject h = m.getHeader();
				System.out.println(h);
				List<String> errors = new ArrayList<>();
				if (h.has("listServices")){
					
					return new Message(new JSONObject().put("result", new JSONObject()
							.put("installedObjects", getInstalledObjects())));
				} else if(h.has("systemInfo")){
					int num = Runtime.getRuntime().availableProcessors();
					JSONObject system = new JSONObject()
							.put("CPU", new JSONObject()
									.put("numCores", num)
									.put("archNumBits", System.getProperty("sun.arch.data.model"))
									.put("endian", System.getProperty("sun.cpu.endian")))
							.put("osname", System.getProperty("os.name"))
							.put("osversion", System.getProperty("os.version"));
							try {
								system.put("name", InetAddress.getLocalHost().getHostName());
							} catch (JSONException | UnknownHostException e) {
								system.put("name", "unknown hostname");
							}
					return new Message(new JSONObject().put("result", new JSONObject().put("system", system)));
					
				} else if(h.has("start")){
					InetSocketAddress isa = getIsa();
					String host = config.getProperty("host", isa.getAddress().getHostAddress());
					String port = config.getProperty("port", isa.getPort()+"");
					
					JSONObject start = m.getHeader().getJSONObject("start");
					boolean silent = m.getHeader().optBoolean("silent");
					
					try {
						JSONObject started = new JSONObject();
						for (String serviceName: start.keySet()){
							String execute_default = serviceName2Filename(serviceName)[0]+" "+host+" "+port;
							String execute = config.getProperty(serviceName, execute_default);
							String io_param = config.getProperty(serviceName+"_io", "unsupported");
							boolean io_unsupported = io_param.equals("unsupported");
							
							Object val = start.get(serviceName);
							int i = 0;
							
							if (val instanceof Number){
								int num = start.getInt(serviceName);
								for (; i < num; i++){
									execute(silent, execute);
								}
							} else if (val instanceof JSONObject){
								JSONObject args = start.getJSONObject(serviceName);
								for (String id: args.keySet()){
									String exec = io_unsupported ? execute : execute +" "+ id + " " + args.getString(id);
									execute(silent, exec);
									i++;
								}
							}
							
							started.put(serviceName, i);
						}
						return new Message(new JSONObject().put("result", new JSONObject().put("started", started)));
					} catch (IOException e){
						return new Message(new JSONObject().put("error", e.toString()));
					}
				} else if (h.has("install")){
					// TODO: test install / deployment of services to nodes
					System.out.println(h);
					// LcClient downloads attachments to the AttachmentAsFile.parentDirectory which is set in main()
					JSONArray install = h.getJSONArray("install");
					for(Object ob: install) {
						AttachmentAsFile att = (AttachmentAsFile)ob;
						File outputFolder = null;
						// unpack zipped attachments:
						if (att.getFormat().equalsIgnoreCase("zip")){
							try {
								outputFolder = new File(att.getName());
								if (outputFolder.exists()) empty(outputFolder); 
								outputFolder.mkdirs();
								unzip(att.getFile(), outputFolder);
							} catch (IOException e) {
								errors.add(e.toString());
								if (Log.DEBUG || Log.TRACE) e.printStackTrace();
								continue;
							}
						}
						String name = att.getString("name");
						String exec = att.getString("exec");
						List<String> io = null;
						List<String> unsupportedIO = Arrays.asList(new String[]{"unsupported"});
						try {;
							io = GUIRun.getOutput(exec+" -io", 1, TimeUnit.SECONDS, unsupportedIO);
							if (io != unsupportedIO) new JSONObject(io);
						} catch (JSONException e){
							io = unsupportedIO;
						} catch (IOException e){
							try {
								Process p = Runtime.getRuntime().exec(exec);
								// apparently exec works, but accepts no -io
								p.destroyForcibly();
								io = unsupportedIO;
							} catch (IOException e1){
								try {
									io = GUIRun.getPATH();
									att.getFile().delete();
									if (outputFolder != null) {
										empty(outputFolder);
										outputFolder.delete();
									}
									errors.add(e.toString()+" - available environment PATH: "+io.get(0));
									if (Log.DEBUG || Log.TRACE) e.printStackTrace(); 
								} catch (IOException e2) {
									e2.printStackTrace();
								}
								continue;
							}
						}
						
						config.put(name, exec);
						config.put(name+"_numCores", att.getInt("numRequestedCPUCores")+"");
						config.put(name+"_io", io.get(0));
						try {
							config.store(new FileOutputStream(configFile), "config file for service control");
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
//						System.out.println(m);
					}
					if (errors.size() == 0) errors = null;
					return new Message(new JSONObject().put("result", new JSONObject()
							.put("installedObjects", getInstalledObjects())
							.putOpt("errors", errors)));
				} else if (h.has("remove")){
					JSONArray serviceNames = h.getJSONArray("remove");
					for (Object o: serviceNames){
						String s = (String) o;
						config.remove(s);
						config.remove(s+"_numCores");
						config.remove(s+"_io");
						String[] files = serviceName2Filename(s);
						for (String f: files){
							new File(f).delete();
						}
					}
					try {
						config.store(new FileOutputStream(configFile), "?");
					} catch (IOException e) {
						e.printStackTrace();
					}
					return new Message(new JSONObject().put("result", new JSONObject().put("removed", serviceNames)));
				}
				return new Message(new JSONObject().put("error", "no valid key ['listServices', 'systemInfo', 'start', 'install', 'remove'] found"));
			}
		};
	}
	
	private void execute(boolean silent, String execute) throws IOException{
		Runtime rt = Runtime.getRuntime();
		if (silent) rt.exec(execute);
		else{
			try { 
				GUIRun.guiRun(execute);
			} catch (IOException e){
				rt.exec(execute);
			}
		}
	}
	
	private String[] serviceName2Filename(String serviceName){
		return new File(".").list((f,s)->{
			int i = s.lastIndexOf(".");
			return s.substring(0,(i < 0) ? s.length() : i).equals(serviceName);
			});
	}
	
	@Override
	protected boolean processIOException(SelectionKey key, IOException e) {
		super.processIOException(key, e);
		if (!(e instanceof EOFException)) System.err.println("Exit: "+e.toString());
		return true;
	}
	
	public static void main( String[] args ){
    	Log.set(Log.LEVEL_DEBUG);
    	DefaultArgsProcessor asp = new DefaultArgsProcessor(args);
    	trace(asp.toString());
    	AttachmentAsFile.parentDirectory = dir;
		System.out.println("registering at "+asp.getHostname()+":"+asp.getPort());
		RemoteServiceControl sc = new RemoteServiceControl(asp);
		new Thread(sc).start();
		sc.connect(asp.getHostname(), asp.getPort());
    }
	
	public static void unzip(File zipFile, File outputFolder) throws IOException{
		byte[] buffer = new byte[1024*16];

    	ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
    	ZipEntry ze = zis.getNextEntry();
    		
    	while(ze!=null){
    		if (ze.isDirectory()) {
    			ze = zis.getNextEntry();
    			continue;
    		}
			String fileName = ze.getName();
			File newFile = new File(outputFolder + File.separator + fileName);
			new File(newFile.getParent()).mkdirs();
			      
			FileOutputStream fos = new FileOutputStream(newFile);             
			
			int len;
			while ((len = zis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
					
			fos.close();   
			ze = zis.getNextEntry();
    	}
    	
        zis.closeEntry();
    	zis.close();
	}
	
	public static void empty(File folder){
		for (File f: folder.listFiles()){
			if (f.isDirectory()) empty(f);
			else f.delete();
		}
	}

	@Override
	public String getDescription() {
		return "This discrption will never be seen by clients since ServiceControl is a balanced Service.";
	}
	
}
