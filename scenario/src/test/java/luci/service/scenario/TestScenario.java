/** MIT License
 * @author Lukas Treyer
 * @date Aug 8, 2016
 * */
package luci.service.scenario;

import static luci.console.TestConsole.setExampleInputAndValidateOutput;
import static luci.console.TestConsole.validateOutput;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import luci.connect.Message;
import luci.console.TestConsole;
import luci.console.TestConsole.BlockingCallHandler;
import luci.core.Luci;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestScenario {
	JSONObject geometry = new JSONObject("{'features':[{'geometry':{'coordinates':"
			+ "[[[99,0,0,5],[101,0,0,5],[101,1,0,5],[100,0,0,5],[99,0,0,5]]],'type':'Polygon'},"
			+ "'type':'Feature','properties':{'geomID':1,'uid':0,'layer':'default',"
			+ "'timestamp':1453215516}}],'type':'FeatureCollection'}");
	JSONObject otherPolygonGeometry = new JSONObject("{'coordinates':"
			+ "[[[88,0,1],[118,0,1],[118,1,1],[80,0,1],[50,0,1],[88,0,1]]],'type':'Polygon'}}");
	JSONObject yetAnotherPolygonGeometry = new JSONObject("{'coordinates':"
			+ "[[[77,0,1,0],[177,0,1,0],[177,1,1,0],[45,0,1,0],[50,0,1,0]]],'type':'Polygon'}}");
	
	Delete del = new Delete();
	JSONObject delJ = new JSONObject().put("run", "scenario.Delete");
	luci.service.scenario.geojson.Update u = new luci.service.scenario.geojson.Update();
	luci.service.scenario.geojson.Get g = new luci.service.scenario.geojson.Get();
	
	static int[] scid = new int[2];
	static Luci luci;
	static TestConsole console;
	static BlockingCallHandler stdHandler;
	
	@BeforeClass
	public static void startupLuci(){
		luci = Luci.start(new String[]{"-noBrowser","-cwd", "../core/target"});
		try {
			console = new TestConsole();
			console.connect("localhost","7654");
			console.awaitConnected();
			stdHandler = console.getCurrentHandler();
		} catch (IOException | InterruptedException e) {}
	}
	
	@Test
	public void t1CreateEmpty() throws Exception{
		scid[0] = setExampleInputAndValidateOutput(new Create())
				.getJSONObject("result").getInt("ScID");
	}
	
	@Test
	public void t2Create() throws Exception {
		luci.service.scenario.geojson.Create c = new luci.service.scenario.geojson.Create();
		c.setInput(new Message(new JSONObject()
				.put("run", "scenario.geojson.Create")
				.put("name", "testX")
				.put("geometry_input", new JSONObject()
						.put("format", "geojson")
						.put("geometry", geometry))));
		scid[1] = validateOutput(c).getJSONObject("result").getInt("ScID");
	}
	
	@Test
	public void t3Get() throws Exception{
		g.setInput(new Message(new JSONObject().put("ScID", scid[1]).put("run", "scenario.geojson.Get")));
		JSONObject geom = validateOutput(g).getJSONObject("result")
				.getJSONObject("geometry_output").getJSONObject("geometry");
		assertFalse(geometry.similar(geom));
	}
	
	@Test
	public void t4Update() throws Exception {
		JSONObject geometry = new JSONObject(this.geometry.toString());
		JSONObject p = ((JSONObject) geometry.getJSONArray("features").get(0)).getJSONObject("properties");
		p.put("geomID", 2).remove("timestamp");
		u.setInput(new Message(new JSONObject()
				.put("run", "scenario.geojson.Update")
				.put("ScID", scid[1])
				.put("geometry_input", new JSONObject()
						.put("format", "geojson")
						.put("geometry", geometry)
						)
				));
		validateOutput(u);
	}
	
	@Test
	public void t5Subscribe() throws Exception {
		console.run("scenario.SubscribeTo", "ScIDs=["+scid[1]+"]", "format=geojson");
		Message m  = console.getCurrentHandler().await();
		new luci.service.scenario.SubscribeTo().getOutputDescription().validate(m.getHeader());
	}
	
	@Test
	public void t6Update() throws Exception {
//		stdHandler.reset();
		JSONObject geometry = new JSONObject(this.geometry.toString());
		JSONObject j = (JSONObject) geometry.getJSONArray("features").get(0);
		JSONObject p = ((JSONObject) geometry.getJSONArray("features").get(0)).getJSONObject("properties");
		p.put("geomID", 2).remove("timestamp");
		j.put("geometry", otherPolygonGeometry);
		j.getJSONObject("properties").remove("timestamp");
		u.setInput(new Message(new JSONObject()
				.put("run", "scenario.geojson.Update")
				.put("ScID", scid[1])
				.put("geometry_input", new JSONObject()
						.put("format", "geojson")
						.put("geometry", geometry)
						)
				));
		validateOutput(u);
		Message m = stdHandler.await();
		JSONObject otherPolygonGeometryReturned = m.getHeader().getJSONObject("result")
				.getJSONObject("geometry_output").getJSONObject("geometry").getJSONArray("features")
				.getJSONObject(0).getJSONObject("geometry");
		u.getOutputDescription().validate(m.getHeader());
		assertTrue(otherPolygonGeometryReturned.similar(otherPolygonGeometry));
	}
	
	@Test
	public void t7Unsubscribe() throws Exception {
		console.run("scenario.UnsubscribeFrom", "ScIDs=["+scid[1]+"]");
		Message m  = console.getCurrentHandler().await();
		new luci.service.scenario.UnsubscribeFrom().getOutputDescription().validate(m.getHeader());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void t8Update() throws Exception {
		Thread.sleep(3);
		JSONObject geometry = new JSONObject(this.geometry.toString());
		JSONObject j = (JSONObject) geometry.getJSONArray("features").get(0);
		j.put("geometry", yetAnotherPolygonGeometry);
		// provoke an exception being thrown with illegal timestamp property (=not removing it here)
//		j.getJSONObject("properties").remove("timestamp");
		u.setInput(new Message(new JSONObject()
				.put("run", "scenario.geojson.Update")
				.put("ScID", scid[1])
				.put("geometry_input", new JSONObject()
						.put("format", "geojson")
						.put("geometry", geometry)
						)
				));
		validateOutput(u);
	}
	
	@Test
	public void t9GetList() throws Exception {
		setExampleInputAndValidateOutput(new GetList());
	}
	
	@Test
	public void t10drop_created() throws Exception {
		del.setInput(new Message(delJ.put("ScIDs", new JSONArray(scid))));
		validateOutput(del);
	}
	
	@Test(expected=luci.core.validation.LcValidationException.class)
	public void t11dropAll() throws Exception {
		del.getInputDescription().validate(delJ.put("ScIDs", "something"));
	}
	
	@Test
	public void t12dropAll() throws Exception {
		del.setInput(new Message(delJ.put("ScIDs", "all")));
		validateOutput(del);
	}
}
