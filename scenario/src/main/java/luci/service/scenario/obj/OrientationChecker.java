package luci.service.scenario.obj;

import java.util.ArrayList;
import java.util.List;

public class OrientationChecker {
    private boolean XYPLANE = false;
    private boolean XZPLANE = false;
    private boolean YZPLANE = false;


    public void checkPlane(List<List<Double>> vertices) {
        XYPLANE = false;
        XZPLANE = false;
        YZPLANE = false;

        for (int i = 0; i < 3; ++i) {
            double current_value = vertices.get(0).get(i);
            double first_value = current_value;
            for (int j = 1; j < vertices.size(); ++j) {
                current_value = vertices.get(j).get(i);
                if (current_value != first_value) {
                    break;
                }
                if (j == vertices.size() - 1) {
                    if (i == 0) YZPLANE = true;
                    if (i == 1) XZPLANE = true;
                    if (i == 2) XYPLANE = true;
                }
            }
        }
    }

    public double checkOrientation(List<List<Double>> vertices) {
        int x = 0;
        int y = 1;
        if (XYPLANE) {
            x = 0;
            y = 1;
        }
        else if (XZPLANE) {
            x = 0;
            y = 2;
        }
        else if (YZPLANE) {
            x = 1;
            y = 2;
        }

        double result = 0;
        for (int i = 1; i < vertices.size(); ++i) {
            double add = (vertices.get(i).get(x) - vertices.get(i-1).get(x))
                    * (vertices.get(i).get(y) + vertices.get(i-1).get(y));
            result += add;
        }

        return result;
    }

    public void inverseOrientation(List<List<Double>> vertices) {
        List<List<Double>> vertices_tmp = new ArrayList<>();
        for (int i = 0; i < vertices.size(); ++i) {
            vertices_tmp.add(vertices.get(vertices.size()-1-i));
        }

        for (int i = 0; i < vertices.size(); ++i) {
            vertices.set(i, vertices_tmp.get(i));
        }
    }

    public List<List<Double>> convert2d(List<List<Double>> vertices) {
        int x = 0;
        int y = 1;
        if (XYPLANE) {
            x = 0;
            y = 1;
        }
        else if (XZPLANE) {
            x = 0;
            y = 2;
        }
        else if (YZPLANE) {
            x = 1;
            y = 2;
        }

        List<List<Double>> result = new ArrayList<>();

        for (int i = 0; i < vertices.size(); ++i) {
            result.add(new ArrayList<>());
            result.get(result.size()-1).add(vertices.get(i).get(x));
            result.get(result.size()-1).add(vertices.get(i).get(y));
        }

        return result;
    }
}
