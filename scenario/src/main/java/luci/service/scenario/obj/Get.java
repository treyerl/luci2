package luci.service.scenario.obj;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.sql.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;

import luci.service.scenario.obj.libgdx.EarClippingTriangulator;
import luci.service.scenario.obj.libgdx.FloatArray;
import luci.service.scenario.obj.libgdx.ShortArray;
import org.h2.mvstore.DataUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Attachment;
import luci.connect.AttachmentAsArray;
import luci.connect.Message;
import luci.core.validation.JsonType;
import luci.service.scenario.GeometrySelector;
import luci.service.scenario.Modifier;
import luci.service.scenario.Scenario;
import luci.service.scenario.ServiceLocalSQL.ServiceLocalSQLGet;
import luci.service.scenario.database.GeometryObjectWriter;
import luci.service.scenario.database.GeometryObjectWriter.GeometryIterator;
import luci.service.scenario.database.TAB;

public class Get extends ServiceLocalSQLGet {
    Modifier modifier = new Modifier();

    @Override
    public JsonType getInputDescription() {
    	return new JsonType(GeometrySelector.createInputDescription(getName()));
    }

    @Override
    public JsonType getOutputDescription() {
        return wrapResultDescription("{'ScID':'number', 'geometry':'attachment', 'OPT deletedIDs':'list'}");
    }

    @Override
    protected JsonType getScenarioServiceOutputDescription() {
        return wrapResultDescription("{'geometry_output':'objgeometry/obj','ScID':'number',"
                + "'lastmodified':'number'}");
    }

    @Override
    public JSONObject getExampleCall() {
        return new JSONObject("{'run':'" + getName() + "', 'ScID':3}");
    }

    protected Message sqlImplementation(Connection con) throws Exception {
        JSONObject h = input.getHeader();
        Scenario scn = new Scenario(h.getInt("ScID"));

        GeometrySelector selector = new GeometrySelector(con);
        selector.setScenario(scn);
        selector.setBatchID(h.optInt("batchID"));
        selector.setLayer(h.optString("layer", null));
        selector.setGeomIDs(h.optJSONArray("geomIDs"));
        selector.setTimeRangeSelection(h.optJSONObject("timerange"));
        selector.setInclHistory(h.optBoolean("inclHistory", false));
        selector.setAttributes(h.optJSONObject("attributes"));
        selector.setOrderBy(h.optJSONArray("orderBy"));
        selector.setOffset(h.optInt("offset"));
        selector.setLimit(h.optInt("limit"));

        // if the scenario gets altered / a new attribute is being added between here and select(con)
        // the new attributes will not get exported; this call makes the selection on the attributes
        LinkedHashMap<String, Short> types = new LinkedHashMap<>();
        JSONArray features = new JSONArray();

        GeometryObjectWriter dbGOW = new GeometryObjectWriter();

        ResultSet rs = selector.select(types);
        Set<Map.Entry<String, Short>> entries = new LinkedHashSet<>();
        Iterator<Map.Entry<String, Short>> it = types.entrySet().iterator();
        int entries_i = 0;
        while(entries_i++ < 9) it.next();
        while(it.hasNext()) entries.add(it.next());

        List<List<Double>> vertices = new ArrayList<>();
        HashMap<Integer, ArrayList<ArrayList<Integer>>> faces = new HashMap<>();

        long maxTimestamp = Long.MIN_VALUE;
        while(rs.next()) {
            //implement OBJ exporter
//            System.out.println(rs.getObject("geom"));

            Object geom = rs.getObject("geom");
            int geomID = rs.getInt("geomID");
            
            if (geom == null) continue;
            
            GeometryIterator gi = dbGOW.fromObject(geom, modifier);
            int type = gi.getType();

            if (!(type == TAB.TRIANGLE || type == TAB.TIN || type == TAB.POLYHEDRALSURFACE)) {
                Iterator<?> it_geom = gi.iterator();
                resetGroup(faces, geomID);
                switch (type) {
                    case TAB.MULTIPOLYGON:
                        while(it_geom.hasNext()) {
                            @SuppressWarnings("unchecked")
							Iterator<Iterator<Iterator<Double>>> it_polygon =
                                    (Iterator<Iterator<Iterator<Double>>>)it_geom.next();
                            polygonTriangulation(it_polygon, vertices, faces, geomID);
                        }
                        break;
                    case TAB.POLYGON:
                        polygonTriangulation(it_geom, vertices, faces, geomID);
                        break;
                }
            }
        }
        
        ArrayList<Integer> geomIDs_deleted = new ArrayList<>();
        ResultSet rs_deleted = selector.selectDeleted();
        while (rs_deleted.next()) {
//            System.out.println(rs_deleted.getInt("geomID"));
            geomIDs_deleted.add(rs_deleted.getInt("geomID"));
        }

        AttachmentAsArray attachment = createAttachment(vertices, faces, scn.getName());
        attachment.convertTo(Attachment.NETWORK);

        Message resultMessage = new Message(new JSONObject().put("result", new JSONObject()
        		.put("ScID", scn.getScID())
        		.put("geometry", attachment)
                .put("deletedIDs", geomIDs_deleted)));
//        System.out.println(resultMessage);
        return resultMessage;
//                new JSONObject()
//                        .put("attachment", new JSONObject()
//                                .put("checksum", attachment.getChecksum()))
//                        .put("ScID", ScID)
//                        .put("format", "obj"))
//                .put("result", attachment));
    }

    private void polygonTriangulation(Iterator it_polygon, List<List<Double>> vertices, HashMap<Integer, ArrayList<ArrayList<Integer>>> faces, int geomID) {

        boolean external_ring = true;

        OrientationChecker oc = new OrientationChecker();
        EarClippingTriangulator ect = new EarClippingTriangulator();

        FloatArray vertices_earclip_arr_2d = new FloatArray();
        List<List<Double>> polygon_ordered = new ArrayList<>();

        while(it_polygon.hasNext()) {
            Iterator<Iterator<Double>> it_linestring = (Iterator<Iterator<Double>>) it_polygon.next();
            List<List<Double>> linestring = new ArrayList<>();
            while (it_linestring.hasNext()) {
                linestring.add(new ArrayList<>());
                Iterator<Double> it_points = it_linestring.next();
                while (it_points.hasNext()) {
                    linestring.get(linestring.size() - 1).add(it_points.next());
                }
            }

            oc.checkPlane(linestring);
            double orientation = oc.checkOrientation(linestring);
            if (external_ring && (orientation < 0)) {
                oc.inverseOrientation(linestring);
            } else if (!external_ring && (orientation > 0)) {
                oc.inverseOrientation(linestring);
            }

            for (int i = 0; i < linestring.size(); i++) {
                polygon_ordered.add(linestring.get(i));
            }

            List<List<Double>> linestring2d = oc.convert2d(linestring);

            for (int i = 0; i < linestring2d.size(); ++i) {
                for (int j = 0; j < linestring2d.get(i).size(); ++j) {
                    double value_double = linestring2d.get(i).get(j);
//                                    System.out.println(i + " " + j + " " + linestring2d.get(i).get(j));
                    vertices_earclip_arr_2d.add((float) value_double);
                }
            }

            external_ring = false;
        }

        ShortArray indices = ect.computeTriangles(vertices_earclip_arr_2d);
//                            System.out.println(indices.toString());
//                            System.out.println(linestring2d);

        ArrayList<ArrayList<Integer>> group = faces.get(geomID);

        for(int i = 0; i < indices.size; i+=3) {
            group.add(new ArrayList<>());

            vertices.add(polygon_ordered.get(indices.get(i)));
            group.get(group.size()-1).add(vertices.size());

            vertices.add(polygon_ordered.get(indices.get(i+1)));
            group.get(group.size()-1).add(vertices.size());

            vertices.add(polygon_ordered.get(indices.get(i+2)));
            group.get(group.size()-1).add(vertices.size());
        }
    }

    private void resetGroup(HashMap<Integer, ArrayList<ArrayList<Integer>>> faces, int geomID) {
        faces.put(geomID, new ArrayList<>());
    }

    private AttachmentAsArray createAttachment(List<List<Double>> vertices, HashMap<Integer, ArrayList<ArrayList<Integer>>> faces, String name) {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();

        try {
            for(List<Double> vertex: vertices) {
                byteStream.write("v ".getBytes());

                for(double pos: vertex) {
                    byteStream.write(String.valueOf(pos).getBytes());
                    byteStream.write(" ".getBytes());
                }
                //byteStream.write(String.valueOf(1.0).getBytes());
                byteStream.write("\n".getBytes());
            }

            Iterator it = faces.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry group = (Map.Entry)it.next();
                byteStream.write("g ".getBytes());
                byteStream.write(String.valueOf(group.getKey()).getBytes());
                byteStream.write("\n".getBytes());

                for(List<Integer> face: (List<List<Integer>>)group.getValue()) {
                    byteStream.write("f ".getBytes());
                    inverseFace(face);
                    for(int index: face) {
                        byteStream.write(String.valueOf(index).getBytes());
                        byteStream.write(" ".getBytes());
                    }
                    byteStream.write("\n".getBytes());
                }
                it.remove();
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }

//        Charset charset = Charset.forName("UTF-8");
//        CharsetEncoder encoder = charset.newEncoder();
//        encoder.reset();
//        try{
//            byteBuffer = encoder.encode(CharBuffer.wrap(string));
//            encoder.flush(byteBuffer);
//        }catch(Exception e){e.printStackTrace();}

        //just for debugging purposes
        try {
            PrintWriter out = new PrintWriter("testfile.obj");
            out.println(byteStream.toString());
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ByteBuffer byteBuffer = ByteBuffer.wrap(byteStream.toByteArray());

        return new AttachmentAsArray("obj", name, byteBuffer);
    }
    
    private void inverseFace(List<Integer> face) {
    	List<Integer> face_tmp = new ArrayList<>();
        for (int i = 0; i < face.size(); ++i) {
        	face_tmp.add(face.get(face.size()-1-i));
        }

        for (int i = 0; i < face.size(); ++i) {
        	face.set(i, face_tmp.get(i));
        }
    }

	/* (non-Javadoc)
	 * @see luci.core.Service#getGeneralDescription()
	 */
	@Override
	public String getGeneralDescription() {
		return "Exports Luci geometry to obj.";
	}
}





