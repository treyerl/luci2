/** MIT License
 * @author Lukas Treyer
 * @date Dec 16, 2015
 * */
package luci.service.scenario;

import java.sql.Connection;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.validation.JsonType;
import luci.service.scenario.Scenario;

public class Create extends ServiceLocalSQL{

	@Override
	protected Message sqlImplementation(Connection con) throws Exception {
		return wrapResult(new Scenario(input.getHeader()).create(con));
	}

	@Override
	public String getGeneralDescription() {
		return "Creates a scenario with no geometry input being necessary as an input.";
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription(Scenario.jsonTypeCreateDescription);
	}
	
	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription(Scenario.jsonTypeOutputDescription);
	}

	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{'name':'aScenarioName'}");
	}

}


