package luci.service.scenario.geojson;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

import luci.service.scenario.Projection;
import org.json.JSONObject;

import luci.connect.JSONFormat;
import luci.connect.Message;
import luci.core.Luci;
import luci.core.validation.JsonType;
import luci.service.scenario.Modifier;
import luci.service.scenario.Scenario;
import luci.service.scenario.ServiceLocalSQL.ServiceLocalSQLUpdate;

public class Update extends ServiceLocalSQLUpdate{
	
	@Override
	public String getGeneralDescription() {
		return "Updates geometry. Geometry having no geomID will be added, as it is presumed "
				+ "that this is new geometry. Updating existing geometry is only possible "
				+ "by providing a 'geomID' or an attributeMap that maps the geometry id having "
				+ "any other name to 'geomID'. "+Create.attachmentAttributesDescription+"\n"
				+ Create.inputParameters;
	}
	
	@Override
	public JsonType getInputDescription() {
		return new JsonType(new JSONObject("{"
				+ "'run':'"+getName()+"',"
				+ "'ScID':'number',"
				+ "'geometry_input':['anyOf',['jsongeometry','geojson'], ['attachment','geojson']],"
				+ "'OPT allowTimestampOverride':'boolean'"
				+ "}"), Modifier.standardDescription());
	}

	@Override
	public JsonType getScenarioServiceOutputDescription() {
		return wrapResultDescription("{'ScID':'number','lastmodified':'number','OPT newIDs':'list'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','ScID':2,'geometry_input':{'format':'geojson',"
				+ "'geometry':{'type':'Point','coordinates':[1,2,3]}}}");
	}
	
	@Override
	public Message sqlImplementation(Connection con) throws Exception {
		JSONObject h = input.getHeader();
		Modifier modifier = Modifier.standard(new Projection(), 
				h.optBoolean("applyTransformation"), 
				h.optBoolean("bakeNurbs"), 
				h.optInt("dim", 3), 
				h.optBoolean("switchLatLon"));
		
		Scenario s = new Scenario(h.getInt("ScID"));
		Upsert up = new Upsert(modifier, false);
		up.allowsTimestampOverride(h.optBoolean("allowTimestampOverride", false));
		up.setGeometryInput((JSONFormat) h.get("geometry_input"));
		
		JSONObject result = new JSONObject();
		if (up.getGeometry().length() > 0){
			Timestamp st = new Timestamp(Luci.newTimestamp());
			List<Long> newIDs = up.setTimestamp(st)
								  .setScenario(s)
								  .execute(con);
			result.put("result", s.setLastModified(st).put("newIDs", newIDs));
		} else {
			result.put("result", s.loadFromRecords(con).setLastModified(con));
		}
		
		return new Message(result);
	}
}
