/** MIT License
 * @author Lukas Treyer
 * @date Dec 16, 2015
 * */
package luci.service.scenario.geojson;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.JSONFormat;
import luci.connect.Message;
import luci.core.validation.JsonType;
import luci.service.scenario.Modifier;
import luci.service.scenario.Scenario;
import luci.service.scenario.ServiceLocalSQL.ServiceLocalSQLUpdate;;

public class Create extends ServiceLocalSQLUpdate {
	public final static String attachmentAttributesDescription = "The attachment or jsongeometry "
			+ "may contain an property 'attributeMap' that maps attributes from the geometry to "
			+ "Luci's special scenario attributes 'geomID','layer','batchID','nurbsDegree',"
			+ "'transform','timestamp','uid','knots'. For instance if the geometry contains an "
			+ "attribute 'id' that Luci should import as the 'geomID', add an attribute to the "
			+ "attachment/jsongeometry as follows: attributeMap:{'geomID':'id'}. ";
//			+ "Additionally 'Create' and 'Update' services are able to deal with a 'crs' property "
//			+ "formatted like 'EPSG:4326'";
	
	public final static String inputParameters = ""
			+ "@input.name not unique name of the scenario\n"
			+ "@input.geometry_input jsongeometry = geojson can be included directly into the header; "
			+ "attachment = geojson geometry can be attached as a file; this might increase Luci's speed "
			+ "for it does not have to scan through huge headers containing geojson just to find out "
			+ "which service to call.";
	@Override
	public String getGeneralDescription() {
		return "Create a scenario using geojson geometry for initialization."+attachmentAttributesDescription+"\n"
				+ inputParameters;
	}
	
	@Override
	public JsonType getInputDescription() {
		JSONObject scenarioCreate = new JSONObject(Scenario.jsonTypeCreateDescription);
		return new JsonType(scenarioCreate, Modifier.standardDescription(), new JSONObject()
				.put("geometry_input", new JSONArray().put("anyOf").put("attachment").put("jsongeometry"))
				.put("run", getName()));
	}

	@Override
	public JsonType getScenarioServiceOutputDescription() {
		return wrapResultDescription("{'ScID':'number','lastmodified':'number','OPT newIDs':'list'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','name':'aScenarioName','geometry_input':"
				+ "{'format':'geojson','geometry':{'type':'Point','coordinates':[1,2,3]}}}");
	}

	@Override
	public Message sqlImplementation(Connection con) throws Exception {
		JSONObject h = input.getHeader();
		Scenario s = new Scenario(h);
		JSONFormat geometry_input = (JSONFormat) h.get("geometry_input");
		Modifier m = new Modifier(s, geometry_input, h);
		s.create(con);
		Upsert up = new Upsert(m, true);
		Timestamp st = new Timestamp(luci.core.Luci.newTimestamp());
		List<Long> newIDs = up.setGeometryInput(geometry_input)
		  .setTimestamp(st)
		  .setScenario(s)
		  .execute(con);
		return new Message(new JSONObject().put("result", s.setLastModified(st).put("newIDs", newIDs)));
	}

	
}