package luci.service.scenario.geojson;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.math3.linear.BlockRealMatrix;
import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.AttachmentAsArray;
import luci.connect.AttachmentAsFile;
import luci.connect.JSONFormat;
import luci.connect.JSONGeometry;
import luci.connect.LcString;
import luci.connect.Message;
import luci.core.validation.JsonType;
import luci.service.scenario.GeometrySelector;
import luci.service.scenario.Modifier;
import luci.service.scenario.Projection;
import luci.service.scenario.Scenario;
import luci.service.scenario.ServiceLocalSQL.ServiceLocalSQLGet;
import luci.service.scenario.TimeRangeSelection;
import luci.service.scenario.database.GeometryObjectWriter;
import luci.service.scenario.database.GeometryObjectWriter.GeometryIterator;
import luci.service.scenario.database.TAB;

public class Get extends ServiceLocalSQLGet{
	Modifier modifier;
	static final int HEADER = 1;
	static final int FILE = 2;
	
	@Override
	public String getGeneralDescription() {
		return "Retrieve the scenario geometry in geojson format.\n"
				+ GeometrySelector.inputParameterDescription;
	}
	
	@Override
	public JsonType getInputDescription() {
		JsonType jsonType = new JsonType(
				GeometrySelector.createInputDescription(getName())
					.put("OPT crs", "string")
					.put("OPT asAttachment", "boolean")
					.put("OPT attachmentIndentation", "number"), 
				Modifier.standardDescription());
		return jsonType;
	}

	@Override
	public JsonType getScenarioServiceOutputDescription() {
		return wrapResultDescription("{'geometry_output':['anyOf', ['jsongeometry','geojson'], ['attachment','geojson']],"
				+ "'ScID':'number','lastmodified':'number'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"', 'ScID':2}");
	}
	
	public Message sqlImplementation(Connection con) throws Exception{
		JSONObject h = input.getHeader();		
		Scenario scn = new Scenario(h.getInt("ScID")).loadFromRecords(con);
		String crs = h.optString("crs", null);
		Projection p;
		if (scn.hasCrs()){
			if (crs != null && !scn.getCrs().equals(crs)) p = new Projection(scn.getCrs(), crs);
			else p = new Projection(scn.getCrs());
		} else if (crs != null){
			throw new IllegalStateException("Cannot project coordinates into '"+crs+"'; scenario has no crs!");
		} else p = new Projection();
		modifier = Modifier.standard(p,
				h.optBoolean("applyTransformation"),
				h.optBoolean("bakeNurbs"),
				h.optInt("dim", 3),
				h.optBoolean("switchLatLon"));

		GeometrySelector selector = new GeometrySelector(con);
		selector.setScenario(scn);
		selector.setBatchID(h.optInt("batchID"));
		selector.setLayer(h.optString("layer", null));
		selector.setGeomIDs(h.optJSONArray("geomIDs"));
		selector.setTimeRangeSelection(h.optJSONObject("timerange"));
		selector.setInclHistory(h.optBoolean("inclHistory", false));
		selector.setAttributes(h.optJSONObject("attributes"));
		selector.setOrderBy(h.optJSONArray("orderBy"));
		selector.setOffset(h.optInt("offset"));
		selector.setLimit(h.optInt("limit"));
		
		// if the scenario gets altered / a new attribute is being added between here and select(con)
		// the new attributes will not get exported; this call makes the selection on the attributes
		LinkedHashMap<String, Short> types = new LinkedHashMap<String, Short>();
		JSONArray features = new JSONArray();
		
		ResultSet rs = selector.select(types);
		Set<Entry<String, Short>> entries = new LinkedHashSet<Entry<String, Short>>();
		Iterator<Entry<String, Short>> it = types.entrySet().iterator();
		int i = 0;
		while(i++ < 9) it.next();
		while(it.hasNext()) entries.add(it.next());
//		System.out.println("types: "+types);
//		System.out.println("entries: "+entries);
		
		long maxTimestamp = Long.MIN_VALUE;
		while(rs.next()){
			// fix properties
			Object geom = rs.getObject("geom");
			JSONObject properties = new JSONObject();
			long t = rs.getTimestamp(2).getTime();
			if (t > maxTimestamp) maxTimestamp = t;
			properties.put("geomID", rs.getLong(1));
			properties.put("timestamp", Scenario.millis2seconds(t));
			int batchID = rs.getInt(3);
			if (batchID > 0) properties.put("batchID", batchID);
			properties.put("layer", rs.getString(4));
			
			// nurb
			int nurb = rs.getInt(5);
			if (nurb != 0) {
				properties.put("nurb", nurb);
				if (!modifier.isBakingCurves) geom = rs.getObject("NURBSGEOM");
			}
			
			// transformation
			double[][] trans = new double[4][];
			Double[][] transformation = TAB.getTransformation(rs.getArray(6), trans);
			modifier.removeTransformationMatrix();
			if (transformation != null){
				if (!modifier.isApplyingTransformation || geom == null)
					properties.put("transform", transformation);
				else modifier.setTransformation(new BlockRealMatrix(trans));
			}
			
			properties.put("uid", rs.getLong(7));
			
			// feature & geometry
			JSONObject feature = new JSONObject();
			feature.put("type", "Feature");
			feature.put("geometry", geometryFromDb(geom));
//			System.out.println(types);
			
			// scenario specific properties
			int k = 10;
			for (Entry<String, Short> e: entries){
				String keyname = e.getKey();
				// thank you H2!...
				if (LcString.isAllUpperCase(keyname)) keyname = keyname.toLowerCase();
				short type = e.getValue();
				String s = null;
//				System.out.println(keyname+", "+TAB.TYPE_NAMES.get(type)+": "+rs.getObject(k));
				switch (type){
				case TAB.NUMERIC: properties.putOpt(keyname, rs.getBigDecimal(k++)); break;
				case TAB.BOOLEAN: properties.putOpt(keyname, rs.getBoolean(k++)); break;
				case TAB.STRING: properties.putOpt(keyname, rs.getString(k++)); break;
				case TAB.JSON: 
					s = rs.getString(k++);
					if (s != null) properties.put(keyname, new JSONObject(s));
					break;
				case TAB.LIST: 
					s = rs.getString(k++);
					if (s != null) properties.put(keyname, new JSONArray(s)); 
					break;
				case TAB.NUMERICARRAY: 
					Array a = rs.getArray(k++);
					if (a != null) properties.put(keyname, new JSONArray((Number[])a.getArray()));
				case TAB.CHECKSUM: 
					Array pointer = rs.getArray(k++);
					if (pointer != null){
						TAB.Checksum chck = new TAB.Checksum(pointer);
						String filename = chck.toString();
						File f = new File(AttachmentAsFile.parentDirectory, filename);
						if (f.exists()){
							properties.put(keyname, AttachmentAsFile.withChecksum(f, chck.md5));
						}
					}
					break;
				}
			}
			feature.put("properties", properties);
			features.put(feature);
		}
		selector.setMaxTimestamp(maxTimestamp);
		
		// deleted geomIDs
		TimeRangeSelection trs = selector.getTimeRangeSelection();
		
		if (trs.isBetween() || trs.isAfter() || trs.isFrom()){
			String sql = "SELECT geomID FROM "+scn.historyTableName()
						+" WHERE timestamp_deleted IS NOT NULL "+trs.getSQL();
			PreparedStatement ps = con.prepareStatement(sql);
			trs.setTimestampSQL(ps, 1);
			JSONObject feature = new JSONObject();
			JSONObject properties = new JSONObject();
			Set<Long> geomids = new HashSet<Long>();

			rs = ps.executeQuery();
			while(rs.next()) geomids.add(rs.getLong(1));
			properties.put("deleted_geomIDs", geomids);
			feature.put("type", "Feature");
			feature.put("properties", properties);
			features.put(feature);
		}
		
		JSONObject fc = new JSONObject();
		fc.put("type", "FeatureCollection");
		fc.put("features", features);
		scn.setLastModified(con);
		int type = HEADER;
		if (h.optBoolean("asAttachment")) type = FILE;
//		return new JSONObject().put("GeoJSON", new JSONObject().put("format", "GeoJSON").put("geometry", fc));
		return new Message(new JSONObject()
				.put("result", new JSONObject()
					.put("geometry_output", pack(scn, fc, type, h.optInt("attachmentIndentation", 0)))
					.put("ScID", scn.getScID())
					.put("lastmodified", scn.getInt("lastmodified"))));
	}
	
	private JSONFormat pack(Scenario s, JSONObject g, int type, int indent){
		if (type == HEADER) 
			return new JSONGeometry(
					s.getName(), 
					new JSONObject().put("format", "geojson").put("geometry", g)
					);
		if (type == FILE) 
			return new AttachmentAsArray(
					"geojson", 
					s.getName(), 
					ByteBuffer.wrap(g.toString(indent).getBytes(Charset.forName("UTF-8")))
					);
		return null;
	}

	public JSONObject geometryFromDb(Object geometry){
		GeometryObjectWriter dbGOW = new GeometryObjectWriter();
		JSONObject geom = null;
		if (geometry != null){
			geom = new JSONObject();
			GeometryIterator gi = dbGOW.fromObject(geometry, modifier);
			int t = gi.getType();
			if (!(t == TAB.TRIANGLE || t == TAB.TIN || t == TAB.POLYHEDRALSURFACE)){
				geom.put("type", TAB.ALLTYPES[t]);
				geom.put("coordinates", JNumberArray.iteratorToJSONArray(gi.iterator()));
			}
		}
		return geom;
	}
	
}
