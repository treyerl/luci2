package luci.service.scenario.geojson;
import static luci.service.scenario.Scenario.epochSeconds2Timestamp;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.linear.BlockRealMatrix;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import luci.connect.Attachment;
import luci.connect.JSONFormat;
import luci.service.scenario.Curve;
import luci.service.scenario.Modifier;
import luci.service.scenario.NurbsCurve;
import luci.service.scenario.Scenario;
import luci.service.scenario.database.GeometryObjectReader;
import luci.service.scenario.database.TAB;

public class Upsert extends luci.service.scenario.Upsert{
	int nurbsDegree = 0;
	JSONArray knots = null;
	JSONObject geometry;
	Map<String, String> attributeMap = new HashMap<>();
	
	public Upsert(Modifier modifier, boolean allowTimestampOverride){
		allowsTimestampOverride(allowTimestampOverride);
		setModifier(modifier);
	}
	
	public JSONObject getGeometry() {
		return this.geometry;
	}
	
	private boolean hasTimestamp(){
		String TIMESTAMP = attributeMap.getOrDefault("timestamp", "timestamp");
		String type = geometry.getString("type");
		if (type.equals("Feature")){
			return geometry.getJSONObject("properties").has(TIMESTAMP);
		} else if (type.equals("FeatureCollection")) {
			for (Object f: geometry.getJSONArray("features")){
				if (((JSONObject) f).getJSONObject("properties").has(TIMESTAMP))
					return true;
			}
		}
		return false;
	}

	public Upsert setGeometryInput(JSONFormat geometry_input) throws FileNotFoundException {
		if (geometry_input instanceof Attachment) {
			InputStream stream = Attachment.getInputStream((Attachment) geometry_input);
			geometry = new JSONObject(new JSONTokener(stream));
		} else if (geometry_input instanceof JSONObject) {
			geometry = geometry_input.getJSONObject("geometry");
		} else if (geometry == null){
			throw new NullPointerException("Geometry must not be null!");
		}
		
		JSONObject attmap = geometry_input.optJSONObject("attributeMap");
		if (attmap == null) attmap = new JSONObject();
		for (String key: attmap.keySet()){
			attributeMap.put(key, attmap.getString(key));
		}
		this.geometry_input = geometry_input;
		
		if (!allowsTimestampOverride() && (modifier.hasTimestamp() || hasTimestamp())) 
			throwTimestampException();
		
		return this;
	}

	public List<Long> implementation(Connection con) throws SQLException {
		List<Long> new_ids = new ArrayList<Long>();
		String move = "INSERT INTO " + scenario.historyTableName() + " SELECT ?, * FROM " + scenario.baseTableName() + " WHERE geomid = ?";
		String del = "DELETE FROM " + scenario.baseTableName() + " WHERE geomid = ?";

		String ID = attributeMap.getOrDefault("geomID", "geomID"); // JAVA8
		String LAYER = attributeMap.getOrDefault("layer", "layer"); // JAVA8
		String BATCHID = attributeMap.getOrDefault("batchID", "batchID"); // JAVA8
		String NURBS = attributeMap.getOrDefault("nurbsDegree", "nurbsDegree"); // JAVA8
		String TRANSFORM = attributeMap.getOrDefault("transform", "transform"); // JAVA8
		String TIMESTAMP = attributeMap.getOrDefault("timestamp", "timestamp"); // JAVA8
		String UID = attributeMap.getOrDefault("uid", "uid"); // JAVA8
		String KNOTS = attributeMap.getOrDefault("knots", "knots");

		Map<String, Short> allAttributes = new HashMap<String, Short>();
		boolean isAutoID = true;

		String layer = modifier.overrideLayer("default");
		int batchID = modifier.overrideBatchID(0);
		long uid = modifier.overrideUID(0);
		Timestamp time = modifier.overrideTimestamp(st);
		int flag = 0;
		String sql = null;
		JSONObjectCollector geometryCollection = new JSONObjectCollector(geometry, ID);
		geometryCollection.onTypeChange(()->scenario.updateAutoIDSequence(con, scenario.baseTableName(), "geomID"));
		for (JSONObject g: geometryCollection) {
			int k = 1;
			nurbsDegree = 0;
			knots = null;
			PreparedStatement ps = null;
			Map<String, Short> attributes = new LinkedHashMap<String, Short>();
			try {
				long geomID = 0;
				double[][] transform = null;
				JSONObject p = null;
				if (g.getString("type").equals("Feature")) {
					p = g.optJSONObject("properties");
					if (p != null) {
						// added to a Feature with no geometry
						if (p.has("deleted_geomIDs")) {
							JSONArray delete_list = p.getJSONArray("deleted_geomIDs");
							if (delete_list.length() > 0) {
								ps = con.prepareStatement(move);
								for (int j = 0; j < delete_list.length(); j++) {
									ps.setTimestamp(1, st);
									ps.setLong(2, delete_list.getLong(j));
									ps.addBatch();
								}
								ps.executeBatch();

								ps = con.prepareStatement(del);
								for (int j = 0; j < delete_list.length(); j++) {
									ps.setLong(1, delete_list.getLong(j));
									ps.addBatch();
								}
								ps.executeBatch();

								continue;
							}
							p.remove("deleted_geomIDs");
						}

						if (p.has(ID)) {
							geomID = p.getLong(ID);
							p.remove(ID);
						}

						if (p.has(TIMESTAMP)) {
							if (allowsTimestampOverride()){
								Object t = p.get(TIMESTAMP);
								p.remove(TIMESTAMP);
								try {
									if (t instanceof Number){
										long l;
										if (t instanceof Integer) l = new Long((int)t);
										else if (t instanceof Long) l = (long) t;
										else l = Long.valueOf(t+"");
										time = modifier.overrideTimestamp(epochSeconds2Timestamp(l));
									}
										
									else if (t instanceof String) {
										try {
											long tl = Long.valueOf((String) t);
											time = modifier.overrideTimestamp(epochSeconds2Timestamp((Long) tl));
										} catch (NumberFormatException e) {
											time = modifier.overrideTimestamp(Timestamp.valueOf((String) t));
										}
									}
								} catch (RuntimeException e) {
									// leave timestamp untouched
									e.printStackTrace();
								}
							} else p.remove(TIMESTAMP);
						}

						if (p.has(BATCHID)) {
							batchID = modifier.overrideBatchID(p.getInt(BATCHID));
							p.remove(BATCHID);
						}
						if (p.has(LAYER)) {
							layer = modifier.overrideLayer(p.getString(LAYER));
							p.remove(LAYER);
						}
						if (p.has(NURBS)) {
							nurbsDegree = p.getInt(NURBS);
							p.remove(NURBS);
						}
						if (p.has(KNOTS)) {
							knots = p.getJSONArray(KNOTS);
							attributes.put("knots", TAB.LIST);
							// p.remove(KNOTS);
						}
						if (p.has(TRANSFORM)) {
							JSONArray t = p.getJSONArray(TRANSFORM);
							p.remove(TRANSFORM);
							transform = new double[4][];
							for (int l = 0; l < 4; l++) {
								JSONArray tt = t.getJSONArray(l);
								double[] tf = transform[l] = new double[4];
								for (int n = 0; n < 4; n++) {
									tf[n] = tt.getDouble(n);
								}
							}
							modifier.setTransformation(new BlockRealMatrix(transform));
						}
						if (p.has(UID)) {
							uid = modifier.overrideUID(p.getLong(UID));
							p.remove(UID);
						}

						Set<String> keySet = p.keySet();
						for (String key : keySet)
							attributes.put(key, TAB.JSONType2SQL(p.get(key)));
					}
					g = g.optJSONObject("geometry");
				}

				if (nurbsDegree != 0)
					attributes.put("nurbsGeom", TAB.GEOMETRY);

				int upsertMask = 0; // for layer, nurb, transform, flag, geom
				if (layer != null)
					upsertMask |= TAB.LAYER_MASK;
				if (nurbsDegree != 0)
					upsertMask |= TAB.LAYER_MASK;
				if (transform != null)
					upsertMask |= TAB.LAYER_MASK;
				if (flag > 0)
					upsertMask |= TAB.LAYER_MASK;
				if (g != null)
					upsertMask |= TAB.GEOMETRY_MASK;
				
				isAutoID = geomID == 0;
				
				scenario.addColumns(con, TAB.merge(allAttributes, attributes));
				sql = getInsertSQL(geomID, attributes.keySet(), upsertMask, con);

				if (isAutoID)
					ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				else
					ps = con.prepareStatement(sql);

				// values always set
				ps.setTimestamp(k++, time);
				ps.setInt(k++, batchID); // sets batchID to 0 by default;
				ps.setLong(k++, uid);

				// optional values for upsert (non-AutoID)
				if (layer != null)
					ps.setString(k++, layer);
				if (nurbsDegree != 0)
					ps.setInt(k++, Math.abs(nurbsDegree));
				if (transform != null)
					setTransformation(ps, k++, transform, con);
				if (flag > 0)
					ps.setInt(k++, flag);
				if (g != null)
					try {
						ps.setObject(k++, geometryToDb(g));
					} catch (JSONException e){
						System.out.println(g);
						throw e;
					}

				// geometry has been set/evaluated, now the ctrlPoints might be
				// stored to an attributes column
				nurbsDegree = 0;

				if (p != null) {
					for (String key : attributes.keySet()) {
						if (p.get(key).equals(JSONObject.NULL)){
							ps.setNull(k++, TAB.H2SQL2JDBC[attributes.get(key)]);
							continue;
						}
						switch (attributes.get(key)) {
						case TAB.NUMERIC:
							Number n = (Number) p.get(key);
							if (n instanceof Integer)
								ps.setInt(k++, (Integer) n);
							else if (n instanceof Long)
								ps.setLong(k++, (Long) n);
							else
								ps.setBigDecimal(k++, new BigDecimal((Double) n));
							break;
						case TAB.STRING:
							ps.setString(k++, p.getString(key));
							break;
						case TAB.BOOLEAN:
							ps.setBoolean(k++, p.getBoolean(key));
							break;
						case TAB.CHECKSUM:
							JSONObject a = p.getJSONObject(key);
							setChecksum(ps, k++, a.getString("format"), 
									a.getJSONObject("attachment").getString("checksum"), con);
							break;
						case TAB.JSON:
							ps.setString(k++, p.getJSONObject(key).toString());
							break;
						case TAB.LIST:
							ps.setString(k++, p.getJSONArray(key).toString());
							break;
						case TAB.GEOMETRY:
							ps.setObject(k++, geometryToDb(g));
						}
					}
				}
				ps.execute();
				
				if (isAutoID) {
					ResultSet rs = ps.getGeneratedKeys();
					while (rs.next()) {
						geomID = rs.getLong(1);
						new_ids.add(geomID);
					}
				}
				
				ps.close();
				
			} catch (SQLException e) {
				System.out.println(sql);
				throw e;
			}
		}
		return new_ids;
	}

	@SuppressWarnings("unchecked")
	public Object geometryToDb(JSONObject geo) {
		GeometryObjectReader dbGOR = new GeometryObjectReader();
		if (geo != null) {
			String type = geo.getString("type").toLowerCase();
			JSONArray c = geo.getJSONArray("coordinates");
			if (nurbsDegree != 0) {
				int resolution = NurbsCurve.RESOLUTION; // TODO: add resolution
														// to the modifier
				switch (type) {
				case "point":
					return dbGOR.toPointObject(new JNumberArray(c), modifier);
				case "linestring":
					return dbGOR.toLineStringObject(Curve.evaluateLineString(new JNumberArray(c),
							new JNumberArray(knots), nurbsDegree, Scenario.getCoordinateDimension(), resolution),
							modifier);
				case "polygon":
					return dbGOR
							.toPolygonObject(
									Curve.evaluateMultiLineString(new JNumberArray(c), new JNumberArray(knots),
											nurbsDegree, Scenario.getCoordinateDimension(), resolution),
									modifier);
				case "multipoint":
					return dbGOR.toMultiPointObject(new JNumberArray(c), modifier);
				case "multilinestring":
					return dbGOR
							.toMultiLineStringObject(
									Curve.evaluateMultiLineString(new JNumberArray(c), new JNumberArray(knots),
											nurbsDegree, Scenario.getCoordinateDimension(), resolution),
									modifier);
				case "multipolygon":
					return dbGOR
							.toMultiPolygonObject(
									Curve.evaluateMultiPolygon(new JNumberArray(c), new JNumberArray(knots),
											nurbsDegree, Scenario.getCoordinateDimension(), resolution),
									modifier);
				}
			} else {
				switch (type) {
				case "point":
					return dbGOR.toPointObject(new JNumberArray(c), modifier);
				case "linestring":
					return dbGOR.toLineStringObject(new JNumberArray(c), modifier);
				case "polygon":
					return dbGOR.toPolygonObject(new JNumberArray(c), modifier);
				case "multipoint":
					return dbGOR.toMultiPointObject(new JNumberArray(c), modifier);
				case "multilinestring":
					return dbGOR.toMultiLineStringObject(new JNumberArray(c), modifier);
				case "multipolygon":
					return dbGOR.toMultiPolygonObject(new JNumberArray(c), modifier);
				default:
					throw new IllegalArgumentException("No OGIS type defined. Could not save " + type + " !");
				}
			}
		}
		return null;
	}
	
	public static class JSONObjectCollector implements Iterator<JSONObject>, Iterable<JSONObject>{
		@FunctionalInterface
		public interface On {
			public void react() throws SQLException;
		}
		Iterator<Object> jsonarrayit;
		List<JSONObject> noID = new LinkedList<>();
		String GEOMID;
		Set<On> onChanges = new HashSet<>();
		public JSONObjectCollector(JSONObject json, String GEOMIDname){
			String t = json.getString("type");
			JSONArray jsone = new JSONArray();
			if (t.equals("FeatureCollection"))
				jsone = json.getJSONArray("features");
			else if (t.equals("GeometryCollection"))
				jsone = json.getJSONArray("geometries");
			else
				jsone.put(json);
			jsonarrayit = jsone.iterator();
			GEOMID = GEOMIDname;
		}
		
		@Override
		public boolean hasNext() {
			return jsonarrayit.hasNext() || noID.size() > 0;
		}
		
		public void onTypeChange(On on){
			onChanges.add(on);
		}

		@Override
		public JSONObject next() {
			if (jsonarrayit.hasNext()){
				JSONObject n = (JSONObject) jsonarrayit.next();
				while (!(n.getString("type").equals("Feature") && n.getJSONObject("properties").has(GEOMID))){
					noID.add(n);
					if (jsonarrayit.hasNext()) n = (JSONObject) jsonarrayit.next();
					else return nextNoID();
				}
				return n;
			}
			return nextNoID();
		}
		
		private JSONObject nextNoID() {
			try {
				for (On change: onChanges){
					change.react();
				}
			} catch (SQLException e){
				throw new RuntimeException(e);
			}
			onChanges.clear();
			return noID.remove(0);
		}

		@Override
		public Iterator<JSONObject> iterator() {
			return this;
		}
	}
}

		


