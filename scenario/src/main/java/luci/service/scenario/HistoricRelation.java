/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package luci.service.scenario;

import java.sql.Connection;
import java.sql.ResultSet;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.LcString;
import luci.connect.Message;
import luci.core.validation.JsonType;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class HistoricRelation extends ServiceLocalSQL {
	@Override
	public String getGeneralDescription() {
		return "compares for each object from the scenario, optionally filtered to the objects given "
				+ "in geomIDs, how many objects in the history table have larger value in the "
				+ "given attribute column and how many have a lower value. This is how the current "
				+ "value can be ranked according to the given attributes in comparison to the "
				+ "historic values.";
	}
	
	@Override
	public JsonType getInputDescription() {
		String inputjson = "{"
				+ "		'XOR ScID':'number',"
				+ "		'relevantAttributes':'list',"
				+ "		'OPT geomIDs':'list',"
				+ "		'OPT timerange':"+TimeRangeSelection.getSpecification()
				+ "	}";
		return new JsonType(inputjson);
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{"
				+ "		'geomIDs':['listOf', {"
				+ "			'geomID':'number',"
				+ "			'ANY attribute':'list'"
				+ "			}]"
				+ "		}"
				+ "}");
	}
	
	@Override
	public JSONObject getExampleCall(){
		return new JSONObject("{'run':'"+getName()+"', 'ScID':1, 'relevantAttributes':['att1', 'att2']}");
	}
	
	@Override
	public Message sqlImplementation(Connection con) throws Exception {
		JSONObject h = input.getHeader();
		JSONArray atts = h.getJSONArray("relevantAttributes");
		int numAtts = atts.length();
		if (h.has("ScID")){
			Scenario scn = new Scenario(h.getInt("ScID"));
			String geomids = "";
			if (h.has("geomIDs")) {
				geomids = "("+LcString.trim(h.getJSONArray("geomIDs").toString(), 1)+")";
			}
			StringBuilder sql = new StringBuilder("SELECT geomID,");
			for (int i = 0; i < numAtts; i++){
				String att = atts.getString(i).split(";")[0];
				sql.append("(SELECT SUM(CASE WHEN h.\""+att+"\" > c.\""+att+"\" THEN 1 ELSE 0 END) || "
						+ "   ' ' || SUM(CASE WHEN h.\""+att+"\" < c.\""+att+"\" THEN 1 ELSE 0 END) "
						+ " FROM "+scn.historyTableName()
						+ "       AS h WHERE h.geomid = c.geomid) AS \""+att+"\"");
			}
			sql.append(" FROM "+scn.baseTableName()+" AS c WHERE geomid in "+geomids);
			ResultSet rs = con.prepareStatement(sql.toString()).executeQuery();
			JSONArray geomIDs_ = new JSONArray();
			while(rs.next()){
				JSONObject geomID = new JSONObject().put("geomID", rs.getInt(1));
				for (int i = 0; i < numAtts; i++){
					String att = atts.getString(i).split(";")[0];
					String str = rs.getString(i+2);
					if (str != null){
						String[] gt_lt_str = str.split(" ");
						JSONArray gt_lt = new JSONArray()
							.put(Integer.parseInt(gt_lt_str[0]))
							.put(Integer.parseInt(gt_lt_str[1]));
						geomID.put(att, gt_lt);
					}
				}
				geomIDs_.put(geomID);
			}
			return wrapResult(new JSONObject().put("geomIDs", geomIDs_));
		}
		
		return null;
	}

	
}
