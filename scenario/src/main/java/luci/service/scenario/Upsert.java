/** MIT License
 * @author Lukas Treyer
 * @date Aug 8, 2016
 * */
package luci.service.scenario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import luci.connect.JSONFormat;
import luci.connect.LcString;
import luci.service.scenario.database.TAB;

public abstract class Upsert {
	protected Modifier modifier;
	protected Scenario scenario;
	protected Timestamp st;
	protected boolean allowsTimestampOverride = false;
	protected JSONFormat geometry_input;
	
	protected void throwTimestampException(){
		throw new IllegalArgumentException("Operation does not allow to set timestamps explicitly.");
	}
		 
	
	public Upsert setModifier(Modifier modifier) {
		if (!allowsTimestampOverride() && modifier.hasTimestamp()) 
			modifier.clearTimestamp();
		this.modifier = modifier;
		return this;
	}
	
	public Upsert setScenario(Scenario si) {
		this.scenario = si;
		return this;
	}
	
	public Upsert setTimestamp(Timestamp st){
		this.st = st;
		return this;
	}
	
	public boolean allowsTimestampOverride(){
		return allowsTimestampOverride;
	}
	
	public Upsert allowsTimestampOverride(boolean allows){
		allowsTimestampOverride = allows;
		return this;
	}
	
	public Upsert setGeometryInput(JSONFormat geometry_input) throws Exception {
		this.geometry_input = geometry_input;
		return this;
	}
	
	public List<Long> execute(Connection con) throws SQLException{
		return implementation(con);
	}
	
	protected abstract List<Long> implementation(Connection con) throws SQLException;
	
	public int addBuiltInFieldsFromMask(StringBuilder sql, int upsertMask){
		int count = 0;
		
		if ((upsertMask & TAB.LAYER_MASK) == TAB.LAYER_MASK) {
			sql.append(", layer");
			count++;
		}
		if ((upsertMask & TAB.NURB_MASK) == TAB.NURB_MASK) {
			sql.append(", nurb");
			count++;
		}
		if ((upsertMask & TAB.TRANSFORM_MASK) == TAB.TRANSFORM_MASK) {
			sql.append(", transform");
			count++;
		}
		if ((upsertMask & TAB.FLAG_MASK) == TAB.FLAG_MASK) {
			sql.append(", flag");
			count++;
		}
		if ((upsertMask & TAB.GEOMETRY_MASK) == TAB.GEOMETRY_MASK) {
			sql.append(", geom");
			count++;
		}
		return count;
	}

	public String getAutoInsertSQL(Set<String> fields, int upsertMask) {
		StringBuilder sql = new StringBuilder("INSERT INTO "+scenario.baseTableName()
				+" (timestamp, batchID, uid");
		int count = addBuiltInFieldsFromMask(sql, upsertMask);
		for (String field: fields) sql.append(", \""+field+"\"");
		sql.append(") VALUES(?"+LcString.multiply(",?", 2+count+fields.size())+")");
		return sql.toString();
	}

	public String getInsertSQL(long geomID, Set<String> fields, int upsertMask, Connection con) 
			throws SQLException {
		boolean isAutoID = geomID == 0;
		if (isAutoID) return getAutoInsertSQL(fields, upsertMask);
		else return getUpsertSQL(geomID, fields, upsertMask, con);
	}

	private String getUpsertSQL(long geomID, Set<String> fields, int upsertMask, Connection con) 
			throws SQLException {
		StringBuilder sql = new StringBuilder("INSERT INTO "+scenario.historyTableName()+" SELECT NULL, * FROM "
				+ scenario.baseTableName()+" WHERE geomid = "+geomID);
		for (String field: fields) sql.append(" AND \""+field+"\" IS NOT NULL");
		sql.append("; ");
		Statement stmnt = con.createStatement();
		stmnt.execute(sql.toString());
		stmnt.close();
		
		sql = new StringBuilder("");
		sql.append("MERGE INTO "+scenario.baseTableName()+"(geomID, timestamp, batchID, uid");
		int count = addBuiltInFieldsFromMask(sql, upsertMask);
		for (String field: fields) sql.append(", \""+field+"\"");
		sql.append(") KEY(geomID) VALUES("+geomID+LcString.multiply(",?", 3+count));
		for (int i = 1; i <= fields.size(); i++) sql.append(", ?");
		sql.append(")");
		return sql.toString();
	}
	
	public static int setChecksum(PreparedStatement ps, int i, String format, String checksum, Connection con) throws SQLException {
		ps.setObject(i, new String[]{format, checksum});
		return i + 1;
	}

	public static void setTransformation(PreparedStatement ps, int i, double[][] transformation, 
			Connection con) throws SQLException {
		Double[][] m = new Double[4][];
		for (int j = 0; j < 4; j++){
			m[j] = new Double[4];
			for (int k = 0; k < 4; k++){
				m[j][k] = transformation[j][k];
			}
		}
		ps.setObject(i, m);
//		System.out.println(Arrays.toString(m[0])+", "+Arrays.toString(m[1])+", "+Arrays.toString(m[2])+", "+Arrays.toString(m[3]));
	}
}
