package luci.service.scenario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.JSON;
import luci.connect.Message;
import luci.core.validation.JsonType;

public class Delete extends ServiceLocalSQL{

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"',"
				+ "	'ScIDs': ['anyOf', ['listOf','number'], ['string','all']],"
				+ "	'OPT attributes':'list',"
				+ "	'OPT geomIDs':'list',"
				+ "	'OPT timerange':"+TimeRangeSelection.getSpecification()+","
				+ "	'OPT history':'boolean'"
				+ "}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'ScIDs':'list'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','ScIDs':[1,2]}");	
	}

	@Override
	protected Message sqlImplementation(Connection con) throws Exception {
		JSONObject h = input.getHeader();
		List<String> optionals = Arrays.asList("attributes", "geomIDs", "timerange", "history");
		Object oscids = h.get("ScIDs");
		Set<Integer> scids = null;
		if (oscids instanceof JSONArray)
			scids = JSON.ArrayToIntSet(h.getJSONArray("ScIDs"));
		
		if (optionals.stream().anyMatch(k -> h.keySet().contains(k))){
			if (scids == null) 
				scids = Scenario.getScenarios(con, null).stream().mapToInt((s)->s.getScID())
						.boxed().collect(Collectors.toSet());
			String sql;
			Scenario scn;
			for (Object o: scids){
				scn = new Scenario((int) o);
				if (h.has("attributes")){
					scn.dropColumns(con, JSON.ArrayToStringSet(h.getJSONArray("attributes")));
				}
				if (h.has("geomIDs")){
					scn.dropGeometries(con, JSON.ArrayToLongSet(h.getJSONArray("geomIDs")));
				}
				if (h.has("timerange")){
					TimeRangeSelection trs = new TimeRangeSelection(h.getJSONObject("timerange"));
					sql = "DELETE FROM "+scn.baseTableName()+" WHERE 1=1 "+trs.getSQL();
					PreparedStatement ps = con.prepareStatement(sql);
					trs.setTimestampSQL(ps, 1);
					ps.execute();
				}
				if (h.has("history")){
					sql = "DELETE FROM "+scn.historyTableName();
					con.createStatement().executeQuery(sql);
				}
			}
		} else {
			if (scids != null) scids = Scenario.dropAll(con, scids);
			else scids = Scenario.dropAll(con);
		}
		return new Message(new JSONObject().put("result", new JSONObject().put("ScIDs", scids)));
	}

	@Override
	public String getGeneralDescription() {
		return "Delete scenarios identified by ScID. If any of the optional values is given, delete "
				+ "geometry from the scenario(s) instead of the scenario(s). Deleting an entire "
				+ "scenario cannot be undone.\n"
				+ "@input.attributes delete this attribute on all objects (= delete the columns of "
				+ "these attributes in the database table)\n"
				+ "@input.geomIDs remove the rows identified by these geomIDs\n"
				+ "@input.timerange remove all objects whose timestamp is within the indicated range\n"
				+ "@input.history clear the history table";
	}

}
