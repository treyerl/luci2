package luci.service.scenario;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.validation.JsonType;
import luci.service.scenario.Scenario;

public class Exists extends ServiceLocalSQL {
	
	public static Map<Integer, String> getScenarioMap(Connection con) throws SQLException{
		Map<Integer, String> map = new HashMap<>();
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT ID,NAME FROM LC_SCENARIOS");
		while(rs.next()){
			map.put(rs.getInt(1), rs.getString(2));
		}
		return map;
	}
	
	public static boolean scenarioName(Connection con, String scenarioName) throws Exception{
		Map<Integer, String> map = getScenarioMap(con);
		return map.containsValue(scenarioName);
	}
	
	public static boolean ScID(Connection con, int ScID) throws Exception {
		Map<Integer, String> map = getScenarioMap(con);
		return map.containsKey(ScID);
	}
	
	@Override
	public Message sqlImplementation(Connection con) throws Exception {
		JSONObject h = input.getHeader();
		boolean exists = false;
		Scenario s = new Scenario();
		if (h.has("scenarioName")) exists = s.setName(h.getString("scenarioName")).isNameInRecords(con);
		else if (h.has("ScID")) exists = s.setScID(h.getInt("ScID")).isScIDInRecords(con);
		return wrapResult(new JSONObject().put("exists", exists));
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'XOR scenarioName':'string','XOR ScID':'number'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'exists':'boolean'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','ScID':''}");
	}

	@Override
	public String getGeneralDescription() {
		return "Tell if a scenario exists either identified by scenario name or ScID.";
	}

}
