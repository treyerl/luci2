package luci.service.scenario.camera;

import luci.connect.Message;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

import org.json.JSONObject;

public class Update extends ServiceLocal {
    @Override
    public String getGeneralDescription() {
        return "Service to update cameras";
    }

    @Override
    public JsonType getInputDescription() {
    	return wrapInputDescription(Camera.getOptionalInputDescription().put("cameraID", "number"));
    }

    @Override
    public JsonType getOutputDescription() {
    	return wrapResultDescription(Camera.getDefaultOutputDescription());
    }

    @Override
    public JSONObject getExampleCall() {
        return wrapExampleCall("{'location':{'locationX':1.2,'locationY':2.3,'locationZ':3.4}}");
    }

    @Override
    public Message implementation() throws Exception {
        JSONObject h = input.getHeader();
        long cameraID = h.getLong("cameraID");
        Camera c = Camera.cameras.get(cameraID);
        
        if (c == null) throw new IllegalArgumentException("No camera with ID "+cameraID);

        if(h.has("location")) {
            c.locationX = h.getJSONObject("location").getDouble("x");
            c.locationY = h.getJSONObject("location").getDouble("y");
            c.locationZ = h.getJSONObject("location").getDouble("z");
        }

        if(h.has("lookAt")) {
            c.lookAtX = h.getJSONObject("lookAt").getDouble("x");
            c.lookAtY = h.getJSONObject("lookAt").getDouble("y");
            c.lookAtZ = h.getJSONObject("lookAt").getDouble("z");
        }

        if(h.has("cameraUp")) {
            c.cameraUpX = h.getJSONObject("cameraUp").getDouble("x");
            c.cameraUpY = h.getJSONObject("cameraUp").getDouble("y");
            c.cameraUpZ = h.getJSONObject("cameraUp").getDouble("z");
        }

        if(h.has("scale")) {
            c.scale = h.getDouble("scale");
        }
        
        if(h.has("fov")){
        	c.fov = h.getDouble("fov");
        }

        JSONObject result = c.asJSON();
        Message m = wrapResult(result);

        c.notifyListeners(this, m, Luci.newTimestamp() - startTime);

        return m;
    }
}
