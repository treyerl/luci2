package luci.service.scenario.camera;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;
import org.json.JSONObject;

public class Get extends ServiceLocal {
    @Override
    public Message implementation() throws Exception {
        JSONObject h = input.getHeader();
        long cameraID = h.getLong("cameraID");
        Camera c = Camera.cameras.get(cameraID);
        return wrapResult(c.asJSON());
    }

    @Override
    public String getGeneralDescription() {
        return "get the camera parameters";
    }

    @Override
    public JsonType getInputDescription() {
        return wrapInputDescription(new JSONObject("{'cameraID':'number'}"));
    }

    @Override
    public JsonType getOutputDescription() {
        return wrapResultDescription(Camera.getDefaultOutputDescription());
    }

    @Override
    public JSONObject getExampleCall() {
        return wrapExampleCall(new JSONObject().put("cameraID", "number"));
    }
}
