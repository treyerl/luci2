package luci.service.scenario.camera;

import luci.connect.Message;
import luci.core.*;
import luci.core.validation.JsonType;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SubscribeTo extends ServiceLocal implements SubscriptionManager {
    static final Set<Class<? extends Listener>> managedSubscriptionTypes;
    static {
        Set<Class<? extends Listener>> l = new HashSet<>();
        l.add(EndOfServiceResultWriter.class);
        managedSubscriptionTypes = Collections.unmodifiableSet(l);
    }
    @Override
    public Set<Class<? extends Listener>> getManagedSubscriptionTypes() {
        return SubscribeTo.managedSubscriptionTypes;
    }

    @Override
    public Message implementation() throws Exception {
        JSONObject h = input.getHeader();
        ServerSocketHandler sh = (ServerSocketHandler) input.getSourceSocketHandler();
        long cameraID = h.getLong("cameraID");
        Camera c = Camera.cameras.compute(cameraID, (id, cam) -> cam == null ? Camera.createDefault(id) : cam);

        sh.getServiceResultWriter().startListeningTo(c);

        JSONObject result = new JSONObject().put("subscribedTo", new JSONObject().put("cameraID", cameraID));
        return wrapResult(result);
    }

    @Override
    public String getGeneralDescription() {
        return "subscribe to a camera for automatic camera updates";
    }

    @Override
    public JsonType getInputDescription() {
        return new JsonType(new JSONObject("{'run':'scenario.camera.SubscribeTo', 'cameraID':'number'}"));
    }

    @Override
    public JsonType getOutputDescription() {
        return new JsonType(new JSONObject("{'subscribedTo':{'cameraID':'number'}}"));
    }

    @Override
    public JSONObject getExampleCall() {
        return new JSONObject("{'run':'scenario.camera.SubscribeTo','cameraID':1}");
    }
}
