package luci.service.scenario.camera;

import luci.connect.Message;
import luci.core.*;
import luci.core.validation.JsonType;
import org.json.JSONObject;

import java.util.Set;


public class UnsubscribeFrom extends ServiceLocal implements SubscriptionManager{
    @Override
    public Set<Class<? extends Listener>> getManagedSubscriptionTypes() {
        return SubscribeTo.managedSubscriptionTypes;
    }

    @Override
    public Message implementation() throws Exception {
        JSONObject h = input.getHeader();
        ServerSocketHandler sh = (ServerSocketHandler) input.getSourceSocketHandler();
        long cameraID = h.getLong("cameraID");
        Camera c = Camera.cameras.get(cameraID);

        unsubscribeFrom(c, sh);

        JSONObject result = new JSONObject().put("unsubscribedFrom", new JSONObject().put("cameraID", cameraID));
        return wrapResult(result);
    }

    @Override
    public String getGeneralDescription() {
        return "unsubscribe from a camera";
    }

    @Override
    public JsonType getInputDescription() {
        return new JsonType(new JSONObject("{'run':'scenario.camera.UnsubscribeFrom', 'cameraID':'number'}"));
    }

    @Override
    public JsonType getOutputDescription() {
        return new JsonType(new JSONObject("{'unsubscribedFrom':{'cameraID':'number'}}"));
    }

    @Override
    public JSONObject getExampleCall() {
        return new JSONObject("{'run':'scenario.camera.UnsubscribeFrom','cameraID':1}");
    }
}
