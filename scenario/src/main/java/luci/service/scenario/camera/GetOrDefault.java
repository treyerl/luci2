package luci.service.scenario.camera;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class GetOrDefault extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		long id = input.getHeader().getLong("cameraID");
		Camera cam = Camera.cameras.get(id);
		if (cam == null){
			cam = Camera.createDefault(id);
		}
		return  wrapResult(cam.asJSON());
	}

	@Override
	public String getGeneralDescription() {
		return "Gets the requested camera or creates one if there is none.";
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'cameraID':'number'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription(Camera.getDefaultOutputDescription());
	}

	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{'cameraID':1}");
	}

}
