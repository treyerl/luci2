/** MIT License
 * @author Lukas Treyer
 * @date Aug 11, 2016
 * */
package luci.service.scenario.camera;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ID;
import luci.core.Listener;
import luci.core.ListenerManager;
import luci.core.TaskInfo;

public class Camera implements ListenerManager{
	static Map<Long, Camera> cameras = new HashMap<>();
	
	public static JSONObject getDefaultOutputDescription(){
		return new JSONObject("{'cameraID':'number'," +
                "'location':{'x':'number','y':'number','z':'number'}," +
                "'lookAt':{'x':'number','y':'number','z':'number'}," +
                "'cameraUp':{'x':'number','y':'number','z':'number'}," +
                "'scale':'number','fov':'number'}");
	}
	
	public static JSONObject getDefaultInputDescription(){
		return new JSONObject("{" +
                "'location':{'x':'number','y':'number','z':'number'}," +
                "'lookAt':{'x':'number','y':'number','z':'number'}," +
                "'cameraUp':{'x':'number','y':'number','z':'number'}," +
                "'scale':'number','OPT fov':'number'}");
	}
	
	public static JSONObject getOptionalInputDescription(){
		return new JSONObject("{" +
                "'OPT location':{'x':'number','y':'number','z':'number'}," +
                "'OPT lookAt':{'x':'number','y':'number','z':'number'}," +
                "'OPT cameraUp':{'x':'number','y':'number','z':'number'}," +
                "'OPT scale':'number','OPT fov':'number'}");
	}
	
	public static Camera create(){
		Camera c;
		long id;
		do {
			id = ID.getNew("cameraID");
		}
		while(cameras.containsKey(id));
		cameras.put(id, c = new Camera(id));
		return c;
	}
	
	public static Camera createDefault(long id){
		if (cameras.containsKey(id))
			throw new IllegalArgumentException("camera already exists");
		Camera c;
		cameras.put(id, c = new Camera(id));
		c.locationX = 10;
        c.locationY = 10;
        c.locationZ = 10;

        c.lookAtX = 0;
        c.lookAtY = 0;
        c.lookAtZ = 0;

        c.cameraUpX = 0;
        c.cameraUpY = 1;
        c.cameraUpZ = 0;

        c.scale = 1;
        c.fov = 60;
		return c;
	}
	
	double locationX, locationY, locationZ;
	double lookAtX, lookAtY, lookAtZ;
	double cameraUpX, cameraUpY, cameraUpZ;
	double scale, fov;
	Set<Listener> listeners = new HashSet<>();
	long id;
	
	public Camera(long id){
		this.id = id;
	}

	@Override
	public boolean addListener(Listener listener) {
		return listeners.add(listener);
	}

	@Override
	public boolean removeListener(Listener listener) {
		return listeners.remove(listener);
	}
	
	public void notifyListeners(TaskInfo ti, Message m, long duration){
		for (Listener scl: listeners){
			scl.getNotified(ti, m, duration);
		}
	}
	
	public JSONObject asJSON(){
		return new JSONObject()
				.put("cameraID", this.id)
				.put("location", new JSONObject()
						.put("x", locationX)
						.put("y", locationY)
						.put("z", locationZ))
				.put("lookAt", new JSONObject()
						.put("x", lookAtX)
						.put("y", lookAtY)
						.put("z", lookAtZ))
				.put("cameraUp", new JSONObject()
						.put("x", cameraUpX)
						.put("y", cameraUpY)
						.put("z", cameraUpZ))
				.put("scale", scale)
				.put("fov", fov);
	}
	
}
