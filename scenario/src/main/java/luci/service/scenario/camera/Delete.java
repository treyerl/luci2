package luci.service.scenario.camera;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;
import org.json.JSONObject;

public class Delete extends ServiceLocal{
    @Override
    public Message implementation() throws Exception {
        JSONObject h = input.getHeader();
        long cameraID = h.getLong("cameraID");

        //TODO: delete all listeners?

        Camera.cameras.remove(cameraID);

        JSONObject result = new JSONObject().put("deletedCameraID", cameraID);
        return wrapResult(result);
    }

    @Override
    public String getGeneralDescription() {
        return "deletes a camera";
    }

    @Override
    public JsonType getInputDescription() {
        return wrapInputDescription(new JSONObject("{'cameraID':'number'}"));
    }

    @Override
    public JsonType getOutputDescription() {
        return wrapResultDescription(new JSONObject("{'deletedCameraID':'number'}"));
    }

    @Override
    public JSONObject getExampleCall() {
        return new JSONObject("{'run':'scenario.camera.Delete','cameraID':1}");
    }
}
