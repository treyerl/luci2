package luci.service.scenario.camera;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;
import org.json.JSONObject;


public class Create extends ServiceLocal{

    @Override
    public Message implementation() throws Exception {
        JSONObject h = input.getHeader();

        Camera c = Camera.create();

        c.locationX = h.getJSONObject("location").getDouble("x");
        c.locationY = h.getJSONObject("location").getDouble("y");
        c.locationZ = h.getJSONObject("location").getDouble("z");

        c.lookAtX = h.getJSONObject("lookAt").getDouble("x");
        c.lookAtY = h.getJSONObject("lookAt").getDouble("y");
        c.lookAtZ = h.getJSONObject("lookAt").getDouble("z");

        c.cameraUpX = h.getJSONObject("cameraUp").getDouble("x");
        c.cameraUpY = h.getJSONObject("cameraUp").getDouble("y");
        c.cameraUpZ = h.getJSONObject("cameraUp").getDouble("z");

        c.scale = h.getDouble("scale");
        c.fov = h.optDouble("fov", 60);

        JSONObject result = c.asJSON();
        return wrapResult(result);
    }

    @Override
    public String getGeneralDescription() {
        return "Creates a new Camera Object";
    }

    @Override
    public JsonType getInputDescription() {
    	return wrapInputDescription(Camera.getDefaultInputDescription());

    }

    @Override
    public JsonType getOutputDescription() {
        return wrapResultDescription(Camera.getDefaultOutputDescription());
    }

    @Override
    public JSONObject getExampleCall() {
        return new JSONObject("{'run':'scenario.camera.Create'," +
                "'location':{'x':100,'y':150,'z':300}," +
                "'lookAt':{'x':0,'y':0,'z':0}," +
                "'cameraUp':{'x':0,'y':1,'z':0}," +
                "'scale':0.3}");
    }
}
