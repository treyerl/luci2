package luci.service.scenario.camera;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class List extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		return wrapResult(new JSONObject().put("cameraIDs", Camera.cameras.keySet()));
	}

	@Override
	public String getGeneralDescription() {
		return "Returns a list with all current camera IDs.";
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'cameraIDs':'list'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{}");
	}
	
}
