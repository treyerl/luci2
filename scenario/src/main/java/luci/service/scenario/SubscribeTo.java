/** MIT License
 * @author Lukas Treyer
 * @date Aug 10, 2016
 * */
package luci.service.scenario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.JSON;
import luci.connect.Message;
import luci.core.EndOfServiceResultWriter;
import luci.core.Listener;
import luci.core.ListenerManager;
import luci.core.ServerSocketHandler;
import luci.core.ServiceLocal;
import luci.core.SubscriptionManager;
import luci.core.validation.JsonType;

public class SubscribeTo extends ServiceLocal implements SubscriptionManager {
	static final Set<Class<? extends Listener>> managedSubscriptionTypes;
	static {
		Set<Class<? extends Listener>> l = new HashSet<>();
		l.add(EndOfServiceResultWriter.class);
		l.add(ScenarioConversionTicket.class);
		managedSubscriptionTypes = Collections.unmodifiableSet(l);
	}
	@Override
	public Message implementation() throws Exception {
		Listener l;
		JSONObject h = input.getHeader();
		String format = h.getString("format");
		Set<Integer> scids = JSON.ArrayToIntSet(h.getJSONArray("ScIDs"));
		ServerSocketHandler sh = (ServerSocketHandler) input.getSourceSocketHandler();
		
		if (format.equals("notification")) 
			 l = sh.getServiceResultWriter();
		else {
			l = sh.getListener(ScenarioConversionTicket.class, k -> new ScenarioConversionTicket(sh));
			((ScenarioConversionTicket)l).requestFormatFor(scids, format);
		}
		for (Integer scid: scids) {
			ListenerManager lm = new Scenario(scid);
			unsubscribeFrom(lm, sh);
			l.startListeningTo(lm);
		}
		return wrapResult(new JSONObject().put("ScIDsListeningTo", Scenario.observedBy(l)));
	}

	@Override
	public String getGeneralDescription() {
		return "Subscribes a client to any geometry updates that are executed on a given scenario.";
	}

	@Override
	public JsonType getInputDescription() {
		List<String> f = new ArrayList<>(Scenario.supportedFormats());
		f.add(0, "string");
		JSONObject desc = new JSONObject()
				.put("ScIDs", new JSONArray().put("listOf").put("number"))
				.put("format", new JSONArray(f).put("notification"));
		return wrapInputDescription(desc);
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'ScIDsListeningTo':['listOf','number']}");
	}

	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall(new JSONObject("{'ScIDs':[1,2],'format':'geojson'}"));
	}

	@Override
	public Set<Class<? extends Listener>> getManagedSubscriptionTypes() {
		return managedSubscriptionTypes;
	}

}
