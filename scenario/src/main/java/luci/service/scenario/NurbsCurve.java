package luci.service.scenario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class NurbsCurve extends Curve implements Iterator<Iterator<Double>>{

	class NurbsEvaluator implements Iterator<Iterator<Double>> {
		private int i = 0, j = 0, mu = 0;
		private double u = 0d, length = 0d, u_k = 0;
		@Override
		public boolean hasNext() {
			return u < maxKnot && i < ctrlPoints.size();
		}

		@Override
		public Iterator<Double> next() {
			u  = u_k + length/resolution*j;
			
			if (j == resolution) {
				j = 0;
				if (i + 1 < ctrlPoints.size()) i++;
				u_k = u;
			}
			if (j == 0){
				mu = multiplicityOf(u);
				if (i + mu-1 < ctrlPoints.size()) {
					i += mu-1;
					length = knots.get(i+1) - knots.get(i);
				} 
			}
			j++;
			System.out.println("u:"+u);
			return kabeja();
		}
		
		public CtrlPoint kabeja() {
	    	CtrlPoint p = new CtrlPoint(new double[dimensions]);
	        double[] n = this.getBasicFunctions();

	        double t = 0.0;

	        for (int j = 0; j <= degree; j++) {
	            int d = i - degree + j;
	            CtrlPoint P = ctrlPoints.get(d);
	            double cw = P.w*n[j];
	            p.add(P.mult(cw));
	            t += cw;
	        }
	        
	        return p.divBy(t);
	    }
		
		private double[] getBasicFunctions() {
	        double[] n = new double[degree + 1];
	        n[0] = 1.0;

	        double[] left = new double[degree + 1];
	        double[] right = new double[degree + 1];

	        for (int j = 1; j <= degree; j++) {
	            left[j] = u - knots.get((i + 1) - j);
	            right[j] = knots.get(i + j) - u;

	            double saved = 0.0;

	            for (int r = 0; r < j; r++) {
	                double t = n[r] / (right[r + 1] + left[j - r]);
	                n[r] = saved + (right[r + 1] * t);
	                saved = left[j - r] * t;
	            }

	            n[j] = saved;
	        }

	        return n;
	    }
		
//		private CtrlPoint deBoor(){
//	    	CtrlPoint p = new CtrlPoint(new double[dimensions]);
//	    	double allWeights = 0d;
//	    	for (int i = 0; i < ctrlPoints.size(); i++){
//	    		double coeff = coeff_recursive(i, degree);
//	    		if (coeff > 0){
//	    			CtrlPoint p_i = ctrlPoints.get(i);
//	    			double weighted_coeff = coeff*p_i.w;
//	    			allWeights += weighted_coeff;
//		    		p = p.add(p_i.mult(weighted_coeff));
//	    		}
//	    	}
//			return p.div(allWeights);
//		}
//		
//		private double coeff_recursive(int i, int n){
//			double u_i = knots.get(i);
//			double u_i1 = knots.get(i+1);
//			if (n == 0){
//				if (u >= u_i && u < u_i1) return 1;
//				return 0;
//			}
//			double u_in = knots.get(i+n);
//			double u_in1 = knots.get(i+n+1);
//			double left = ((u-u_i) / (u_in - u_i));
//			double right = ((u_in1 - u) / (u_in1 - u_i1));
//			return  left * coeff_recursive(i, n-1) + right * coeff_recursive(i+1, n-1);
//		}

	}
	
	private ArrayList<CtrlPoint> ctrlPoints;
	private ArrayList<Double> knots;
	private int i;
	private double maxKnot = 0;

	public NurbsCurve(Iterator<Iterator<Double>> ctrlPts, Iterator<Double> knots, 
			int degree, int dim, int res){
		super(degree, dim, res);
		this.knots = new ArrayList<Double>();
		ctrlPoints = new ArrayList<CtrlPoint>();
		while(ctrlPts.hasNext()) ctrlPoints.add(new CtrlPoint(ctrlPts.next()));
		while(knots.hasNext()) this.knots.add(knots.next());
		validateKnotVector();
	}
	
	public NurbsCurve(ArrayList<CtrlPoint> points, ArrayList<Double> k,
			int degree, int dim, int res){
		super(degree, dim, res);
		ctrlPoints = points;
		knots = k;
	}
	
	public Iterator<Iterator<Double>> evaluate(){
		return new NurbsEvaluator();
	}

	@Override
	public boolean hasNext() {
		return i < ctrlPoints.size();
	}

	@Override
	public Iterator<Double> next() {
		return ctrlPoints.get(i++);
	}
	
	public CtrlPoint nextCtrlPoint(){
		return ctrlPoints.get(i++);
	}
	
	private void validateKnotVector(){
		for (int i = 0; i < knots.size(); i++){
			double knot = knots.get(i);
			if (knot > maxKnot) maxKnot = knot;
			else if (knot < maxKnot) 
				throw new IllegalStateException("knots "+knots+" not in ascending order!");
		}
	}
	
	private int multiplicityOf(Double knot){
		return Collections.frequency(knots, knot);
	}
	
}
