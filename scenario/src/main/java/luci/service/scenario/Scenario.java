package luci.service.scenario;
import static com.esotericsoftware.minlog.Log.debug;
import static com.esotericsoftware.minlog.Log.error;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.h2gis.h2spatialext.CreateSpatialExtension;
import org.json.JSONArray;
import org.json.JSONObject;

import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import luci.connect.LcString;
import luci.connect.Message;
import luci.core.DataModel;
import luci.core.Factory;
import luci.core.Listener;
import luci.core.Luci;
import luci.core.ServerSocketHandler;
import luci.core.Service;
import luci.core.TaskInfo;
import luci.service.scenario.ServiceLocalSQL.ServiceLocalSQLGet;
import luci.service.scenario.database.TAB;

public class Scenario extends JSONObject implements DataModel, TaskInfo{
	public final static GeometryFactory GF = new GeometryFactory();
	
	public static class Coordinate extends JSONObject{
		double lat, lon, alt = 0;
		public Coordinate(double lat, double lon){
			put("lat", this.lat = lat);
			put("lon", this.lon = lon);
		}
		public Coordinate(double lat, double lon, double alt){
			this(lat, lon);
			if (alt != Double.NaN) put("altitude", this.alt = alt);
		}
		
		public Coordinate(Point p) {
			this(p.getX(), p.getY());
		}
		public Coordinate(JSONObject j){
			this(j.getDouble("lat"), j.getDouble("lon"), j.optDouble("altitude"));
		}
		public double getLat(){
			return lat;
		}
		
		public double getLon(){
			return lon;
		}
		
		public double getAltitude(){
			return alt;
		}
		/**
		 * @return
		 */
		public Point toPoint() {
			return GF.createPoint(new com.vividsolutions.jts.geom.Coordinate(lat, lon, alt));
		}
	}
	
	private static String url;
	private static String path;
	private static HikariDataSource ds;
	private static Map<Integer, Set<Listener>> subscriptions = new HashMap<>();

	public void init(String dbPath) throws SQLException {
		path = dbPath;
		url = "jdbc:h2:" + path;// + ";FILE_LOCK=FS"; // + ";FILE_LOCK=NO" for development only!
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(url);
		config.setUsername("");
		config.setPassword("");
		config.addDataSourceProperty("cachePrepStmts", "true");
		config.addDataSourceProperty("prepStmtCacheSize", "250");
		config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

		ds = new HikariDataSource(config);
		initializeDatabase();
	}
	
	Connection getConnection() throws SQLException {
//		trace(Thread.currentThread().getStackTrace()[2].getMethodName());
		return ds.getConnection();
	}

	public static String getURL() {
		return url;
	}

	public static String getPath() {
		return path;
	}

	private void initializeDatabase() throws SQLException { 
		Connection con = getConnection();
		CreateSpatialExtension.initSpatialExtension(con);
		
		String sequence_name = TAB.SCENARIOS + "_ID_SEQ";
		String scenario_tables = "" + "CREATE SEQUENCE IF NOT EXISTS " + sequence_name + " START WITH 1 INCREMENT BY 1;"

				+ "CREATE TABLE IF NOT EXISTS " + TAB.SCENARIOS 
				+ "(" + "id bigint default "
				+ sequence_name + ".nextval," 
				+ "timestamp timestamp NOT NULL,"
				+ "name character varying(256) NOT NULL," 
				+ "georef geometry,"
				+ "unit character varying(4),"
				+ "crs character varying(256), "
				+ "PRIMARY KEY (id, timestamp)" + ");"

				+ "CREATE TABLE IF NOT EXISTS " + TAB.SCN_STR_TYPES 
				+ "(" + "scid bigint,"
				+ "att_name varchar," 
				+ "format varchar(32)," 
				+ "PRIMARY KEY (scid, att_name)" + ");";

		String[] tables = { scenario_tables };

		for (String table : tables) {
			PreparedStatement ps = con.prepareStatement(table);
			ps.execute();
		}
		con.close();
	}
	
	public static Timestamp epochSeconds2Timestamp(long seconds){
		return new Timestamp(seconds * 1000);
	}
	
	public static long millis2seconds(long millis){
		return (long) (millis / 1000);
	}
	
	public static List<Scenario> getScenarios(Connection con, Set<Integer> ScIDs) throws SQLException{
		List<Scenario> scenarioInfos = new ArrayList<>();
		String selectedScIDs = (ScIDs != null && ScIDs.size() > 0) ? "WHERE id in ("
							   + LcString.trim(ScIDs.toString(), 1)+")" : "";
		String sql = "SELECT * FROM "+TAB.SCENARIOS+" "+selectedScIDs;
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		while(rs.next()) scenarioInfos.add(new Scenario(rs));
		return scenarioInfos;
	}
	
	public static void setLastModified(Connection con, Collection<Scenario> scenarios) throws SQLException{
		for (Scenario s: scenarios) s.setLastModified(con);
	}
	
	public static Set<Integer> dropAll(Connection con) throws SQLException {
		return dropAll(con, null);
	}
	
	public static Set<Integer> dropAll(Connection con, Set<Integer> ScIDs) throws SQLException{
		Set<Integer> deleted = new HashSet<>();
		try {
			for (Scenario si: getScenarios(con, ScIDs)) {
				si.drop(con);
				deleted.add(si.getScID());
			}
		} catch (SQLException e){
			throw new SQLException(e.getMessage()+" deleted Scenarios "+deleted);
		}
		return deleted;
	}
	
	public void updateAutoIDSequence(Connection con, String tableName, String idName, long id)
			throws SQLException {
		String sql = "ALTER SEQUENCE "+tableName+"_"+idName+"_seq RESTART WITH "+id;
		con.prepareStatement(sql).execute();		
	}

	public static int getCoordinateDimension() {
		return 3;
	}
	
	private static String getType(short tableType){
		switch(tableType){
		case(TAB.INTEGER): return "bigint";
		case(TAB.NUMERIC): return "numeric";
		case(TAB.STRING): return "varchar";
		case(TAB.BOOLEAN): return "boolean";
		case(TAB.CHECKSUM): return "array";
		case(TAB.TIMESTAMP): return "timestamp";
		case(TAB.GEOMETRY): return "geometry";
		case(TAB.NUMERICARRAY): return "array";
		case(TAB.JSON): return "varchar";
		case(TAB.LIST): return "varchar";
		default: return null;
		}
	}
	
	private static String getSafeType(short tableType){
		String t = getType(tableType);
		if (t == null) return "varchar";
		return t;
	}
	
	private static short getType(String h2Type){
		switch(h2Type){
		case "BIGINT": return TAB.INTEGER;
		case "INTEGER": return TAB.INTEGER;
		case "DECIMAL": return TAB.NUMERIC;
		case "FLOAT": return TAB.NUMERIC;
		case "DOUBLE": return TAB.NUMERIC;
		case "NUMERIC": return TAB.NUMERIC;
		case "SMALLINT": return TAB.INTEGER;
		case "ARRAY": return TAB.CHECKSUM;
		case "VARCHAR": return TAB.STRING;
		case "CHAR": return TAB.STRING;
		case "CLOB": return TAB.STRING;
		case "BOOLEAN": return TAB.BOOLEAN;
		case "JSON": return TAB.JSON;
		case "LIST": return TAB.LIST;
		case "STRING": return TAB.STRING;
		case "GEOMETRY": return TAB.GEOMETRY;
		case "TIMESTAMP": return TAB.TIMESTAMP;
		default: return 0;
		}
	}
	
	// columns: id, timestamp, name, georef, unit, crs
	public static final String jsonTypeOutputDescription = new JSONObject()
			.put("ScID", "number")
			.put("name", "string")
			.put("OPT geoRef", new JSONObject()
					.put("lat", "number")
					.put("lon", "number")
					.put("OPT altitude", "number"))
			.put("OPT coordRef", new JSONObject()
					.put("XOR unit", new JSONArray("['string','mm','cm','m','km','inch','foot','yard','mile']"))
					.put("XOR crs", "string"))
			.put("created", "number")
			.put("lastmodified", "number")
			.toString(); 
	
	public static final String jsonTypeCreateDescription = new JSONObject()
			.put("name", "string")
			.put("OPT geoRef", new JSONObject()
					.put("lat", "number")
					.put("lon", "number")
					.put("OPT altitude", "number"))
			.put("OPT coordRef", new JSONObject()
					.put("XOR unit", new JSONArray("['string','mm','cm','m','km','inch','foot','yard','mile']"))
					.put("XOR crs", "string"))
			.toString(); 
	
	Timestamp created, lastmodified;
	Coordinate georef;
	String name, crs, unit;
	int ScID = 0;
	Set<Listener> listeners;
	long callID, taskID, parentID;
	private Listener caller;
	
	public Scenario(){}
	
	Scenario(ResultSet rs) throws SQLException{
		fromResultSet(rs);
	}
	
	private void fromResultSet(ResultSet rs) throws SQLException{
		setScID(rs.getInt(1));
		setCreated(rs.getTimestamp(2));
		setName(rs.getString(3));
		Object o = rs.getObject(4);
		if (o != null && o instanceof Point) 
			setGeoref(new Coordinate((Point) o));
		setUnit(rs.getString(5));
		setCrs(rs.getString(6));
	}
	
	public Scenario(int ScID){
		setScID(ScID);
	}
	
	public Scenario(JSONObject j){
		setName(j.getString("name"));
		if (j.has("geoRef")){
			setGeoref(new Coordinate(j.getJSONObject("geoRef")));
		}
		if (j.has("coordRef")){
			JSONObject cr = j.getJSONObject("coordRef");
			setUnit(cr.optString("unit", null));
			setCrs(cr.optString("crs", null));
		}
	}
	
	public int hashCode(){
		return ScID;
	}
	
	/**
	 * @return the scID
	 */
	public int getScID() {
		return ScID;
	}
	/**
	 * @param scID the scID to set
	 */
	Scenario setScID(int scID) {
		put("ScID", ScID = scID);
		listeners = subscriptions.get(ScID);
		return this;
	}
	/**
	 * @return the timestamp
	 */
	public Timestamp getCreated() {
		return created;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	Scenario setCreated(Timestamp timestamp) {
		put("created", millis2seconds((this.created = timestamp).getTime()));
		return this;
	}
	
	/**
	 * @return the timestamp
	 */
	Timestamp getLastModified() {
		return lastmodified;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public Scenario setLastModified(Timestamp timestamp) {
		if (lastmodified != null && !timestamp.after(lastmodified))
			return this;
		put("lastmodified", millis2seconds((this.lastmodified = timestamp).getTime()));
		return this;
	}
	
	public Scenario setLastModified(Connection con) throws SQLException{
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT max(timestamp) FROM LC_SC_"+getScID());
		if (rs.next()) {
			Timestamp st = rs.getTimestamp(1);
			if (st != null){
				setLastModified(st);
				return this;
			}
		}
		setLastModified(created);
		return this;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	Scenario setName(String name) {
		put("name", this.name = name);
		return this;
	}
	
	/**
	 * @return the crs
	 */
	public String getCrs() {
		return crs;
	}
	/**
	 * @param crs the crs to set
	 */
	public Scenario setCrs(String crs) {
		if (unit != null) throw new RuntimeException("Cannot set CRS with unit != null");
		if (crs != null && crs.toLowerCase().startsWith("epsg")) put("crs", this.crs = crs);
		return this;
	}
	
	public boolean hasUnit(){
		return unit != null;
	}
	
	public boolean hasCrs(){
		return crs != null;
	}
	/**
	 * @return the lastmodified
	 */
	Timestamp getLastmodified() {
		return lastmodified;
	}
	
	/**
	 * @return the georef
	 */
	public Coordinate getGeoref() {
		return georef;
	}
	/**
	 * @param georef the georef to set
	 */
	Scenario setGeoref(Coordinate georef) {
		put("geoRef", this.georef = georef);
		return this;
	}
	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public Scenario setUnit(String unit) {
		if (crs != null) throw new RuntimeException("Cannot set unit with CRS != null");
		if (unit != null) put("unit", this.unit = unit);
		return this;
	}
	
	public Scenario clearUnit(){
		unit = null;
		return this;
	}
	
	private void addToRecords(Connection con) throws SQLException{
		// columns: id, timestamp, name, georef, unit, crs, status
		String sql = "INSERT INTO "+TAB.SCENARIOS+"(timestamp, name, georef, unit, crs) "
				+ "VALUES(?,?,?,?,?);";
		PreparedStatement ps = null;
		setCreated(new Timestamp(new Date().getTime()));
		setLastModified(created);
		ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		ps.setTimestamp(1, created);
		ps.setString(2, name);
		if (georef != null) ps.setObject(3, georef.toPoint());
		else ps.setNull(3, Types.OTHER);
		if (unit != null) ps.setString(4, unit);
		else ps.setNull(4, Types.VARCHAR);
		if (crs != null) ps.setString(5, crs);
		else ps.setNull(5, Types.VARCHAR);
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		setScID(rs.getInt(1));
	}
	
	public Scenario updateRecords(Connection con) throws SQLException{
		if (ScID == 0) throw new RuntimeException("Cannot update Scenario with ScID == 0");
		String sql = "UPDATE "+TAB.SCENARIOS+" SET name=?,georef=?, unit=?, crs=?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, name);
		if (georef != null) ps.setObject(2, georef.toPoint());
		else ps.setNull(2, Types.OTHER);
		if (unit != null) ps.setString(3, unit);
		else ps.setNull(3, Types.VARCHAR);
		if (crs != null) ps.setString(4, crs);
		else ps.setNull(4, Types.VARCHAR);
		ps.executeUpdate();
		return this;
	}
	
	public Scenario loadFromRecords(Connection con) throws SQLException{
		if (ScID == 0) throw new RuntimeException("ScID not set; unable to load from records");
		String sql = "SELECT * FROM "+TAB.SCENARIOS+" WHERE id="+ScID;
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		if (rs.next()) fromResultSet(rs);
		return this;
	}
	
	public boolean isScIDInRecords(Connection con) throws SQLException{
		if (ScID > 0){
			String sql = "SELECT id FROM "+TAB.SCENARIOS+" WHERE id="+ScID;
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) return true;
		}
		return false;
	}
	
	public boolean isNameInRecords(Connection con) throws SQLException{
		if (name != null){
			String sql = "SELECT id FROM "+TAB.SCENARIOS+" WHERE name="+name;
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) return true;
		}
		return false;
	}
	
	public String baseTableName(){
		if (ScID == 0) throw new NullPointerException("ScID cannot be equal to 0");
		return TAB.scn_base + ScID;
	}
	
	public String historyTableName(){
		if (ScID == 0) throw new NullPointerException("ScID cannot be equal to 0");
		return TAB.scn_base + ScID + "_h";
	}
	
	public String variantMaskTableName(){
		if (ScID == 0) throw new NullPointerException("ScID cannot be equal to 0");
		return TAB.scn_base + ScID + "_variant_mask";
	}
	
	public String variantsTableName(){
		if (ScID == 0) throw new NullPointerException("ScID cannot be equal to 0");
		return TAB.scn_base + ScID + "_variants";
	}
	
	public Scenario create(Connection con) throws Exception {
		// columns: id, timestamp, name, georef, unit, crs
		if (isScIDInRecords(con)) throw new RuntimeException("The scenario exists already!");
		addToRecords(con);
//		int ScID = scenario.getScID();
		PreparedStatement ps = null;
		String sequence_name = getSequenceNameGeomID();
		
		String sql_create_tables = ""
				+ "CREATE SEQUENCE "+sequence_name+" START WITH 1 INCREMENT BY 1;"			
		
				+ "CREATE TABLE "+baseTableName()+ " ("
				+ "geomID bigint default "+sequence_name+".nextval, "
				+ "timestamp timestamp NOT NULL, "
				+ "batchID bigint,"
				+ "layer character varying(256),"
				+ "nurb int,"
				+ "transform array,"
				+ "uid bigint,"
				+ "flag int, "
				+ "geom geometry,"
				+ "PRIMARY KEY (geomID)"
				+ ");"
		
				+ "CREATE TABLE "+historyTableName()+" ("
				+ "timestamp_deleted timestamp, "
				+ "geomID bigint, "
				+ "timestamp timestamp NOT NULL, "
				+ "batchID bigint,"
				+ "layer character varying(256),"
				+ "nurb int,"
				+ "transform array,"
				+ "uid bigint,"
				+ "flag int, "
				+ "geom geometry,"
				+ "PRIMARY KEY (geomID, timestamp)"
				+ ");"

				+ "CREATE TABLE IF NOT EXISTS "+variantMaskTableName()+"("
				+ "geomID bigint auto_increment, "
				+ "PRIMARY KEY(geomID)"
				+ ");"
				// versions are being added dynamically here
				
				+ "CREATE TABLE IF NOT EXISTS "+variantsTableName()+"("
				+ "versionID bigint auto_increment,"
				+ "versionKey text, "
				+ "PRIMARY KEY (versionID)"
				+ ");"; // like 1.2.1.1.2.3
		try {
			ps = con.prepareStatement(sql_create_tables);
			ps.execute();
		} catch (SQLException e) {
			error("create scenario failed!");
			debug(e.toString());
			debug("SQL String: "+sql_create_tables);
			throw e;
		}
		ps.close();
		return this;
	}
	
	public void drop(Connection con) throws SQLException {
		String sequence_name =  getSequenceNameGeomID();
		String sql = "DROP TABLE IF EXISTS "+baseTableName()+"; "
					+ "DROP TABLE IF EXISTS "+historyTableName()+"; "
					+ "DROP TABLE IF EXISTS "+variantMaskTableName()+"; "
					+ "DROP TABLE IF EXISTS "+variantsTableName()+"; "
					+ "DROP SEQUENCE IF EXISTS "+sequence_name+"; ";
		Statement stmt = con.createStatement();
		stmt.execute(sql);
		
		sql = "DELETE FROM "+TAB.SCENARIOS+" WHERE id = "+getScID()+";"
			 +"DELETE FROM "+TAB.SCN_STR_TYPES+" WHERE scid = "+getScID()+";";
		stmt.execute(sql);
	}
	
	private String getSequenceNameGeomID() {
		return baseTableName()+"_GEOMID_SEQ";
	}
	
	public LinkedHashMap<String,Short> getColumnNamesAndTypes(Connection con)
			throws SQLException {

		String sql = "SELECT t.column_name, "
				+ "(CASE WHEN t.data_type = "+Types.VARCHAR+" THEN "
				+ "COALESCE((SELECT format FROM "+TAB.SCN_STR_TYPES+" WHERE scid = "+getScID()+" AND ATT_NAME = t.column_name), t.type_name) "
				+ "ELSE  t.type_name END) "
				+ "AS TYPE FROM INFORMATION_SCHEMA.COLUMNS AS t WHERE TABLE_NAME = '"+baseTableName().toUpperCase()+"'";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.execute();
		ResultSet rs = ps.getResultSet();
		LinkedHashMap<String, Short> stringMap = new LinkedHashMap<String, Short>();

		while(rs.next()){
			stringMap.put(rs.getString(1), getType(rs.getString(2)));
		}
		
		return stringMap;
	}
	
	public int dropColumns(Connection con, Set<String> columnNames) throws SQLException {
		String sql = "ALTER TABLE "+baseTableName()+" DROP COLUMN IF EXISTS ?";
		PreparedStatement ps = con.prepareStatement(sql);
		int i=0;
		for(String column: columnNames) {
			ps.setString(1, column);
			ps.executeQuery();
			i++;
		}
		return i;
	}
	
	public int dropGeometries(Connection con, Set<Long> geomIDs) throws SQLException {
		String sql = "DELETE FROM "+baseTableName()+" WHERE geomID = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		int i=0;
		for(Long geomID: geomIDs) {
			ps.setLong(1, geomID);
			ps.executeQuery();
			i++;
		}
		return i;
	}
	
	public int addColumns(Connection con, Map<String, Short> attributes) throws SQLException {
		String sql = "";
		String sql_h = "";
		Map<String, Short> stringTypes = new HashMap<String, Short>();
		for (Entry<String, Short> e: attributes.entrySet()){
			short t = e.getValue();
			sql = "ALTER TABLE "+baseTableName()+" ADD COLUMN IF NOT EXISTS \""+e.getKey()+"\" "+getSafeType(t)+";";
			sql_h = "ALTER TABLE "+historyTableName()+" ADD COLUMN IF NOT EXISTS \""+e.getKey()+"\" "+getSafeType(t)+";";
			PreparedStatement ps = con.prepareStatement(sql+sql_h);
			ps.execute();
			ps.close();
			if (TAB.TYPE_NAMES.containsKey(t)) stringTypes.put(e.getKey(), t);
		}		
		for (Entry<String, Short> e: stringTypes.entrySet()){
			// TODO: check upper case / if it's necessary
			sql = "INSERT INTO "+TAB.SCN_STR_TYPES+" (scid, att_name, format) SELECT "+getScID()+", '"+e.getKey()+"',"
					+ " '"+TAB.TYPE_NAMES.get(e.getValue()).toUpperCase()+"' WHERE NOT EXISTS ("
					+ "SELECT * FROM "+TAB.SCN_STR_TYPES+" WHERE scid = "+getScID()+" AND att_name = '"+e.getKey()+"');";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.execute();
			ps.close();
		}
		return 0;
	}

	public void updateAutoIDSequence(Connection con, String tableName, String idName)
			throws SQLException {
		String sql = "ALTER SEQUENCE "+tableName+"_"+idName+"_seq RESTART WITH "
				+ "(SELECT coalesce(MAX("+idName+")+1,1) FROM "+tableName+")";
		try {
			Statement st = con.createStatement();
			st.execute(sql);
			st.close();
		} catch (SQLException e){
			throw e;
		}		
	}

	static public double NaN_to_0(double d){
		if (Double.isNaN(d)) return 0d;
		else return d;
	}
	
	static List<Integer> observedBy(Listener l){
		return l.listensTo().stream()
			.filter((slm) -> slm instanceof Scenario)
			.map((slm)->(Scenario)slm)
			.map((s)->s.getScID())
			.collect(Collectors.toList());
	}
	
	static Set<Integer> observedBy(ServerSocketHandler sh){
		Set<Integer> all = new HashSet<>();
		for (Listener l: sh.getListeners().values()){
			all.addAll(observedBy(l));
		}
		return all;
	}

	@Override
	public boolean addListener(Listener listener) {
		if (listeners == null) subscriptions.put(ScID, listeners = new HashSet<>());
		return listeners.add((ScenarioConversionTicket) listener);
	}

	@Override
	public boolean removeListener(Listener listener) {
		if (listeners != null) return listeners.remove(listener);
		return false;
	}

	@Override
	public long getCallID() {
		return callID;
	}

	@Override
	public long getTaskID() {
		return taskID;
	}

	@Override
	public long getParentID() {
		return parentID;
	}

	@Override
	public Set<? extends Listener> getListeners() {
		return listeners;
	}
	
	@Override
	public Listener getCaller(){
		return caller;
	}
	
	void updateNotification(TaskInfo ti, Message m, long duration){
		callID = ti.getCallID();
		taskID = ti.getTaskID();
		parentID = ti.getParentID();
		caller = ti.getCaller();
		if (listeners != null){
			Map<String, ServiceLocalSQLGet> converters = new HashMap<>();
			long lm = m.getHeader().getJSONObject("result").getLong("lastmodified");
			for (Listener l: listeners){
				if (l instanceof ScenarioConversionTicket){
					String f = ((ScenarioConversionTicket) l).getFormatForScenario(ScID);
					ServiceLocalSQLGet converter = converters.computeIfAbsent(f, k -> {
						Factory sf = Luci.observer().getServiceFactory("scenario."+f+".Get");
						Message input = new Message(new JSONObject()
								.put("run", sf.getName())
								.put("ScID", ScID)
								.put("timerange", new JSONObject().put("from", lm)));
						try {
							return (ServiceLocalSQLGet) sf.createService(input).setCallID(ti.getCallID());
						} catch (Exception e) {
							l.getNotified(ti, new Message(new JSONObject().put("error", e.getMessage())), duration);
							return null;
						}
					});
					converter.addListener(l);
					converter.setCaller(ti.getCaller());
				} else l.getNotified(ti, m, duration);
			}
			for (Service s: converters.values()) s.start();
		}
	}

	/**
	 * @return
	 */
	public static List<String> supportedFormats() {
		// TODO: Scenario: generate supportedFormats dynamically
		return Arrays.asList(new String[]{"geojson","obj"});
	}
}
