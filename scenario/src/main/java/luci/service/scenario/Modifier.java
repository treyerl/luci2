package luci.service.scenario;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.JSONFormat;

public class Modifier {
	
	public static JSONObject standardDescription(){
		return new JSONObject()
				.put("OPT applyTransformation", "boolean")
				.put("OPT bakeNurbs", "boolean")
				.put("OPT dim", new JSONArray("['number','[1-3]']"))
				.put("OPT switchLatLon", "boolean");
	}
	
	private Projection projection;
	public final boolean isApplyingTransformation;
	public final boolean isBakingCurves;
	public final int coordinateDimension;
	public final int batchID;
	public final String layer;
	public Timestamp timestamp;
	public final long uid;
	private boolean isSwitchingLatLon;
	private RealMatrix transformationMatrix;
	private Map<String, String> stringsToOverride;
	private Map<String, Number> numbersToOverride;
	private Map<String, Boolean>booleansToOverride;
	private Map<String, JSONObject> JSONObjectsToOverride;
	private Map<String, JSONArray> JSONArraysToOverride;

	public Modifier(Projection p, boolean doesTransformation, boolean doesCurves, 
			boolean isSwitchingLatLon, int dim, int batchID, String layer, Timestamp timestamp, long uid) {
		this.projection = p;
		this.isApplyingTransformation = doesTransformation;
		this.isBakingCurves = doesCurves;
		this.isSwitchingLatLon = isSwitchingLatLon;
		this.coordinateDimension = dim;
		this.batchID = batchID;
		this.layer = layer;
		this.timestamp = timestamp;
		this.uid = uid;
		stringsToOverride = new HashMap<String, String>();
		numbersToOverride = new HashMap<String, Number>();
		booleansToOverride = new HashMap<String, Boolean>();
		JSONObjectsToOverride = new HashMap<String, JSONObject>();
		JSONArraysToOverride = new HashMap<String, JSONArray>();
	}
	
	public static Modifier standard(Projection p, boolean applyTransformation, boolean bakeNurbs, 
			int dim, boolean switchLatLon){
		return new Modifier(p, applyTransformation, bakeNurbs, switchLatLon, dim, 0, null, null, 0);
	}
	
	public static Projection getProjection(Scenario s, JSONFormat geometry_input){
		String gcrs = geometry_input.optString("crs", null);
		Projection p;
		if (s.hasCrs()){
			if (gcrs != null) p = new Projection(gcrs, s.getCrs());
		} else if (gcrs != null){
			s.clearUnit();
			s.setCrs(gcrs);
		}
		if (s.hasCrs()) p = new Projection(s.getCrs());
		else p = new Projection();
		return p;
	}
	
	public Modifier(){
		this(new Projection(), false, false, false, 3, 0, null, null, 0);
	}
	
	public Modifier(Scenario s, JSONFormat geometry_input, JSONObject header){
		this(getProjection(s, geometry_input), 
				header.optBoolean("applyTransformation"), 
				header.optBoolean("bakeNurbs"),  
				header.optBoolean("switchLatLon"),
				header.optInt("dim", 3),
				0, null, null, 0);
	}
	
	
	
	
	public Projection setProjection(Projection projection){
		Projection old = this.projection;
		this.projection = projection;
		return old;
	}
	
	public Projection getProjection(){
		return this.projection;
	}
	
	public void switchLatLon(){
		this.isSwitchingLatLon = true;
	}
	
	public boolean isSwitchingLatLon(){
		return this.isSwitchingLatLon;
	}
	
	public void setTransformation(RealMatrix m){
		transformationMatrix = m;
	}
	
	public RealMatrix getTransformationMatrix(){
		return transformationMatrix;
	}
	
	public RealMatrix removeTransformationMatrix(){
		RealMatrix m = this.transformationMatrix;
		this.transformationMatrix = null;
		return m;
	}
	
	public int overrideBatchID(int batchID){
		return (this.batchID != 0) ? this.batchID : batchID;
	}
	
	public String overrideLayer(String layer){
		return this.layer != null ? this.layer : layer;
	}
	
	public boolean hasTimestamp(){
		return timestamp != null;
	}
	
	public Timestamp overrideTimestamp(Timestamp timestamp){
		return this.timestamp != null ? this.timestamp : timestamp;
	}
	
	public long overrideUID(long uid){
		return this.uid != 0 ? this.uid : uid;
	}
	
	public RealMatrix to_scenario(RealMatrix m){
		double[] v = m.getColumn(3);
		to_scenario(v);
		m.setColumnVector(3, new ArrayRealVector(v));
		return m;
	}
	
	public double[] to_scenario(double[] co){
		// Luci (and proj4j) is lat / lon
		// users should switch if their data is lon / lat
		if (isSwitchingLatLon) switchLatLon(co);
		projection.to_scenario(co);
		return co;
	}
	
	public double[] from_scenario(double[] co){
		co = projection.to_scenario(transform(co));
		// Luci (and proj4j) is lat / lon
		// users should switch if their data is lon / lat
		if (isSwitchingLatLon) switchLatLon(co);
		return co;
	}
	
	public double[] switchLatLon(double[] co){
		double co0 = co[0];
		co[0] = co[1];
		co[1] = co0;
		return co;
	}
	
	private double[] transform(double[] co){
		if (isApplyingTransformation && transformationMatrix != null){
			ArrayRealVector v = new ArrayRealVector(applyDimension(4, co));
			// set the fourth element to 1 otherwise the translation part of 
			// the transformation matrix will not be applied correctly
			v.setEntry(3, 1);
			co = transformationMatrix.preMultiply(v).toArray();
		}
		return applyDimension(coordinateDimension, co);
	}
	
	private double[] applyDimension(int dim, double[] co){
		if (dim == co.length) return co;
		double[] dimco = new double[dim];
		for (int i = 0; i < Math.min(dim, co.length); i++){
			dimco[i] = co[i];
		}
		for(int i = co.length; i < dim; i++){
			dimco[i] = 0;
		}
		return dimco;
	}
	
	public boolean shouldStoreProjectionSRID(){
		return projection.getToSRID() != 0;
	}
	
	public int getToSRID(){
		return projection.getToSRID();
	}
	
	public int getFromSRID(){
		return projection.getToSRID();
	}
	
	public String putStringToOverride(String key, String string){
		return stringsToOverride.put(key, string);
	}
	
	public String overrideString(String key, String string){
		return stringsToOverride.getOrDefault(key, string);
	}
	
	public Number putNumberToOverride(String key, Number n){
		return numbersToOverride.put(key, n);
	}
	
	public Number overrideNumber(String key, Number n){
		return numbersToOverride.getOrDefault(key, n);
	}
	
	public boolean putBooleanToOverride(String key, boolean b){
		return booleansToOverride.put(key, b);
	}
	
	public boolean overrideBoolean(String key, boolean b){
		return booleansToOverride.getOrDefault(key, b);
	}
	
	public JSONObject putJSONObjectToOverride(String key, JSONObject json){
		return JSONObjectsToOverride.put(key, json);
	}
	
	public JSONObject overrideJSONObject(String key, JSONObject json){
		return JSONObjectsToOverride.getOrDefault(key, json);
	}
	
	public JSONArray putJSONArrayToOverride(String key, JSONArray list){
		return JSONArraysToOverride.put(key, list);
	}
	
	public JSONArray overrideJSONArray(String key, JSONArray list){
		return JSONArraysToOverride.getOrDefault(key, list);
	}

	/**
	 * 
	 */
	public void clearTimestamp() {
		timestamp = null;
	}
}
