package luci.service.scenario.database;

import static luci.service.scenario.Scenario.GF;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.WKBWriter;

import luci.service.scenario.Modifier;
import luci.service.scenario.Scenario;

public class GeometryObjectReader {

	WKBWriter bw = new WKBWriter();
	
	
	
	public Point toPoint(Iterator<Double> c, Modifier m){
		double[] co = new double[]{
				Scenario.NaN_to_0(c.next()), 
				Scenario.NaN_to_0(c.next()), 
				Scenario.NaN_to_0((c.hasNext())?c.next():0d)
				};
		m.to_scenario(co);
		Point point = GF.createPoint(new Coordinate(co[0], co[1], co[2]));
		if (m.shouldStoreProjectionSRID()) point.setSRID(m.getToSRID());
//		System.out.println(point);
		return point;
	}

	public Object toPointObject(Iterator<Double> c, Modifier m){
		return toPoint(c, m);
	}
	
	public byte[] toPointBytes(Iterator<Double> c, Modifier m){
		return bw.write(toPoint(c, m));
	}
	
	public LinearRing toLinearRing(Iterator<Iterator<Double>> points, Modifier m) {
		List<Coordinate> coords = new ArrayList<Coordinate>();
		while(points.hasNext()){
			coords.add(toPoint(points.next(), m).getCoordinate());
		}
		// quick check for ring
		if (coords.size() < 3){
			System.out.println(coords.size());
			throw new IllegalArgumentException("More than 3 points needed for a valid ring!");
		}
		Coordinate p1 = coords.get(0);
		Coordinate pn = coords.get(coords.size()-1);
		if (!p1.equals(pn)){
			coords.add(p1);
		}
		
		Coordinate[] co = new Coordinate[coords.size()];
		LinearRing lr = GF.createLinearRing(coords.toArray(co));
//		System.out.println("LinearRing: "+lr);
		if (m.shouldStoreProjectionSRID()) lr.setSRID(m.getToSRID());
		return lr;
	}
	
	public Object toLinearRingObject(Iterator<Iterator<Double>> points, Modifier m){
		return toLinearRing(points, m);
	}
	
	public byte[] toLinearRingBytes(Iterator<Iterator<Double>> points, Modifier m){
		return bw.write(toLinearRing(points, m));
	}
	
	public LineString toLineString(Iterator<Iterator<Double>> points, Modifier m) {
		List<Coordinate> coords = new ArrayList<Coordinate>();
		while(points.hasNext()){
			coords.add(toPoint(points.next(), m).getCoordinate());
		}
		Coordinate[] pts = new Coordinate[coords.size()];
		LineString ls = GF.createLineString(coords.toArray(pts));
		if (m.shouldStoreProjectionSRID()) ls.setSRID(m.getToSRID());
		return ls;
	}
	
	public Object toLineStringObject(Iterator<Iterator<Double>> points, Modifier m){
		return toLineString(points, m);
	}
	
	public byte[] toLineStringBytes(Iterator<Iterator<Double>> points, Modifier m){
		return bw.write(toLineString(points, m));
	}
	
	public Polygon toPolygon(Iterator<Iterator<Iterator<Double>>> rings, Modifier m){
		List<LinearRing> pg_lrs = new ArrayList<LinearRing>();
		while(rings.hasNext()){
			pg_lrs.add(toLinearRing(rings.next(), m));
		}
		
		if (pg_lrs.size() == 0){
			System.out.println("strange");
		}
		
		LinearRing shell = pg_lrs.get(0);
		LinearRing[] holes = new LinearRing[pg_lrs.size()-1];
		for(int i = 1; i < pg_lrs.size(); i++) {
			holes[i-1] = pg_lrs.get(i);
		}
		Polygon p;
		if(holes.length==0)  p = GF.createPolygon(shell, null);
		else  p = GF.createPolygon(shell, holes);
		if (m.shouldStoreProjectionSRID()) p.setSRID(m.getToSRID());
		return p;
	}
	
	public Object toPolygonObject(Iterator<Iterator<Iterator<Double>>> rings, Modifier m){
		return toPolygon(rings, m);
	}
	
	public byte[] toPolygonBytes(Iterator<Iterator<Iterator<Double>>> rings, Modifier m){
		return bw.write(toPolygon(rings, m));
	}
	
	public MultiPoint toMultiPoint(Iterator<Iterator<Double>> points, Modifier m){
		List<Point> pg_points = new ArrayList<Point>();
		while(points.hasNext()){
			pg_points.add(toPoint(points.next(), m));
		}
		Point[] pts = new Point[pg_points.size()];
		MultiPoint mp = GF.createMultiPoint(pg_points.toArray(pts));
		if (m.shouldStoreProjectionSRID()) mp.setSRID(m.getToSRID());
		return mp;
	}
	
	public Object toMultiPointObject(Iterator<Iterator<Double>> points, Modifier m){
		return toMultiPoint(points, m);
	}
	
	public byte[] toMultiPointBytes(Iterator<Iterator<Double>> points, Modifier m){
		return bw.write(toMultiPoint(points, m));
	}
	
	public MultiLineString toMultiLineString(Iterator<Iterator<Iterator<Double>>> lineCollection, Modifier m){
		List<LineString> pg_lines = new ArrayList<LineString>();
		while(lineCollection.hasNext()){
			pg_lines.add(toLineString(lineCollection.next(), m));
		}
		LineString[] lns = new LineString[pg_lines.size()];
		lns = pg_lines.toArray(lns);
		MultiLineString mls = GF.createMultiLineString(lns);
		if (m.shouldStoreProjectionSRID()) mls.setSRID(m.getToSRID());
		return mls;
	}
	
	public Object toMultiLineStringObject(Iterator<Iterator<Iterator<Double>>> lineCollection, Modifier m){
		return toMultiLineString(lineCollection, m);
	}
	
	public byte[] toMultiLineStringBytes(Iterator<Iterator<Iterator<Double>>> lineCollection, Modifier m){
		return bw.write(toMultiLineString(lineCollection, m));
	}
	
	public MultiPolygon toMultiPolygon(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Modifier m){
		List<Polygon> pg_polys = new ArrayList<Polygon>();
		while(polyCollection.hasNext()){
			pg_polys.add(toPolygon(polyCollection.next(), m));
		}
		Polygon[] polys = new Polygon[pg_polys.size()];
		MultiPolygon mpl = GF.createMultiPolygon(pg_polys.toArray(polys));
		if (m.shouldStoreProjectionSRID()) mpl.setSRID(m.getToSRID());
		return mpl;
	}
	
	public Object toMultiPolygonObject(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Modifier m){
		return toMultiPolygon(polyCollection, m);
	}
	
	public byte[] toMultiPolygonBytes(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Modifier m){
		return bw.write(toMultiPolygon(polyCollection, m));
	}
	
	@SuppressWarnings("rawtypes")
	public GeometryCollection toGeometryCollection(Iterator collection, Modifier m){
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("rawtypes")
	public Object toGeometryCollectionObject(Iterator collection, Modifier m){
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("rawtypes")
	public byte[] toGeometryCollectionBytes(Iterator collection, Modifier m){
		throw new UnsupportedOperationException();
	}
}
