package luci.service.scenario.database;

import java.sql.Array;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class TAB {
	public final static String scn_base = "lc_sc_";
	public final static String SCENARIOS = "lc_scenarios";
	public final static String SCN_STR_TYPES = "lc_scn_str_types";
	public final static short SCN = 3;
	public final static short SCN_HISTORY = 4;
	public final static short SCN_VARIANT_MASK = 5;
	public final static short SCN_VARIANTS = 6;

	public final static int NURB_CIRCULARSTRING = 1;
	public final static int NURB_CIRCLE = 2;
	public final static int NURB_2BEZIER = 3;
	public final static int NURB_3BEZIER = 4;

	public final static short NUMERIC = 1;
	public final static short INTEGER = 2;
	public final static short STRING = 3;
	public final static short BOOLEAN = 4;
	public final static short CHECKSUM = 5;
	public final static short TIMESTAMP = 6;
	public final static short GEOMETRY = 7;
	public final static short NUMERICARRAY = 8;
	public final static short JSON = 10;
	public final static short LIST = 11;
	
	public final static short[] H2SQL2JDBC= new short[]{Types.NULL, Types.DECIMAL, Types.INTEGER, 
		Types.VARCHAR, Types.BOOLEAN, Types.ARRAY, Types.TIMESTAMP, Types.OTHER, Types.ARRAY, 
		Types.VARCHAR, Types.VARCHAR};

	public static final int LINEARRING = 0;
	public static final int POINT = 1;
	public static final int LINESTRING = 2;
	public static final int POLYGON = 3;
	public static final int MULTIPOINT = 4;
	public static final int MULTILINESTRING = 5;
	public static final int MULTIPOLYGON = 6;
	public static final int GEOMETRYCOLLECTION = 7;

	public static final int TRIANGLE = 8;
	public static final int TIN = 9;
	public static final int POLYHEDRALSURFACE = 10;

	public static final String[] ALLTYPES = new String[] { "", // internally
																// used
																// LinearRing
																// does not have
																// any text in
																// front of it
			"Point", "LineString", "Polygon", "MultiPoint", "MultiLineString", "MultiPolygon", "GeometryCollection",
			"Triangle", "Tin", "PolyhedralSurface" };

	public final static int LAYER_MASK = 0b00001;
	public final static int NURB_MASK = 0b00010;
	public final static int TRANSFORM_MASK = 0b00100;
	public final static int FLAG_MASK = 0b01000;
	public final static int GEOMETRY_MASK = 0b10000;

	public final static Map<Short, String> TYPE_NAMES = new HashMap<Short, String>();
	static {
		TYPE_NAMES.put(NUMERIC, "numeric");
		TYPE_NAMES.put(STRING, "string");
		TYPE_NAMES.put(BOOLEAN, "boolean");
		TYPE_NAMES.put(CHECKSUM, "checksum");
		TYPE_NAMES.put(TIMESTAMP, "timestamp");
		TYPE_NAMES.put(GEOMETRY, "geometry");
		TYPE_NAMES.put(NUMERICARRAY, "numeric_array");
		TYPE_NAMES.put(JSON, "json");
		TYPE_NAMES.put(LIST, "list");
	}

	public static class Checksum {
		public final String md5;
		public final String format;

		public Checksum(Array pointer) throws SQLException {
			Object[] c = (Object[]) pointer.getArray();
			if (c.length > 0)
				format = (String) c[0];
			else
				format = null;
			if (c.length > 1)
				md5 = (String) c[1];
			else
				md5 = null;
		}

		public String toString() {
			return md5 + "." + format;
		}
	}

	public static Double[][] getTransformation(Array pointer, double[][] t2) throws SQLException {
		if (pointer != null) {
			Double[][] t;
			t = new Double[4][];
			Object[] dbT = (Object[]) pointer.getArray();
			for (int i = 0; i < 4; i++) {
				Object[] DD = (Object[]) dbT[i];
				double[] dd = t2[i] = new double[4];
				t[i] = new Double[4];
				for (int j = 0; j < 4; j++) {
					dd[j] = t[i][j] = (double) DD[j];
				}
			}
			return t;
		} else
			return null;
	}

	public static Map<String, Short> merge(Map<String, Short> att_base, Map<String, Short> att_add) {
		Map<String, Short> added = new LinkedHashMap<String, Short>();
		for (String att : att_add.keySet()) {
			short t = att_add.get(att);
			if (att_base.containsKey(att)) {
				if (t != att_base.get(att)) {
					String key = att + "_" + TYPE_NAMES.get(t);
					att_base.put(key, t);
					added.put(key, t);
				}
			} else {
				att_base.put(att, t);
				added.put(att, t);
			}
		}
		return added;
	}

	public static short JSONType2SQL(Object o) {
		if (o instanceof JSONArray)
			return LIST;
		else if (o instanceof JSONObject) {
			if (((JSONObject) o).has("attachment"))
				return CHECKSUM;
			return JSON;
		} else if (o instanceof String)
			return STRING;
		else if (o instanceof Number)
			return NUMERIC;
		else if (o instanceof Boolean)
			return BOOLEAN;
		return 0;
	}

}
