package luci.service.scenario.database;

import java.util.Iterator;
//import java.util.PrimitiveIterator; //JAVA 1.8

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.Triangle;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKBReader;

import luci.service.scenario.Scenario;
import luci.service.scenario.Modifier;

public class GeometryObjectWriter {
	WKBReader br = new WKBReader();
	GeometryFactory GF = new GeometryFactory();

	protected int levelsForNumParts = 1;
	
	public void setLevelsForNumParts(int l){
		levelsForNumParts = l;
	}

	@SuppressWarnings("rawtypes")
	public class GeometryIterator {
		private Iterator<?> it;
		private Geometry geom;

		public GeometryIterator(Iterator<?> it, Geometry geom) {
			this.it = it;
			this.geom = geom;
		}

		public Iterator iterator() {
			return it;
		}

		public int getType() {
			return java.util.Arrays.asList(TAB.ALLTYPES).indexOf(geom.getGeometryType());
		}

		public Geometry getGeometry() {
			return geom;
		}

		public int numParts() {
			switch (geom.getGeometryType()) {
			case "Point":
				return 1;
			case "Linestring":
				return geom.getNumPoints();
			case "LinearRing":
				return ((LinearRing) geom).getNumPoints();
			case "Polygon":
				return ((Polygon) geom).getNumInteriorRing() + 1;
			case "MultiPoint":
				return ((MultiPoint) geom).getNumPoints();
			case "MultiLineString":
				return getNumPartsMultiLevel(levelsForNumParts, (GeometryCollection) geom);
			case "MultiPolygon":
				return getNumPartsMultiLevel(levelsForNumParts, (GeometryCollection) geom);
			case "GeometryCollection":
				return getNumPartsMultiLevel(levelsForNumParts, (GeometryCollection) geom);
			default:
				return 0;
			}
		}

		private int getNumPartsMultiLevel(int level, GeometryCollection g) {
			int num = 0;
			if (level > 1) {
				GeometryCollection mlstr = (GeometryCollection) geom;
				for (int i = 0; i < mlstr.getNumGeometries(); i++) {
					Geometry subG = mlstr.getGeometryN(i);
					if (subG instanceof GeometryCollection) {
						num += getNumPartsMultiLevel(level - 1, (GeometryCollection) mlstr.getGeometryN(i));
					} else if (subG instanceof Polygon) {
						num += ((Polygon) subG).getNumInteriorRing() + 1;
					} else {
						num++;
					}
				}
			} else
				num = g.getNumGeometries();
			return num;
		}

		public int numPoints() {
			switch (geom.getGeometryType()) {
			case "Point":
				return 1;
			case "Linestring":
				return geom.getNumPoints();
			case "LinearRing":
				return ((LinearRing) geom).getNumPoints();
			case "Polygon":
				return ((Polygon) geom).getNumPoints();
			case "MultiPoint":
				return ((MultiPoint) geom).getNumPoints();
			case "MultiLineString":
				return ((MultiLineString) geom).getNumPoints();
			case "MultiPolygon":
				return ((MultiPolygon) geom).getNumPoints();
			case "GeometryCollection":
				return ((GeometryCollection) geom).getNumPoints();
			default:
				return 0;
			}
		}

		public double[] getBBox() {
			if (geom.getGeometryType() == "Point")
				return new double[] { 0, 0, 0, 0, 0, 0 };
			double[] bbox = getInfiniteBoundingBox();
			subGeomBBox(geom, bbox);
			return bbox;
		}

		private void subGeomBBox(Geometry g, double[] bbox) {
			if (g instanceof GeometryCollection) {
				GeometryCollection gc = (GeometryCollection) g;
				for (int i = 0; i < gc.getNumGeometries(); i++) {
					subGeomBBox(gc.getGeometryN(i), bbox);
				}
			} else {
				subBox(g, bbox);
			}
		}

		private void subBox(Geometry g, double[] bbox) {
			for (Coordinate co : g.getCoordinates()) {
				if (co.x < bbox[0])
					bbox[0] = co.x; // X MIN
				if (co.y < bbox[1])
					bbox[1] = co.y; // Y MIN
				if (co.x > bbox[2])
					bbox[2] = co.x; // X MAX
				if (co.y > bbox[3])
					bbox[3] = co.y; // Y MAX
				if (co.z < bbox[4])
					bbox[4] = co.z; // Z MIN
				if (co.z > bbox[5])
					bbox[5] = co.z; // Z MAX
			}
		}

		public int[] getSubTypes() {
			if (geom.getGeometryType() == "Point")
				return new int[] { TAB.POINT };
			Geometry g = (Geometry) geom;
			int[] types = new int[g.getNumGeometries()];
			for (int i = 0; i < g.getNumGeometries(); i++)
				types[i] = java.util.Arrays.asList(TAB.ALLTYPES).indexOf(g.getGeometryType());
			return types;
		}

		public boolean hasM() {
			return false;
		}
	}

	public GeometryIterator fromBytes(byte[] b, Modifier m) {
		Geometry geom = null;
		try {
			geom = br.read(b);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new GeometryIterator(switchGeom(geom, m), geom);
	}

	public GeometryIterator fromObject(Object geom, Modifier m) {
		Geometry g = (Geometry) geom;
		return new GeometryIterator(switchGeom(g, m), g);
	}

	public Iterator<?> switchGeom(Geometry geom, Modifier m) throws IllegalArgumentException {
		switch (geom.getGeometryType()) {
		case "Point":
			return fromPoint((Point) geom, m);
		case "LineString":
			return fromLineString((LineString) geom, m);
		case "LinearRing":
			return fromLinearRing((LinearRing) geom, m);
		case "Polygon":
			return fromPolygon((Polygon) geom, m);
		case "MultiPoint":
			return fromMultiPoint((MultiPoint) geom, m);
		case "MultiLineString":
			return fromMultiLineString((MultiLineString) geom, m);
		case "MultiPolygon":
			return fromMultiPolygon((MultiPolygon) geom, m);
		case "GeometryCollection":
			return fromCollection((GeometryCollection) geom, m);
		default:
			throw new IllegalArgumentException("Unkown type " + geom.getGeometryType());
		}
	}

	@SuppressWarnings("rawtypes")
	public Iterator fromCollection(final GeometryCollection coll, final Modifier m) {
		class IterG implements Iterator {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < coll.getNumGeometries();
			}

			@Override
			public Object next() {
				return switchGeom(coll.getGeometryN(i++), m);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new IterG();
	}

	public Iterator<Double> fromPoint(Point pt, Modifier m) {
		final double[] d = m.from_scenario(new double[] { Scenario.NaN_to_0(pt.getCoordinate().x),
				Scenario.NaN_to_0(pt.getCoordinate().y), Scenario.NaN_to_0(pt.getCoordinate().z) });
		class IterDouble implements Iterator<Double> {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < d.length;
			}

			@Override
			public Double next() {
				return d[i++];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new IterDouble();
	}

	public Iterator<Iterator<Double>> fromLineString(final LineString ls, final Modifier m) {
		class PointIterator implements Iterator<Iterator<Double>> {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < ls.getNumPoints();
			}

			@Override
			public Iterator<Double> next() {
				return fromPoint((Point) ls.getPointN(i++), m);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new PointIterator();
	}

	private Iterator<Iterator<Double>> fromLinearRing(final LinearRing lr, final Modifier m) {
		class PointIterator implements Iterator<Iterator<Double>> {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < lr.getNumPoints();
			}

			@Override
			public Iterator<Double> next() {
				return fromPoint((Point) lr.getPointN(i++), m);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new PointIterator();
	}

	public Iterator<Iterator<Iterator<Double>>> fromPolygon(final Polygon poly, final Modifier m) {
		class RingIterator implements Iterator<Iterator<Iterator<Double>>> {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < poly.getNumInteriorRing() + 1;
			}

			@Override
			public Iterator<Iterator<Double>> next() {
				if (i == 0) {
					i++;
					return fromLineString(poly.getExteriorRing(), m);
				} else {
					i++;
					return fromLineString(poly.getInteriorRingN(i - 2), m);
				}
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new RingIterator();
	}

	public Iterator<Iterator<Double>> fromMultiPoint(final MultiPoint mp, final Modifier m) {
		class MultiPointIterator implements Iterator<Iterator<Double>> {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < mp.getNumPoints();
			}

			@Override
			public Iterator<Double> next() {
				return fromPoint((Point) mp.getGeometryN(i++), m);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new MultiPointIterator();
	}

	public Iterator<Iterator<Iterator<Double>>> fromMultiLineString(final MultiLineString ml, final Modifier m) {
		class LineIterator implements Iterator<Iterator<Iterator<Double>>> {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < ml.getNumGeometries();
			}

			@Override
			public Iterator<Iterator<Double>> next() {
				return fromLineString((LineString) ml.getGeometryN(i++), m);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new LineIterator();
	}

	public Iterator<Iterator<Iterator<Iterator<Double>>>> fromMultiPolygon(final MultiPolygon mpoly, final Modifier m) {
		class PolygonIterator implements Iterator<Iterator<Iterator<Iterator<Double>>>> {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < mpoly.getNumGeometries();
			}

			@Override
			public Iterator<Iterator<Iterator<Double>>> next() {
				return fromPolygon((Polygon) (mpoly.getGeometryN(i++)), m);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new PolygonIterator();
	}

	public Iterator<Iterator<Double>> fromTriangle(final Triangle triangle, final Modifier m) {
		class TriangleIterator implements Iterator<Iterator<Double>> {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i <= 3;
			}

			@Override
			public Iterator<Double> next() {
				i++;
				if (i == 1)
					return fromPoint(GF.createPoint(triangle.p0), m);
				else if (i == 2)
					return fromPoint(GF.createPoint(triangle.p1), m);
				else
					return fromPoint(GF.createPoint(triangle.p2), m);

			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new TriangleIterator();
	}

	public double[] getInfiniteBoundingBox() {
		return new double[] { Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY,
				Double.NEGATIVE_INFINITY,

				Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY,
				Double.NEGATIVE_INFINITY };
	}
}
