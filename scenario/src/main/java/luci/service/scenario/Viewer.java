package luci.service.scenario;


import luci.connect.Message;
import luci.core.RemoteHandle;
import luci.core.ServiceBalancer;
import luci.core.ServiceRemote;
import luci.core.validation.JsonType;
import org.json.JSONObject;

public class Viewer extends ServiceBalancer{
    @Override
    public void distribute() {
        JSONObject h = input.getHeader();
        System.out.println(h);
        long workerID = h.getLong("workerID");

        RemoteHandle rh = fsb.getAllWorkers().get(workerID);
        ServiceRemote sr = new ServiceRemote(fsb, rh);
        sr.setInput(new Message(h));
        distributedCalls.add(sr);
    }

    @Override
    protected boolean joinResults() {
        result = (Message)results.toArray()[0];
        return true;
    }

    @Override
    public boolean addRemote(RemoteHandle node, Message input) {
        return false;
    }

    @Override
    public boolean removeRemote(RemoteHandle node) {
        return false;
    }

    @Override
    public JSONObject getRemoteInputDescription() {
        return new JSONObject("{'run':'scenario.Viewer'," +
                "'XOR geometry':'attachment','XOR ScID':'number'," +
                "'OPT height':'number','OPT width':'number'," +
                "'OPT positionX':'number','OPT positionY':'number'," +
                "'OPT cameraID':'number'}");
    }

    @Override
    public JSONObject getRemoteOutputDescription() {
        return new JSONObject("{'XOR result':'json','XOR error':'string'}");
    }

    @Override
    public String getGeneralDescription() {
        return null;
    }

    @Override
    public JsonType getInputDescription() {
        return new JsonType(new JSONObject("{'run':'scenario.Viewer'," +
                "'workerID':'number'," +
                "'XOR geometry':'attachment','XOR ScID':'number'," +
                "'OPT height':'number','OPT width':'number'," +
                "'OPT positionX':'number','OPT positionY':'number'," +
                "'OPT cameraID':'number'}"));
    }

    @Override
    public JsonType getOutputDescription() {
        return new JsonType(new JSONObject("{'XOR result':'json','XOR error':'string'}"));
    }

    @Override
    public JSONObject getExampleCall() {
        return new JSONObject();
    }
}
