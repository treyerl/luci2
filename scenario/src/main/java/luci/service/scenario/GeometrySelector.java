package luci.service.scenario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.LcString;

public class GeometrySelector {
	
	public final static String inputParameterDescription = "@input.ScID the id of the scenario\n"
			+ "@input.attributes string key value pair; limits the exported geometry to "
			+ "object where attribute 'key' is equal to 'value'\n"
			+ "@input.orderBy a list containing only strings; will sort the geometry according to "
			+ "first attribute, second, third, etc.";
	
	private final static String inputParametersJSONString = "{"
			+ "'OPT batchID':'number',"
			+ "'OPT layer':'string',"
			+ "'OPT geomIDs':'list',"
			+ "'OPT timerange':"+TimeRangeSelection.getSpecification()+","
			+ "'OPT inclHistory':'boolean',"
			+ "'OPT attributes':{'ANY key':'string'},"
			+ "'OPT orderBy':['listOf', 'string'],"
			+ "'OPT offset':'number',"
			+ "'OPT limit':'number'"
			+ "}";
	
	public final static JSONObject createInputDescription(String serviceName){
		JSONObject d = new JSONObject(GeometrySelector.inputParametersJSONString);
		d.put("run", serviceName).put("ScID", "number");
		return d;
	}
	
	private Scenario scenario;
	private int batchID;
	private String layer;
	private long[] geomIDs;
	private TimeRangeSelection trs;
	private boolean inclHistory;
	private Map<String, String> attributes;
	private String[] orderBy;
	private int offset;
	private int limit;
	private long maxTimestamp;
	private Connection con;
	
	public GeometrySelector(Connection con){
		this.con = con;
	}
	
	private String getFieldSelectionString(Set<String> fields){
		StringBuilder s = new StringBuilder();
		for (String field: fields) s.append("\""+field+"\", ");
		if (fields.size() > 0) s.delete(s.length()-2, s.length());
		return s.toString();
	}
	public String getSelectionSQL() throws SQLException {
		return getSelectionSQL(null);
	}
	
	public String getSelectionSQL(LinkedHashMap<String, Short> columns) throws SQLException{
		try {
			LinkedHashMap<String, Short> types = scenario.getColumnNamesAndTypes(con);
			if (columns != null) columns.putAll(types);
			Set<String> fields = types.keySet();
			String sel = getFieldSelectionString(fields);
			
			String b = (batchID > 0) ? " AND batchID = "+batchID+" " : "";
			String l = (layer != null) ? "AND layer = ? " : "";
			String g = (geomIDs != null && geomIDs.length >  0) ? " AND GEOMID IN ( "+LcString.trim(Arrays.toString(geomIDs), 1)+" ) " : "";
			String a = "";
			if (attributes != null && attributes.size() > 0){
				StringBuilder aBuilder = new StringBuilder();
				Iterator<Entry<String, String>> it = attributes.entrySet().iterator();
				while(it.hasNext()){
					Entry<String, String> e = it.next();
					if (fields.contains((e.getKey()))) aBuilder.append(" AND ? = ?");
					else it.remove();
				}
				if (attributes.size() > 0) a = aBuilder.toString();
			}
			String o = "";
			if (orderBy != null){
				StringBuilder oBuilder = new StringBuilder(" ORDER BY ");
				for (int i = 0; i < orderBy.length; i++){
					String by = orderBy[i];
					String bySafe = by.split(";")[0];
					String byClean = bySafe.substring(1);
					if (i > 0) oBuilder.append(", ");
					if (by.startsWith("+")) {
						if (fields.contains(byClean)) oBuilder.append(" \""+byClean+"\" ASC");
					} else if (by.startsWith("-")) {
						if (fields.contains(byClean)) oBuilder.append(" \""+byClean+"\" DESC");
					} else {
						if (fields.contains(bySafe)) oBuilder.append(" \""+bySafe+"\" ASC");
					}
				}
				if (oBuilder.length() > 10) o = oBuilder.toString();
			} else if (inclHistory){
				o = " ORDER BY timestamp DESC ";
			}
			String limit = (this.limit > 0) ? " LIMIT "+this.limit+" " : "";
			String offset = (this.offset > 0) ? " OFFSET "+this.offset+" " : "";
			String sql = "SELECT "+sel+" FROM "+scenario.baseTableName()+" WHERE 1=1 "+b+l+g+a+trs.getSQL()+o+limit+offset;
			if (inclHistory) 
				sql = 	"( SELECT "+sel+" FROM "+scenario.baseTableName()+" WHERE 1=1 "+b+l+g+a+trs.getSQL()
						+ " UNION SELECT "+sel+" FROM "+scenario.historyTableName()+" WHERE 1=1 "+b+l+g+a+trs.getSQL()+") "+o+limit+offset;
			return sql;
		} catch (SQLException e) {
			throw e;
		}
		
	}

	//only returns the deleted geometries in the time range
	public String getSelectionDeletedGeomIDsSQL() {
		String sel = "geomid";
		String sql = "SELECT "+sel+" FROM "+scenario.historyTableName()+" WHERE 1=1 "+trs.getSQLDeleted();
//		System.out.println(sql);
		return sql;
	}

	public ResultSet selectDeleted() throws SQLException {
		String sql = getSelectionDeletedGeomIDsSQL();
		PreparedStatement ps = con.prepareStatement(sql);
		trs.setTimestampSQL(ps, 1);
		try {
			return ps.executeQuery();
		} catch (SQLException e){
			throw e;
		}
	}
	
	public ResultSet select(LinkedHashMap<String, Short> types) throws SQLException{
		String sql = getSelectionSQL(types);
		PreparedStatement ps = con.prepareStatement(sql);
		int i = 1;
		if (layer != null) ps.setString(i++, layer);
		if (attributes != null && attributes.size() > 0) {
			for (Entry<String, String> e : attributes.entrySet()){
				ps.setString(i++, e.getKey());
				ps.setString(i++, e.getValue());
			}
		}
		i = trs.setTimestampSQL(ps, i);
		if(inclHistory) {
			i = trs.setTimestampSQL(ps, i);
		}
//		System.out.println(sql);
//		System.out.println(trs.getFrom());

		try {
			return ps.executeQuery();
		} catch (SQLException e){			
			throw e;
		}
	}
	
	public String toString(){
		try {
			return getSelectionSQL(new LinkedHashMap<>());
		} catch (SQLException e) {
			return e.toString();
		}
	}

	public Scenario getScenario() {
		return scenario;
	}
	
	public void setScenario(Scenario scn) {
		scenario = scn;
	}

	public int getBatchID() {
		return batchID;
	}

	public void setBatchID(int batchID) {
		this.batchID = batchID;
	}

	public String getLayer() {
		return layer;
	}

	public void setLayer(String layer) {
		this.layer = layer;
	}

	public long[] getGeomIDs() {
		return geomIDs;
	}

	public void setGeomIDs(JSONArray ids) {
		if (ids != null){
			geomIDs = new long[ids.length()];
			for (int i = 0; i < ids.length(); i++){
				geomIDs[i] = ids.getLong(i);
			}
		}
	}

	public TimeRangeSelection getTimeRangeSelection() {
		return trs;
	}
	
	public void setTimeRangeSelection(TimeRangeSelection trs){
		this.trs = trs;
	}
	
	public void setTimeRangeSelection(JSONObject json) {
		if (json != null) trs = new TimeRangeSelection(json);
		else trs = TimeRangeSelection.NULL();
	}

	public boolean isInclHistory() {
		return inclHistory;
	}

	public void setInclHistory(boolean inclHistory) {
		this.inclHistory = inclHistory;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	
	public void setAttributes(JSONObject attributes) {
		if (attributes != null) {
			this.attributes = new HashMap<>();
			for (String key: attributes.keySet()){
				this.attributes.put(key, attributes.getString(key));
			}
		}
	}

	public String[] getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String[] orderBy) {
		this.orderBy = orderBy;
	}
	
	public void setOrderBy(JSONArray oby) {
		if (oby != null) {
			orderBy = new String[oby.length()];
			for (int i = 0; i < oby.length(); i++){
				orderBy[i] = oby.getString(i);
			}
		}
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public long getMaxTimestamp() {
		return maxTimestamp;
	}

	public void setMaxTimestamp(long maxTimestamp) {
		this.maxTimestamp = maxTimestamp;
	}
}
