/** MIT License
 * @author Lukas Treyer
 * @date Dec 16, 2015
 * */
package luci.service.scenario;

import java.sql.Connection;
import java.util.List;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.validation.JsonType;
import luci.service.scenario.Scenario;

public class GetList extends ServiceLocalSQL{
	
	@Override
	public Message sqlImplementation(Connection con) throws Exception {
		List<Scenario> scenarios = Scenario.getScenarios(con, null);
		Scenario.setLastModified(con, scenarios);
		return wrapResult(new JSONObject().put("scenarios", scenarios));
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription(new JSONObject());
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'scenarios':['listOf', "+Scenario.jsonTypeOutputDescription+"]}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"'}");
	}

	@Override
	public String getGeneralDescription() {
		return "Return a json object with scenario ids (ScIDs) as keys and scenario name as values.";
	}

}
