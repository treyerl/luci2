/** MIT License
 * @author Lukas Treyer
 * @date Aug 14, 2016
 * */
package luci.service.scenario;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import luci.core.EndOfServiceResultWriter;
import luci.core.ListenerManager;
import luci.core.ServerSocketHandler;

public class ScenarioConversionTicket extends EndOfServiceResultWriter {
	Map<Integer, String> formatPerScenario = new HashMap<>();

	public ScenarioConversionTicket(ServerSocketHandler sh) {
		super(sh);
	}
	
	public ScenarioConversionTicket requestFormatFor(int ScID, String format){
		formatPerScenario.put(ScID, format);
		return this;
	}
	
	public ScenarioConversionTicket requestFormatFor(Collection<Integer> ScIDs, String format){
		for (int ScID: ScIDs) formatPerScenario.put(ScID, format);
		return this;
	}
	
	public String getFormatForScenario(int ScID){
		return formatPerScenario.get(ScID);
	}
	
	public boolean startListeningTo(ListenerManager slm){
		return super.startListeningTo(slm);
	}
	
}
