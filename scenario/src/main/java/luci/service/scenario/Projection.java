package luci.service.scenario;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osgeo.proj4j.CRSFactory;
import org.osgeo.proj4j.CoordinateReferenceSystem;
import org.osgeo.proj4j.CoordinateTransform;
import org.osgeo.proj4j.CoordinateTransformFactory;
import org.osgeo.proj4j.ProjCoordinate;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

/**
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 * @param center: scenario geo reference; used to calculate a dynamic mercator transformation 
 * in case a transformation to WGS84 (and from there to other CRS's) is needed
 */
public class Projection extends ServiceLocal{
	public final static int earth_radius = 6378137;
	private int fromSRID = 0;
	private int toSRID = 0;
	private double[] center = null;
	private CoordinateTransform transform = null;
	private CoordinateReferenceSystem fromCRS;
	private CoordinateReferenceSystem toCRS;
//	private int dim = 4;
	
	public static Projection NULL(){
		return new Projection();
	}
	
	/** no transformation; 
	 *  SRID is NOT stored when saving to DB 
	 */
	public Projection(){
	}
	
	/** no transformation; 
	 *  SRID is NOT stored when saving to DB
	 *  @param center: double[], 
	 */
	public Projection(double[] center){
		this.center = center;
		CRSFactory crsFactory = new CRSFactory();
		toCRS = crsFactory.createFromName("EPSG:4326");
		fromCRS = crsFactory.createFromName("EPSG:4326");
	}
	
	/**no transformation; 
	 * SRID is stored when saving to DB
	 * @param fromToCrs: String
	 */
	public Projection(String fromToCrs){
		fromSRID = toSRID = parseEPSG(fromToCrs);
		CRSFactory crsFactory = new CRSFactory();
		this.fromCRS = crsFactory.createFromName(fromToCrs);
		this.toCRS = crsFactory.createFromName(fromToCrs);
	}
	
	/**transformation: crs --> dynamic mercator,
	 * SRID is NOT stored, 
	 * do NOT call from_scenario() of this instance
	 * @param fromCrs
	 * @param center
	 */
	public Projection(String fromCrs, double[] center){
		CRSFactory crsFactory = new CRSFactory();
		CoordinateTransformFactory transFactory = new CoordinateTransformFactory();
		
		this.center = validateCenter(center);
		this.fromSRID = parseEPSG(fromCrs);
		this.toSRID = 4326;
		
		
		fromCRS = crsFactory.createFromName(fromCrs);
		toCRS = crsFactory.createFromName("EPSG:4326");
		
		this.transform = transFactory.createTransform(fromCRS, toCRS);
	}
	
	/**transformation: dynamic mercator --> crs;
	 * SRID would be stored but this Projection is intended for transforming scenario geometry to another CRS.
	 * Hence, do NOT call to_scenario of this instance.
	 * @param center
	 * @param toCrs
	 */
	public Projection(double[] center, String toCrs){
		CRSFactory crsFactory = new CRSFactory();
		CoordinateTransformFactory transFactory = new CoordinateTransformFactory();
		
		center = validateCenter(center);
		fromSRID = 4326;
		toSRID = parseEPSG(toCrs);
		
		fromCRS = crsFactory.createFromName("EPSG:4326");
		toCRS = crsFactory.createFromName(toCrs);
		
		transform = transFactory.createTransform(fromCRS, toCRS);
	}
	
	/**transformation: crs --> crs, 
	 * SRID is stored when saving to DB
	 * @param fromCrs
	 * @param toCrs
	 */
	public Projection(String fromCrs, String toCrs){
		CRSFactory crsFactory = new CRSFactory();
		CoordinateTransformFactory transFactory = new CoordinateTransformFactory();
		
		fromSRID = parseEPSG(fromCrs);
		toSRID = parseEPSG(toCrs);
		
		fromCRS = crsFactory.createFromName(fromCrs);
		toCRS = crsFactory.createFromName(toCrs);			

		this.transform = transFactory.createTransform(fromCRS, toCRS);
	}
	
	public double[] to_scenario(JSONArray c){
		double[] co = new double[]{c.getDouble(0), c.getDouble(1), c.optDouble(2,0)};
		to_scenario(co);
		return co;
	}
	
	public double[] to_scenario(double[] co){
		if (!isByPassed()){
			ProjCoordinate p1 = new ProjCoordinate(co[0],co[1]);
			ProjCoordinate p2 = new ProjCoordinate();
			transform.transform(p1, p2);
			co[0] = p2.x;
			co[1] = p2.y;
			if (co.length > 2) co[2] = (Double.isNaN(p2.z)) ? 0: p2.z;
			if (this.center != null){
				wgs84_to_mercator(co);
			}
		}
		return co;
	}
	
	public double[] from_scenario(double[] co){
		if (!isByPassed()){
			if (this.center != null){
				mercator_to_wgs84(co);
			}
			ProjCoordinate p1 = new ProjCoordinate(co[0], co[1]);
			ProjCoordinate p2 = new ProjCoordinate();
			transform.transform(p1, p2);
			co[0] = p2.x;
			co[1] = p2.y;
		}
		return co;
	}
	
	public int getFromSRID(){
		return fromSRID;
	}
	
	public int getToSRID(){
		return toSRID;
	}
	
	public double[] getCenter(){
		return this.center;
	}
	
	// TRANSVERSE MERCATOR PROJECTION
	// ref: https://github.com/vvoovv/blender-geo/blob/master/transverse_mercator.py
	
	private void wgs84_to_mercator(double[] co){
		float scale = 1.0F;
		// FIXME: seems to work with EPSG:21781 only if lat/lon are flipped (co[0 <-> 1], co[1 <-> 0])
		double lat = Math.toRadians(co[1]);
		double lon = Math.toRadians(co[0] - center[1]);
		double B = Math.sin(lon) * Math.cos(lat);
		co[0] = 0.5 * scale * earth_radius * Math.log((1+B)/(1-B));
		co[1] = scale * earth_radius * ( Math.atan(Math.tan(lat)/Math.cos(lon)) - Math.toRadians(center[0]) );
	}
	
	private void mercator_to_wgs84(double[] co){
		float scale = 1.0F;
		double x = co[0]/(scale * earth_radius);
		double y = co[1]/(scale * earth_radius);
		double D = y + Math.toRadians(center[0]);
		double lon = Math.atan(Math.sinh(x)/Math.cos(D));
		double lat = Math.asin(Math.sin(D)/Math.cosh(x));

		co[0] = center[1] + Math.toDegrees(lon);
		co[1] = Math.toDegrees(lat);
	}
	
	private double[] validateCenter(double[] center){
		if (center != null) return center;
		else return new double[]{0,0,0};
	}
	
	private int parseEPSG(String epsg){
		System.out.println("parseEPSG: "+epsg);
		return Integer.parseInt(epsg.split(":")[1]);
	}

//	/**
//	 * @return
//	 */
//	public String getFromEPSG() {
//		if (fromSRID != 0) return "EPSG:"+fromSRID;
//		else return null;
//	}

	public boolean fromCrsHasLatLon(){
		return fromCRS != null && !fromCRS.getProjection().isRectilinear();
	}
	
	public boolean toCrsHasLatLon(){
		return toCRS != null && !toCRS.getProjection().isRectilinear();
	}
	
	public boolean isByPassed(){
		return transform == null;
	}
	
	public String getFromCRSCodename(){
		if (center != null && fromSRID == 4326) 
			return String.format("MERCATOR:%f,%f", center[0], center[1]);
		else if (fromSRID != 0) return "EPSG:"+fromSRID;
		else return null;
	}
	
	public String getToCRSCodename(){
		if (center != null && toSRID == 4326)
			return String.format("MERCATOR:%f,%f", center[0], center[1]);
		else if (toSRID != 0) return "EPSG:"+toSRID;
		else return null;
	}

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		CoordinateTransformFactory transFactory = new CoordinateTransformFactory();
		CRSFactory crsFactory = new CRSFactory();
		fromCRS = crsFactory.createFromName(h.getString("fromCRS"));
		toCRS = crsFactory.createFromName(h.getString("toCRS"));
		transform = transFactory.createTransform(fromCRS, toCRS);
		
		JSONArray points = h.getJSONArray("points");
		JSONArray pointsTo = new JSONArray();
		for (Object o: points){
			JSONArray p1 = (JSONArray) o;
			ProjCoordinate pc1 = new ProjCoordinate(p1.getDouble(0), p1.getDouble(1));
			ProjCoordinate pc2 = new ProjCoordinate();
			transform.transform(pc1, pc2);
			pointsTo.put(new JSONArray().put(pc2.x).put(pc2.y));
		}
		return wrapResult(new JSONObject().put("points", pointsTo).put("crs", toCRS.getName()));
//		JSONArray P1 = h.getJSONArray("point");
//		ProjCoordinate p1 = new ProjCoordinate(P1.getDouble(0),P1.getDouble(1));
//		ProjCoordinate p2 = new ProjCoordinate();
//		transform.transform(p1, p2);
//		return wrapResult("{'point':["+p2.x+","+p2.y+"],'crs':'"+toCRS.getName()+"'}");
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'fromCRS':'string','toCRS':'string','points':'list'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'points':'list','crs':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','fromCRS':'EPSG:4326','toCRS':'EPSG:21781',"
				+ "'points':[[47.37240, 8.50535]]}");
	}

	@Override
	public String getGeneralDescription() {
		return "Used to project coordinates from one coordinate system to another. Look at the "
				+ "exmample call to get an idea how to format input values.\n"
				+ "@input.points a two-dimensional array; array of 2D points";
	}
}
