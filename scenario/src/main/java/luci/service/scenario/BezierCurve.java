package luci.service.scenario;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BezierCurve extends Curve implements Iterator<Iterator<Double>>{
	
	/** evaluates piecewise bezier curves using the Bernstein polynomial
	 */
	class PiecewiseBezierEvaluator implements Iterator<Iterator<Double>> { // = Iterator<HullPoint>
		private int i = 0;
		private float t = 0;
		private List<CtrlPoint> bezier = new ArrayList<CtrlPoint>();
		private int count = 0;
		
		@Override
		public boolean hasNext() {
			return ctrlPoints.hasNext() || i <= resolution;
		}

		@Override
		public Iterator<Double> next() {
			if ((i == resolution && ctrlPoints.hasNext()) || i == 0){
				if (i == 0)if (ctrlPoints.hasNext()) bezier.add(nextCtrlPoint());
				else for (int j = 0; j < degree; j++) bezier.remove(0);
				for (int i = 0; ctrlPoints.hasNext() && i < degree; i++) {
					if (ctrlPoints.hasNext()) bezier.add(nextCtrlPoint());
					else throw new IndexOutOfBoundsException("Piecewise Bezier curves must have "
							+ degree+ "(degree) times "+count+"(control points) points");
				}
				i = 0;
//				W = 0d;
//				for (int j = 0; j < order; j++) W += hullSegment.get(j).w;
			}
			t = 1f/resolution*i++;
			return evaluate();
		}
		
		// calculates Bernstein polynomials
		private Iterator<Double> evaluate(){
			double x = 0, y = 0, z = 0;
			for (int j = 0; j < order; j++) {
				CtrlPoint hp = bezier.get(j);
				double c = BC(degree, j)*Math.pow(1-t, degree-j)*Math.pow(t, j);//*hp.w/W;
				x += c*hp.x;
				y += c*hp.y;
				if (dimensions == 3) z += c*hp.z;
			}
			if (dimensions == 3) return new IterPoint(x,y,z);
			else return new IterPoint(x,y);
		}
		
		// factorial
		private float F(int n){
			int i = 1;
			while(i < n){
				i *= ++i;
			}
			return i;
		}
		
		// binomialCoefficient
		private float BC(int n, int k){
			return F(n)/(F(k)*F(n-k));
		}
	}
	
	protected Iterator<Iterator<Double>> ctrlPoints;
	public BezierCurve(Iterator<Iterator<Double>> ctrlPoints, int degree, int dim, int res) {
		super(degree, dim, res);
		this.ctrlPoints = ctrlPoints;
	}
	
	public Iterator<Iterator<Double>> evaluate(){
		return new PiecewiseBezierEvaluator();
	}

	@Override
	public boolean hasNext() {
		return ctrlPoints.hasNext();
	}

	@Override
	public Iterator<Double> next() {
		return new CtrlPoint(ctrlPoints.next());
	}

	public CtrlPoint nextCtrlPoint(){
		return new CtrlPoint(ctrlPoints.next());
	}
}
