package luci.service.scenario;

import java.util.Iterator;

public class Curve {
	
	public class CtrlPoint implements Iterator<Double> {
		double x, y;
		double z = 0;
		double w = 1;
		double[] all;
		int i = 0;
		
		public CtrlPoint(double[] co){
			x = co[0];
			y = co[1];
			if (co.length > 3) {
				z = co[2];
				w = co[3];
			} else if (co.length > 2) w = co[2];
			updateAll();
		}
		
		public CtrlPoint(Iterator<Double> co){
			double zw = 1;
			x = co.next();
			y = co.next();
			if (co.hasNext()) zw = co.next();
			if (co.hasNext()) w = co.next();
			else w = zw;
			updateAll();
		}
		
		private void updateAll(){
			if (dimensions == 3) all = new double[]{x,y,w};
			else if (dimensions == 4) all = new double[]{x,y,z,w};
			else throw new IllegalStateException("NurbsCoordinates must either be 3 or 4 dimensional");
		}
		
		@Override
		public boolean hasNext() {
			return i < dimensions;
		}

		@Override
		public Double next() {
//			System.out.println(Arrays.toString(all)+", "+i+", dimensions: "+dimensions);
			return all[i++];
		}
		
		public CtrlPoint newCopy(){
			return new CtrlPoint(all);
		}
		
		public CtrlPoint mult(double f){
			if (dimensions == 4) return new CtrlPoint(new double[]{x*f,y*f,z*f, w});
			return new CtrlPoint(new double[]{x*f,y*f, w});
		}
		
		public CtrlPoint div(double f){
			if (dimensions == 4) return new CtrlPoint(new double[]{x/f,y/f,z/f, w});
			return new CtrlPoint(new double[]{x/f,y/f, w});
		}
		
		public CtrlPoint plus(CtrlPoint p){
			if (dimensions == 4) return new CtrlPoint(new double[]{x+p.x,y+p.y,z+p.z, w});
			return new CtrlPoint(new double[]{x+p.x,y+p.y, w});
		}
		
		public CtrlPoint minus(CtrlPoint p){
			if (dimensions == 4) return new CtrlPoint(new double[]{x-p.x,y-p.y,z-p.z, w});
			return new CtrlPoint(new double[]{x-p.x,-p.y, w});
		}
		
		public CtrlPoint multBy(double f){
			x *= f; y *= f; z *= f;
			updateAll();
			return this;
		}
		
		public CtrlPoint divBy(double f){
			x /= f; y/=f; z/=f;
			updateAll();
			return this;
		}
		
		public CtrlPoint add(CtrlPoint p){
			x += p.x; y += p.y; if (dimensions == 4) z += p.z;
			updateAll();
			return this;
		}
		
		public CtrlPoint sub(CtrlPoint p){
			x -= p.x; y -= p.y; if (dimensions == 4) z -= p.z;
			updateAll();
			return this;
		}
	}
	
	class IterPoint implements Iterator<Double> {
		private double[] xyzw;
		private int i;
		IterPoint(double x, double y, double z, double w) {
			i = 0;
			xyzw = new double[]{x,y,z,w};
		}
		IterPoint(double x, double y, double w) {
			i = 0;
			xyzw = new double[]{x,y,w};
		}
		IterPoint(double[] xyzw){
			this.xyzw = xyzw;
			i = 0;
		}
		IterPoint(double x, double y){
			this.xyzw = new double[]{x,y};
			i = 0;
		}
		@Override
		public boolean hasNext() {
			return i < xyzw.length;
		}

		@Override
		public Double next() {
			return xyzw[i++];
		}
	}
	
	private class EvaluatedMultiLineString implements Iterator<Iterator<Iterator<Double>>> {
		private Iterator<Iterator<Iterator<Double>>> ctrlPointStrings;
		private Iterator<Iterator<Double>> knotVectors;
		
		public EvaluatedMultiLineString( Iterator<Iterator<Iterator<Double>>> ctrlPointStrings,
				Iterator<Iterator<Double>> knotVectors){
			this.ctrlPointStrings = ctrlPointStrings;
			this.knotVectors = knotVectors;
		}
		
		@Override
		public boolean hasNext() {
			return ctrlPointStrings.hasNext() && knotVectors.hasNext();
		}

		@Override
		public Iterator<Iterator<Double>> next() {
			return evaluateLineString(ctrlPointStrings.next(), knotVectors.next(), 
					degree, dimensions, resolution);
		}
	}

	private class EvaluatedMultiPolygon implements Iterator<Iterator<Iterator<Iterator<Double>>>> {
		private Iterator<Iterator<Iterator<Iterator<Double>>>> ctrlPtStringGroups;
		private Iterator<Iterator<Iterator<Double>>> knotVectorGroups;
		public EvaluatedMultiPolygon(Iterator<Iterator<Iterator<Iterator<Double>>>> ctrlPtStringGroups,
				Iterator<Iterator<Iterator<Double>>> knotVectorGroups){
			this.ctrlPtStringGroups = ctrlPtStringGroups;
			this.knotVectorGroups = knotVectorGroups;
		}
		
		@Override
		public boolean hasNext() {
			return ctrlPtStringGroups.hasNext() && knotVectorGroups.hasNext();
		}

		@Override
		public Iterator<Iterator<Iterator<Double>>> next() {
			return new EvaluatedMultiLineString(ctrlPtStringGroups.next(), knotVectorGroups.next());
		}
	}
	
	public static Iterator<Iterator<Double>> evaluateLineString(
			Iterator<Iterator<Double>> ctrlPts,
			Iterator<Double> knotVector,
			int degree, int dim, int res){
		if (degree > 0) return new NurbsCurve(ctrlPts, knotVector, degree, dim, res).evaluate();
		if (degree < 0) return new BezierCurve(ctrlPts, -degree, dim, res).evaluate();
		else return null;
	}
	
	public static Iterator<Iterator<Iterator<Double>>> evaluateMultiLineString(
			Iterator<Iterator<Iterator<Double>>> ctrlPtStrings, 
			Iterator<Iterator<Double>> knotVectors, 
			int degree, int dim, int res){
		return new Curve(degree, dim, res).new EvaluatedMultiLineString(ctrlPtStrings, knotVectors);
	}
	
	public static Iterator<Iterator<Iterator<Iterator<Double>>>> evaluateMultiPolygon(
			Iterator<Iterator<Iterator<Iterator<Double>>>> ctrlPtStringGroups,
			Iterator<Iterator<Iterator<Double>>> knotVectorGroups,
			int degree, int dim, int res){
		return new Curve(degree, dim, res).new EvaluatedMultiPolygon(ctrlPtStringGroups, knotVectorGroups);
	}
	
	
	public final static int RESOLUTION = 24;
	
	int degree;
	int order;
	int dimensions;
	int resolution;
	
	public Curve(int degree, int dim, int res) {
		this.degree = degree;
		this.order = degree + 1;
		this.resolution = res;
		this.dimensions = dim;
	}
	
	public int setDimensions(int dim){
		int oldDim = dimensions;
		dimensions = dim;
		return oldDim;
	}
	
	public int getDimensions(){
		return dimensions;
	}
	
	public int getDegree(){
		return degree;
	}
	
	public int setRsolution(int res){
		int oldRes = resolution;
		resolution = res;
		return oldRes;
	}
	
	public int getResolution(){
		return resolution;
	}
}
