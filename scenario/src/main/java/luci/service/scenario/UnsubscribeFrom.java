/** MIT License
 * @author Lukas Treyer
 * @date Aug 10, 2016
 * */
package luci.service.scenario;

import java.util.Set;

import org.json.JSONObject;

import luci.connect.JSON;
import luci.connect.Message;
import luci.core.Listener;
import luci.core.ServerSocketHandler;
import luci.core.ServiceLocal;
import luci.core.SubscriptionManager;
import luci.core.validation.JsonType;

public class UnsubscribeFrom extends ServiceLocal implements SubscriptionManager{

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Set<Integer> scids = JSON.ArrayToIntSet(h.getJSONArray("ScIDs"));
		ServerSocketHandler sh = (ServerSocketHandler) input.getSourceSocketHandler();
		for (Integer scid: scids) unsubscribeFrom(new Scenario(scid), sh);
		return wrapResult(new JSONObject().put("ScIDsListeningTo", Scenario.observedBy(sh)));
	}

	@Override
	public String getGeneralDescription() {
		return "Unsubscribes a client from any geometry updates that are executed on a given scenario.";
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'ScIDs':['listOf', 'number']}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'ScIDsListeningTo':['listOf', 'number']}");
	}

	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall(new JSONObject("{'ScIDs':[1,2]}"));
	}

	@Override
	public Set<Class<? extends Listener>> getManagedSubscriptionTypes() {
		return SubscribeTo.managedSubscriptionTypes;
	}

}
