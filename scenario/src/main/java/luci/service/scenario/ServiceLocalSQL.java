package luci.service.scenario;

import java.sql.Connection;

import luci.connect.Message;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;
import luci.core.validation.LcValidationException;

public abstract class ServiceLocalSQL extends ServiceLocal {
	public static abstract class ServiceLocalSQLUpdate extends ServiceLocalSQLGet {
		JsonType mustOutput = new JsonType("{'result':{'ScID':'number','lastmodified':'number'}}");
		@Override
		public Message implementation() throws Exception {
			Message result = super.implementation();
			int ScID = result.getHeader().getJSONObject("result").getInt("ScID");
			new Scenario(ScID).updateNotification(this, result, Luci.newTimestamp() - this.startTime);
			return result;
		}
	}
	
	public static abstract class ServiceLocalSQLGet extends ServiceLocalSQL {
		JsonType mustOutput = new JsonType("{'result':{'ScID':'number','lastmodified':'number','geometry_output':'any'}}");
		String name;
		@Override
		public JsonType getOutputDescription() {
			JsonType out = getScenarioServiceOutputDescription();
			if (mustOutput.equals(out)){
				throw new LcValidationException(getName()+" does not comply with required output "+mustOutput);
			}
			return out;
		}
		protected abstract JsonType getScenarioServiceOutputDescription();
	}
	
	@Override
	public Message implementation() throws Exception {
		Connection con = ((Scenario) Luci.getDataModel()).getConnection();
		try {
			return sqlImplementation(con);
		} catch (Exception e){
			throw e;
		} finally {
			con.close();
		}
	}
	protected abstract Message sqlImplementation(Connection con) throws Exception;

}
