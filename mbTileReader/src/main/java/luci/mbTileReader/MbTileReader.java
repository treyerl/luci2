/** MIT License
 * @author Lukas Treyer
 * @date Jan 5, 2016
 * */
package luci.mbTileReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.zip.Inflater;

import luci.connect.Attachment;
import luci.connect.TileReader;

public class MbTileReader implements TileReader{
	private static Map<String, String> mimetypes = new HashMap<>();
	static {
		mimetypes.put("png", "image/png");
		mimetypes.put("jpg", "image/jpg");
		mimetypes.put("jpeg80", "image/jpg");
		mimetypes.put("mvt", "application/vnd.mapbox-vector-tile");
		mimetypes.put("pbf", "application/octet-stream");
		mimetypes.put("any", "application/octet-stream");
	}
	public class Tile implements luci.connect.TileReader.Tile{
		private String format;
		private byte[] data;
		Tile(String format, byte[] data){
			this.format = format;
			this.data = data;
		}
		
		public String getFormat(){
			return format;
		}
		
		public byte[] getData(){
			return data;
		}

		@Override
		public String getMimeType() {
			String mime = mimetypes.get(getFormat());
			if (mime == null) return mimetypes.get("any");
			return mime;
		}
	}
	private final Connection connection;
	private final File tileFile;
	public MbTileReader() throws FileNotFoundException, IOException, SQLException {
		Properties config = new Properties();
		config.load(new FileInputStream(new File(Attachment.getCwd(), "Luci.conf")));
		String cwd = Attachment.getCwd().getPath().replace("\\", "/");
		String path = config.getProperty("tileFile", cwd+"/data/tiles.mbtiles");
		tileFile = new File(path);
		if (!tileFile.exists()) {
			throw new FileNotFoundException("tile source '"+tileFile+"' not available");
		}
			
		connection = DriverManager.getConnection("jdbc:sqlite:"+tileFile.getAbsolutePath());
	}
	
	public String getTileSource(){
		return tileFile.getAbsolutePath();
	}
	
	public luci.connect.TileReader.Tile read(int zoom, int x, int y, String format, String scheme) 
			throws SQLException{
		String sql = String.format("SELECT tile_data FROM images WHERE tile_id = (SELECT tile_id FROM map WHERE "
				+ "zoom_level = %d AND "
				+ "tile_column = %d AND "
				+ "tile_row = %d)" , zoom, x, scheme.equals("xyz")?y_tms_to_xyz(zoom, y):y);
    	Statement statement = connection.createStatement();
		statement.setQueryTimeout(30);  // set timeout to 30 sec.
		ResultSet rs = statement.executeQuery(sql);
		if(rs.next()) {
			byte[] b = rs.getBytes(1);
			if (format.equals("pbf")) b = decompressByteArray(b);
			return new Tile(format, b);
		}
    	return null;
	}

	private int y_tms_to_xyz(int zoom, int y){
	    return (int) ((Math.pow(2, zoom)-1) - y);
	}
	
	public byte[] decompressByteArray(byte[] bytes) {
		ByteArrayOutputStream baos = null;
		Inflater iflr = new Inflater();
		iflr.setInput(bytes);
		baos = new ByteArrayOutputStream();
		byte[] tmp = new byte[4 * 1024];
		try {
			while (!iflr.finished()) {
				int size = iflr.inflate(tmp);
				baos.write(tmp, 0, size);
			}
		} catch (Exception ex) {

		} finally {
			try {
				if (baos != null)
					baos.close();
			} catch (Exception ex) {
			}
		}

		return baos.toByteArray();
	}
}
