
package luci.viewer;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import luci.connect.Attachment;
import luci.connect.AttachmentAsArray;
import luci.connect.LcRemoteService;
import luci.connect.Message;
import luci.viewer.oobjloader.builder.Build;
import luci.viewer.oobjloader.builder.Face;
import luci.viewer.oobjloader.builder.FaceVertex;
import luci.viewer.oobjloader.parser.Parse;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.esotericsoftware.minlog.Log.trace;

//import java.awt.event.MouseWheelEvent;

public class Viewer extends LcRemoteService implements GLEventListener{
    private static java.awt.Frame frame;
    private static GLCanvas canvas;
//    private final static FPSAnimator animator = new FPSAnimator(30);

    private int width = 800;
    private int height = 600;
    private int positionX = 100;
    private int positionY = 100;

    private int cameraID = 0;

    private float view_rotx = 0.0f, view_roty = 0.0f, view_rotz = 0.0f;
    private float view_translatex = 0, view_translatey = 0, view_translatez = 0;
    private float view_scale = 1;
    private int geometry_list = 0;
    private final int swapInterval;

    private float camera_locationX = 0, camera_locationY = 0, camera_locationZ = 0;
    private float camera_lookAtX = 0, camera_lookAtY = 0, camera_lookAtZ = 0;
    private float camera_upX = 0, camera_upY = 0, camera_upZ = 0;

    private int texture = 0;

    private float max_x = 0;
    private float max_y = 0;
    private float min_x = 10;
    private float min_y = 10;
    private float extentX;
    private float extentY;
    private float extentZ;

    private int t_width;
    private int t_height;

//    private long currTime;
//    private long prevTime;

    private static HashMap<String, ArrayList<Face>> geometry;

    private InputStream fileInput;
    private int ScID;

    boolean is_init = false;
    private GLU glu;

    private int prevMouseX, prevMouseY;

    float grey[] = { 0.6f, 0.6f, 0.6f, 0.7f };

    public Viewer(DefaultArgsProcessor ap) {
        super(ap);
        this.swapInterval = 1;
    }

    public static void main(String[] args) {
        frame = new java.awt.Frame("Viewer");
        frame.setLayout(new java.awt.BorderLayout());
        frame.setUndecorated(true);
//        frame.getRootPane().setWindowDecorationStyle(JRootPane.NONE);

        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {
                    @Override
                    public void run() {
//                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });

        canvas = new GLCanvas();
//        animator.add(canvas);
        // GLCapabilities caps = new GLCapabilities(GLProfile.getDefault());
        // GLCanvas canvas = new GLCanvas(caps);

        geometry = new HashMap<>();

        DefaultArgsProcessor asp = new DefaultArgsProcessor(args);
        trace(asp.toString());
        System.out.println("registering at "+asp.getHostname()+":"+asp.getPort());

        final Viewer viewer = new Viewer(asp);
        new Thread(viewer).start();
        viewer.connect(asp.getHostname(), asp.getPort());
    }

    public void setGeometry(int g1) {
        geometry_list = g1;
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        System.out.println("init");
        GL2 gl = drawable.getGL().getGL2();
        glu = new GLU();

//        float red[] = { 0.8f, 0.6f, 0.0f, 0.7f };
//        float green[] = { 0.0f, 0.8f, 0.2f, 0.7f };
//        float blue[] = { 0.2f, 0.2f, 1.0f, 0.7f };

        float ambient[] = {0.2f, 0.2f, 0.2f, 1f};
        float diffuse[] = {0.3f, 0.3f, 0.3f, 1f};
        float specular[] = {0.4f, 0.4f, 0.4f, 1f};

        float pos[] = { -1e3f, 0.0f, -1e3f, 0.0f };
        float pos2[] = {0.0f, 1e3f, 1e3f, 0.0f };
        float pos3[] = {1e3f, 0.0f, -1e3f, 0.0f};
        float pos4[] = {0.0f, -1e3f, -1e3f, 0.0f};

        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, pos, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, specular, 0);

        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, pos2, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, ambient, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, specular, 0);

        gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_POSITION, pos2, 0);
        gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_AMBIENT, ambient, 0);
        gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_SPECULAR, specular, 0);

        gl.glLightfv(GL2.GL_LIGHT3, GL2.GL_POSITION, pos4, 0);
        gl.glLightfv(GL2.GL_LIGHT3, GL2.GL_AMBIENT, ambient, 0);
        gl.glLightfv(GL2.GL_LIGHT3, GL2.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL2.GL_LIGHT3, GL2.GL_SPECULAR, specular, 0);


//        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_LIGHT0);
        gl.glEnable(GL2.GL_LIGHT1);
        gl.glEnable(GL2.GL_LIGHT2);
        gl.glEnable(GL2.GL_LIGHT3);
        gl.glEnable(GL2.GL_DEPTH_TEST);

        geometry_list = gl.glGenLists(1);
        gl.glNewList(geometry_list, GL2.GL_COMPILE);
        gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, grey, 0);
        loadObjModel(gl);
        gl.glEndList();

        gl.glEnable(GL2.GL_NORMALIZE);

//        MouseListener mouseListener = new ViewerMouseAdapter();
//        KeyListener keyListener = new ViewerKeyAdapter();
        java.awt.Component comp = (java.awt.Component) drawable;
//        new AWTMouseAdapter(mouseListener, drawable).addTo(comp);
//        new AWTKeyAdapter(keyListener, drawable).addTo(comp);

        send(new Message(new JSONObject("{'result':{'status':'obj file viewer initiated'}}")));
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        System.out.println("Viewer: Reshape "+x+"/"+y+" "+width+"x"+height);
        GL2 gl = drawable.getGL().getGL2();

        gl.setSwapInterval(swapInterval);

        float h = (float)height / (float)width;

        gl.glMatrixMode(GL2.GL_PROJECTION);

        gl.glLoadIdentity();
        gl.glFrustum(-1.0f, 1.0f, -h, h, 1.0f, 100000.0f);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();

//        gl.glScalef(view_scale, view_scale, view_scale);
//        gl.glRotatef(view_rotx, 1.0f, 0.0f, 0.0f);
//        gl.glRotatef(view_roty, 0.0f, 1.0f, 0.0f);
//        gl.glRotatef(view_rotz, 0.0f, 0.0f, 1.0f);
//        gl.glTranslatef(view_translatex, view_translatey, view_translatez);
//        gl.glTranslatef(0.0f, 0.0f, -10000.0f);
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        setGeometry(0);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
//        angle += 2.0f;

        GL2 gl = drawable.getGL().getGL2();
        gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

        gl.glPushMatrix();

        glu.gluLookAt(camera_locationX, camera_locationY, camera_locationZ,
                camera_lookAtX, camera_lookAtY, camera_lookAtZ,
                camera_upX, camera_upY, camera_upZ);


        gl.glScalef(view_scale, view_scale, view_scale);

//        glu.gluPerspective(45.0f, 1.333, 0.001, 100000);

        gl.glCallList(geometry_list);
        gl.glPopMatrix();
    }

    public void bindTexture() {
    }

    public void parseObjModel() {
        Build builder = new Build();

        try {
            Parse parser = new Parse(builder, fileInput);

            if (max_x < builder.maxX) max_x = builder.maxX;
            if (max_y < builder.maxY) max_y = builder.maxY;
            if (min_x > builder.minX) min_x = builder.minX;
            if (min_y > builder.minY) min_y = builder.minY;

            extentX = max_x - min_x;
            extentY = max_y - min_y;

            Iterator it = builder.groups.entrySet().iterator();
            while(it.hasNext()) {
                Map.Entry group = (Map.Entry) it.next();

//                System.out.println(group.getKey());

                if (geometry.containsKey(group.getKey())) {
                    geometry.get(group.getKey()).clear();
                } else geometry.put((String) group.getKey(), new ArrayList<>());

                geometry.put((String) group.getKey(), (ArrayList<Face>) group.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void loadObjModel(GL2 gl) {
        gl.glShadeModel(GL2.GL_FLAT);

        long startTime = System.nanoTime();
        gl.glNormal3f(0.0f, 0.0f, 1.0f);
        Iterator it_g = geometry.entrySet().iterator();
        while(it_g.hasNext()) {
            Map.Entry group = (Map.Entry) it_g.next();
            for(Face face: (ArrayList<Face>)group.getValue()) {
                gl.glBegin(GL2.GL_TRIANGLES);
                for(FaceVertex faceVertex: face.vertices) {
                    gl.glVertex3f(faceVertex.v.x, faceVertex.v.y, faceVertex.v.z);
                    gl.glNormal3f(face.faceNormal.x, face.faceNormal.y, face.faceNormal.z);
                }
                gl.glEnd();
            }
        }
        System.out.println(System.nanoTime() - startTime);

        gl.glEnable(GL2.GL_TEXTURE_2D);
        try{
            File im = new File("image_test.png");
            Texture t = TextureIO.newTexture(im, true);
            texture = t.getTextureObject(gl);
        }catch(IOException e){
            System.out.println("no texture loaded");
        }

        gl.glBindTexture(GL2.GL_TEXTURE_2D, texture);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(0.0f, 0.0f); gl.glVertex3f(min_x, min_y, 0.0f);
        gl.glTexCoord2f(1.0f, 0.0f); gl.glVertex3f(max_x, min_y, 0.0f);
        gl.glTexCoord2f(1.0f, 1.0f); gl.glVertex3f(max_x, max_y, 0.0f);
        gl.glTexCoord2f(0.0f, 1.0f); gl.glVertex3f(min_x, max_y, 0.0f);
//        gl.glTexCoord2f(0.0f, 0.0f); gl.glVertex3f(0, 0, 0.0f);

        gl.glEnd();
        gl.glFlush();

    }

    @Override
    public String getDescription() {
        return "OpenGL viewer for OBJ file format";
    }

    protected JSONObject specifyInputs() {
        return new JSONObject("{'run':'scenario.Viewer'," +
                "'XOR geometry':'attachment','XOR ScID':'number'," +
                "'OPT height':'number','OPT width':'number'," +
                "'OPT positionX':'number','OPT positionY':'number'," +
                "'OPT cameraID':'number'}");
    }

    protected JSONObject specifyOutputs() {
        return new JSONObject("{'XOR result':'json','XOR error':'string'}");
    }

    protected JSONObject exampleCall() {
        return new JSONObject("{'run':'scenario.Viewer', 'geometry':'attachment'}");
    }

    @Override
    protected ResponseHandler newResponseHandler() {

        return new RemoteServiceResponseHandler(){

            public void processResult(Message m) {
//                System.out.println(m.getHeader());
//                System.out.println(m.getHeader().getJSONObject("result"));
                JSONObject h = m.getHeader();
                System.out.println(m);
                if(h.getString("serviceName").equals("DistanceToWalls")) {
                    BufferedImage img = new BufferedImage(t_width, t_height, BufferedImage.TYPE_INT_RGB);
                    Graphics2D graphics = img.createGraphics();

                    System.out.println("marker");

                    Attachment att = m.getAttachmentsSortedByPosition()[0];
                    try {
                        InputStream is = Attachment.getInputStream(att);
//                        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                        int nRead;
                        byte[] data = new byte[4];

                        ArrayList<Float> floats = new ArrayList<Float>();
                        float max_dist = Float.MIN_VALUE;
                        float min_dist = Float.MAX_VALUE;

                        while ((nRead = is.read(data, 0, data.length)) != -1) {
                            float f =  ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                            floats.add(f);

                            System.out.println(f);

                            if(max_dist < f) max_dist = f;
                            if(min_dist > f) min_dist = f;
                        }


                        for(int i = 0; i < t_height; i++) {
                            for(int j = 0; j < t_width; j++) {
                                int index = i*t_width + j;
                                float dist = floats.get(index);
                                System.out.println("index: " + index);
                                float scaled = (dist - min_dist)*(1/(max_dist - min_dist));
                                if (scaled <= 0.5) {
                                    graphics.setColor(new Color(0, 2f*scaled, 1 - 2f*scaled, 1.0f));
                                } else {
                                    graphics.setColor(new Color(2f*(scaled - 0.5f), 1 - 2f*(scaled - 0.5f), 0, 1.0f));
                                }
                                System.out.println("value: " + dist);
                                System.out.println("x: " + j);
                                System.out.println("y: " + i);

                                graphics.fillRect(j, t_height - i - 1, 1, 1);
                            }
                        }

                        System.out.println("write image_test.png");
                        File outputfile = new File("image_test.png");
                        ImageIO.write(img, "png", outputfile);

                        canvas.disposeGLEventListener(Viewer.this, true);
                        canvas.addGLEventListener(Viewer.this);
                        canvas.display();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(h.getString("serviceName").equals("scenario.geojson.Update")) {
                    System.out.println("update geometry");

                    Attachment att = m.getAttachmentsSortedByPosition()[0];
                    try {
                        fileInput = Attachment.getInputStream(att);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    parseObjModel();

                    canvas.disposeGLEventListener(Viewer.this, true);
                    canvas.addGLEventListener(Viewer.this);
                    canvas.display();

                }
                else if(h.getString("serviceName").equals("scenario.obj.Get")) {
                    System.out.println("load new geometry");

                    JSONObject r = h.getJSONObject("result");
                    System.out.println(r);
                    if(r.has("deletedIDs")) {
                        JSONArray deletedIDs = r.getJSONArray("deletedIDs");
                        for (int i = 0; i < deletedIDs.length(); i++) {
                            String id = deletedIDs.get(i).toString();
                            geometry.remove(id);
                        }
                    }

                    Attachment att = m.getAttachmentsSortedByPosition()[0];
                    try {
                        fileInput = Attachment.getInputStream(att);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    parseObjModel();

                    if(!is_init) {
                        canvas.addGLEventListener(Viewer.this);
                        frame.add(canvas, java.awt.BorderLayout.CENTER);
                        frame.validate();
                        frame.setVisible(true);

//                        animator.start();

                        is_init = true;
                    }
                    else {
                        canvas.disposeGLEventListener(Viewer.this, true);
                        canvas.addGLEventListener(Viewer.this);
                        canvas.display();
                    }


                    t_height = 0;
                    t_width = 0;
                    
                    runIfExists("DistanceToWalls", () -> {
                    	System.out.println("max x:" + max_x);
                        System.out.println("max y:" + max_y);
                        System.out.println("min x:" + min_x);
                        System.out.println("min y:" + min_y);
                        // creation of grid
                        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                        for(float i = min_y; i <= max_y; i+=1) {
                            for(float j = min_x; j <= max_x; j+=1) {
                                byte[] bi = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(j).array();
                                byte[] bj = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(i).array();
                                byte[] b0 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(0).array();

                                byteStream.write(bi,0,4);
                                byteStream.write(bj,0,4);
                                byteStream.write(b0,0,4);

                                t_width++;
                            }
                            t_height++;
                        }

                        t_width /= t_height;

                        ByteBuffer byteBuffer = ByteBuffer.wrap(byteStream.toByteArray());
                        AttachmentAsArray attachment = new AttachmentAsArray("float32array", "grid", byteBuffer);
//                attachment.convertTo(Attachment.NETWORK);

                        Message distance_to_walls_message = new Message(new JSONObject()
                                .put("run", "DistanceToWalls")
                                .put("ScID", ScID)
                                .put("mode", "points")
                                .put("points", attachment));
                        send(distance_to_walls_message);
                    });
                }
                else if(h.getString("serviceName").equals("scenario.camera.Update") || h.getString("serviceName").equals("scenario.camera.Get")) {
                    JSONObject result = h.getJSONObject("result");
//                    System.out.println(result);

                    camera_locationX = (float)result.getJSONObject("location").getDouble("x");
                    camera_locationY = (float)result.getJSONObject("location").getDouble("y");
                    camera_locationZ = (float)result.getJSONObject("location").getDouble("z");

                    camera_lookAtX = (float)result.getJSONObject("lookAt").getDouble("x");
                    camera_lookAtY = (float)result.getJSONObject("lookAt").getDouble("y");
                    camera_lookAtZ = (float)result.getJSONObject("lookAt").getDouble("z");

                    camera_upX = (float)result.getJSONObject("cameraUp").getDouble("x");
                    camera_upY = (float)result.getJSONObject("cameraUp").getDouble("y");
                    camera_upZ = (float)result.getJSONObject("cameraUp").getDouble("z");

                    view_scale = (float)result.getDouble("scale");
                    canvas.display();
                }
            }
            public Message implementation(Message m) {
                JSONObject h = m.getHeader();
                if(h.has("width")) {
                    width = m.getHeader().getInt("width");
                }
                if(h.has("height")) {
                    height = m.getHeader().getInt("height");
                }
                if(h.has("positionX")) {
                    positionX = m.getHeader().getInt("positionX");
                }
                if(h.has("positionY")) {
                    positionY = m.getHeader().getInt("positionY");
                }

                frame.setSize(width, height);
                frame.setLocation(positionX, positionY);

                if(m.getHeader().has("geometry")) {
                    Attachment att = m.getAttachmentsSortedByPosition()[0];
                    try {
                        fileInput = Attachment.getInputStream(att);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    canvas.addGLEventListener(Viewer.this);
                    frame.add(canvas, java.awt.BorderLayout.CENTER);
                    frame.validate();

                    frame.setVisible(true);
//                    animator.start();

                    is_init = true;
                }
                else if(m.getHeader().has("ScID")) {
                    ScID = m.getHeader().getInt("ScID");
                    send(new Message(new JSONObject("{'run':'scenario.obj.Get', 'ScID':" + ScID + "}")));
                    JSONObject scenarioSubscribe = new JSONObject().put("run", "scenario.SubscribeTo")
                            .put("ScIDs", new JSONArray().put(ScID))
                            .put("format", "obj");
                    System.out.println(scenarioSubscribe);
                    send(new Message(scenarioSubscribe));
                }
                if(m.getHeader().has("cameraID")) {
                    cameraID = m.getHeader().getInt("cameraID");
                    send(new Message(new JSONObject("{'run':'scenario.camera.Get', 'cameraID':" + cameraID + "}")));
                    send(new Message(new JSONObject("{'run':'scenario.camera.SubscribeTo', 'cameraID':" + cameraID + "}")));
//                    System.out.println("cameraID: " + cameraID);
                }

//                try {
//                    fileInput = new FileInputStream("/home/aforino/ETH/iA/testfiles/obj/ZH_ESUM.obj");
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
                return new Message(new JSONObject("{'result':{'status':'started'}}"));
            }
        };
    }

//    class ViewerKeyAdapter extends KeyAdapter {
//        @Override
//        public void keyPressed(KeyEvent e) {
//            int kc = e.getKeyCode();
//            if(KeyEvent.VK_LEFT == kc) {
//                view_roty -= 1;
//            } else if(KeyEvent.VK_RIGHT == kc) {
//                view_roty += 1;
//            } else if(KeyEvent.VK_UP == kc) {
//                view_rotx -= 1;
//            } else if(KeyEvent.VK_DOWN == kc) {
//                view_rotx += 1;
//            }
//        }
//    }
//
//    class ViewerMouseAdapter extends MouseAdapter {
//        boolean bRot = false;
//        boolean bScale = false;
//
//        @Override
//        public void mouseWheelMoved(MouseEvent e) {
//            view_scale += e.getRotation()[1];
//        }
//
//        @Override
//        public void mousePressed(MouseEvent e) {
//            if(e.getButton() == MouseEvent.BUTTON1) {
//                bRot = true;
//                bScale = false;
//            }
//            else if(e.getButton() == MouseEvent.BUTTON2 ||
//                    e.getButton() == MouseEvent.BUTTON4 ||
//                    e.getButton() == MouseEvent.BUTTON5) {
//                bRot = false;
//                bScale = true;
//            }
//            else {
//                bRot = false;
//                bScale = false;
//            }
//
//            prevMouseX = e.getX();
//            prevMouseY = e.getY();
//        }
//
//        @Override
//        public void mouseReleased(MouseEvent e) {
//        }
//
//        @Override
//        public void mouseDragged(MouseEvent e) {
//            final int x = e.getX();
//            final int y = e.getY();
//            Object source = e.getSource();
//            GLAutoDrawable glad = (GLAutoDrawable) source;
//            int width = glad.getSurfaceWidth();
//            int height = glad.getSurfaceHeight();
//            float transX = 10.0f*(float)(x-prevMouseX)/(float)width;
//            float transY = 10.0f*(float)(prevMouseY-y)/(float)height;
//
//            float thetaY = 360.0f * ( (float)(x-prevMouseX)/(float)width);
//            float thetaX = 360.0f * ( (float)(prevMouseY-y)/(float)height);
//
//            prevMouseX = x;
//            prevMouseY = y;
//
//            if(bRot) {
//                view_rotx += thetaX;
//                view_roty += thetaY;
//            }
//            if(bScale) {
//                view_translatex += transX*(maxExtent/100);
//                view_translatey += transY*(maxExtent/100);
//            }
//        }
//    }
}