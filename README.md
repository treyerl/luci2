#** Luci - Lightweight Urban Computation Interchange **#

[Article in Spatial Information Research](http://link.springer.com/article/10.1007%2Fs41324-016-0025-y)

# **Who is it for?**
![WhoIsItFor.jpg](https://bitbucket.org/repo/M9EBx5/images/423764332-WhoIsItFor.jpg)
Originally developed for urban planning, LUCI can be used for multiple purposes. Feel free to interpret the "U" in LUCI as "universal" and start using it, even if you don't work on urban planning related projects.

#**What is it for?**
![WhatIsItFor.jpg](https://bitbucket.org/repo/M9EBx5/images/1498426150-WhatIsItFor.jpg)The image shows some ideas what LUCI could be used for (bottom row) and/or what it is currently being developed for (top row).

# **How do I use it?**

## **Download** Executables

Choose a binary from the [Download Page](https://bitbucket.org/treyerl/luci2/downloads), unpack the zip and double click on the jar file.

* __NOTE__ 1: You need [`Java8`](http://www.oracle.com/technetwork/java/javase/downloads/index.html) to run Luci.
* __NOTE__ 2: as we are currently developing Luci always check the date of binaries.
## **Screencast** Basics

There is a 3min screencast getting you started (click on the image below)  

[![run3.gif](https://bitbucket.org/repo/M9EBx5/images/4020710662-run3.gif)](https://vimeo.com/152971479)

## **Download**, compile from source and run

First, you must have [`git`](https://git-scm.com),
[`JDK`](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (preferably Java 8),
and 
[`maven`](https://maven.apache.org/).
Given Java and maven are set up correctly, running luci as easy as running these four commands:

    git clone https://bitbucket.org/treyerl/luci2.git
    cd luci2
    mvn clean install
    
choose:

* `mvn exec:java -pl core` to run Luci without the scenario module (=without the geometry repository)
* `mvn exec:java -pl scenario` to run Luci including the scenario module
* `mvn exec:java -pl core -PmbTileReader` or `mvn exec:java -pl scenario -PmbTileReader` to make the integrated webserver read .mbtiles-files exported from [TileMill](https://www.mapbox.com/tilemill/) or QGIS' [QTiles](https://github.com/nextgis/QTiles) plugin

## **API**

The API documentation (2016-08-05) of the built-in services is available [here](http://treyerl.bitbucket.org/luci/api.html).
Once you run Luci, you can also [refer to your local API](http://localhost:8080/api.html) which also includes your currently connected remote services.

##Using the **console**: 

The console provides a simple way for developers to test their services.

* go to [Luci's workflow web-editor / standard page](http://localhost:8080)
* click on the console button ![console.png](https://bitbucket.org/repo/M9EBx5/images/2135740918-console.png) at the bottom left corner
* the second icon from the left opens a snippet file from which you can copy/paste test commands to the console
* a console window as shown below should open up
* type `connect` to connect to localhost:7654, Luci's default address
* type `list commands` to see the commands of the client console
* type `list services` to get a list of all available services
* type `run <servicename> <parameters1={'key1':'value1'}>` to run services, e.g. `run test.Fibonacci amount=4` or `run RemoteRegister serviceName=test exampleCall={'run':'test'} description="A RemoteService for testing service registration."` Note that parameters are separated by whitespaces and the values in the `key=value pair` can have json syntax. If your value is a string with whitespaces put it into `"quotes"`. 
* **TAB completion**: when typing `run test.Fib` the `onacci` can be completed by typing `TAB`. This works for service names as well as file names.
* type `json 0` so that json outputs are on one line
* type `example <servicename>` to get an example, e.g. `example test.Fibonacci`
* copy the example call you get
* type `json 4` to reset json indentation
* paste the example call you just copied (and press enter)
![Screen Shot 2016-08-05 at 00.16.41.png](https://bitbucket.org/repo/M9EBx5/images/2754745858-Screen%20Shot%202016-08-05%20at%2000.16.41.png)


# **What is it?** 
![WhatIsIt.jpg](https://bitbucket.org/repo/M9EBx5/images/2020884011-WhatIsIt.jpg)
The whole project has a modular structure which is being reflected in the following enumeration:

`luci.connect`(for clients, used by Luci as well) [more](https://bitbucket.org/treyerl/luci2/raw/master/connect/) :

* Asynchronous Calls (buzzword: push-notifications)
* a message protocol similar to JSON-RPC including binary attachments and self-recovery procedure

`luci.core` [more](https://bitbucket.org/treyerl/luci2/raw/master/core/):

* Remote Procedures (Services) on one place (middleware)
* connections through TCP-sockets and Websockets supported (linking from one to the other if necessary)
* Remote/Server Events
* Remote Workflow Management-API
* Embedded Webserver; adapted to Luci's needs
* Web-based Workflow Editor
* Web-based Service Administration: install, remove, start, stop services on any machine in your network that runs Luci's ServiceControl (see module below)
* easily promote a service with one single registration call
* optionally implement a job splitter to share the workload of a single service call among your connected worker nodes
* service permissions per user / usergroup

`luci.scenario` [more](https://bitbucket.org/treyerl/luci2/raw/master/scenario/):

* this is the default implementation of a data repository; you can implement your own
* GIS-based geometry repository
* history with all changes
* timestamp based updates when using geojson
* supported formats: geojson, obj

`luci.mbTileReader` [more](https://bitbucket.org/treyerl/luci2/raw/master/mbTileReader/):

* embedded tile server for mbtiles files (=host maps directly on luci)

`luci.ServiceControl` [more](https://bitbucket.org/treyerl/luci2/raw/master/ServiceControl/):

* a client for remotely installing, removing, starting, stopping services

`luci.viewer` [more](https://bitbucket.org/treyerl/luci2/raw/master/viewer/):

* a Java/OpenGL-based 3D viewer used to visualize scenarios on multiple screens / devices


#How is it **implemented**?

* Using Java's NIO package for networking
* modular maven project
* written entirely in Java with pure java libraries (e.g. H2GIS, except for the mbTileReader that reads sqlite files)
* connection libraries for clients / services in [Haskell](https://github.com/achirkin/qua-kit/tree/master/libs/hs/luci-connect), [C++ library](https://bitbucket.org/treyerl/libluci_c), [lucipy](https://bitbucket.org/treyerl/lucipy/overview) for python 3.4+, [luc#](https://bitbucket.org/treyerl/luc), Java [`luci.connect`](https://bitbucket.org/treyerl/luci2/raw/master/connect/), Javascript

# How do I **connect** to it? How do I adapt my model / functionality into a service?

* [Implement a service as a Luci module (running as plugin to luci) in Java](https://bitbucket.org/treyerl/luci2/wiki/Import%20Luci%20as%20your%20webserver%20and_or%20implement%20a%20service%20as%20a%20Luci%20module)
* [Implement a Remote Service in Java](https://bitbucket.org/treyerl/luci2/wiki/Create%20a%20new%20Remote%20Service%20in%20Java)
* Use the one of the following libraries to connect your client or remote service:
	* Java: see [`luci.connect`](https://bitbucket.org/treyerl/luci2/raw/master/connect/)	
	* Python: [lucipy](https://bitbucket.org/treyerl/lucipy/overview) - connect to luci using python 3.4+
	* C#, [luC#](https://bitbucket.org/treyerl/luc)
	* [Haskell](https://github.com/achirkin/qua-kit/tree/master/libs/hs/luci-connect)
	* [C++ library](https://bitbucket.org/treyerl/libluci_c)
	* [Javascript](https://bitbucket.org/treyerl/luci2/raw/master/core/src/main/resources/luci/core/web/luciConnect.js?at=master&fileviewer=file-view-default): available at [localhost:8080/luciConnect.js](localhost:8080/luciConnect.js), test either with your browser's console or the javascript console available [localhost:8080/console/](http://localhost:8080/console/)
* [Luci Message Protocol](https://bitbucket.org/treyerl/luci2/wiki/Luci%20networking%20or%20how%20to%20write%20a%20connection%20library%20in%20your%20preferred%20language) to implement a connection library in your preferred language
* __hidden trick__: in the console type `debug` to turn on debugging which makes the console print out otherwise hidden information such as the compiled JSON string being sent to LUCI or progress notifications. This interactively teaches you a lot about the messaging protocol.
* look at the [API](http://treyerl.bitbucket.org/luci/api.html) or [your local version of if](http://localhost:8080/api.html)

## How do I **work on Luci itself**?

* Check out this git repository. The most simple way to do it, is with an IDE, e.g. Eclipse: Window --> Show View --> Other: Choose "Git Repositories". In that view click on the symbol with the green arrow and paste the URL of this repository in the form. On the last page of that form choose the location on your disk where the repository should be copied to.
* File --> Import... --> Maven > Existing Maven Projects. For Root Directory choose the directory to which you cloned the repository before. Luci consists out of 6 submodules.
* To run Luci-Core in Eclipse (build main project first before building submodules):
![run_luci_eclipse.jpg](https://bitbucket.org/repo/M9EBx5/images/1902531848-960792455-run_luci_eclipse.jpg)

## Who do I **talk to**?
* Create [an issue](https://bitbucket.org/treyerl/luci2/issues)
* [Lukas Treyer](https://www.ia.arch.ethz.ch/msc-lukas-treyer/), ETH Zurich
* [Artem Chirkin](https://www.ia.arch.ethz.ch/chirkin/), ETH Zurich
* [Reinhard Koenig](http://www.uni-weimar.de/en/architecture-and-urbanism/chairs/computational-architecture/), University of Weimar
* [Bernhard Klein](http://www.fcl.ethz.ch/person/dr-bernhard-klein/), FCL Singapore