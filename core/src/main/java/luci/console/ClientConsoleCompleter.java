/** MIT License
 * @author Lukas Treyer
 * @date Dec 17, 2015
 * */
package luci.console;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;

import jline.console.completer.FileNameCompleter;
import luci.connect.Message;

/**Used for code completion that is being initiated by pressing "Tab" key. 
 * 
 * @author Lukas Treyer
 *
 */
public class ClientConsoleCompleter extends FileNameCompleter {
	ClientConsole console;
	CountDownLatch cdl;
	String last;
	Set<String> serviceNames = new HashSet<>();
	List<String> serviceCompletionCommands = Arrays.asList(new String[]{"run","inputs","outputs","info"}); 
	
	ClientConsoleCompleter(ClientConsole c){
		console = c;
	}
	
	/**Prints an error.
	 * @return The index of the buffer for which the completion will be relative
	 */
	private int printErr(){
		console.print(new JSONObject().put("error", "service list not available; connected?"));
		return 0;
	}
	
	/**Completes service names; calls luci and waits 2 seconds for the answer to get a list with 
	 * all service names.
	 * @param command String the name of the command that processes the service name (e.g. info, input, run, etc)
	 * @param bufferPiece String the piece that should be completed
	 * @param candidates List&lt;CharSequence&gt; 
	 * @return The index of the buffer for which the completion will be relative
	 */
	private int serviceCompletion(String command, String bufferPiece, List<CharSequence> candidates){
		if (console.con == null) return printErr();
		cdl = new CountDownLatch(1);
		try {
			console.con.sendAndReceive(new Message(new JSONObject().put("run", "ServiceList")), 
			console.new CallHandler(){
				@Override
				public void processResult(Message m){
					JSONObject json = m.getHeader();
					JSONArray services = json.getJSONObject("result").getJSONArray("serviceNames");
					for (int i = 0; i < services.length(); i++){
						serviceNames.add(services.getString(i));
					}
					cdl.countDown();
				}
			});
			cdl.await(2,TimeUnit.SECONDS);
			if (bufferPiece.equals("")){
				candidates.addAll(serviceNames);
			} else {
				List<String> srvCandidates = serviceNames.stream().filter(
						s -> s.startsWith(bufferPiece)).collect(Collectors.toList());
				candidates.addAll(srvCandidates);
			}
			return command.length() + 1;	
			
		} catch (InterruptedException e) {
			return printErr();
		}
		
	}
	
	@Override
	public int complete(String buffer, final int cursor, final List<CharSequence> candidates) {
		String[] split = buffer.split("\\s");
		int len = 0;
		for (int i = 0; i < split.length -1; i++) len += split[i].length()+1;
		final String bufferPiece = split[split.length-1];
		
		if (serviceCompletionCommands.contains(split[0])) {
			if (split.length > 2) {
				if (bufferPiece.contains("=")) {
					String[] attSplit = bufferPiece.split("=");
					String file = attSplit.length > 1 ? attSplit[1] : "";
					len += attSplit[0].length()+1;
					return len + super.complete(file, cursor, candidates);
				}
				else return len;
			} 
			else if (split.length == 2) return serviceCompletion(split[0], split[1], candidates);
			else if (split.length == 1 ) return serviceCompletion(split[0], "", candidates);
		}

		List<String> commandCandidates = console.methods.keySet().stream().filter(
				s -> s.startsWith(bufferPiece)).collect(Collectors.toList());
//		trace(""+commandCandidates);
		if (commandCandidates.size() > 0){
			candidates.addAll(commandCandidates);
			return len;
		}
//		trace("file completion");
		return len + super.complete(bufferPiece, cursor, candidates);
	}
}
