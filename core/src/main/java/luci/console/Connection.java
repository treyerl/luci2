/** MIT License
 * @author Lukas Treyer
 * @date Dec 15, 2015
 * */
package luci.console;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;

import org.json.JSONObject;

import com.esotericsoftware.minlog.Log;

import luci.connect.Attachment;
import luci.connect.LcClient;
import luci.connect.Message;
import luci.connect.TcpSocketHandler;

/**The lcClient being used to communicate with luci. In addition to a regular lcClient it knows
 * about the ClientConsole that is using it in order to print error message to console.
 * @author Lukas Treyer
 *
 */
class Connection extends LcClient {	
	InetSocketAddress isa;
	ClientConsole console;
	boolean isEarlyResult;
	
	Connection(ClientConsole c){
		console = c;
		debug = true;
		onEarlyResult((tcp)->isEarlyResult=true);
		postRead((tcp)->{
			if (console.ISLOOPBACK && isEarlyResult){
				if (tcp.getAmountAttachmentBytesRead() < tcp.getAmountAttachmentBytesWritten())
					tcp.suppressWriteInterest();
				else tcp.restoreWriteInterest();
			}
		});
		postWrite((tcp)->{
			if (console.ISLOOPBACK){
				Message msg = tcp.getCurrentMessageToWrite();
				if (msg != null && !msg.writtenOnce() && msg.hasAttachments())
					tcp.suppressWriteInterest();
			}
		});
	}
	
	/**can be called only when thread is running - otherwise selector is null
	 * @param key
	 */
	public InetSocketAddress connect(String hostname, int port) {
		return (isa = super.connect(hostname, port));
	}
	
	protected TcpSocketHandler confirmConnection(SelectionKey key) throws IOException{
		try {
			TcpSocketHandler sh = super.confirmConnection(key);
			console.print("    "+console.green+"connected"+console.normal+" to "+isa);
			return sh;
		} catch (Exception e){
			throw new IOException();
		}
		
	}
	
	public int howToHandleAttachments() {
		return Attachment.FILE;
	}
	
	public void run(){
		connect(console.hostname, console.port);
		super.run();
	}
	
	public boolean isConnected(){
		return selector != null;
	}

	@Override
	protected boolean processException(Exception e) {
		console.print(new JSONObject().put("error", e.toString()));
		console.print(ClientConsole.PRE_PROGRESS()+"closing connection");
		console.con = null;
		if (Log.TRACE) e.printStackTrace();
		return true;
	}
	
	@Override
	protected boolean processIOException(SelectionKey key, IOException e){
		ResponseHandler rsp = getStdResponseHandler();
		if (rsp != null) rsp.processError(new Message(new JSONObject().put("error", "connection failure")));
		console.con = null;
		return true;
	}

	@Override
	protected ResponseHandler newResponseHandler() {
		return console.newCallHandler();
	}
	
}
