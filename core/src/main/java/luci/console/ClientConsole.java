/** MIT License
 * Copyright (c) 2015 Lukas Treyer, ETH Zurich
 * @author Lukas Treyer
 * @date Dec 6, 2015
 * */
package luci.console;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONWriter;

import com.esotericsoftware.minlog.Log;

import jline.console.ConsoleReader;
import jline.console.CursorBuffer;
import luci.connect.AttachmentAsFile;
import luci.connect.AttachmentSpot;
import luci.connect.LcClient.ResponseHandler;
import luci.connect.LcString;
import luci.connect.Message;
import luci.console.JSONParent.SortedKeysJSONObject;

/**THE Luci (example / console) CLIENT </br>
 * NIO reference: http://adblogcat.com/asynchronous-java-nio-for-dummies/
 */
public class ClientConsole implements Runnable {
	
	/* COMMANDLINE COLORS */
	final String black;
	final String red;
	final String green;
	final String yellow;
	final String blue;
	final String pink;
	final String cyan;
	final String grey;
	final String bold;
	final String normal;
	final String resetColor;
	
	/* prefixes to be printed before the corresponding line of output */
	private static String PRE_OK = "--> ";
	private static String PRE_OK_WITH_ERRORS = "--> ";
	private static String PRE_ERROR = "ERR ";
	private static String PRE_PROGRESS = "....";
	private static String PRE_RUN = "RUN ";
	private static String PRE_TIME = "";
	
	public static String PRE_OK(){ return PRE_OK; }
	public static String PRE_OK_WITH_ERRORS(){ return PRE_OK_WITH_ERRORS; }
	public static String PRE_ERROR(){ return PRE_ERROR; }
	public static String PRE_PROGRESS(){ return PRE_PROGRESS; }
	public static String PRE_RUN(){ return PRE_RUN; }
	public static String PRE_TIME(){ return PRE_TIME;}
	
	/* used for terminal commands that retrieve information about services */
	final int INFO = 0;
	final int INFO_INPUTS = 1;
	final int INFO_OUTPUTS = 2;
	final int INFO_EXAMPLE = 3;	
	String[] types = new String[]{"info","inputs","outputs","example"};
	
	static {
		Log.set(Log.LEVEL_TRACE);
//		Log.set(Log.LEVEL_INFO);
	}

	/**Standard response handler of ClientConsole. Prints all responses to the console.
	 */
	public class CallHandler extends ResponseHandler{
		long callTime;
		
		protected CallHandler(){
			callTime = now();
		}
		
		long now(){
			return System.currentTimeMillis();
		}
		
		long duration(){
			return Math.max(now() - callTime, 1);
		}
		
		/**Handles results and replaces the json attachment representation with filename representation.
		 */
		public void processResult(Message m){
			con.isEarlyResult = false;
			for (AttachmentSpot as:  m.getAttachmentSpots()){
				as.getParent().put(as.getName(), 
					((AttachmentAsFile) m.getAttachmentByChecksum(as.getChecksum()))
						.getFile().getAbsolutePath());
			}
			if (TOFILE){
				try {
					File f;
					int i = 0;
					do {
						f = new File(AttachmentAsFile.parentDirectory, m.getCallID()+""+(i++)+".json");
					} while(!f.createNewFile());
					FileWriter fw = new FileWriter(f);
					fw.write(m.getHeader().toString(jsonIndentation));
					fw.close();
					print("stored answer in "+f);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				print(duration(), m.getHeader());
			}
		}
		public void processProgress(Message m){
			print(duration(), m.getHeader());
		}
		public void processError(Message m){
			con.isEarlyResult = false;
			print(duration(), m.getHeader());
		}
	}
	
	/**ResponseHandler for any calls requesting information about services.
	 */
	class InfoHandler extends CallHandler {
		private int INFOTYPE;
		InfoHandler(int t){ INFOTYPE = t; }
		/**Expects results to contain information about services.
		 */
		@Override
		public void processResult(Message m){
			JSONObject json = m.getHeader();
			JSONObject services = json.getJSONObject("result");
			for (String name: services.keySet()){
				JSONObject info = services.getJSONObject(name);
				switch(INFOTYPE){
				case INFO_INPUTS: info = info.optJSONObject("inputs"); break;
				case INFO_OUTPUTS: info = info.optJSONObject("outputs"); break;
				case INFO_EXAMPLE: info = info.optJSONObject("example"); break;
				}
				print(bold+yellow+"    "+name);
				if (info == null) print(callTime - now(), 
						"    "+bold+red+"usage: "+yellow+types[INFOTYPE]
						+" serviceName1 serviceName2"+normal
						+" Type 'list services' to list services.");
				else {
					info = new SortedKeysJSONObject(info);
					int i = jsonIndentation;
					print(callTime - now(), normal+"    "+LcString.indent(info.toString(i),i));
				}
			}
		}
	}
	
	String hostname = InetAddress.getLocalHost().getHostName();
	int port = 7654, jsonIndentation = 4;
	BufferedReader instream;
	ConsoleReader reader;
	PrintWriter out;
	String prompt = "<-- ";
	boolean isColorSupported = System.console() != null;
	final Map<String, Method> methods = new TreeMap<String, Method>();
	Connection con;
	Thread conThread;
	boolean DEBUG = false, ISLOOPBACK = false, TOFILE = false;
	Set<String> serviceNames = new HashSet<>();
	private ClientConsoleCompleter ccc;
	
	/**Defines all the terminal color prefixes and the output line prefixes. Scans the ClientConsole
	 * class for protected methods which will be recognized at runtime as commands.
	 * @param hostAddress String
	 * @param port int
	 * @throws IOException
	 */
	protected ClientConsole() throws IOException{
		reader = new ConsoleReader();
        out = new PrintWriter(reader.getOutput());
		if (isColorSupported){
			black = "\u001B[30m";
			red = "\u001B[31m";
			green = "\u001B[32m";
			yellow = "\u001B[33m";
			blue = "\u001B[34m";
			pink = "\u001B[35m";
			cyan = "\u001B[36m";
			grey = "\u001B[37m";
			bold = "\u001B[1m";
			normal = "\u001B[0m";
			resetColor = "\u001B[39;49m";
			
			PRE_OK = bold+resetColor+"--> "+normal;
			PRE_OK_WITH_ERRORS = bold+red+"--> "+resetColor+normal;
			PRE_ERROR = bold+red+"ERR "+resetColor+normal;
			PRE_PROGRESS = cyan+"....";
			PRE_RUN = pink+"RUN ";
			PRE_TIME = blue;
			prompt = bold+yellow+"<-- "+resetColor+normal;
			
		} else {
			black = "";
			red = "";
			green = "";
			yellow = "";
			blue = "";
			pink = "";
			cyan = "";
			grey = "";
			bold = "";
			normal = "";
			resetColor = "";
		}
		reader.setPrompt(prompt);
		reader.setCopyPasteDetection(true);
		
		
		// put all PROTECTED methods into the methods map
		// ==> so if you want to create a new command for the console add a PROTECTED method
		for (Method m: getClass().getDeclaredMethods()){
			if ((m.getModifiers() & Modifier.PROTECTED) == Modifier.PROTECTED) 
				methods.put(m.getName(), m);
		}
		ccc = new ClientConsoleCompleter(this);
		reader.addCompleter(ccc);
	}
	
	private void setHostAddress(String h){
		this.hostname = h;
	}
	
	private void setPort(int p){
		this.port = p;
	}
	
	/**
	 * @return false if not connected, else true
	 */
	boolean conCheck(boolean silent) {
		if (con == null || !con.isConnected()) {
			if (!silent) print(new JSONObject().put("error", "not connected"));
			return false;
		}
		return true;
	}
	
	boolean conCheck(){
		return conCheck(false);
	}
	
	/**
	 * @return true if colored console output is supported else false
	 */
	public boolean isColorSupported(){
		return isColorSupported;
	}
	
	/**Lists console commands (like this one "list") or services by running ServiceList service
	 * @param commands_or_services
	 * @throws JSONException
	 * @throws IOException
	 */
	protected void list(String commands_or_services) throws JSONException, IOException{
		if (commands_or_services == null || commands_or_services.equals("commands")){
			for (Method m: methods.values()){
				out.print("    "+m.getName());
				for (Parameter p: m.getParameters()){
//					out.print(" '"+p.getType().getSimpleName()+"'");
					out.print(" '"+p.getName()+"'");
				}
				out.println();
			}
		} else if (commands_or_services.equals("services")) {
			if (conCheck()) {
				send(new JSONObject().put("run", "ServiceList"), new CallHandler(){
					public void processResult(Message m){
						JSONObject json = m.getHeader();
						print(duration(), yellow+"    available services:");
						JSONArray services = json.getJSONObject("result").getJSONArray("serviceNames");
						for (int i = 0; i < services.length(); i++){
							print(normal+"    "+services.getString(i));
						}
					}
				});
			}
		} else {
			print(PRE_ERROR+"unrecognized argument'"+commands_or_services+"'");
		}
	}
	
	protected void cancel(String callID){
		if (conCheck()){
			if (callID == null) {
				print(PRE_ERROR+"Missing callID!");
				return;
			}
			try {
				send(new JSONObject().put("cancel", Integer.parseInt(callID)));
			} catch (NumberFormatException e){
				print(PRE_ERROR+e.toString());
			}
		}
	}
	
	/**Toggles debug information on/off that is being printed before a message is being sent.
	 */
	protected void debug(){
		DEBUG = !DEBUG;
		print(blue+"<-- DEBUG::"+(DEBUG ? "ON":"OFF"));
	}
	
	/**
	 * Toggles whether answers should be stored to files
	 */
	protected void file(){
		TOFILE = !TOFILE;
		print(blue+"<-- TOFILE::"+(TOFILE ? "ON":"OFF"));
	}
	
	protected void loopback(){
		ISLOOPBACK = !ISLOOPBACK;
		print(blue+"<-- LOOPBACK::"+(ISLOOPBACK ? "ON":"OFF"));
	}
	
	protected void json(String s){
		if (s == null) s = "4";
		jsonIndentation = Integer.valueOf(s);
		print(blue+" - json indentation at "+s);
	}

	/**Connects using the given credentials.
	 * @param hostName String
	 * @param port int
	 */
	protected void connect(String hostName, String port) {
		if (conCheck(true/*silent*/)) disconnect();
		if (hostName != null) this.hostname = hostName;
		try {
			if (port != null) this.port = Integer.valueOf(port);
			con = new Connection(this);
			conThread = new Thread(con);
			conThread.start();
			
		} catch (NumberFormatException e){
			print(new JSONObject().put("error", e.toString()));
		}
	}
	
	/**Disconnects but does not exit the console.
	 */
	protected void disconnect(){
		if (conCheck()){
			conThread.interrupt();
			if (isColorSupported) print(normal+grey+"    disconnected"+resetColor);
			else print("    disconnected");
			con = null;
		}
	}
	
	
	/**Being called by info, example, inputs, outputs. All of them need to call "ServiceInfo", which
	 * is being executed in this method.
	 * @param info int info type: INFO, INFO_EXAMPLE, INFO_INPUTS, INFO_OUTPUTS
	 * @param serviceNames String[] list of services names
	 * @throws IOException
	 */
	private void info(int info, String[] serviceNames) throws IOException{
		if (conCheck()) {
			JSONObject command = new JSONObject().put("run", "ServiceInfo");
			if (serviceNames.length > 0) command.put("serviceNames", serviceNames);
			send(command, new InfoHandler(info));
		}
	}
	
	
	/**Displays all infos about the given serviceNames (input + outputs + exampleCalls).
	 * If no serviceName is given "info" shows the specification of jsongeometry and attachment. 
	 * @param serviceNames String[] list of services names
	 * @throws IOException
	 */
	protected void info(String...serviceNames) throws IOException{
		info(INFO, serviceNames);
	}
	
	/**Displays the example call of the services defined in serviceNames.
	 * @param serviceNames String[] list of services names
	 * @throws JSONException
	 * @throws IOException
	 */
	protected void example(String...serviceNames) throws JSONException, IOException{
		info(INFO_EXAMPLE, serviceNames);
	}
	
	/**Displays the inputs of the services defined in serviceNames.
	 * @param serviceNames String[] list of services names
	 * @throws JSONException
	 * @throws IOException
	 */
	protected void inputs(String...serviceNames) throws JSONException, IOException{
		info(INFO_INPUTS, serviceNames);
	}
	
	/**Displays the outputs of the services defined in serviceNames.
	 * @param serviceNames String[] list of services names
	 * @throws JSONException
	 * @throws IOException
	 */
	protected void outputs(String...serviceNames) throws JSONException, IOException{
		info(INFO_OUTPUTS, serviceNames);
	}
	
	/**Runs a service named with serviceName and all input parameters represented as a 
	 * key value pair that is divided/linked by a "=" symbol.
	 * @param serviceName Sting the name of the service to be run.
	 * @param inputs String[]{"key1=value1", "key2={'json':['is','being','parsed']}", 
	 * "key3=url?k1=v1&k2=v2"} where only the first "=" is being taken into account.
	 * @throws IOException
	 */
	protected void run(String serviceName, String...inputs) throws IOException{
		if (conCheck()) {
			Object firstName = serviceName;
			try {
				firstName = new JSONArray(serviceName);
			} catch (JSONException e){
				try {
					firstName = Integer.parseInt(serviceName);
				} catch (NumberFormatException e2){
					// firstName = serviceName
				}
			}
			JSONObject command = new JSONObject().put("run", firstName);
			for (String s: inputs){
				String[] pair = s.split("=", 2);
				Object value;
				if (pair.length < 2) value = JSONObject.NULL;
				else {
					value = pair[1];
					try {
						value = new JSONObject(pair[1]);
					} catch (JSONException e){
//						if (Log.DEBUG || Log.TRACE) e.printStackTrace();
						// value = pair[1];
					}
					try {
						value = new JSONArray(pair[1]);
					} catch(JSONException e){
						// value = pair[1];
					}
					try {
						if (value instanceof String){
							String sval = (String) value;
							if (sval.equals("true")) value = true;
							else if (sval.equals("false")) value = false;
							else if (sval.contains(".")) {
								value = Double.parseDouble(sval);
							} else {
								value = Integer.parseInt(sval);
							}
		 				}
					} catch (NumberFormatException e){
						// value = pair[1];
					}
				}
				command.put(pair[0], value);
			}
			send(command);
		}
	}
	
	/**Shutdown the console.
	 */
	protected void exit(){
		if (isColorSupported) print(blue+"Exit Luci ClientConsole"+resetColor+normal);
		out.flush();
		out.close();
		System.exit(0);
	}

	/**Thread loop waiting for console input
	 */
	public void run() {
		printInfo();
		String in;
		try {
			while((in = reader.readLine()) != null){
				String[] allargs = in.split("\\s+(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				String name = allargs[0];
				String[] args = new String[0];
				if (allargs.length > 1) args = new String[allargs.length - 1];
				System.arraycopy(allargs, 1, args, 0, args.length);
				try {
					JSONObject command = new JSONObject(name);
					send(command);
				} catch (JSONException e){
					if (methods.containsKey(name)){
						Method m = methods.get(name);
						int numParams = m.getParameterCount();
						Class<?>[] params = m.getParameterTypes();
						if (numParams == 2){
							Class<?> last = params[1];
							if (last.isArray()){
								
								if (args.length > 1) {
									String[] nArgs = new String[args.length - 1];
									System.arraycopy(args, 1, nArgs, 0, nArgs.length);
									Object[] inv = new Object[]{args[0], nArgs};
									m.invoke(this, inv);
								} else if (args.length > 0){
									m.invoke(this, new Object[]{args[0], new String[0]});
								} else {
									print(new JSONObject().put("error", "wrong number of arguments"));
								}
								
							} else {
								if (numParams != args.length){
									Object[] newArgs = new String[numParams];
									for (int i = 0; i < Math.min(numParams, args.length); i++){
										newArgs[i] = args[i];
									}
									m.invoke(this, newArgs);
								}
							}
						} else if (numParams == 1){
							if (params[0].isArray()) {
								m.invoke(this, new Object[]{args});
							} else {
								m.invoke(this, new Object[]{args.length > 0 ? args[0]:null});
//								Object[] nArgs = new Object[numParams];
//								System.arraycopy(args, 0, nArgs, 0, args.length);
							}
						} else m.invoke(this, (Object[]) args);
						
					} else {
						print(new JSONObject().put("error", "command '"+LcString.limit(name, 15)
							+"' not found"));
					}
				}
			}
		} catch (InvocationTargetException e){
			print(new JSONObject().put("error", e.getTargetException().toString()));
//			e.getTargetException().printStackTrace();
			exit();
		} catch (IOException | IllegalAccessException | IllegalArgumentException e) {
			print(new JSONObject().put("error", e.toString()));
//			e.printStackTrace();
			exit();
		}
	}
	
	/**send a JSONObject using the connection defined in the constructor.
	 * Scans the given JSONObject for any file attachments using scanForAndReplaceWithFileAttachments()
	 * @param command JSONObject
	 */
	private void send(JSONObject command) {
// DEBUGGING: send the message twice = stresstest for attachment forwarding.
//		JSONObject c2 = new JSONObject(command.toString());
		scanForAndReplaceWithFileAttachments(command);
		Message m = new Message(command);
		if (DEBUG) print(blue+"<-- "+m);
		if (conCheck()) con.sendAndReceive(m, newCallHandler());
//		scanForAndReplaceWithFileAttachments(c2);
//		if (conCheck()) con.sendAndReceive(new Message(c2), new CallHandler());
	}
	
	/**This method allows the TestOutputCompliance class to return its own CallHandler when testing
	 * registration and subscription services using the console
	 * @return
	 */
	protected ResponseHandler newCallHandler(){
		return new CallHandler();
	}
	
	private void send(JSONObject command, CallHandler ch) {
//		JSONObject c2 = new JSONObject(command.toString());
		scanForAndReplaceWithFileAttachments(command);
		if (DEBUG) print(blue+"<-- "+command);
		if (conCheck()) con.sendAndReceive(new Message(command), ch);
//		scanForAndReplaceWithFileAttachments(c2);
//		if (conCheck()) con.sendAndReceive(new Message(c2), ch);
	}

	/**Scans a json object for strings that can be resolved as pathnames and replaces those strings
	 * with AttachmentAsFile objects.
	 * @param json JSONObject
	 */
	private void scanForAndReplaceWithFileAttachments(JSONObject json) {
		Iterator<String> keys = json.keys();
		while(keys.hasNext()){
			String key = keys.next();
			Object val = json.get(key);
			if (val instanceof JSONObject){
				JSONObject j = (JSONObject) val;
				
				try {
					AttachmentAsFile a = checkJSONObject(j);
					if (a != null) json.put(key, a);
					else scanForAndReplaceWithFileAttachments((JSONObject)val);
				} catch (FileNotFoundException e){
					print(new JSONObject().put("error", key+": No such file: "+j.optString("attachment")));
				}
				
			} else if (val instanceof String){
				File file = new File((String) val);
				if (file.exists()){
					AttachmentAsFile a = new AttachmentAsFile(file.getAbsoluteFile());
					json.put(key, a);
				}
			} else if (val instanceof JSONArray){
				json.put(key, resolveJSONArray((JSONArray) val));
			}
		}
	}
	
	private AttachmentAsFile checkJSONObject(JSONObject j) throws FileNotFoundException{
		if (j.has("format") && j.has("attachment")){
			File file = new File(j.optString("attachment"));
			if (file.exists()){
				AttachmentAsFile a = new AttachmentAsFile(file.getAbsoluteFile());
				a.copyFrom(j);
				return a;
			} else {
				j.put("nonExisting", j.remove("attachment"));
				throw new FileNotFoundException();
			}
		}
		return null;
	}
	
	private JSONArray resolveJSONArray(JSONArray val){
		JSONArray j = (JSONArray) val;
		JSONArray replacement = new JSONArray();
		for (Object o: j){
			if (o instanceof JSONObject){
				JSONObject json = (JSONObject) o;
				try {
					AttachmentAsFile a = checkJSONObject(json);
					if (a != null) replacement.put(a);
					else {
						scanForAndReplaceWithFileAttachments(json);
						replacement.put(json);
					}
				} catch (FileNotFoundException e){
					print(new JSONObject().put("error", ": No such file: "+json.optString("attachment")));
				}
				
			} else if (o instanceof String){
				File file = new File((String) o);
				if (file.exists()){
					replacement.put(new AttachmentAsFile(file.getAbsoluteFile()));
				} else replacement.put(o);
			} else if (o instanceof JSONArray){
				replacement.put(resolveJSONArray((JSONArray) o));
			} else {
				replacement.put(o);
			}
		}
		return replacement;
	}
	
	/**Print a string to console.
	 * @param s String
	 */
	public void print(String s){print(0, s);}
	public void print(long duration, String s){
		try {
			String time = duration > 0 ? time(duration) : "";
			CursorBuffer stashed = reader.getCursorBuffer().copy();
			if (isColorSupported) {
				reader.getOutput().write("\u001b[1G\u001b[K");
				time = PRE_TIME+time+resetColor+normal;
			}
			out.println(s+" "+time);
//			out.flush();
			if (isColorSupported) 
				reader.resetPromptLine(reader.getPrompt(), stashed.toString(), stashed.cursor);
			else out.flush();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	static String time(long millis){
		return time(millis, false);
	}
	
	static String longtime(long millis){
		return time(millis, true);
	}
	
	static String time(long millis, boolean longtime){
		int MS = (int) (millis % 1000);
		int s = (int) millis / 1000;
		int S = s % 60;
		int m = s / 60;
		int M = m % 60;
		int h = m / 60;
		int H = h % 24;
		int d = h / 24;
		int D = d % 7;
		int w = d / 7;
		if (longtime) return String.format("%01d weeks %01d days %02d:%02d:%02d", w,D,H,M,S,MS);
		return String.format("%02d:%02d:%02d:%03d", h,M,S,MS);
	}
	
	/**Print a json object to console. Based on the principle key [result, error, progress, callID, 
	 * run] that is present the output color is being adjusted is available. 
	 * @param json JSONObject
	 */
	public void print(JSONObject json){print(0, json);}
	public void print(long duration, JSONObject json) {
		json = new SortedKeysJSONObject(json);
		if (json.has("result")){
			JSONObject result = json.getJSONObject("result");
			if (result.has("errors")) print(duration, PRE_OK_WITH_ERRORS+json.toString(4));
			else print(duration, PRE_OK+json.toString(jsonIndentation));
		} else if (json.has("error")) {
			print(duration, PRE_ERROR+json.getString("error"));
		} else if (json.has("progress")){
			if (DEBUG) print(duration, PRE_PROGRESS+String.format("%s, callID: %d, progress: %d%%", 
					json.getString("serviceName"),
					json.getLong("callID"),
					json.getInt("progress")
					));
		} else if (json.has("newCallID")){
			if (DEBUG) print(duration, PRE_PROGRESS+"newCallID: "+json.getLong("newCallID"));
		} else if (json.has("run"))
			print(duration, PRE_RUN+json.getString("run"));
	}
	
	/**Prints the usage information at startup.
	 */
	private void printInfo(){
		if (isColorSupported) out.print(bold+blue);
		out.println("**************** LUCI CLIENT CONSOLE ********************");
		out.println("type 'list commands' for a list of all commands (of this console)");
		out.println("type 'list services' for a list of all services (of luci)");
		out.println("type in / copy paste JSON strings with files being represented as paths");
		out.println("command: exit (exits the client console)");
		out.println("working directory: "+new File(".").getAbsolutePath());
		
		out.println("");
		out.flush();
	}
	
	static String validateHostAddress(String h) throws MalformedURLException{
		new URL(h);
		return h;
	}
	
	/**Starting the console process. Usage:
	 * java -cp "luci.core.JarIfNotInLibFolder:lib/*" luci.console.ClientConsole hostname port parentDirectory
	 * @param args String[] 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException{
		ClientConsole console = new ClientConsole();
		String hostAddress = null;
		try {
			hostAddress = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		int port = 7654;
		File dst = new File(System.getProperty("user.home"), "LuciClientFiles");
		if (args.length > 0){
			List<String> arguments = Arrays.asList(args);
			int h = arguments.indexOf("-h");
			try {
				if (h >= 0) hostAddress = validateHostAddress(arguments.get(h+1));
				console.setHostAddress(hostAddress);
			} catch (IndexOutOfBoundsException | MalformedURLException e){
				console.print(PRE_ERROR+e.getMessage()+": host address remains at "+hostAddress);
			}
			int p = arguments.indexOf("-p");
			try {
				if (p >= 0) port = Integer.valueOf(arguments.get(p+1));
				console.setPort(port);
			} catch(IndexOutOfBoundsException | NumberFormatException e){
				console.print(PRE_ERROR+e.getMessage()+": port remains at "+port);
			}
			
			int a = arguments.indexOf("-attsTo");
			if (a >= 0) {
				try {
					File customDst = new File(arguments.get(a+1));
					if (customDst.exists()) throw new FileNotFoundException();
					else dst = customDst;
				} catch (IndexOutOfBoundsException | FileNotFoundException e){
					console.print(PRE_ERROR+e.getMessage()+": attachment destination directory remains at "+dst);
				}
			}
		}
		AttachmentAsFile.filenameFromChecksum = false;
		AttachmentAsFile.parentDirectory = dst;
		new Thread(console).start();
	}
}
