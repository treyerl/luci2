/** MIT License
 * @author Lukas Treyer
 * @date Apr 18, 2016
 * */
package luci.console;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONString;

import luci.connect.LcString;
import luci.core.validation.Key;

public interface JSONParent {
	public JSONParent getParent();
	
	default int countParents(){
		JSONParent p = getParent();
		int i = 0;
		while (p != null){
			p = p.getParent();
			i++;
		}
		return i;
	}
	
	static class IndentedJSONArray extends JSONArray implements JSONParent{
		JSONParent parent;
		IndentedJSONArray(JSONArray ja, JSONParent p){
			super();
			for (Object o: ja){
				put(o);
			}
			parent = p;
		}
		
		IndentedJSONArray(Collection<?> c, JSONParent p){
			super(c);
			parent = p;
		}
		
		IndentedJSONArray(Object a, JSONParent p){
			super(a);
			parent = p;
		}
		
		public String toString(){
			return toString(0);
		}
		
		public String toString(int indentFactor){
			String comma = ",";
			boolean allNumbers = true;
			for (Object o: this){
				if (!(o instanceof Number)) {
					allNumbers = false;
					break;
				}
			}
			String muted = "", indent = "";
			String indend = "";
			String indent2 = "";
			if (indentFactor > 0) {
				muted = indent = LcString.fill(" ", indentFactor*(countParents()+1));
				indent2 = LcString.fill(" ", indentFactor*(countParents()));
				indend = "\n";
			}
			
			StringBuilder sb = new StringBuilder("["+indend);
			int i = 0;
			int line = 0;
			for (Object o: this){
				if (++i == length()) comma = "";
				if (o instanceof Number) {
					String s = indent+o2string(o, indentFactor, this)+comma;
					line += s.length();
					sb.append(s);
				}
				else {
					String s = indent+o2string(o, indentFactor, this)+comma;
					line += s.length();
					sb.append(s);
				}
				if (!allNumbers) sb.append(indend);
				else {
					if (line > 50) {
						sb.append(indend);
						line = 0;
						indent = muted;
					} else {
						indent = "";
					}
				}
			}
			if (indent.equals("") && indentFactor > 0) sb.append(indend);
			sb.append(indent2+"]");
			return sb.toString();
			
		}
		
		public JSONParent getParent(){
			return parent;
		}
		
	}
	public static class SortedKeysJSONObject extends JSONObject implements JSONParent{
		private JSONParent parent;
		public SortedKeysJSONObject(JSONObject value){
			this(value, null);
		}
		SortedKeysJSONObject(JSONObject value, JSONParent p) {
			super(value, value.keySet().toArray(new String[value.length()]));
			parent = p;
		}
		
		
		SortedKeysJSONObject(Map<?, ?> map, JSONParent p){
			super(map);
			parent = p;
		}
		
		public JSONParent getParent() {
			return parent;
		}
		
		public String toString(){
			return toString(0);
		}
		
		public String toString(int indentFactor){
			String comma = ",";
			int i = 0;
			String indent = "";
			String indend = "";
			String indent2 = "";
			if (indentFactor > 0) {
				indent = LcString.fill(" ", indentFactor*(countParents()+1));
				indent2 = LcString.fill(" ", indentFactor*(countParents()));
				indend = "\n";
			}
			StringBuilder sb = new StringBuilder("{"+indend);
			List<Key> keys = new ArrayList<>();
			for (String key: keySet()) keys.add(new Key(key));
			keys.sort((a,b)->a.compareTo(b));
			for (Key key: keys){
				if (++i == keys.size()) comma = "";
				sb.append(indent+"\""+key+"\":"+o2string(get(key.toString()), indentFactor, this)+comma+indend);
			}
			sb.append(indent2+"}");
			return sb.toString();
		}
	}
	
	default String o2string(Object value, int indentFactor, JSONParent p) {
		if (value == null || value.equals(null)) {
            return "null";
        } else if (value instanceof JSONObject) {
        	return new SortedKeysJSONObject((JSONObject) value, p).toString(indentFactor);
        } else if (value instanceof JSONArray) {
            return new IndentedJSONArray((JSONArray) value, p).toString(indentFactor);
        } else if (value instanceof Map) {
            Map<?, ?> map = (Map<?, ?>) value;
            return new SortedKeysJSONObject(map, p).toString(indentFactor);
        } else if (value instanceof Collection) {
            Collection<?> coll = (Collection<?>) value;
            return new IndentedJSONArray(coll, p).toString(indentFactor);
        } else if (value.getClass().isArray()) {
        	return new IndentedJSONArray(value, p).toString(indentFactor);
        } else if (value instanceof Number) {
            return JSONObject.numberToString((Number) value);
        } else if (value instanceof Boolean) {
            return value.toString();
        } else if (value instanceof JSONString) {
            Object o;
            try {
                o = ((JSONString) value).toJSONString();
            } catch (Exception e) {
                throw new JSONException(e);
            }
            return o != null ? o.toString() : JSONObject.quote(value.toString());
        } else {
            return JSONObject.quote(value.toString());
        }
	}
}
