/** MIT License
 * @author Lukas Treyer
 * @date Aug 30, 2016
 * */
package luci.console;

import java.util.concurrent.CountDownLatch;

import luci.connect.LcRemoteService;
import luci.connect.Message;

public abstract class LcRemoteServiceTest extends LcRemoteService {
	public abstract class RemoteServiceTestResponseHandler extends RemoteServiceResponseHandler {
		
		CountDownLatch cdl;
		Message m;
		public RemoteServiceTestResponseHandler(){
			cdl = new CountDownLatch(1);
		}
		
		@Override
		public void processResult(Message m) {
			this.m = m;
			cdl.countDown();
		}
		
		@Override
		public void processProgress(Message m){
			// no need for progress messages
		}
		
		public Message await() throws InterruptedException{
			cdl.await();
			return m;
		}
		
		public void reset(){
			if (cdl.getCount() > 0) 
				throw new IllegalStateException("Call handler still waiting. Cannot reset");
			cdl = new CountDownLatch(1);
		}
	}

	
	RemoteServiceTestResponseHandler currentHandler;
	CountDownLatch cdl;
	public LcRemoteServiceTest(DefaultArgsProcessor ap) {
		super(ap);
		cdl = new CountDownLatch(1);
	}
	
	public LcRemoteServiceTest(){
		this(new DefaultArgsProcessor());
	}
	
	protected abstract RemoteServiceTestResponseHandler newRemoteServiceTestResponseHandler();
	
	protected RemoteServiceTestResponseHandler newResponseHandler(){
		currentHandler = newRemoteServiceTestResponseHandler();
		cdl.countDown();
		return currentHandler;
	}
	
	public void awaitConnected() throws InterruptedException{
		cdl.await();
	}
	
	public RemoteServiceTestResponseHandler getCurrentHandler(){
		return currentHandler;
	}

}
