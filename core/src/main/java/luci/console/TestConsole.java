/** MIT License
 * @author Lukas Treyer
 * @date Aug 12, 2016
 * */
package luci.console;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.json.JSONObject;

import luci.connect.Message;
import luci.connect.LcClient.ResponseHandler;
import luci.core.Service;
import luci.core.ServiceLocal;

public class TestConsole extends ClientConsole{
	
	public static JSONObject setExampleInputAndValidateOutput(Service s) throws Exception{
		s.setInput(new Message(s.getExampleCall()));
		return validateOutput(s);
	}
	
	public static JSONObject validateOutput(Service s) throws Exception {
		JSONObject jsonOutput;
		s.getInputDescription().validate(s.getInput().getHeader());
		if (s instanceof ServiceLocal){
			jsonOutput = ((ServiceLocal) s).implementation().getHeader();
		} else {
			 jsonOutput = s.getResult().getHeader();
		}
		s.getOutputDescription().validate(jsonOutput);
		//System.out.println(s.getName()+": output as specified");
		return jsonOutput;
	}
	
	public static class BlockingCallHandler extends ResponseHandler{
		CountDownLatch cdl;
		Message m;
		BlockingCallHandler(){
			cdl = new CountDownLatch(1);
		}
		@Override
		public void processResult(Message m) {
			this.m = m;
			cdl.countDown();
		}
		
		@Override
		public void processProgress(Message m){
			// no need for progress messages
		}
		
		public Message await() throws InterruptedException{
			cdl.await();
			return m;
		}
		
		public void reset(){
			if (cdl.getCount() > 0) 
				throw new IllegalStateException("Call handler still waiting. Cannot reset");
			cdl = new CountDownLatch(1);
		}
	}
	
	BlockingCallHandler currentHandler;
	CountDownLatch cdl;
	public TestConsole() throws IOException {
		super();
		cdl = new CountDownLatch(1);
	}
	
	public void run(String serviceName, String...strings ) throws IOException{
		super.run(serviceName, strings);
	}
	
	public void connect(String host, String port){
		super.connect(host, port);
	}
	
	public void debug(){
		super.debug();
	}
	
	protected ResponseHandler newCallHandler(){
		currentHandler = new BlockingCallHandler();
		cdl.countDown();
		return currentHandler;
	}
	
	public void awaitConnected() throws InterruptedException{
		cdl.await();
	}
	
	public BlockingCallHandler getCurrentHandler(){
		return currentHandler;
	}
}


