/** MIT License
 * @author Lukas Treyer
 * @date Apr 22, 2016
 * */
package luci.service;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class GetStartupTime extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		return wrapResult(new JSONObject().put("startupTime", Luci.getStartupTime()));
	}
	
	@Override
	public String getGeneralDescription(){
		return "Returns the startup of time of Luci.\n"
				+ "@output.result.startupTime in milliseconds since 1.1.1970";
	}
	
	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return this.wrapResultDescription("{'startupTime':'number'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject().put("run", getName());
	}

}
