/** MIT License
 * @author Lukas Treyer
 * @date Dec 15, 2015
 * */
package luci.service;

import java.util.Set;

import org.json.JSONObject;

import luci.connect.JSON;
import luci.connect.Message;
import luci.core.Factory;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class ServiceInfo extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		JSONObject out = new JSONObject();
		Set<String> names = JSON.ArrayToStringSet(h.optJSONArray("serviceNames"));
		if (names == null){
			names = Luci.observer().getServiceNames();
		}
		for (String name: names){
			Factory f = Luci.observer().getServiceFactory(name);
			out.put(name, f.getInfo(h.optBoolean("inclDescr", false)));
		}
		
		return new Message(new JSONObject().put("result", out));
	}
	
	@Override
	public String getGeneralDescription(){
		return "Returns all known information on selected services such as a short description like "
				+ "this one, in- and output description and an example call. \n"
				+ "@input.serviceNames Optional list of selected service names for which information should be "
				+ "returned. If no such list is given, the result will show the structure of general "
				+ "json structures such as 'attachment', 'jsongeometry'\n"
				+ "@input.inclDescr indicates whether descriptions such as this one should be included in the result";
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"',"
				+ "'OPT serviceNames':'list', 'OPT inclDescr':'boolean'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'XOR result':{'ANY serviceName':{"
				+ "		'inputs':'json',"
				+ "		'outputs':'json',"
				+ "		'example':'json',"
				+ "		'OPT numNodes':'number'"
				+ "		}"
				+ "	},"
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'ServiceInfo','serviceNames':['RemoteRegister','ServiceList']}");
	}

}
