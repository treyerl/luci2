/** MIT License
 * @author Lukas Treyer
 * @date Jun 14, 2016
 * */
package luci.service.user;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.LcString;

public class User extends Subject implements Serializable{
	public class EmailAddressTakenException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		
	}
	private static final long serialVersionUID = 1L;
	
	private int zip;
	private String email, password, surname, organization, street, city, country;
	Set<Integer> groupIDs = new HashSet<>();
	
	public User(String email, String password){
		setEmail(email);
		setPassword(password);
		id = globalID++;
	}
	
	/**
	 * @return the groupID
	 */
	public Set<Integer> getGroupIDs() {
		return groupIDs;
	}
	
	public Set<Group> getGroups(){
		return groupIDs.stream().map((id)->(Group) allSubjects().get(id)).collect(Collectors.toSet());
	}
	
	/**
	 * @return the zip
	 */
	public int getZip() {
		return zip;
	}
	/**
	 * @param zip the zip to set
	 */
	public User setZip(int zip) {
		this.zip = zip;
		return this;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}
	/**
	 * @param surname the surname to set
	 */
	public User setSurname(String surname) {
		this.surname = surname;
		return this;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public User setEmail(String email) {
		if (email == null) throw new IllegalArgumentException("email cannot be null");
		if (!(email.indexOf("@") > 1 && 
			  email.indexOf("@") < email.length() - 2 &&
			  email.lastIndexOf(".") > email.indexOf("@") + 2))
			throw new IllegalArgumentException("mal formatted email address "+email);
		
		for (Subject s: allSubjects().values()){
			if (s instanceof User && ((User)s).getEmail().equalsIgnoreCase(email))
//				throw new IllegalArgumentException("Email address '"+email+"' already taken "/*+" by user with ID '"+s.getID()*/+"'!");
				throw new EmailAddressTakenException();
		}
		this.email = email;
		return this;
	}
	/**
	 * @return the password
	 */
	public boolean authenticate(String email, String password) {
		return this.email.equals(email) && this.password.equals(password);
	}
	/**hex representation of a sha1 encrypted password
	 * @param password the password to set
	 */
	public User setPassword(String password) {
		if (password == null) throw new IllegalArgumentException("password cannot be null");
		if (!LcString.isHex(password)) 
			throw new IllegalArgumentException("Passwords must be hexadecimal representation of the sha1 encryption of a password. "+password+" not a hex!");
		if (password.length() != 40)
			throw new IllegalArgumentException("Passwords must a 40 character long hexadecimal representation of the sha1");
		this.password = password;
		return this;
	}
	
	/**
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}
	/**
	 * @param organization the organization to set
	 */
	public User setOrganization(String organization) {
		this.organization = organization;
		return this;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public User setStreet(String street) {
		this.street = street;
		return this;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public User setCity(String city) {
		this.city = city;
		return this;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public User setCountry(String country) {
		this.country = country;
		return this;
	}

	@Override
	public String asString() {
		return "User: "+email+":"+id;
	}

	@Override
	public JSONObject asJSONObject() {
		JSONObject j = new JSONObject()
			.put("id", id)
			.put("type", "user")
			.put("groupIDs", groupIDs)
			.put("email", email)
			.putOpt("name", name)
			.putOpt("surname", surname)
			.putOpt("street", street)
			.putOpt("city", city)
			.putOpt("country", country)
			.putOpt("organization", organization);
		if (zip > 0) j.put("zip", zip);
		return j;
	}
	
	public static JSONObject jsonDescription(){
		JSONObject j = new JSONObject()
				.put("id", "number")
				.put("type", "string")
				.put("groupIDs", new JSONArray().put("listOf").put("number"))
				.put("email", "string")
				.put("OPT name", "string")
				.put("OPT surname", "string")
				.put("OPT street", "string")
				.put("OPT city", "string")
				.put("OPT country", "string")
				.put("OPT organization", "string")
				.put("OPT zip", "number");
		return j;
	}
}
