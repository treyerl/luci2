/** MIT License
 * @author Lukas Treyer
 * @date Jun 15, 2016
 * */
package luci.service.user;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.validation.JsonType;
import luci.core.ServerSocketHandler;
import luci.core.ServiceLocal;

public class Authenticate extends ServiceLocal {

	/* (non-Javadoc)
	 * @see luci.core.ServiceLocal#implementation()
	 */
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		String email = h.getString("email");
		String password = h.getString("password");
		for (Subject s: Subject.allSubjects().values()){
			if (s instanceof User && ((User)s).authenticate(email, password)){
				ServerSocketHandler ssh = (ServerSocketHandler) input.getSourceSocketHandler();
				if (ssh != null) ssh.setUser((User)s);
				return wrapResult(new JSONObject().put("success", true));
			}
		}
		return wrapResult(new JSONObject().put("success", false));
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getGeneralDescription()
	 */
	@Override
	public String getGeneralDescription() {
		return "Authenticates a user\n"
				+ "@inputs.password 40 characters hex representation of a sha1 encoded password string";
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getInputDescription()
	 */
	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'email':'string','password':'string'}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getOutputDescription()
	 */
	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'success':'boolean'}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getExampleCall()
	 */
	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{'email':'fancy@address.com','password':'13A45F568855DCAA4A3D1234FFFEBBEE8903DF03'}");
	}

}
