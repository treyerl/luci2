/** MIT License
 * @author Lukas Treyer
 * @date Jun 15, 2016
 * */
package luci.service.user;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Delete extends ServiceLocal {

	/* (non-Javadoc)
	 * @see luci.core.ServiceLocal#implementation()
	 */
	@Override
	public Message implementation() throws Exception {
		// TODO: Delete User: ask scenario somehow if that's ok
		JSONObject h = input.getHeader();
		int id = h.getInt("id");
		Subject s = Subject.allSubjects().get(id);
		if (s instanceof User){
			User u = (User) s;
			for (Group g: u.getGroups()){
				if (g.owner == id){
					throw new IllegalStateException(
							"Cannot delete; User '"+u.getEmail()+"' owns group '"+g.getName()+"'");
				}
			}
		}
		if (Subject.allSubjects().remove(h.getInt("id")) != null){
			Subject.userStore.commit();
			return wrapResult("{'success':true}");
		}
		return wrapResult("{'success':false}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getGeneralDescription()
	 */
	@Override
	public String getGeneralDescription() {
		return "Deletes a user if it does not own a group or [TODO] own a scenario";
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getInputDescription()
	 */
	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'id':'number'}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getOutputDescription()
	 */
	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'success':'boolean'}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getExampleCall()
	 */
	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{'id':5}");
	}

}
