/** MIT License
 * @author Lukas Treyer
 * @date Jun 14, 2016
 * */
package luci.service.user;

import java.io.Serializable;
import java.util.Map;

import org.h2.mvstore.MVStore;
import org.json.JSONObject;

import luci.core.Luci;

public abstract class Subject implements Serializable {
	private static final long serialVersionUID = 1L;

	static int getMaxID(){
		if (userStore == null) userStore = MVStore.open(Luci.cwd()+"/data/users");
		Map<Integer, Subject> users = userStore.openMap("users");
		return users.keySet().stream().max(Integer::max).orElse(1);
	}
	
	public static Map<Integer, Subject> allSubjects(){
		return userStore.openMap("users");
	}
	
	static MVStore userStore = MVStore.open(Luci.cwd()+"/data/users");
	static int globalID = getMaxID();
	
	int id;
	String name;
	
	public int getID(){
		return id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public Subject setName(String name) {
		this.name = name;
		return this;
	}
	
	public abstract String asString();
	
	public abstract JSONObject asJSONObject();
	
}
