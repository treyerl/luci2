/** MIT License
 * @author Lukas Treyer
 * @date Jun 14, 2016
 * */
package luci.service.user;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONObject;

public class Group extends Subject implements Serializable {
	private static final long serialVersionUID = 1L;
	Set<Integer> members;
	int owner;
	
	public Group(String name, User owner) {
		setOwner(owner.getID());
		setName(name);
		id = globalID++;
		owner.getGroupIDs().add(id);
	}
	
	public boolean addMember(User u){
		return members.add(u.getID());
	}
	
	public Set<User> getMembers(){
		Map<Integer, Subject> userMap = Subject.userStore.openMap("users");
		return members.stream().map((id)->(User)userMap.get(id)).collect(Collectors.toSet());
	}
	
	/**
	 * @return the owner
	 */
	public int getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(int owner) {
		this.owner = owner;
	}

	/* (non-Javadoc)
	 * @see luci.service.user.Subject#asString()
	 */
	@Override
	public String asString() {
		return "Group: "+name+":"+id;
	}

	/* (non-Javadoc)
	 * @see luci.service.user.Subject#asJSONObject()
	 */
	@Override
	public JSONObject asJSONObject() {
		return new JSONObject()
				.put("id", id)
				.put("type", "group")
				.put("members", members.stream().map(
					(id)->((User)allSubjects().get(id)).getEmail()).collect(Collectors.toSet()))
				.put("owner", owner);
	}
	
}
