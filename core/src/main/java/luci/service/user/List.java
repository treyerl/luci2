/** MIT License
 * @author Lukas Treyer
 * @date Jun 15, 2016
 * */
package luci.service.user;

import java.util.HashSet;
import java.util.Set;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class List extends ServiceLocal {

	/* (non-Javadoc)
	 * @see luci.core.ServiceLocal#implementation()
	 */
	@Override
	public Message implementation() throws Exception {
		Set<JSONObject> users = new HashSet<>();
		Set<JSONObject> groups = new HashSet<>();
		for (Subject s: Subject.allSubjects().values()){
			if (s instanceof User) users.add(s.asJSONObject());
			else groups.add(s.asJSONObject());
		}
		return wrapResult(new JSONObject().put("users", users).put("groups", groups));
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getGeneralDescription()
	 */
	@Override
	public String getGeneralDescription() {
		return "Get a list of users";
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getInputDescription()
	 */
	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getOutputDescription()
	 */
	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'users':['listOf',{'id':'number','email':'string','groupIDs':'list'}],"
				+ "'groups':['listOf', {'id':'number','owner':'number'}]}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getExampleCall()
	 */
	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{}");
	}

}
