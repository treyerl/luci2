/** MIT License
 * @author Lukas Treyer
 * @date Jun 15, 2016
 * */
package luci.service.user;

import org.json.JSONObject;

import luci.connect.JSON;
import luci.connect.Message;
import luci.core.Luci;
import luci.core.Service;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Permission extends ServiceLocal {

	/* (non-Javadoc)
	 * @see luci.core.ServiceLocal#implementation()
	 */
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		JSONObject add = (h.has("add")) ? h.getJSONObject("add") : new JSONObject();
		JSONObject remove = (h.has("remove")) ? h.getJSONObject("remove") : new JSONObject();
		for (String serviceName: add.keySet()){
			Luci.observer().addPermissionFor(serviceName, JSON.notNullInteger(JSON.ArrayToIntSet(add.getJSONArray(serviceName))));
		}
		for (String serviceName: remove.keySet()){
			Luci.observer().removePermissionFor(serviceName, JSON.ArrayToIntSet(remove.getJSONArray(serviceName)));
		}
		return wrapResult(Service.config);
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getGeneralDescription()
	 */
	@Override
	public String getGeneralDescription() {
		return "Allows to restrict given services only to given userIDs/groupIDs.";
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getInputDescription()
	 */
	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'OPT add':{'ANY serviceName':'list'},'OPT remove':{'ANY serviceName':'list'}}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getOutputDescription()
	 */
	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'ANY serviceName':'list'}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getExampleCall()
	 */
	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{'add':{'ServiceControl':[1,2]}}");
	}

}
