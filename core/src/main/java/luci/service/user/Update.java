/** MIT License
 * @author Lukas Treyer
 * @date Jun 15, 2016
 * */
package luci.service.user;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Update extends ServiceLocal {

	/* (non-Javadoc)
	 * @see luci.core.ServiceLocal#implementation()
	 */
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		int id = h.getInt("id");
		User u = (User) Subject.allSubjects().get(id);
		if (u == null) throw new IllegalArgumentException("no user with id '"+id+"'");
		if (h.has("email")) 	u.setEmail(h.getString("email"));
		if (h.has("password")) 	u.setPassword(h.getString("password"));
		if (h.has("surname")) 	u.setSurname(h.getString("surname"));
		if (h.has("organization")) u.setOrganization(h.getString("organization"));
		if (h.has("street")) 	u.setStreet(h.getString("street"));
		if (h.has("zip")) 		u.setZip(h.getInt("zip"));
		if (h.has("city")) 		u.setCity(h.getString("city"));
		if (h.has("country")) 	u.setCountry(h.getString("country"));
		if (h.has("name"))		u.setName(h.getString("name"));
		if (h.length() > 2){
			Subject.allSubjects().put(u.getID(), u);
			Subject.userStore.commit();
		}
		return wrapResult(new JSONObject().put("updatedUser", u.asJSONObject()));
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getGeneralDescription()
	 */
	@Override
	public String getGeneralDescription() {
		return "Udpate user information and does sanity checks on some of the properties.";
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getInputDescription()
	 */
	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription(new JSONObject()
				.put("id", "number")
				.put("OPT email", "string")
				.put("OPT password", "string")
				.put("OPT name", "string")
				.put("OPT surname", "string")
				.put("OPT organization", "string")
				.put("OPT street", "string")
				.put("OPT street", "string")
				.put("OPT zip", "number")
				.put("OPT city", "string")
				.put("OPT country", "string"));
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getOutputDescription()
	 */
	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription(new JSONObject().put("updatedUser", User.jsonDescription()));
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getExampleCall()
	 */
	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{'id':4,'email':'mynew@email.address'}");
	}

}
