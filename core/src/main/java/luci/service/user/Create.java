/** MIT License
 * @author Lukas Treyer
 * @date Jun 14, 2016
 * */
package luci.service.user;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Create extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		User u = new User(h.getString("email"), h.getString("password"))
				.setSurname(h.optString("surname", null))
				.setOrganization(h.optString("organization", null))
				.setStreet(h.optString("street", null))
				.setZip(h.optInt("zip", 0))
				.setCity(h.optString("city", null))
				.setCountry(h.optString("country", null));
		u.setName(h.optString("name", null));
		Subject.allSubjects().put(u.getID(), u);
		Subject.userStore.commit();
		return wrapResult(new JSONObject().put("id", u.getID()));
	}

	@Override
	public String getGeneralDescription() {
		return "Create a user by giving username, email, and sha1 encrypted password represented"
				+ "with hexadecimals\n"
				+ "@input.password hexadecimal representation of a sha1 encrypted password string\n"
				+ "@output.error if the username or the email are taken already an error is thrown";
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription(new JSONObject()
				.put("email", "string")
				.put("password", "string")
				.put("OPT name", "string")
				.put("OPT surname", "string")
				.put("OPT organization", "string")
				.put("OPT street", "string")
				.put("OPT street", "string")
				.put("OPT zip", "number")
				.put("OPT city", "string")
				.put("OPT country", "string"));
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'id':'number'}");
	}

	@Override
	public JSONObject getExampleCall() {
		// password = myPassword
		return wrapExampleCall("{'name':'user1','email':'abc@domain.com','password':'5413EE24723BBA2C5A6BA2D0196C78B3EE4628D1'}");
	}

}
