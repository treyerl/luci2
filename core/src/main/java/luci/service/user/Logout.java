/** MIT License
 * @author Lukas Treyer
 * @date Jun 15, 2016
 * */
package luci.service.user;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServerSocketHandler;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Logout extends ServiceLocal {

	/* (non-Javadoc)
	 * @see luci.core.ServiceLocal#implementation()
	 */
	@Override
	public Message implementation() throws Exception {
		ServerSocketHandler ssh = (ServerSocketHandler) input.getSourceSocketHandler();
		ssh.setUser(null);
		return wrapResult("{'success':true}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getGeneralDescription()
	 */
	@Override
	public String getGeneralDescription() {
		return "resets the connection to an unauthenticated state";
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getInputDescription()
	 */
	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getOutputDescription()
	 */
	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'success':'boolean'}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getExampleCall()
	 */
	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{}");
	}

}
