/** MIT License
 * @author Lukas Treyer
 * @date Jul 29, 2016
 * */
package luci.service;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class JsonTypes extends ServiceLocal {
	@Override
	public Message implementation() throws Exception {
		JSONObject out = new JSONObject();
		out.put("attachment", JsonType.getAttachment().toJSONObject());
		out.put("jsongeometry", JsonType.getJsonGeometry().toJSONObject());
		return wrapResult(out);
	}

	@Override
	public String getGeneralDescription() {
		return "Returns the json specification of attachments and json geometry objects.";
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'attachment':'attachment','jsongeometry':'jsongeometry'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{}");
	}

}
