/** MIT License
 * @author Lukas Treyer
 * @date Apr 25, 2016
 * */
package luci.service;

import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Factory;
import luci.core.FactoryRemoteServices;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.FactoryObserver.NotAServiceException;
import luci.core.validation.JsonType;

public class ServiceHistory extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Factory f = Luci.observer().getServiceFactory(h.getString("serviceName"));
		if (f == null) throw new NotAServiceException(h.getString("serviceName"));
		if (f instanceof FactoryRemoteServices){
			FactoryRemoteServices frs = (FactoryRemoteServices) f;
			int back = h.optInt("period", 60)*1000;
			List<JSONObject> wlist = frs.getWorkerHistory(back).stream().map(
					(e)->e.toJSON()).collect(Collectors.toList());
			List<JSONObject> clist = frs.getCallHistory(back).stream().map(
					(e)->e.toJSON()).collect(Collectors.toList());
			wlist.sort((j1,j2)->new Long(j1.getLong("tst")).compareTo(j2.getLong("tst")));
			clist.sort((j1,j2)->new Long(j1.getLong("tst")).compareTo(j2.getLong("tst")));
			
			return wrapResult(new JSONObject()
					.put("workers", wlist)
					.put("calls", clist));
		} else throw new IllegalStateException(f.getName()+" is not a remote service");
	}
	
	@Override
	public String getGeneralDescription() {
		return "Returns the history of a requested service as in number of calls and available"
				+ "workers either since startup time or during the requested period\n"
				+ "@input.period in seconds\n"
				+ "@output.calls TODO: fix LcMetaJson to allow to define lists with a generic customized json object\n"
				+ "@output.workers TODO: fix LcMetaJson to allow to define lists with a generic customized json object";
	}
	
	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"','serviceName':'string', 'OPT period':'number'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{"
				+ "'workers':['listOf', {'timestamp':'number','allWorkers':'number','idleWorkers':'number'}],"
				+ "'calls':['listOf', {'timestamp':'number','callID':'number','writingTime':'number','readingTime':'number','runningTime':'number','idleTime':'number'}]"
				+ "}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','serviceName':'ServiceControl'}");
	}
}
