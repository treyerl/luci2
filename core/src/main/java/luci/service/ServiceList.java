/** MIT License
 * Copyright (c) 2015 Lukas Treyer, ETH Zurich
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * */
package luci.service;

import java.util.HashSet;
import java.util.Set;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class ServiceList extends ServiceLocal{
	
	@Override
	public String getGeneralDescription() {
		return "Get a list of all loaded and registered services as a list of service names.\n"
				+ "@output.installed.services executables returning service in- & output information\n"
				+ "@output.installed.files executables not able to register as a service (e.g. PowerPoint Presentation)";
	}
	
	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'serviceNames':'list','installed':{'services':'list','files':'list'}}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'ServiceList'}");
	}
	
	private JSONObject getInstalled(){
		Set<String> services = new HashSet<>();
		Set<String> files = new HashSet<>();
		JSONObject inst = ServiceControl.installed;
		for (String key: inst.keySet()){
			if (JSONObject.NULL.equals(inst.getJSONObject(key).opt("io"))) files.add(key);
			else services.add(key);
		}
		return new JSONObject().put("services", services).put("files", files);
	}

	@Override
	public Message implementation() throws Exception {
		return new Message(new JSONObject().put("result", new JSONObject()
			.put("serviceNames", Luci.observer().getServiceNames())
			.put("installed", getInstalled())));
	}
}
