package luci.service;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.esotericsoftware.minlog.Log;

import luci.connect.Attachment;
import luci.connect.JSON;
import luci.connect.LcString;
import luci.connect.Message;
import luci.connect.SocketHandler;
import luci.connect.TcpSocketHandler;
import luci.core.Factory;
import luci.core.FactoryRemoteServices;
import luci.core.validation.JsonType;
import luci.core.Luci;
import luci.core.RemoteHandle;
import luci.core.ServerSocketHandler;
import luci.core.Service;
import luci.core.ServiceBalancer;
import luci.core.Listener;
import luci.core.ListenerManager;
import luci.core.ServiceLocal;
import luci.core.ServiceRemote;
import luci.core.TaskInfo;

public class ServiceControl extends ServiceBalancer {
	
	public final static JSONObject installed = JSON.fromFile(new File("data", "installedServices.json"));
	
	public static void init(){
		subscribe();
	}
	
	private static void subscribe() {
		Listener scl = new Listener(){
			Set<ListenerManager> listensTo = new HashSet<>();
			@Override
			public void getNotified(TaskInfo ti, Message m, long duration) {
				if (m.getHeader().has("result")){
					ServerSocketHandler sh = (ServerSocketHandler) m.getSourceSocketHandler();
					if (sh != null){ // mainly for UnitTest
						InetSocketAddress isa = sh.getRemoteSocketAddress();
						String ip = LcString.IPfromInetAddress(isa.getAddress());
						NodeInfo n = nodeInfos.get(ip);
						
						// RemoteRegister
						if (ti.getName().equals("RemoteRegister")){
							if (n == null) nodeInfos.put(ip, (n = new NodeInfo(ip)));
							n.addConnection(sh);
						// RemoteDeregister
						} else if (n != null && sh.getNumRegistered() == 0){
							n.removeConnection(sh);
						}
					}
				}
				
			}

			@Override
			public Set<ListenerManager> listensTo() {
				return listensTo;
			}
		};
		Luci.observer().onLoad((numFactories)->{
			scl.startListeningTo(Luci.observer().getServiceFactory("RemoteRegister"));
			scl.startListeningTo(Luci.observer().getServiceFactory("RemoteDeregister"));
		});
		
	}
	
	public static class NodeInfo {
		
		transient Map<Long, ServerSocketHandler> connections = new HashMap<>();
		JSONObject installedObjects;
		JSONObject system;
		String ip;
		transient RemoteHandle remoteControl;
		int numCores = 0;
		
		String name;
		Map<String,Set<Integer>> rhIdsPerService = new HashMap<>();
		
		public NodeInfo(String ip){
			this.ip = ip;
		}
		
		public void addConnection(ServerSocketHandler ssh){
			connections.putIfAbsent(ssh.getID(), ssh);
		}
		
		public void removeConnection(ServerSocketHandler ssh){
			connections.remove(ssh.getID());
		}
		
		JSONObject representConnections(){
			JSONObject r = new JSONObject();
			for (RemoteHandle rh: getRemoteHandles()){
				JSONObject rr = new JSONObject();
				r.put(rh.getWorkerID()+"", rr);
				rr.put("serviceName", rh.getServiceName());
				rr.put("avgIdleTimeRatio", rh.getIdleTimeRatio());
				rr.put("transmissionComputationRatio", rh.getTransmissionComputationRatio());
				rr.put("bytesRead", rh.getNumReadBytes());
				rr.put("bytesSent", rh.getNumSentBytes());
			}
			return r;
		}
		
		/**
		 * @return
		 */
		public List<RemoteHandle> getRemoteHandles() {
			return connections.values().stream()
					.map(ssh -> ssh.getRemoteHandles())
					.flatMap(ri -> ri.values().stream())
					.collect(Collectors.toList());
		}

		JSONObject toJSON(){
			Map<String, List<RemoteHandle>> perService = getRemoteHandles().stream().collect(
					Collectors.groupingBy(RemoteHandle::getServiceName));
			JSONObject busy = new JSONObject();
			JSONObject idle = new JSONObject();
			JSONObject running = new JSONObject();
			JSONObject services = new JSONObject()
					.put("connected", representConnections())
					.put("busy", busy)
					.put("idle", idle)
					.put("running", running);
			
			for (String s: perService.keySet()){
				List<RemoteHandle> svs = perService.get(s);
				long numBusy = svs.stream().filter(rh->rh.isBusy()).count();
				busy.put(s, numBusy);
				idle.put(s, svs.size() - numBusy);
				running.put(s, svs.size());
			}
					
			JSONObject json = new JSONObject();
			json.put("services", services)
				.putOpt("system", system)
				.putOpt("installedServices", installedObjects);
			return json;
		}
		
		Map<String, Integer> getNumWorkersPerService(){
			Map<String, Integer> map = new HashMap<>();
			for (RemoteHandle rh: getRemoteHandles()){
				String serviceName = rh.getServiceName();
				Integer num = map.get(serviceName);
				map.put(serviceName, (num != null) ? num++ : 1);
			}
			return map;
		}
		
		Map<String, Set<RemoteHandle>> getWorkersPerService(){
			Map<String, Set<RemoteHandle>> map = new HashMap<>();
			for (RemoteHandle rh: getRemoteHandles()){
				String serviceName = rh.getServiceName();
				Set<RemoteHandle> set = map.get(serviceName);
				if (set == null) map.put(serviceName, (set = new HashSet<>()));
				set.add(rh);
			}
			return map;
		}
		
		// might return negative values!
		Integer getNumFreeCores(){
			int free = numCores;
			for (RemoteHandle rh: getRemoteHandles()){
				String serviceName = rh.getServiceName();
				int req = 1;
				if (installedObjects.has(serviceName)){
					req = installedObjects.getJSONObject(serviceName).getInt("numRequestedCPUCores");
				}
				free -= req;
			}
			return free;
		}
		
		public Collection<ServerSocketHandler> getConnections(){
			return connections.values();
		}
		
		void setSystem(JSONObject system){
			this.system = system;
			this.numCores = system.getJSONObject("CPU").getInt("numCores");
		}
		
		int getNumWorkers(){
			return connections.size();
		}
		
		int getNumIdleWorkers(){
			return (int) getRemoteHandles().stream().filter((rh)->!rh.isBusy()).count();
		}
		int getNumBusyWorkers(){
			return (int) getRemoteHandles().stream().filter((rh)->rh.isBusy()).count();
		}
	}
	
	// #####################
	// SERVICE CONTROL CLASS
	// #####################
	
	public static final Map<String, NodeInfo> nodeInfos = new HashMap<>();
	boolean start = false;
	JSONObject jRes;
	
	@Override
	public String getGeneralDescription(){
		return "Used to remotely administrate installed services on a selected machine or on all"
				+ "machines having a 'ServiceControl' remote service running.\n"
				+ "@input.info get filtered information about either selected machines (nodes) or about selected"
				+ "serviceNames on all machines or full information about all machines in case this value is 'null'\n"
				+ "@input.info.nodes a list of IPs (IPv4)\n"
				+ "@input.install install files on either selected machines (nodes) or on all "
				+ "machines (services)\n"
				+ "@input.install.nodes values for 'ANY IP' or 'services' is a list holding only "
				+ "attachments (and no other type)\n"
				+ "@input.install.nodes.IP the attachment must hold attributes: {'exec': 'string (the string to start the service in console)','numRequestedCPUCores':'number (0=all available cores)'}\n"
				+ "@input.remove remove services identified by their name (serviceName) either on "
				+ "selected machines (nodes) or on all machines (services)\n"
				+ "@input.start startup a given number of services either globally or on selected "
				+ "machines (nodes) \n"
				+ "@input.start.nodes.IP.serviceName the service to start on a machine: either pass "
				+ "a number to specify how many services should be started or pass a json object "
				+ "consisting of id (=instanceID or persistentID) and the corresponding args string"
				+ "to be passed on startup \n"
				+ "@input.stop stop a given amount of services either globally or on specified machines (nodes).\n"
				+ "@output.result.info.nodes.ip.installedServices.serviceName.exec to be run in console\n"
				+ "@output.result.info.nodes.ip.installedServices.serviceName.numRequestedCPUCores 0 = using all available cores\n"
				+ "@output.result.info.nodes.ip.system.CPU.archNumBits int; 32 or 64\n"
				+ "@output.result.info.nodes.ip.system.CPU.endian 'big' or 'little'\n"
				+ "@output.result.info.services.serviceName.avgDuration in milliseconds\n"
				+ "@output.result.info.services.serviceName.nodes list of IPv4\n"
				+ "@output.result.info.services.serviceName.available not busy\n"
				+ "@output.result.info.services.serviceName.numCalls how many times this service has been called already\n"
				+ "@output.result.info.quickPool thread pool that handles all services (threads) having an avgDuration < 2 seconds\n"
				+ "@output.result.info.slowPool thread pool that handles all services (threads) having an avgDuration > 2 seconds\n"
				+ "@output.result.installed.IP list of service names installed per IP\n"
				+ "@output.result.removed.IP list of services names removed per IP\n"
				+ "@output.result.started.IP number of services started per IP and service\n"
				+ "@output.result.stopped.IP number of services stopped per IP and service\n"
				+ "@output.result.errors 'TODO' in case at least one of the items to install/remove/start/stop produced an error this list contains one error string per item";
	}
	
	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"',"
				+ "'XOR info'    :['anyOf', 'null', {'XOR nodes':['listOf','string'], 'XOR serviceNames':['listOf', 'string'], 'OPT threadPool':'boolean'}],"
				+ "'XOR install' : {'XOR nodes':{'ANY IP':['listOf','attachment']},               'XOR services':['listOf','attachment']},"
				+ "'XOR start'   : {'XOR nodes':{'ANY IP':{'ANY serviceName':['anyOf', 'number', {'ANY id':'string'}]}}, 'XOR services':{'ANY serviceName':'number'}},"
				+ "'XOR stop'    : {'XOR nodes':{'ANY IP':{'ANY serviceName':'number'}}, 'XOR services':{'ANY serviceName':'number'}},"
				+ "'XOR remove'  : {'XOR nodes':{'ANY IP':['listOf', 'string']},                   'XOR services':['listOf', 'string']}"
				+ "}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{"
				+ "'XOR result':{"
				+ " 'XOR info':{"
				+ "		'XOR nodes':{"
				+ "			'ANY ip':{"
				+ "				'services':{'running':{'ANY serviceName':'number'},'idle':{'ANY serviceName':'number'},'busy':{'ANY serviceName':'number'}},"
				+ "				'OPT system':{"
				+ "					'CPU':{'numCores':'number','archNumBits':'number','endian':'string'},"
				+ "					'osname':'string',"
				+ "					'osversion':'string',"
				+ "					'name':'string'},"
				+ "				'OPT installedServices':{'ANY serviceName':{'exec':'string','numRequestedCPUCores':'number'}}}"
				+ "		},"
				+ "		'XOR services':{"
				+ "			'ANY serviceName':{'avgDuration':'number','numCalls':'number',"
				+ "				'OPT available':'number','OPT running':'number','OPT nodes':'list'}"
				+ "		},"
				+ "		'remoteSummary':{"
				+ "			'idle':'number',"
				+ "			'busy':'number'"
				+ "		},"
				+ "		'OPT quickPool':{'avgWaitingTime':'number','waitingInQueue':'number',"
				+ "			'poolSize':'number','maxPoolSize':'number','numCalls':'number'},"
				+ "		'OPT slowPool':{'avgWaitingTime':'number','waitingInQueue':'number',"
				+ "			'poolSize':'number','maxPoolSize':'number','numCalls':'number'}"
				+ " },"
				+ "	'XOR started'  :{'ANY IP':{'ANY serviceName':'number'}},"
				+ " 'XOR stopped'  :{'ANY IP':{'ANY serviceName':'number'}},"
				+ " 'XOR installed':{'ANY IP':['listOf','string']},"
				+ " 'XOR removed'  :{'ANY IP':['listOf','string']},"
				+ " 'OPT errors':'list'"
				+ "},"
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject().put("run", getName()).put("serviceName", getName()).put("info", JSONObject.NULL);
	}

	@Override
	public JSONObject getRemoteInputDescription() {
		return new JSONObject("{'run':'"+getName()+"',"
				+ "'XOR install':['attachment'],"
				+ "'XOR remove':['string'],"
				+ "'XOR start':{'ANY serviceName':['number',{'ANY id':'string'}]},"
				+ "'XOR listServices':'boolean',"
				+ "'XOR systemInfo':'boolean',"
				+ "'OPT silent':'boolean'}");
	}

	@Override
	public JSONObject getRemoteOutputDescription() {
		return new JSONObject("{'XOR result':{"
				+ " 'XOR started':{'ANY serviceName':'number'},"
				+ " 'XOR removed':['string'],"
				+ " 'XOR installedObjects':{'ANY serviceName':{'exec':'string','numRequestedCPUCores':'number','io':'json'}},"
				+ " 'XOR system':{"
				+ "			'CPU':{'numCores':'number','archNumBits':'number','endian':'string'},"
				+ "			'osname':'string',"
				+ "			'osversion':'string',"
				+ "			'name':'string'},"
				+ " 'OPT errors':'list'"
				+ "},"
				+ "'XOR error':'string'}");
	}
	
	@Override
	public void distribute() {
		JSONObject h = input.getHeader();
		jRes = new JSONObject();
		
		if (h.has("info")){
			JSONObject info = new JSONObject();
			jRes.put("info", info);
			info.put("remoteSummary", Luci.observer().sumRemoteServices());
			JSONObject command = new JSONObject().put("run", getName()).put("listServices", true);
			JSONObject infoRequest = h.optJSONObject("info");
			// remoteSummary
			if (infoRequest != null && infoRequest.optBoolean("threadPool")){
				info.put("quickPool", ServiceLocal.poolInfo(ServiceLocal.getQuickPool()));
				info.put("slowPool", ServiceLocal.poolInfo(ServiceLocal.getSlowPool()));
			}
			// nodes
			if (infoRequest == null || infoRequest.has("nodes")){
				JSONObject nodes = new JSONObject();
				info.put("nodes", nodes);
				Set<String> requestedNodes = null;
				if (infoRequest != null) {
					requestedNodes = new HashSet<>();
					JSONArray n = infoRequest.getJSONArray("nodes");
					for (Object o: n) requestedNodes.add(o.toString());
				}
				boolean hasControllable = false;
				for (Entry<String, NodeInfo> en: nodeInfos.entrySet()){
					String ip = en.getKey();
					NodeInfo n = en.getValue();
					if (requestedNodes != null && !requestedNodes.contains(ip)) continue;
					nodes.put(ip, n.toJSON());
					if (n.remoteControl != null) {
						hasControllable = true;
						splitAdd(command, n.remoteControl);
					}
				}
				if (hasControllable) return;
				else hasFinalAnswers();
			// serviceNames
			} else if (infoRequest.has("serviceNames")){
				JSONObject services = new JSONObject();
				info.put("services", services);
				Set<String> requestedServices = null;
				JSONArray s = infoRequest.getJSONArray("serviceNames");
				if (s.length() > 0){
					requestedServices = new HashSet<>();
					for (Object o: s) requestedServices.add(o.toString());
				}
				for (String n: Luci.observer().getServiceNames()){
					if (requestedServices != null && !requestedServices.contains(n))
						continue;
					Factory f = Luci.observer().getServiceFactory(n);
					JSONObject si = new JSONObject();
					services.put(n, si);
					si.put("avgDuration", f.getAvgDuration())
					  .put("numCalls", f.getNumCalls());
					if (f instanceof FactoryRemoteServices){
						FactoryRemoteServices frs = (FactoryRemoteServices) f;
						Set<String> ips = new HashSet<>();
						si.put("idle", frs.getNumIdleWorkers())
						  .put("busy", frs.getNumBusyWorkers());
						Collection<RemoteHandle> rhs = frs.getAllWorkers().values();
						synchronized(rhs){
							for (RemoteHandle rh: rhs){
								ips.add(rh.getConnection().getRemoteSocketAddress().getAddress().getHostAddress());
							}
						}
						si.put("nodes", ips);
					}
				}
				hasFinalAnswers();
			}
			
		} else if (h.has("install")){
			JSONObject installed = new JSONObject();
			jRes.put("installed", installed);
			JSONObject installRequest = h.getJSONObject("install");
			if (installRequest.has("nodes")){
				JSONObject requestedNodes = installRequest.getJSONObject("nodes");
				if (requestedNodes.length() > 1 && input.anyNetworkAttachment() /*for websocket connections*/){
					input.setAttachmentType(Attachment.DOWNLOAD);
					input.onAttachmentsReady((length)->{
						JSONObject unreachable = installServicesPerNode(requestedNodes);
						for (String key: unreachable.keySet()) 
							installed.put(key, unreachable.get(key));
						// start the workers by ourselves since "splitCalls" got filled only later / 
						// once the attachments were ready
						startWorkers();
					});
					unmuteInputSocketHandler();
				} else {
					installServicesPerNode(requestedNodes);
				}
			} else if (installRequest.has("services")){
				JSONArray servicesToInstall = installRequest.getJSONArray("services");
				Set<NodeInfo> controllable = getControllableNodes();
				if (controllable.size() > 1){
					input.setAttachmentType(Attachment.DOWNLOAD);
					input.onAttachmentsReady((length)->{
						for (NodeInfo n: controllable) 
							installServices(servicesToInstall, n.remoteControl);
						startWorkers();
					});
					unmuteInputSocketHandler();
				} else {
					for (NodeInfo n: controllable) 
						installServices(servicesToInstall, n.remoteControl);
				}
			}
			if (distributedCalls.size() > 0) return;
			else hasFinalAnswers();
		} else if (h.has("remove")){
			JSONObject removed = new JSONObject();
			jRes.put("removed", removed);
			JSONObject removeRequest = h.getJSONObject("remove");
			if (removeRequest.has("nodes")){
				JSONObject requestedNodes = removeRequest.getJSONObject("nodes");
				for (String ip: requestedNodes.keySet()){
					NodeInfo n = nodeInfos.get(ip);
					if (n == null) {
						removed.put(ip, "unknown node");
						continue;
					}
					if (n.remoteControl != null){
						JSONObject command = new JSONObject()
								.put("run", getName())
								.put("remove", requestedNodes.get(ip));
						splitAdd(command, n.remoteControl);
					} else removed.put(ip, "not controllable");
				}
			} else if (removeRequest.has("services")){
				JSONObject command = new JSONObject()
						.put("run", getName())
						.put("remove", removeRequest.getJSONArray("services"));
				for (NodeInfo n: getControllableNodes()){
					splitAdd(command, n.remoteControl);
				}
			}
			if (distributedCalls.size() > 0) return;
			else hasFinalAnswers();
		} else if (h.has("start")){
			JSONObject started = new JSONObject();
			jRes.put("started", started);
			JSONObject startRequest = h.getJSONObject("start");
			if (startRequest.has("nodes")){
				JSONObject requestedNodes = startRequest.getJSONObject("nodes");
				for (String ip: requestedNodes.keySet()){
					NodeInfo n = nodeInfos.get(ip);
					if (n == null || n.remoteControl == null) {
						started.put(ip, new JSONObject());
						continue;
					}
					JSONObject command = new JSONObject()
							.put("run", getName())
							.put("start", requestedNodes.getJSONObject(ip));
					splitAdd(command, n.remoteControl);
				}
			} else if (startRequest.has("services")){
				// first get a list of installed services on all nodes
				// and distribute the start call then (see joinResults() where the lists of the
				// installed services are being joined)
				JSONObject command = new JSONObject()
						.put("run", getName())
						.put("listServices", true);
				for (NodeInfo n: getControllableNodes()){
					splitAdd(command, n.remoteControl);
				}
				start = true;
			}
			if (distributedCalls.size() > 0) return;
			else hasFinalAnswers();
		} else if (h.has("stop")){
			JSONObject stopped = new JSONObject();
			jRes.put("stopped", stopped);
			JSONObject stopRequest = h.getJSONObject("stop");
			if (stopRequest.has("nodes")){
				JSONObject requestedNodes = stopRequest.getJSONObject("nodes");
				for (String ip: requestedNodes.keySet()){
					NodeInfo n = nodeInfos.get(ip);
					if (n == null) continue;
					JSONObject perNode = requestedNodes.getJSONObject(ip);
					Map<String, Set<RemoteHandle>> numPerService = n.getWorkersPerService();
					for (String serviceName: perNode.keySet()){
						if (!numPerService.containsKey(serviceName)) continue;
						
						List<RemoteHandle> remotes = new ArrayList<>(numPerService.get(serviceName));
						int numRemove = Math.min(remotes.size(), perNode.getInt(serviceName));
						remotes.sort((a,b)->new Boolean(a.isBusy()).compareTo(b.isBusy()));
						for (int i = 0; i < numRemove; i++){
							RemoteHandle rh = remotes.get(i);
							rh.deregister();
							ServerSocketHandler ssh = rh.getConnection();
							if (ssh.getNumRegistered() == 0)
								n.removeConnection(ssh);
						}
						
					}
				}
			} else if (stopRequest.has("services")){
				JSONObject requestedServices = stopRequest.getJSONObject("services");
				for (String serviceName: requestedServices.keySet()){
					Factory f = Luci.observer().getServiceFactory(serviceName);
					if (f == null || !(f instanceof FactoryRemoteServices)) {
						stopped.put(serviceName, 0);
						continue;
					}
					FactoryRemoteServices frs = (FactoryRemoteServices) f;
					int numRemove = Math.min(frs.getAllWorkers().size(), 
							requestedServices.getInt(serviceName));
					int numRemoved = 0;
					Collection<RemoteHandle> idleWorkers = frs.getIdleWorkers();
					synchronized(idleWorkers){
						for (RemoteHandle rh: new HashSet<>(idleWorkers)){
							rh.deregister();
							if (++numRemoved == numRemove) break;
						}
					}
					Collection<RemoteHandle> allWorkers = frs.getAllWorkers().values();
					synchronized(allWorkers){
						Iterator<RemoteHandle> allIt = allWorkers.iterator();
						while(allIt.hasNext() && numRemoved++ < numRemove){
							RemoteHandle rh = allIt.next();
							allIt.remove();
							rh.onAvailable(()->{try {
								rh.deregister();
							} catch (Exception e) {
								e.printStackTrace();
							}
							return false;});
							NodeInfo ni = nodeInfos.get(rh.getIP());
							ServerSocketHandler ssh = rh.getConnection();
							if (ssh.getNumRegistered() == 0)
								ni.removeConnection(ssh);
						}
					}
				}
			}
			hasFinalAnswers();
		}
		// send the result compiled by joinResults() 
		notifyListeners(result, Luci.newTimestamp() - startTime);
		
	}
	
	private void unmuteInputSocketHandler(){
		SocketHandler sh = input.getSourceSocketHandler();
		if (sh instanceof TcpSocketHandler){
			((TcpSocketHandler) sh).restoreReadInterest();
		}
	}
	
	private void startWorkers(){
		for (Service worker: distributedCalls){
			startListeningTo(worker);
			worker.start();
		}
	}
	
	private void checkAllAttachments(JSONArray attachments){
		for (Object o: attachments){
			Attachment a = (Attachment) o;
			if (!a.has("exec")) 
				throw new JSONException(a.getName()+" is missing an 'exec' attribute!");
			if (!a.has("numRequestedCPUCores")) 
				throw new JSONException(a.getName()+" is missing a 'numRequestedCPUCores' attribute");
		}
	}
	
	private void installServices(JSONArray attachments, RemoteHandle remoteControl){
		checkAllAttachments(attachments);
		JSONObject command = new JSONObject()
				.put("run", getName())
				.put("install", attachments);
		splitAdd(command, remoteControl);
	}
	
	private JSONObject installServicesPerNode(JSONObject nodes){
		JSONObject unreachable = new JSONObject();
		for (String nodeIP: nodes.keySet()){
			NodeInfo n = nodeInfos.get(nodeIP);
			if (n != null){
				if (n.remoteControl != null) {
					installServices(nodes.getJSONArray(nodeIP), n.remoteControl);
				} else unreachable.put(nodeIP, "node not controllable");
			} else unreachable.put(nodeIP, "unknown node");
		}
		if (distributedCalls.size() == 0){
			throw new RuntimeException("None of the indicated IP's point to a controllable node");
		}
		return unreachable;
	}
	
	public int howToHandleAttachments(){
		return Attachment.NETWORK;
	}
	
	private void splitAdd(JSONObject command, RemoteHandle rh){
		ServiceRemote sr = new ServiceRemote(fsb, rh);
		sr.setInput(new Message(command));
		distributedCalls.add(sr);
	}
	
	private Set<NodeInfo> getControllableNodes(){
		return getControllableNodeStream().collect(Collectors.toSet());
	}
	
	private Stream<NodeInfo> getControllableNodeStream(){
		return nodeInfos.values().stream().filter(n->n.remoteControl != null);
	}
	
	private void distributeStartRequest(JSONObject startRequest){
		JSONObject requestedServices = startRequest.getJSONObject("services");
		List<NodeInfo> controllable = getControllableNodeStream().collect(Collectors.toList());
		controllable.sort((a,b)->a.getNumFreeCores().compareTo(b.getNumFreeCores())*-1);
		
		// no we're not shutting down any service - that's in the responsibility of the user
		
//		// do we need to try to stop some services after all?
//		// = are more services requested to be started than we have unused cores?
//		// in case we do: gently stop (=wait until they're done with their current task)
//		// workers of those services who had most idle workers during the last minute
//		if (totalNumStartRequested > numUnusedCores){
//			Map<Float, FactoryRemoteServices> ranking = new TreeMap<>();
//			
//			for (FactoryRemoteServices frs: Luci.observer.getRemoteServiceFactories()){
//				// only try to stop services that are not requested by this call
//				if (!requestedServices.keySet().contains(frs.getName())){
//					// average numIdle within the last minute
//					ranking.put(frs.avgNumIdleDuringTheLast(60000), frs);
//				}
//			}
//			
//			for (Entry<Float, FactoryRemoteServices> en: ranking.entrySet()){
//				FactoryRemoteServices frs = en.getValue();
//				int numClose = Math.min(en.getKey().intValue(), numAdditionallyNeeded);
//				numAdditionallyNeeded -= frs.close(numClose);
//				if (numAdditionallyNeeded <= 0) break;
//			}
//			numAllowed = totalNumStartRequested - numUnusedCores - numAdditionallyNeeded; 
//		}
		Map<NodeInfo, JSONObject> commands = new HashMap<>();
		for (String serviceName: requestedServices.keySet()){
			int numToStart = requestedServices.getInt(serviceName);
			int numStarted = 0;
			while(numStarted < numToStart){
				for (NodeInfo n: controllable){
					if (n.installedObjects.has(serviceName)){
						JSONObject sCmd = commands.get(n);
						if (sCmd == null) commands.put(n, (sCmd = new JSONObject()));
						// in case we have a negative number of free cores (e.g. more than one 
						// multi-threaded service is running already on that node)
						int numStart = Math.max(Math.min(n.getNumFreeCores(), numToStart), 1);
						// 
						if (sCmd.has(serviceName)) sCmd.put(serviceName, sCmd.getInt(serviceName)+numStart);
						else sCmd.put(serviceName, numStart);
						numStarted += numStart;
					}
					if (numStarted == numToStart) break;
				}
			}
		}
		
		for (Entry<NodeInfo, JSONObject> en: commands.entrySet()){
			JSONObject command = new JSONObject()
					.put("run", "start")
					.put("start", en.getValue());
			splitAdd(command, en.getKey().remoteControl);
		}		
	}
	
	@Override
	public boolean joinResults() {
		Iterator<Message> mres = results.iterator();
		Map<String, JSONObject> resByIP = new HashMap<>();
		while(mres.hasNext()){
			Message m = mres.next();
			InetSocketAddress isa = m.getSourceSocketHandler().getRemoteSocketAddress();
			String ip = isa.getAddress().getHostAddress();
			NodeInfo n = nodeInfos.get(ip);
			JSONObject inH = m.getHeader().getJSONObject("result");
			resByIP.put(ip, inH);
			if (inH.has("installedObjects")){
				mres.remove();
				n.installedObjects = inH.getJSONObject("installedObjects");
			} else if (inH.has("system")){
				mres.remove();
				n.setSystem(inH.getJSONObject("system"));
			} else if (inH.has("removed")){
				mres.remove();
				for (Object service: inH.getJSONArray("removed")){
					n.installedObjects.remove((String) service);
				}
			}
		}
		if (start){
			distributedCalls.clear();
			JSONObject h = input.getHeader();
			distributeStartRequest(h.getJSONObject("start"));
			start = false;
			return false;
		} else {
			if (jRes.has("info")){
				JSONObject info = jRes.getJSONObject("info");
				if (info.has("nodes")){
					JSONObject newNodes = new JSONObject();
					JSONObject nodes = info.getJSONObject("nodes");
					for (String ip: nodes.keySet()){
						newNodes.put(ip, nodeInfos.get(ip).toJSON());
					}
					info.put("nodes", newNodes);
				}
			} else if (jRes.has("installed")){
				JSONObject installed = jRes.getJSONObject("installed");
				for(String ip: resByIP.keySet()){
					JSONObject i = resByIP.get(ip);
					installed.put(ip, i.getJSONObject("installedObjects").keySet());
					if (i.has("errors")){
						if (!jRes.has("errors")) jRes.put("errors", new JSONArray());
						JSONArray errors = jRes.getJSONArray("errors");
						for (Object error: i.getJSONArray("errors")){
							errors.put(error);
						}
					}
				}
				updateMyRecords();
			} else if (jRes.has("started")){
				JSONObject started = jRes.getJSONObject("started");
				for (String ip: resByIP.keySet()){
					started.put(ip, resByIP.get(ip).getJSONObject("started"));
				}
			} else if (jRes.has("removed")){
				JSONObject removed = jRes.getJSONObject("removed");
				for (String ip: resByIP.keySet()){
					removed.put(ip, resByIP.get(ip).getJSONArray("removed"));
				}
				updateMyRecords();
			}
			result = new Message(new JSONObject().put("result", jRes));
			return true;
		}
	}
	
	private void updateMyRecords(){
		boolean changed = false;
		Set<String> unknown = new HashSet<String>(installed.keySet());
		for (NodeInfo ni: nodeInfos.values()){
			for (String objectName: ni.installedObjects.keySet()){
				unknown.remove(objectName);
				if (!installed.has(objectName)){
					changed = true;
					installed.put(objectName, 
							ni.installedObjects.getJSONObject(objectName));
				}
			}
		}
		for (String key: unknown) {
			changed = true;
			installed.remove(key);
		}
		if (changed)
			try {
				JSON.toFile(installed, new File("data", "installedServices.json"));
			} catch (IOException e) {
				Log.error(e.toString());
				if (Log.DEBUG || Log.TRACE) e.printStackTrace();
			}
	}
	
	
	@Override
	public boolean addRemote(RemoteHandle node, Message input) {
		String ip = node.getIP();
		NodeInfo n = nodeInfos.get(ip);
		if (n == null) nodeInfos.put(ip, (n = new NodeInfo(ip)));
		n.remoteControl = node;
		JSONObject c = new JSONObject().put("run", getName()).put("systemInfo", true);
		jRes = new JSONObject();
		ServiceRemote sf = new ServiceRemote(fsb, node);
		sf.setInput(new Message(c));
		startListeningTo(sf);
		sf.start();
		return false;
	}

	@Override
	public boolean removeRemote(RemoteHandle node) {
		String ip = node.getIP();
		nodeInfos.remove(ip);
		return true;
	}
}
