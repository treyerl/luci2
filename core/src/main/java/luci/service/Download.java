package luci.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONObject;

import luci.connect.LcString;
import luci.connect.Message;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Download extends ServiceLocal {
	
	public static File download(URL url, File to, JSONObject infos) 
			throws IOException, NoSuchAlgorithmException, InterruptedException{
		URLConnection con = url.openConnection();
		con.setConnectTimeout(2000);
		long len = con.getContentLengthLong();
		if (len == -1) len = Long.MAX_VALUE;
		String name = url.toString().substring(url.toString().lastIndexOf("/")+1);
		String format = null;
		String contentType = con.getContentType();
		if (contentType != null){
			// some mimetypes have a charset specification after a ";"
			String mimetype = contentType.split(";")[0];
			format = mimetype.split("/")[1];
			if (!name.contains(".")) name = name + "."+format;
		}
		
		// throws UnknownHostException if there is no "internet connection"
		ReadableByteChannel rbc = Channels.newChannel(con.getInputStream());
		File f = new File(to, name);
		FileOutputStream fos = new FileOutputStream(f);
		long numRead = 0, r = 0;
		ByteBuffer bb = ByteBuffer.allocateDirect(16*1024);
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		FileChannel fc = fos.getChannel();
		while(numRead < len){
			numRead += r = rbc.read(bb);
			if (r == 0) {
				Thread.sleep(2);
				continue;
			}
			if (r == -1) break;
			bb.flip();
			while (bb.hasRemaining()) fc.write(bb);
			bb.rewind();
			md5.update(bb);
			bb.compact();
		}
 		String checksum = LcString.getHexFromBytes(md5.digest());
		fos.close();
		// if the file with that checksum exists already delete the file we just downloaded
		File newFile = new File(to, (format != null) ? checksum+"."+format : checksum);
		if (!f.renameTo(newFile)) f.delete();
		infos.put("length", numRead);
		infos.put("checksum", checksum);
		infos.put("format", (format != null) ? format : "unknown");
		return newFile;
	}
	
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		URL url = new URL(h.getString("url"));
		JSONObject infos = new JSONObject();
		download(url, Luci.getAttachmentsFolder(), infos);
		return new Message(new JSONObject().put("result", infos));
	}
	
	public Message testImplementation() {
		try {
			return implementation();
		} catch (Exception e){
			return new Message(new JSONObject().put("error", e.getMessage()));
		}
	}
	
	@Override
	public String getGeneralDescription(){
		return "Downloads a file from a given URL and stores is in Luci's attachment folder as "
				+ "&lt;md5 checksum&gt;.&lt;format&gt; \n @input.url the URL to be used\n"
				+ "@output.result.checksum the checksum in hexadecimal representation\n"
				+ "@output.result.format equals the string typically put as a file ending\n"
				+ "@output.result.length the length of the attachments as number of bytes";
	}
	
	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"','url':'string'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'length':'number','checksum':'string', 'format':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject().put("run", getName()).put("url", "https://de.wikipedia.org/wiki/Luci");
	}

}
