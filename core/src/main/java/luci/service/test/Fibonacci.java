/** MIT License
 * @author Lukas Treyer, Alessandro Forino
 * @date Jan 14, 2016
 * */
package luci.service.test;

import java.math.BigDecimal;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

/**
 * 
 * @author treyerl
 *
 */
public class Fibonacci extends ServiceLocal{

	@Override
	public Message implementation() throws Exception {
		JSONObject header = input.getHeader();
		int amount = header.getInt("amount");
		boolean onlyLast = header.optBoolean("onlyLast", false);
		JSONArray seq;
		if (amount > 93) seq = fibBig(amount);
		else seq = fibLong(amount);
		if (onlyLast) seq = new JSONArray().put(seq.get(seq.length()-1));
		return wrapResult(new JSONObject().put("fibonacci_sequence", seq));
	}
	
	private JSONArray fibBig(int amount){
		JSONArray seq = new JSONArray();
		seq.put(new BigDecimal(0));
		seq.put(new BigDecimal(1));
		if(amount > 1) {
			for(int i = 2; i < amount; i++)
//				seq.put(new BigDecimal(seq.getString(i-2)).add(new BigDecimal(seq.getString(i-1))).toString());
				seq.put(seq.getBigDecimal(i-2).add(seq.getBigDecimal(i-1)));
		}
		return seq;
	}
	
	private JSONArray fibLong(int amount){
		JSONArray seq = new JSONArray();
		seq.put(0);
		seq.put(1);
		if(amount > 1) {
			for(int i = 2; i < amount; i++) 
				seq.put(seq.getLong(i-2)+seq.getLong(i-1));
		}
		return seq;
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':"+getName()+",'amount':'number', 'OPT onlyLast':'boolean'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription(new JSONObject().put("fibonacci_sequence", "list"));
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject().put("run", getName()).put("amount", 10);
	}

	@Override
	public String getGeneralDescription() {
		return "A service that serves as an (implementation) example.\n"
				+ "@input.amount how many numbers should be calculated\n"
				+ "@input.onlyLast return only the last number";
	}

}
