/** MIT License
 * @author Lukas Treyer
 * @date Jan 14, 2016
 * */
package luci.service.test;

import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Randomly extends ServiceLocal{

	@Override
	public Message implementation() throws Exception {
		Random r = new Random();
		JSONObject header = input.getHeader();
		JSONArray keynames = header.optJSONArray("keys");
		JSONObject outputs = new JSONObject();
		if (keynames != null){
			JSONObject kvp = new JSONObject();
			for (int i = 0; i < keynames.length(); i++) 
				kvp.put(keynames.getString(i), Math.abs(r.nextInt()));
			outputs.put("keyValuePairs", kvp);
		} else {
			int amount = header.getInt("amount");
			JSONArray randomNumbers = new JSONArray();
			for (int i = 0; i < amount; i++) randomNumbers.put(Math.abs(r.nextInt()));
			outputs.put("randomNumbers", randomNumbers);
		}
		return wrapResult(outputs);
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"','XOR amount':'number','XOR keys':'list'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'XOR randomNumbers':'list', 'XOR keyValuePairs':'json'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"', 'keys':['first','two','car']}");
	}
	
	@Override
	public String getGeneralDescription() {
		return "Get either an amount of random numbers or create key-value-pairs from a key list.";
	}


}
