/** MIT License
 * @author Lukas Treyer
 * @date Dec 13, 2015
 * */
package luci.service.test;
import luci.connect.Attachment;
import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

import org.json.JSONObject;

public class FileEcho extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		h.remove("run");
		// don't create a new message, since there might be listeners attached to the message
		// listening for onAttachmentsReady event
		input.setHeader(new JSONObject().put("result", h));
		return input;
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"',"
				+ "'ANY filenames':'attachment'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'XOR result':{"
				+ "'ANY sameFilenames':'attachment'}"
				+ ", 'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'test.FileEcho','file1':'test.jpg'}");
	}
	
	public int howToHandleAttachments(){
		return Attachment.NETWORK;
	}

	@Override
	public String getGeneralDescription() {
		return "A service to test sending and receiving attachments. Mainly used by connection"
				+ "library developers.";
	}
}
