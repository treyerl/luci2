/** MIT License
 * @author Lukas Treyer
 * @date Jan 21, 2016
 * */
package luci.service.test;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Error extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		Object message = input.getHeader().opt("message");
		if (message == null) message = "You asked for it!";
		throw new RuntimeException(message.toString());
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"','OPT message':'string'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'error':'string'}");
	}
	
	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"', 'message':'Give me an error!'}");
	}

	@Override
	public String getGeneralDescription() {
		return "A service that simulates an error being thrown.\n"
				+ "@input.message optional error message";
	}

}
