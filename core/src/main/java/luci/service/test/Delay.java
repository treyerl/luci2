/** MIT License
 * @author Lukas Treyer
 * @date Jan 21, 2016
 * */
package luci.service.test;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Delay extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		double seconds = h.getDouble("seconds");
		JSONObject nh = new JSONObject(h, JSONObject.getNames(h));
		nh.remove("seconds");
		nh.remove("run");
		Thread.sleep((long) (seconds * 1000));
		return new Message(new JSONObject().put("result", nh));
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"','seconds':'number', "
			+ "'ANY inputs':'any'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'ANY outputs':'any'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','seconds':5,'input1':'a string'}");
	}

	@Override
	public String getGeneralDescription() {
		return "Used to simulate a service with a long calculation time.\n"
				+ "@input.inputs any inputs are being forwarded";
	}

}
