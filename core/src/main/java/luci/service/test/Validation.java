/** MIT License
 * @author Lukas Treyer
 * @date Aug 2, 2016
 * */
package luci.service.test;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Validation extends ServiceLocal {
	private String geojson = "{'type':'Polygon',"
			+ "'coordinates':[[[0,0],[5,5],[-5,5]],[[0,2],[3,4],[-3,4]]]}";
	private String topojson = "{'type':'Topology','objects':{'firstObject':"+geojson+"}}";
	
	@Override
	public Message implementation() throws Exception {
		List<String> ets = new ArrayList<>();
		ets.add("{'run':'"+getName()+"','key1':5,'key2':'string1','key3':98,'key5':{'format':'geojson','geometry':"+geojson+"}}");
		ets.add("{'run':'"+getName()+"','key1':'x','key2':'string7','key3':98,'key5':{'format':'geojson','geometry':"+geojson+"}}");
		ets.add("{'run':'"+getName()+"','key1':'x','key2':'string1','key3':99,'key5':{'format':'geojson','geometry':"+geojson+"}}");
		ets.add("{'run':'"+getName()+"','key1':'x','key2':'string1','key3':98,'key5':{'format':'topojson','geometry':"+topojson+"}}");
		ets.add("{'run':'"+getName()+"','key1':5,'key2':'string7','key3':99,'key5':{'format':'topojson','geometry':"+topojson+"}}");
		return wrapResult(new JSONObject().put("passed", true).put("errorTestStrings", ets));
	}

	@Override
	public String getGeneralDescription() {
		return "A service to test how type and value constraint validation works.\n"
				+ "@input.key1 normal string\n"
				+ "@input.key2 string values constraint to 'string1' and 'string2'\n"
				+ "@input.key3 number values constraint to 4 ranges\n"
				+ "@input.key4 optional attachment that must be of type 'jpg'\n"
				+ "@input.key5 geojson geometry\n"
				+ "@input.key6 optional attachment that requires types to be one of 'jpg' or 'png'\n"
				+ "@input.key7 optional attachment with predefied required keys";
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{"
				+ "'key1':'string',"
				+ "'key2':['string','string1','string2'],"
				+ "'key3':['number','1-10','[24.3-26.5]','33,34,35,[88-99)'],"
				+ "'OPT key4':['attachment','jpg'],"
				+ "'key5':['jsongeometry','geojson'],"
				+ "'OPT key6':['attachment','jpg','png'],"
				+ "'OPT key7':{'format':'jpg','attachment':{'checksum':'string','position':'number','length':'number'},"
				+ "		'name':'string','additional required key':'string','keyX':['listOf', 'string'],"
				+ "		'keyY':{'nested':'number','required':'string'}}"
				+ "}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'passed':'boolean','errorTestStrings':'list'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall(new JSONObject()
				.put("key1", "test string")
				.put("key2", "string1")
				.put("key3", 2)
				.put("key5", new JSONObject("{'format':'geojson','geometry':"+geojson+"}")));
	}

}
