/** MIT License
 * @author Lukas Treyer
 * @date Aug 31, 2016
 * */
package luci.service.test;

import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static java.nio.file.StandardOpenOption.WRITE;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.Random;
import java.util.UUID;

import org.json.JSONObject;

import luci.connect.Attachment;
import luci.connect.AttachmentAsArray;
import luci.connect.AttachmentAsFile;
import luci.connect.Message;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class RandomBytes extends ServiceLocal {

	/* (non-Javadoc)
	 * @see luci.core.ServiceLocal#implementation()
	 */
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Object s = h.get("size");
		Random r = new Random();
		Attachment a;
		long size;
		if (s instanceof String){
			String str = (String) s;
			long strnm = Long.parseLong(str.substring(0, str.length() - 2));
			if (str.endsWith("KB")) size = strnm * 1024;
			else if (str.endsWith("MB")) size = strnm * 1014 * 1024;
			else if (str.endsWith("GB")) size = strnm * 1014 * 1024 * 1024;
			else throw new IllegalArgumentException("allowed size formats: xxKB, xxMB, xxGB; no whitespace");
		} else if (s instanceof Number){
			size = (Long) s;
		} else {
			throw new IllegalArgumentException("size needs to be of type number or string");
		}
		if (Attachment.tooLargeForMemory(size)){
			File f = new File(Luci.getAttachmentsFolder(), UUID.randomUUID().toString()+".bytes");
			Path p = f.toPath();
			long numWritten = 0;
			try (FileChannel fc = (FileChannel.open(p, CREATE_NEW, WRITE))) {
				ByteBuffer bb = ByteBuffer.allocate(1024*16);
				while(numWritten < size){
					r.nextBytes(bb.array());
					bb.rewind();
					while(bb.remaining() > 0 && (numWritten += fc.write((ByteBuffer) bb.limit(
							(int) Math.min(bb.capacity(), size-numWritten)))) < size);
				}
			} catch (IOException x) {
			    System.out.println("I/O Exception: " + x);
			}
			a = new AttachmentAsFile(f).deleteOnceWritten();
		} else {
			ByteBuffer bb = ByteBuffer.allocate((int) size);
			r.nextBytes(bb.array());
			a = new AttachmentAsArray("bytes", "random", bb);
		}
		return wrapResult(new JSONObject().put("randomBytes", a));
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getGeneralDescription()
	 */
	@Override
	public String getGeneralDescription() {
		return "Generates a byte array of requested size, filled with random bytes.";
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getInputDescription()
	 */
	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'size':['anyOf','number','string']}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getOutputDescription()
	 */
	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'randomBytes':'attachment'}");
	}

	/* (non-Javadoc)
	 * @see luci.core.Service#getExampleCall()
	 */
	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{'size':'30MB'}");
	}

}
