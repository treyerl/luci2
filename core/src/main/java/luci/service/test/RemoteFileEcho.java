/** MIT License
 * @author Lukas Treyer
 * @date Feb 5, 2016
 * */
package luci.service.test;
import static com.esotericsoftware.minlog.Log.trace;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.stream.Collectors;

import org.json.JSONObject;

import com.esotericsoftware.minlog.Log;

import luci.connect.Attachment;
import luci.connect.Message;
import luci.console.LcRemoteServiceTest;
import luci.core.validation.JsonType;

public class RemoteFileEcho extends LcRemoteServiceTest {
	
	public RemoteFileEcho(DefaultArgsProcessor asp) {
		super(asp);
	}

	public RemoteFileEcho() {}

	protected JSONObject specifyInputs() {
		return new JSONObject("{'run':'"+getName()+"','ANY filename':'attachment'}");
	}

	protected JSONObject specifyOutputs() {
		return new JSONObject("{'XOR result':{'ANY filename':'attachment'}, 'XOR error':'string'}");
	}
	
	protected JSONObject exampleCall() {
		return new JSONObject("{'run':'"+getName()
				+"', 'aFile':"+JsonType.getAttachment()+"}");
	}
	
	public int howToHandleAttachments() {
		return Attachment.NETWORK;
	}

	protected RemoteServiceTestResponseHandler newRemoteServiceTestResponseHandler() {
		return new RemoteServiceTestResponseHandler(){
			public void processResult(Message m) {
				System.out.println(m.getHeader().getJSONObject("result"));
			}
			public Message implementation(Message m) {
				JSONObject h = m.getHeader();
				System.out.println("Echoing "+m.getNumAttachments()+" files; total length "
						+m.getSumAttachmentsLength()+" "+m.getAttachmentMap().values().stream().map(
						(a)->a.getChecksum()).collect(Collectors.toSet()));
				h.remove("run");
				m.setHeader(new JSONObject().put("result", h));
				return m;
			}
		};
	}

	public static void main(String[] args){
		Log.set(Log.LEVEL_TRACE);
		DefaultArgsProcessor asp = new DefaultArgsProcessor(args);
    	trace(asp.toString());
		System.out.println("registering at "+asp.getHostname()+":"+asp.getPort());
		RemoteFileEcho rfe = new RemoteFileEcho(asp);
		new Thread(rfe).start();
		rfe.connect(asp.getHostname(), asp.getPort());
	}
	
	public boolean processIOException(SelectionKey key, IOException e){
		key.cancel();
		return true;
	}

	@Override
	public String getDescription() {
		return "A remote service to test sending and receiving attachments to remote services. "
				+ "Mainly used by connection library developers.";
	}
}
