/** MIT License
 * @author Lukas Treyer
 * @date Mar 29, 2016
 * */
package luci.service.workflow;

import org.json.JSONObject;

import luci.connect.Attachment;
import luci.connect.AttachmentSpot;
import luci.connect.AttachmentSpot.JSONObjectArray;
import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.Workflow;
import luci.core.WorkflowElement;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;

/** Adds & removes in & output slots of workflows
 */
public class UpdateIO extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		long taskID = h.getLong("taskID");
		WorkflowElement wfe = WorkflowElements.get(taskID);
		if (wfe instanceof Workflow){
			JSONObject in  = h.optJSONObject("inputs");
			JSONObject out = h.optJSONObject("outputs");
			if (in != null)  set(in, wfe.getInputDescription(), wfe.getInputMessage());
			if (out != null) set(out, wfe.getOutputDescription(), ((Workflow) wfe).getOutputMessage());
			return new Message(new JSONObject().put("result", wfe.toJSONObject()));
		}
		return new Message(new JSONObject().put("error", taskID+" is no workflow"));
	}
	
	private void set(JSONObject definition, JsonType desc, Message msg){
		JSONObject to = msg.getHeader();
		for (String name: definition.keySet()){
			JSONObject def = definition.getJSONObject(name);
			desc.add(name, def.getString("type"));
			Object defaultValue = def.get("default");
			if (defaultValue instanceof Attachment){
				Attachment a = (Attachment) defaultValue;
				msg.setAttachment(a, new AttachmentSpot(name, new JSONObjectArray(to), a.getChecksum()));
			}
			to.put(name, defaultValue);
		}
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"','taskID':'number',"
				+ "'OPT inputs':{'ANY keyname': {"
				+ "		'type':'any',"
				+ "		'default':'any'"
				+ "		}"
				+ "},"
				+ "'OPT outputs':{'ANY keyname':{"
				+ "		'type':'any',"
				+ "		'default':'any'"
				+ "		}"
				+ "}"
				+ "}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'XOR result':"+Workflow.getJSONDescription()+","
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','taskID':7, 'inputs': "
				+ "{'amount':{'type':'number','default':'11'}}}");
	}

	@Override
	public String getGeneralDescription() {
		return "Updates in & outputs of a workflow; difference to task.Update: it does not set the "
				+ "values for in & outputs, but actually creates in & outputs dynamically for a "
				+ "workflow, its key, type and default value";
	}

}
