/** MIT License
 * @author Lukas Treyer
 * @date Mar 24, 2016
 * */

package luci.service.workflow;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.Workflow;
import luci.core.WorkflowElement;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;

public class Save extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		long taskID = input.getHeader().getLong("taskID");
		WorkflowElement wfe = WorkflowElements.strictlyGet(taskID);
		if (wfe instanceof Workflow){
			return new Message(new JSONObject().put("result", 
					new JSONObject().put("change", ((Workflow) wfe).safe()).put("taskID", taskID)));
		}
		throw new IllegalArgumentException(
			"WorkflowElement referenced by "+taskID+" is no Workflow but "+wfe.getClass().getSimpleName());
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"','taskID':'number'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'XOR result':{'change':'boolean'},"
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject().put("run", getName()).put("taskID", 6);
	}

	@Override
	public String getGeneralDescription() {
		return "Saves a workflow to disk (in Luci). Luci uses H2's mvstore to serialize workflows"
				+ " to disk. In the future this would also allow to restore earlier versions of the "
				+ "workflow (no version tree, but linear versions).\n"
				+ "@output.change indicates whether the workflow has actually been saved";
	}

}
