/** MIT License
 * @author Lukas Treyer
 * @date Mar 24, 2016
 * */

package luci.service.workflow;

import java.util.Map.Entry;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.Workflow;
import luci.core.validation.JsonType;

public class List extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		String name = input.getHeader().optString("name", null);
		JSONObject names = new JSONObject();
		for(Entry<Long, String> en: Workflow.names.entrySet()){
			if (name == null || en.getValue().equals(name))
				names.put(en.getKey()+"", en.getValue());
		}
		return new Message(new JSONObject().put("result", names));
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"', 'OPT name':'string'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'result':{'ANY numberAsString':'string'}}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject().put("run", getName());
	}

	@Override
	public String getGeneralDescription() {
		return "Returns a list of all workflows (not only root workflows).\n"
				+ "@input.name restrict the list to only one entry, e.g. {'1':'New Workflow'}\n"
				+ "@output.result {'taskID':'name'}, not a list but a json object; since keys in "
				+ "json cannot be numbers, the taskIDs are converted to string";
	}

}
