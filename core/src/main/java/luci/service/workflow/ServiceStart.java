/** MIT License
 * @author Lukas Treyer
 * @date May 31, 2016
 * */
package luci.service.workflow;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Luci;
import luci.core.Service;
import luci.core.ServiceLocal;
import luci.core.Workflow;
import luci.core.Workflow.ServiceConfig;
import luci.core.validation.JsonType;

public class ServiceStart extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Workflow wf = ServiceGet.validateTaskID(h.getLong("taskID"));
		Map<String, Map<Integer, ServiceConfig>> configs = 
			wf.getWorkflowConfig().getConfigsPerIp(ServiceGet.getIDsJSONArray(h.optJSONArray("serviceIDs")));
		JSONObject nodes = new JSONObject();
		for (Entry<String, Map<Integer, ServiceConfig>> en: configs.entrySet()){
			Map<String, Map<Integer, String>> node = new HashMap<>();
			for (Entry<Integer, ServiceConfig> en2: en.getValue().entrySet()){
				ServiceConfig conf = en2.getValue();
				String serviceName = conf.getServiceName();
				Map<Integer, String> perServiceName = node.get(serviceName);
				if (perServiceName == null) 
					node.put(serviceName, (perServiceName = new HashMap<>()));
				perServiceName.put(conf.getId(), conf.getArgs());
			}
			nodes.put(en.getKey(), node);
		}
		System.out.println(nodes);
		JSONObject command = new JSONObject()
				.put("run", "ServiceControl")
				.put("start", new JSONObject().put("nodes", nodes));
		Service s = Luci.observer().createService("ServiceControl", new Message(command));
		s.addListeners(this.getListeners());
		s.start();
		this.getListeners().clear();
		return null;
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'taskID':'number','OPT serviceIDs':'list'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'started':{'ANY IP':{'ANY serviceName':'number'}}}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject().put("run", getName()).put("taskID", 5);
	}

	@Override
	public String getGeneralDescription() {
		return "Start up all the services that are configured to run with the given workflow (taskID).\n"
				+ "@input.serviceIDs optionally restrict the instances to be started only to the given IDs.";
	}

}
