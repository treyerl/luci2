/** MIT License
 * @author Lukas Treyer
 * @date May 25, 2016
 * */
package luci.service.workflow;

import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Factory;
import luci.core.FactoryObserver.NotAServiceException;
import luci.core.FactoryRemoteServices;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.Workflow;
import luci.core.WorkflowElement;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;

public class ServiceGet extends ServiceLocal {
	
	static Workflow validateTaskID(long taskID){
		WorkflowElement wfe = WorkflowElements.get(taskID);
		if (wfe == null)
			throw new IllegalArgumentException("invalid taskID '"+taskID+"'");
		if (!(wfe instanceof Workflow))
			throw new IllegalArgumentException("taskID '"+taskID+"' is not pointing to a workflow");
		return (Workflow) wfe;
	}
	
	static String validateServiceName(String serviceName){
		try {
			Factory f = Luci.observer().getServiceFactory(serviceName);
			if (!(f instanceof FactoryRemoteServices)) 
				throw new IllegalArgumentException(serviceName+" not a remote service");
		} catch (NotAServiceException e){
			// apparently it must be a remote service; local services are always registered
		}
		return serviceName;
	}
	
	static Set<Integer> getIDsJSONArray(JSONArray ids){
		if (ids != null){
			Set<Integer> sids = new HashSet<>();
			for (Object id: ids) sids.add((Integer) id);
			return sids;
		}
		return null;
	}
	
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Workflow wf = validateTaskID(h.getLong("taskID"));
		return wrapResult(new JSONObject(wf.getWorkflowConfig()
				.getConfigsPerIpAsJSON(getIDsJSONArray(h.optJSONArray("ids")))));
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'taskID':'number', 'OPT ids':'list'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'ANY IP':{'ANY id':"
				+ "{'serviceName':'string','args':'string','isAutoID':'boolean'}}}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','taskID':7}");
	}

	@Override
	public String getGeneralDescription() {
		return "Gets the services, the instance ids and corresponding startup args associated with this workflow.\n"
				+ "@input.taskID the taskID to indentify the workflow\n"
				+ "@input.ids filter the instances to show only the ones with the given ids";
	}

}
