/** MIT License
 * @author Lukas Treyer
 * @date May 25, 2016
 * */
package luci.service.workflow;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.Workflow;
import luci.core.Workflow.ServiceConfig;
import luci.core.validation.JsonType;

public class ServiceRemove extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Workflow wf = ServiceGet.validateTaskID(h.getLong("taskID"));
		ServiceConfig sconfig = wf.getWorkflowConfig().remove(h.getInt("id"));
		if (sconfig != null) ServiceGet.validateServiceName(sconfig.getServiceName());
		return wrapResult(new JSONObject().put("success", sconfig != null));
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'taskID':'number','id':'number'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'success':'boolean'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':"+getName()+", 'taskID':1,'id':5}");
	}

	@Override
	public String getGeneralDescription() {
		return "Remove a service instance from a workflow configuration.\n"
				+ "@input.id the instance id";
	}

}
