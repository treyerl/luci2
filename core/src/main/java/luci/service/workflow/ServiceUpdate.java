/** MIT License
 * @author Lukas Treyer
 * @date May 25, 2016
 * */
package luci.service.workflow;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.Workflow;
import luci.core.Workflow.ServiceConfig;
import luci.core.Workflow.WorkflowConfig;
import luci.core.validation.JsonType;

public class ServiceUpdate extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Workflow wf = ServiceGet.validateTaskID(h.getLong("taskID"));
		ServiceGet.validateServiceName(h.getString("serviceName"));
		WorkflowConfig wfconfig = wf.getWorkflowConfig();
		return wrapResult(new JSONObject().put("success", true)
			.put("id", wfconfig.setServiceConfig(new ServiceConfig(h))));
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'taskID':'number','ip':'string','serviceName':'string',"
				+ "'id':'number','args':'string','OPT isAutoID':'boolean'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'success':'boolean'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':"+getName()+", 'taskID':1, 'serviceName':'3DViewer','id':5,"
				+ "'args':' -screen 4 -x 1920 -y 2160 -w 1920 -h 1080 -frameless yes','ip':'127.0.0.1'}");
	}

	@Override
	public String getGeneralDescription() {
		return "Update a service instance that is coupled to a workflow configuration.\n"
				+ "@input.args arguments to be used to start the service with\n"
				+ "@input.id the instance id\n"
				+ "@input.ip the IPv4 of the machine on which the service should run\n"
				+ "@input.serviceName the name of the service to start (using exec parameter from "
				+ "the service installation)";
	}

}
