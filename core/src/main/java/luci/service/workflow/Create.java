/** MIT License
 * @author Lukas Treyer
 * @date Mar 24, 2016
 * */

package luci.service.workflow;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.Workflow;
import luci.core.validation.JsonType;

public class Create extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		Workflow wf = new Workflow(input);
		if (wf.getName() == null) wf.setName("Workflow."+wf.getTaskID());
		return new Message(new JSONObject().put("result", wf.toJSONObject()));
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"',"
				+ "'OPT name':'string',"
				+ "'OPT group':'list'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'XOR result':"+Workflow.getJSONDescription()+","
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','group':[5,7,8,13]}");
	}

	@Override
	public String getGeneralDescription() {
		return "Create a workflow.\n"
				+ "@input.group a list of taskIDs that should be grouped together into a workflow";
	}

}
