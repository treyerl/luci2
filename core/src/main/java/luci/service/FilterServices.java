/** MIT License
 * @author Lukas Treyer
 * @date Jul 29, 2016
 * */
package luci.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.JSON;
import luci.connect.Message;
import luci.core.Factory;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class FilterServices extends ServiceLocal {
	
	Set<String> keys;
	Set<String> types;
	JSONObject jsonMatch;
	int rcrLevel = 0;
	
	boolean recursiveKeyMatch(JSONObject j, int level){
		if (rcrLevel > 0 && level < rcrLevel || rcrLevel == 0){
			if (j.keySet().containsAll(keys)) return true;
			else {
				for (String key: j.keySet()){
					Object o = j.get(key);
					if (o instanceof JSONObject && recursiveKeyMatch((JSONObject) o, level + 1))
						return true;
					else if (o instanceof JSONArray){
						for (Object oa: (JSONArray)o){
							if (oa instanceof JSONObject && recursiveKeyMatch((JSONObject) oa, level + 2))
								return true;
						}
					}
				}
				return false;
			}
		} else return false;
	}
	
	boolean recursiveValueMatch(JSONObject j, int level){
		if (rcrLevel > 0 && level < rcrLevel || rcrLevel == 0){
			if (JSON.ObjectValuesToStringSet(j).containsAll(types)) return true;
			else {
				for (String key: j.keySet()){
					Object o = j.get(key);
					if (o instanceof JSONObject && recursiveValueMatch((JSONObject) o, level + 1))
						return true;
					else if (o instanceof JSONArray){
						JSONArray ja = (JSONArray)o;
						if (ja.length() == 1) return types.contains(ja.optString(0));
						for (Object oa: ja){
							if (oa instanceof JSONObject && recursiveValueMatch((JSONObject) oa, level + 2))
								return true;
						}
					} 
				}
				return false;
			}
		} else return false;
	}
	
	boolean recursiveJsonMatch(JSONObject testee, JSONObject template, int level){
		if (rcrLevel > 0 && level < rcrLevel || rcrLevel == 0){
			Set<String> templateStrings = JSON.ObjectValuesToStringSet(template);
			Set<Integer> templateInts = JSON.ObjectValuesToIntSet(template);
			Set<String> testeeStrings = JSON.ObjectValuesToStringSet(template);
			Set<Integer> testeeInts = JSON.ObjectValuesToIntSet(template);
			boolean hasAnyArrayOrObject = false;
			for (String key: template.keySet()) {
				Object o = template.get(key);
				if (o instanceof JSONObject || o instanceof JSONArray){
					hasAnyArrayOrObject = true;
					break;
				}
			}
			if (!hasAnyArrayOrObject 
					&& testee.keySet().containsAll(template.keySet())
					&& testeeStrings.containsAll(templateStrings)
					&& testeeInts.containsAll(templateInts))
				return true;
			else {
				for (String key: template.keySet()){
					Object o = template.get(key);
					Object ot = testee.opt(key);
					if (o instanceof JSONObject && ot instanceof JSONObject){
						if ((recursiveJsonMatch((JSONObject)ot, (JSONObject)o, level + 1)))
							return true;
					} else if (o instanceof JSONArray && ot instanceof JSONArray){
						JSONArray ja = (JSONArray) o;
						JSONArray ta = (JSONArray) ot;
						for (int i = 0; i < ja.length(); i++){
							Object oa = ja.get(i);
							Object ota = ta.get(i);
							if (oa instanceof JSONObject && ota instanceof JSONObject){
								if (recursiveJsonMatch((JSONObject)ota, (JSONObject)oa, level + 2))
									return true;
							}
						}
					}
				}
				return false;
			}
		} else return false;
	}
	
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Set<String> serviceNames = new HashSet<>();
		rcrLevel = h.optInt("rcrLevel");
		if (h.has("keys")){
			keys = JSON.ArrayToStringSet(h.getJSONArray("keys"));
		} else if(h.has("types")){
			types = JSON.ArrayToStringSet(h.getJSONArray("types"));
		} else if (h.has("jsonMatch")){
			jsonMatch = h.getJSONObject("jsonMatch");
		}
		for (String serviceName : Luci.observer().getServiceNames()){
			Factory sf = Luci.observer().getServiceFactory(serviceName);
			JSONObject info = sf.getInfo(false);
			if (keys != null && recursiveKeyMatch(info, 0)) 
				serviceNames.add(serviceName);
			else if (types != null && recursiveValueMatch(info, 0)) 
				serviceNames.add(serviceName);
			else if (jsonMatch != null && recursiveJsonMatch(info, jsonMatch, 0)) 
				serviceNames.add(serviceName);
		}
		List<String> sortedServiceNames = new ArrayList<>(serviceNames);
		sortedServiceNames.sort(Comparator.naturalOrder());
		return wrapResult(new JSONObject().put("serviceNames", sortedServiceNames));
	}
	
	@Override
	public String getGeneralDescription() {
		return "Filters services according to a) its keys at a given recursion level, b) its "
				+ "types at a given recursion level or c) a given json template with values "
				+ "equal to 'null' meaning only keys are compared. Recursion level 0 means all"
				+ " levels are checked; restrict to one level with rcrLevel = 1.";
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription(""
				+ "{'XOR keys':['listOf','string'],"
				+ " 'XOR types':['listOf','string'],"
				+ " 'XOR jsonMatch':'json',"
				+ " 'OPT rcrLevel':'number'"
				+ "}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'serviceNames':['listOf', 'string']}");
	}
	@Override
	public JSONObject getExampleCall() {
		return wrapExampleCall("{'keys':['customKey1', 'customKey2']}");
	}

}
