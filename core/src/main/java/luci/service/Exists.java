package luci.service;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class Exists extends ServiceLocal {
	
	public boolean serviceName(String serviceName){
		return Luci.observer().getServiceNames().contains(serviceName);
	}
	
	public boolean taskID(int taskID){
		try {
			return luci.core.WorkflowElement.WorkflowElements.get(taskID) != null;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		if (h.has("serviceName")){
			return wrapResult(new JSONObject().put("exists", serviceName(h.getString("serviceName"))));
		} else if (h.has("taskID")){
			return wrapResult(new JSONObject().put("exists", taskID(h.getInt("taskID"))));
		}
		return null;
	}
	
	@Override
	public String getGeneralDescription(){
		return "Tests whether a service with the given serviceName or a task with given taskID exists.\n"
				+ "@input.taskID the taskID of the task to be tested \n "
				+ "@input.serviceName the name of the service to be tested";
	}
	
	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'XOR serviceName':'string','XOR instanceID':'number'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'exists':'boolean'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'Exists','serviceName':'Download'}");
	}

}
