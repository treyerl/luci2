/** MIT License
 * @author Lukas Treyer
 * @date Jan 9, 2016
 * */
package luci.service;

import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.JSON;
import luci.connect.Message;
import luci.core.FactoryRemoteServices;
import luci.core.Luci;
import luci.core.RemoteHandle;
import luci.core.ServerSocketHandler;
import luci.core.ServiceLocal;
import luci.core.validation.JsonType;

public class RemoteDeregister extends ServiceLocal {
	
	public JSONObject deregister(RemoteHandle rh){
		rh.deregister();
		ServerSocketHandler ssh = rh.getConnection();
		if (ssh.getNumRegistered() == 0) ssh.stopListening();
		FactoryRemoteServices frs = rh.getFactory();
		return new JSONObject()
				.put("remainsAvailable", Luci.observer().hasService(frs.getName()))
				.put("deregisteredName", frs.getName())
				.put("workerID", rh.getWorkerID())
				.put("nodeIP", rh.getIP()
				);
	}
	
	@Override
	public String getGeneralDescription(){
		return "Deregisters a client from a service registration. This will cancel any running "
				+ "service call, cancel all subscriptions of this client and, if the service to be "
				+ "deregistered is the last remaining of its kind, its serviceName will be removed"
				+ "from the list of available services.\n"
				+ "@output.result.deregisteredName Intended to inform listeners about the service name that was deregistered\n"
				+ "@output.result.id int, the id that was deregistered\n"
				+ "@output.result.nodeIP Intended to inform listeners about the IP of the machine that deregistered the service\n"
				+ "@output.result.remainsAvailable boolean to indicate whether other services with the same"
				+ "name remain available (true) or whether the service is being deleted from the list"
				+ "of available services (false)";
	}
	
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		ServerSocketHandler sh = (ServerSocketHandler) input.getSourceSocketHandler();
		Map<Long, RemoteHandle> remoteHandles = sh.getRemoteHandles();
		Set<Long> requestedIDs = JSON.ArrayToLongSet(h.optJSONArray("workerIDs"));
		if (requestedIDs == null) requestedIDs = remoteHandles.keySet();
		if (requestedIDs.size() == 0) 
			return new Message(new JSONObject().put("error", "No service registered!"));
		JSONArray deregistered = new JSONArray();
		for (Long id: requestedIDs){
			RemoteHandle r = sh.getRemoteHandles().get(id);
			if (r != null) deregistered.put(deregister(r));
		}
		return wrapResult(new JSONObject().put("deregisteredServices", deregistered));
	}

	@Override
	public JsonType getInputDescription() {
		return wrapInputDescription("{'OPT workerIDs':['listOf','number']}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'deregisteredServices':['listOf',{'remainsAvailable':'boolean',"
				+ "'deregisteredName':'string','workerID':'number','nodeIP':'string'}]}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject().put("run", getClass().getSimpleName());
	}

}
