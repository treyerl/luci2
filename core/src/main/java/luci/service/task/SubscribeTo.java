/** MIT License
 * @author Lukas Treyer
 * @date Jan 18, 2016
 * */
package luci.service.task;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Message;
import luci.core.EndOfServiceNotificationWriter;
import luci.core.EndOfServiceResultWriter;
import luci.core.Listener;
import luci.core.ListenerManager;
import luci.core.Luci;
import luci.core.ServerSocketHandler;
import luci.core.ServiceLocal;
import luci.core.SubscriptionManager;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;

public class SubscribeTo extends ServiceLocal implements SubscriptionManager {
	static final Set<Class<? extends Listener>> managedSubscriptionTypes;
	static {
		Set<Class<? extends Listener>> l = new HashSet<>();
		l.add(EndOfServiceResultWriter.class);
		l.add(EndOfServiceNotificationWriter.class);
		managedSubscriptionTypes = Collections.unmodifiableSet(l);
	}
	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Listener listener;
		ServerSocketHandler sh = (ServerSocketHandler) input.getSourceSocketHandler();
		if (h.has("inclResults") && h.getBoolean("inclResults"))
			 listener = sh.getServiceResultWriter();
		else listener = sh.getServiceNotificationWriter();
		
		if (h.has("serviceName")) {
			ListenerManager lm = Luci.observer().getServiceFactory(h.getString("serviceName"));
			unsubscribeFrom(lm, sh);
			listener.startListeningTo(lm);
		} else {
			JSONArray taskIDs = h.getJSONArray("taskIDs");
			for (int i = 0; i < taskIDs.length(); i++){
				listener.startListeningTo(WorkflowElements.get(taskIDs.getInt(i)));
			}
		}
		return wrapResult(new JSONObject().put("success", true));
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"', 'XOR serviceName':'string', "
				+ " 'XOR taskIDs':'list', 'OPT inclResults':'boolean'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'success':'boolean'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"', 'taskIDs':[12,15], inclResults: true}");
	}

	@Override
	public String getGeneralDescription() {
		return "Subscribes the current client to a taskID or any call to a service indicated by"
				+ "serviceName\n"
				+ "@input.serviceName if a client subscribes to a service, it gets a notification "
				+ "upon any output created by a service with that name (technically: the client "
				+ "subscribes to the service factory rather than to a specific service)\n"
				+ "@input.taskIDs a list of taskIDs to which the client should be subscribed\n"
				+ "@input.inclResults specifies whether the client that subscribes only get a "
				+ "notification (false) or all the results produced by a service (true)";
	}

	@Override
	public Set<Class<? extends Listener>> getManagedSubscriptionTypes() {
		return managedSubscriptionTypes;
	}

}
