/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package luci.service.task;

import java.util.Set;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.ServiceLocal;
import luci.core.Workflow;
import luci.core.WorkflowElement;
import luci.core.WorkflowElement.Position;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;

public class Update extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		WorkflowElement wfe = WorkflowElements.strictlyGet(h.getInt("taskID"));
		Set<String> unknownKeys = null;
		// inputs
		if (h.has("inputs")){
			JSONObject inputs = h.getJSONObject("inputs");
			unknownKeys = wfe.setAndValidateInputs(inputs);
		}
		// outputs
		if (wfe instanceof Workflow && h.has("outputs")){
			((Workflow) wfe).updateOutputSubscriptions(h.getJSONObject("outputs"));
		}
		
		// position
		if (h.has("position")){
			JSONObject p = h.getJSONObject("position");
			wfe.setPosition(new Position().setX(p.getInt("x")).setY(p.getInt("y")));
		}
		// listensToDone
		wfe.updateSubscriptions(h.optJSONArray("listensToDone"));
		if (h.has("name")) wfe.setName(h.getString("name"));
		JSONObject result = new JSONObject().put("taskID", wfe.getTaskID());
		if (unknownKeys != null && unknownKeys.size() > 0) result.put("unknownKeys", unknownKeys);
		return new Message(new JSONObject().put("result", result));
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"',"
				+ "'taskID':'number','OPT inputs':'json','OPT outputs':'json',"
				+ "'OPT position':{'x':'number','y':'number'},"
				+ "'OPT listensToDone':'list', 'OPT name':'string'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'XOR result':{'taskID':'number', 'OPT unknownKeys':'list'},"
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','taskID':5,'inputs':{'in1':25}}");
	}

	@Override
	public String getGeneralDescription() {
		return "Update a the input values / subscriptions of a task.\n"
				+ "@input.inputs inputs to be changed; subscriptions are represented by 'property':{'taskID':'number','key':'string (output key)'}\n"
				+ "@input.listensToDone a list of taskIDs to which the task should listen generally (=not to a specific output)\n"
				+ "@input.name the name of the task (can be different to service name)\n"
				+ "@input.outputs if the task to be updated is a workflow that is contained by "
				+ "another workflow (e.g. a group) the workflow can have outputs that might be linked "
				+ "to outputs of some task it contains";
	}

}
