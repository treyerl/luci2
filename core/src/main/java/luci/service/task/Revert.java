package luci.service.task;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.WorkflowElement;
import luci.core.validation.JsonType;

public class Revert extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		long taskID = input.getHeader().getLong("taskID");
		WorkflowElement wfe = Luci.observer().loadWorkflowElement(taskID);
		if (wfe == null) throw new IllegalStateException("No stored task with ID "+taskID);
		return new Message(new JSONObject().put("result", wfe.toJSONObject()));
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"','taskID':'number'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'XOR result':"
				+ WorkflowElement.getJSONDescription()
					.put("OPT elements", "list")
					.put("OPT services", "list")+","
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','taskID':3}");
	}

	@Override
	public String getGeneralDescription() {
		return "Reverts a task to the last saved state.\n"
				+ "@output.result refer to <a href='#task.get'>task.Get</a> for descriptions.";
	}

}
