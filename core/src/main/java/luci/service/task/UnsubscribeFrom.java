/** MIT License
 * @author Lukas Treyer
 * @date Jan 19, 2016
 * */
package luci.service.task;

import java.util.HashSet;
import java.util.Set;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Listener;
import luci.core.Luci;
import luci.core.ServerSocketHandler;
import luci.core.ServiceLocal;
import luci.core.SubscriptionManager;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;

public class UnsubscribeFrom extends ServiceLocal implements SubscriptionManager{

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		ServerSocketHandler ssh = (ServerSocketHandler) input.getSourceSocketHandler();
		JSONObject r = new JSONObject().put("result", new JSONObject().put("success", true));
		if (h.has("serviceName")) {
			String serviceName = h.getString("serviceName");
			if (!unsubscribeFrom(Luci.observer().getServiceFactory(serviceName), ssh))
				r = new JSONObject().put("error", "No subscription found for "+serviceName);
		} else if (h.has("taskIDs")) {
			org.json.JSONArray taskIDs = h.getJSONArray("taskIDs");
			Set<Long> notFor = new HashSet<>();
			for (int i = 0; i < taskIDs.length(); i++){
				long taskID = taskIDs.getLong(i);
				if (!ssh.stopListeningTo(WorkflowElements.get(taskID))) notFor.add(taskID);
			}
			if (notFor.size() > 0)
				r = new JSONObject().put("error", "No subscription found for "+notFor);
		}
		return new Message(r);
		
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"','XOR serviceName':'string','XOR taskIDs':'list'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return wrapResultDescription("{'success':'boolean'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"', 'taskIDs':[12,15]}");
	}

	@Override
	public String getGeneralDescription() {
		return "Unsubscribe from a service or a list of taskIDs.";
	}

	@Override
	public Set<Class<? extends Listener>> getManagedSubscriptionTypes() {
		return SubscribeTo.managedSubscriptionTypes;
	}
}
