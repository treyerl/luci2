/** MIT License
 * @author Lukas Treyer
 * @date Dec 13, 2015
 * */
package luci.service.task;

import luci.connect.Message;
import luci.core.Factory;
import luci.core.Factory.Task;
import luci.core.validation.JsonType;
import luci.core.Luci;
import luci.core.ServiceLocal;

import org.json.JSONObject;

public class Create extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		Factory f = Luci.observer().getServiceFactory(h.getString("serviceName"));
		Task i = f.createTask(input);
		return new Message(new JSONObject().put("result", 
				new JSONObject().put("taskID", i.getTaskID()).put("name", i.getName())));
	}
	
	@Override
	public String getGeneralDescription() {
		return "Create a task. A task corresponds to a node in the workflow diagram.\n"
				+ "@input.parentID the taskID of the workflow in which this task is being created\n"
				+ "@input.position x/y from top left, defaults to 50/50\n"
				+ "@input.inputs if omitted example values will be inserted\n"
				+ "@input.listensToDone a list of taskIDs to which this task generally listens (=not listens to specific outputs)";
	}
	
	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"',"
				+ "'serviceName':'string',"
				+ "'parentID':'number',"
				+ "'position':{'x':'number','y':'number'},"
				+ "'OPT inputs':'json',"
				+ "'OPT listensToDone':'list'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'XOR result':{'taskID':'number','name':'string'},"
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'task.Create','serviceName':'ServiceList',"
							+ "'parentID':0,'position':{'x':200,'y':100}}");
	}

}
