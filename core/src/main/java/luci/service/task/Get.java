/** MIT License
 * @author Lukas Treyer
 * @date Dec 17, 2015
 * */
package luci.service.task;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Factory;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.WorkflowElement;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;

public class Get extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		JSONObject result = new JSONObject();
		Factory f;
		if (h.has("taskID")){
			int taskID = h.getInt("taskID");
			WorkflowElement wfe = WorkflowElements.strictlyGet(taskID);
			result.put("task", wfe.toJSONObject());
		} else if (h.has("serviceName")) {
			f = Luci.observer().getServiceFactory(h.getString("serviceName"));
			result.put("taskIDs", f.getTaskIDs());
		}
		return new Message(new JSONObject().put("result", result));
	}
	
	@Override
	public String getGeneralDescription() {
		return "Returns infos about a task (requesting a taskID) or returns a list if ids representing"
				+ "the same service\n"
				+ "@input.serviceName requests a list of taskID representing the same service\n"
				+ "@input.taskID requests infos about a specific task\n"
				+ "@output.result.task.elements if the task is a workflow this list contains all children\n"
				+ "@output.result.task.services if the task is a workflow this list contains all services that need to started up (if any are set)\n"
				+ "@output.result.task.inputSchema the current input values\n"
				+ "@output.result.task.inputs the inputs with subscriptions being represented as {'taskID':'number','key':'string (the name of the output key of the task referenced by taskID)'}\n"
				+ "@output.result.task.listensToDone a list of taskIDs the task is listening to generally (=not to specific outputs)\n"
				+ "@output.result.task.name can be different from service name\n"
				+ "@output.result.task.parentID the taskID of a workflow to which this task belongs; 0 = no parent ID / root workflow\n"
				+ "@output.result.task.position {'x':'number','y':'number'}\n";
	}
	
	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"',"
				+ "'XOR taskID':'number',"
				+ "'XOR serviceName':'string'}");
	}

	@Override
	public JsonType getOutputDescription() {
		String task = WorkflowElement.getJSONDescription()
				.put("OPT elements", "list")
				.put("OPT services", "list")
				.toString();
		return new JsonType("{'XOR result':{"
				+ "		'XOR task':"+task+","
				+ "		'XOR taskIDs':'list'},"
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','taskID':3}");
	}

}
