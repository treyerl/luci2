/** MIT License
 * @author Lukas Treyer
 * @date Mar 24, 2016
 * */

package luci.service.task;

import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Factory;
import luci.core.Luci;
import luci.core.ServiceLocal;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;

public class Remove extends ServiceLocal {

	@Override
	public Message implementation() throws Exception {
		JSONObject h = input.getHeader();
		JSONObject result = new JSONObject();
		Set<Long> success = new HashSet<>();
		Set<Long> failure = new HashSet<>();
		if (h.has("taskIDs")){
			JSONArray taskIDs = h.getJSONArray("taskIDs");
			
			for (int i = 0; i < taskIDs.length(); i++){
				long id = taskIDs.getLong(i);
				try {
					WorkflowElements.remove(id);
					success.add(id);
				} catch (Exception e){
					failure.add(id);
				}
			}
			
		} else if (h.has("serviceName")) {
			Factory f = Luci.observer().getServiceFactory(h.getString("serviceName"));
			f.removeAllTasks(success, failure);
		}
		result.put("success", success).put("failure", failure);
		return wrapResult(new JSONObject().put("taskIDs", result));
	}

	@Override
	public JsonType getInputDescription() {
		return new JsonType("{'run':'"+getName()+"',"
				+ "'XOR taskIDs':'list',"
				+ "'XOR serviceName':'string'}");
	}

	@Override
	public JsonType getOutputDescription() {
		return new JsonType("{'XOR result':{'taskIDs':{'success':'list','failure':'list'}},"
				+ "'XOR error':'string'}");
	}

	@Override
	public JSONObject getExampleCall() {
		return new JSONObject("{'run':'"+getName()+"','taskIDs':[3,4]}");
	}

	@Override
	public String getGeneralDescription() {
		return "Remove tasks.\n"
				+ "@input.serviceName removes all tasks representing the service with this name\n"
				+ "@input.taskIDs a list of taskIDs to be removed";
	}

}
