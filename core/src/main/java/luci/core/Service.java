/** MIT License
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * */


package luci.core;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;

import luci.connect.JSON;
import luci.connect.Message;
import luci.core.validation.JsonType;
import luci.service.user.Group;
import luci.service.user.Subject;
import luci.service.user.User;

public abstract class Service implements TaskInfo{
	private final static File configFile = new File(Luci.cwd(), "data"+File.separator+"servicePermissions.json");
	public final static JSONObject config = JSON.fromFile(configFile);
	
	protected long startTime = 0;
	private long parentID = 0;
	private long taskID = 0;
	private long callID = 0;
	protected Message input;
	private Listener caller;
	private Future<Message> futureResult;
	protected final Set<Listener> listeners = new CopyOnWriteArraySet<>();
	private Set<Integer> permittedTo;
	
	void setPermissions(){
		permittedTo = JSON.notNullInteger(JSON.ArrayToIntSet(config.optJSONArray(getName())));
	}
	
	public Service setCaller(Listener caller){
		this.caller = caller;
		return this;
	}
	
	public Listener getCaller(){
		return caller;
	}
	
// ------------ publish n' subscribe ------------ //	
	
	public final boolean addListener(final Listener listener) {
		return listeners.add(listener);
	}
	
	public final boolean addListeners(Set<Listener> listenersToAdd){
		return listeners.addAll(listenersToAdd);
	}

	public final boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}
	
	public Set<Listener> getListeners(){
		return listeners;
	}
	
	protected void replaceServiceResultWriterSocketHandlerWith(ServerSocketHandler node) {
		for (Listener sl: listeners) 
			if (sl instanceof EndOfServiceResultWriter) 
				((EndOfServiceResultWriter) sl).setSocketHandler(node);
	}
	
// ------------- when called as worker for instances ----------- //

	public final void setTaskID(long id){
		if (taskID != 0) throw new IllegalStateException("Task ID has been set already");
		if (id <= 0) throw new NumberFormatException("Task ID must be > 0");
		taskID = id;
	}
	
	public long getTaskID() {
		return taskID;
	}
	
	public final void setParentID(long id){
		if (parentID != 0) throw new IllegalStateException("ParentID has been set already");
		if (id <= 0) throw new NumberFormatException("ParentID must be > 0");
		parentID = id;
	}
	
	public long getParentID(){
		return parentID;
	}
	
// ------------- user permissions -------------- //
	void permitTo(Set<Integer> userIDs) throws IOException{
		if (userIDs.stream().anyMatch((id)-> id < 0)){
			throw new IllegalArgumentException("userIDs must be all positive");
		}
		if (permittedTo.addAll(userIDs)){
			config.put(getName(), permittedTo);
			JSON.toFile(config, configFile);
		}
	}
	
	void cancelPermissionFor(Set<Integer> userIDs) throws IOException{
		if (permittedTo.removeAll(userIDs)){
			config.put(getName(), permittedTo);
			JSON.toFile(config, configFile);
		}
	}
	
	public boolean isPermittedFor(User u){
		if (u == null) return permittedTo.size() == 0;
		return isPermittedFor(u.getID()) || u.getGroupIDs().stream().anyMatch((id)->permittedTo.contains(id));
	}
	
	public boolean isPermittedFor(int id){
		if (permittedTo.size() > 0){
			return  permittedTo.contains(id);
		}
		return true;
	}
	
	public Set<Subject> getSubjectsWithPermission(){
		return Subject.allSubjects().values().stream().filter(
				(s)->permittedTo.contains(s.getID())).collect(Collectors.toSet());
	}
	
	public Set<User> getUsersWithPermission(){
		return getSubjectsWithPermission().stream().filter(
				(s)-> s instanceof User).map((s)->(User)s).collect(Collectors.toSet());
	}
	
	public Set<Group> getGroupsWithPermission(){
		return getSubjectsWithPermission().stream().filter(
				(s)-> s instanceof Group).map((s)->(Group)s).collect(Collectors.toSet());
	}
	
// ------------------ service ------------------ //	
	
	public String getName(){
		return getClass().getName().replace("luci.service.", "");
	}
	
	public void setInput(Message input){
		if (!isRunning()) this.input = input;
		else throw new IllegalStateException("Cannot set inputs while service is running!");
	}
	
	public Message getInput(){
		return input;
	}
	
	public Service setCallID(long callID){
		this.callID = callID;
		return this;
	}
	
	public long getCallID(){
		return callID;
	}
	
	/** Alias to {@link luci.core.Service#getFutureResult()}
	 * @throws InterruptedException 
	 */
	public Future<Message> run(Message initProgress, long initDuration) {
		return getFutureResult(initProgress, initDuration);
	}
	
	public Future<Message> run() {
		return getFutureResult();
	}
	
	/**If the service has not yet been started, it is being started. Otherwise the existing 
	 * Future&lt;Message&gt; is being returned
	 * @return Future&lt;Message&gt;
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	Future<Message> getFutureResult(Message initProgress, long initDuration)  {
		if (futureResult == null) {
//			not really necessary. let user send progress messages themselves
//			progressNotification(initProgress, initDuration);
			futureResult = start();
		}
		return futureResult;
	}
	
	public Future<Message> getFutureResult() {
		return getFutureResult(new Message(new JSONObject("{'progress':0}")), 0);
	}
	
	/**blocks if the future/result is not done yet 
	 * @return
	 * @throws IOException 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public Message getResult() throws IOException, InterruptedException, ExecutionException{
		Future<Message> future = getFutureResult();
		return future.get();
	}
	
	public abstract Future<Message> start() ;
	
	/**Notify service listeners of a progress
	 * @param p Message containing a percentage and optional intermediate results. Header format:
	 * <pre>{
	 * 'progress':'number in [0, 100)', 
	 * 'callID':'number', (will be added by Luci) 
	 * }</pre>
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws JSONException 
	 */
	private void progressNotification(Message p, long duration) {
		JSONObject header = p.getHeader();
		if (header.has("progress")){
			notifyListeners(p, duration);
		} else notifyListeners(new Message(new JSONObject().put("error", 
				this.getName()+": Malformated progress message; 'progress key required")), duration);
	}
	
	public void progressNotification(Message p){
		progressNotification(p, Luci.newTimestamp() - startTime);
	}
	
	public void errorNotification(String error){
		errorNotification(new Message(new JSONObject().put("error", error)));
	}
	
	public void errorNotification(Message m) {
		m.setSourceSocketHandler(input.getSourceSocketHandler());
		m.setCallID(callID);
		notifyListeners(m, Luci.newTimestamp() - startTime);
	}
	
	public abstract String getGeneralDescription();
	
	/**
	 * @return LcMetaJSONObject that describes the inputs of the service.The LcMetaJSONObject is
	 * a JSONObject used by Luci to verify the inputs being sent upon a service call
	 */
	public abstract JsonType getInputDescription();
	
	/**
	 * @return LcMetaJSONObject that describes the outputs of the service.
	 */
	public abstract JsonType getOutputDescription();
	
	/**
	 * @return true if the service is running at the moment
	 */
	public abstract boolean isRunning();
	
	public abstract JSONObject getExampleCall();
	
	/**
	 * @return int - one of the constants defined in Attachment: FILE, ARRAY, NETWORK, DOWNLOAD
	 */
	public abstract int howToHandleAttachments();
}
