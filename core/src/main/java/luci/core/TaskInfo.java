package luci.core;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.pmw.tinylog.Logger;

import com.esotericsoftware.minlog.Log;

import luci.connect.Message;
import luci.connect.SocketHandler;
import luci.connect.TcpSocketHandler;

public interface TaskInfo extends ListenerManager{
	public long getCallID();
	public long getTaskID();
	public long getParentID();
	public String getName();
	
	public Set<? extends Listener> getListeners();
	
	public Listener getCaller();
	
	default void notifyListeners(Message m, long duration) {
		// services might return null instead of a message, see e.g. luci.service.workflow.ServiceStart 
		if (m == null) return;
		
		Set<? extends Listener> listeners = getListeners();
		SocketHandler sh = m.getSourceSocketHandler();
		
		// logging errors and results not progress messages
		Set<String> keys = m.getHeader().keySet();
		if (keys.contains("result") || keys.contains("error")){
			String fromIP = "source disconnected";
			if (sh != null && sh.getRemoteSocketAddress() != null) {
				InetAddress addr = sh.getRemoteSocketAddress().getAddress();
				fromIP = addr.getHostAddress()+":"+sh.getRemoteSocketAddress().getPort();
			}
			List<String> toIPs = listeners.stream()
				.filter(l -> l instanceof EndOfServiceResultWriter)
				.map(l -> {
					InetSocketAddress addr = ((EndOfServiceResultWriter) l).getSocketHandler().getRemoteSocketAddress();
					if (addr != null && addr.getAddress() != null)
						return addr.getAddress().getHostAddress()+":"+addr.getPort();
					return "receiver disconnected";
				})
				.collect(Collectors.toList());
			if (keys.contains("result")) 
				Logger.info("[ok],"+getCallID()+","+getName()+","+duration+","+fromIP+",\""+toIPs+"\",");
			else if (keys.contains("error"))
				Logger.info(keys+","+getCallID()+","+getName()+","+duration+","+fromIP+",\""+toIPs+"\""+",\""+m.getHeader().getString("error").replace("\"", "'")+"\"");
		}
		
		// notification
		synchronized(listeners){
			// download attachments if necessary
			if (m.hasAttachments() && (listeners.size() > 2 || listeners.stream().anyMatch(
					(l)->l instanceof Factory && ((Factory)l).numListeners() > 0
					)) && sh instanceof TcpSocketHandler){
				System.out.println("trying to download attachments");
				m.onAttachmentsReady((l)->{
					for (Listener listener : listeners) {
						try {
							listener.getNotified(this, m, duration);
						} catch (Exception e){
							if (Log.DEBUG) e.printStackTrace();
							// do nothing else if a notification fails
						}
					}
				});
				((TcpSocketHandler)sh).forwardUnlock();
				return;
			}
			for (Listener listener : listeners) {
				try {
					listener.getNotified(this, m, duration);
				} catch (Exception e){
					if (Log.DEBUG) e.printStackTrace();
					// do nothing else if a notification fails
				}
			}
		}
	}
}
