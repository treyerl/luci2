/** MIT License
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * */

package luci.core;

import java.lang.reflect.Constructor;

import luci.connect.Message;

public class FactoryLocalServices extends Factory{
	
	private Class<ServiceLocal> serviceClass;
	public FactoryLocalServices(Class<ServiceLocal> s) throws Exception {
		this(init(s));
		serviceClass = s;
	}
	private FactoryLocalServices(Service s){
		super(s.getName(),
				s.getGeneralDescription(),
				s.getInputDescription(), 
				s.getOutputDescription(), 
				s.getExampleCall()
				);
	}
	
	public Service newService(Message input) throws Exception{
		Constructor<ServiceLocal> ct = serviceClass.getDeclaredConstructor();
		ServiceLocal s = ct.newInstance();
		s.setFactory(this);
		return s;
	}
	
	private static Service init(Class<ServiceLocal> serviceClass) throws Exception{
		Constructor<ServiceLocal> ct = serviceClass.getConstructor();
		return ct.newInstance();
	}
}
