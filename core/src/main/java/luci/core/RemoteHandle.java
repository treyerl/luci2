/** MIT License
 * @author Lukas Treyer
 * @date Aug 26, 2016
 * */
package luci.core;

import java.util.HashSet;
import java.util.function.Supplier;

import org.json.JSONException;
import org.json.JSONObject;

import luci.connect.Message;
import luci.core.FactoryRemoteServices.CallHistoryEntry;

public class RemoteHandle {
	
	long id;
	ServerSocketHandler ssh;
	FactoryRemoteServices frs;
	ServiceRemote current;
	long idleSince = 0, runningSince = 0, writingSince = 0, readingSince = 0, numSentBytes = 0, 
			numReadBytes = 0;
	float idleTimeRatio = 0, trafficTimeRatio = 0, tcRatio = 0;
	int numCalls = 0;
	boolean running = false;
	private HashSet<Supplier<Boolean>> onAvailables = new HashSet<>();
	
	public RemoteHandle(ServerSocketHandler ssh, FactoryRemoteServices frs, long id){
		this.id = id == 0 ? ID.getNew("workerID") : id;
		this.ssh = ssh;
		this.frs = frs;
	}
	
	public long getWorkerID(){
		return id;
	}
	
	public ServerSocketHandler getConnection(){
		return ssh;
	}
	
	public FactoryRemoteServices getFactory(){
		return frs;
	}
	
	public String getIP(){
		return ssh.getRemoteSocketAddress().getAddress().getHostAddress();
	}
	
	/**
	 * @param sr
	 */
	public void sendRun(ServiceRemote s) {
		Message m = (current = s).getInput();
		m.cancelForwardUnlocks();
		m.onWritten((length) -> {
			runningSince = s.startTime = Luci.newTimestamp();
			numSentBytes += length;
			
		});
		/**
		 * @devInfo
		 * One client might register multiple services.
		 * One serviceCall might spawn several sub service calls (a service calling other other services)
		 * --> so a call to a remote should be identified by the id of the remote handle rather than
		 * the actual callID.
		 */
		m.getHeader().put("callID", id);
		ssh.write(m);
		writingSince = Luci.newTimestamp();
		running = true;
	}
	/**
	 * @param callID
	 */
	public void sendCancel(long callID) {
		try {
			ssh.write(new Message(new JSONObject().put("cancel", callID)));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 */
	public void handleCancel() {
		if (current != null){
			current.cancel(false);
			current.errorNotification("service cancelled by service provider");
		}
	}
	
	/**
	 * @param newTimestamp
	 */
	RemoteHandle setIdleSince(long t){
		if (runningSince != 0){
			long idleTime = writingSince - idleSince;
			long runningTime = readingSince - runningSince;
			long writingTime = runningSince - writingSince;
			long readingTime = t - readingSince;
			long totalTime = idleTime + runningTime + writingTime + readingTime;
			float idleRatio = (float)idleTime / totalTime;
			float trafficRatio = (float)(writingTime + readingTime) / totalTime;
			float tcCurrent = (float)(writingTime + readingTime) / (writingTime + readingTime + runningTime); 
			idleTimeRatio = ((idleTimeRatio * numCalls) + idleRatio) / (numCalls + 1);
			trafficTimeRatio = ((trafficTimeRatio * numCalls) + trafficRatio) / (numCalls + 1);
			tcRatio = ((tcRatio * numCalls) + tcCurrent) / (numCalls + 1);
			frs.callHistory.add(new CallHistoryEntry(current.getCallID(), t, 
					writingTime, readingTime, runningTime, idleTime));
		}
		idleSince = t;
		return this;
	}
	
	/**
	 * @return
	 */
	public boolean isBusy(){
		synchronized(frs.availableWorkers){
			return !frs.availableWorkers.contains(this);
		}
	}
	
	public String getServiceName(){
		return frs.getName();
	}

	/**
	 * @return
	 */
	public float getIdleTimeRatio(){
		return idleTimeRatio;
	}

	/**
	 * @return
	 */
	public long getNumReadBytes(){
		return numReadBytes;
	}

	/**
	 * @return
	 */
	public long getNumSentBytes() {
		return numSentBytes;
	}
	

	/**
	 * @param now
	 * @return
	 */
	public long getIdleTime(long now) {
		return now - idleSince;
	}
	
	public float getTransmissionComputationRatio(){
		return tcRatio;
	}
	
	public RemoteHandle register(Message m){
		frs.addRemote(this, m);
		ssh.register(this);
		return this;
	}
	
	public RemoteHandle deregister(){
		frs.deregister(this);
//		ssh.deregister(this);
		return this;
	}

	/**
	 * @param object
	 */
	public void onAvailable(Supplier<Boolean> oa) {
		synchronized(onAvailables){
			onAvailables.add(oa);
		}
	}
	
	/**
	 * @param numRead
	 * @return
	 */
	RemoteHandle becomeAvailable(long numRead) {
		if (running) {
			running = false;
			setIdleSince(Luci.newTimestamp());
			numCalls++;
			numReadBytes += numRead;
			boolean shouldIt = true;
			for (Supplier<Boolean> onAvailable: onAvailables){
				if (!onAvailable.get())
					shouldIt = false;
			}
			if (shouldIt) {
				frs.availableWorkers.add(this);
			}
			onAvailables.clear();
			return this;
		}
		throw new IllegalStateException("Cannot accept two results! Result for call '"
				+current.getCallID()+"' already set!");
	}


	
}
