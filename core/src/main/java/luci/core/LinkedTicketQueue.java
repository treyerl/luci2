/** MIT License
 * @author Lukas Treyer
 * @date Aug 20, 2016
 * */
package luci.core;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import luci.connect.On.OnInt;


public class LinkedTicketQueue<E> extends LinkedBlockingQueue<E>{
	
	public static abstract class Ticket<E> {
		long waitingSince;
		public Ticket(){
			waitingSince = Luci.newTimestamp();
		}
		public abstract void handle(E e);
		public long getWaitingDuration(long now){
			return now - waitingSince;
		}
		public long getWaitingDuration(){
			return getWaitingDuration(Luci.newTimestamp());
		}
	}
	
	private static final long serialVersionUID = 123456789L;
	private final LinkedList<Ticket<E>> tickets = new LinkedList<>();
	private Set<OnInt> sizeChangeListeners = new HashSet<>();
	
	public void onSizeChange(OnInt on){
		synchronized (sizeChangeListeners) {
			sizeChangeListeners.add(on);
		}
	}
	
	public Ticket<E> book(Ticket<E> t){
		synchronized(tickets){
			E el = this.poll();
			if (el != null) {
				for (OnInt newsize: sizeChangeListeners) newsize.react(size()); 
				t.handle(el);
				return null;
			} else {
				tickets.add(t);
			}
		}
		return t;
	}
	
	public boolean cancel(Ticket<E> t){
		synchronized(tickets){
			return tickets.remove(t);
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.concurrent.LinkedBlockingQueue#offer(java.lang.Object)
	 */
	@Override
	public boolean offer(E e) {
		synchronized(tickets){
			Ticket<E> t = tickets.pollFirst();
			if (t != null) {
				t.handle(e);
				// true = Queue not full yet, false = could not be added because the queue is full
				return true;
			}
			for (OnInt newsize: sizeChangeListeners) newsize.react(size());
		}
		return super.offer(e);
	}
	
	/* (non-Javadoc)
	 * @see java.util.concurrent.LinkedBlockingQueue#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o) {
		for (OnInt newsize: sizeChangeListeners) newsize.react(size());
		return super.remove(o);
	}
	
	public long getMaxWaitingTime(){
		long max = 0;
		long now = Luci.newTimestamp();
		for (Ticket<E> t: tickets){
			long dur = t.getWaitingDuration(now);
			if (dur > max) max = dur;
		}
		return max;
	}
}
