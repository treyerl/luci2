/** MIT License
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * */
package luci.core;

import static com.esotericsoftware.minlog.Log.error;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import luci.connect.Attachment;
import luci.connect.Message;
import luci.connect.TcpSocketHandler;
import luci.service.user.User;

/**Main behavior implemented as static methods so it can be re-used by LcWebSocket.
 * Processes requests of clients.
 * @author Lukas Treyer
 *
 */
public class ClientHandler extends TcpSocketHandler implements ServerSocketHandler{
	
	Map<Long, Set<Service>> pendingServices = new HashMap<Long, Set<Service>>();
	private long id;
	private User u;
	final EndOfServiceResultWriter finalWriter;
	Map<Class<? extends Listener>, Listener> writers = new HashMap<>();
	private Map<Long, RemoteHandle> remoteHandles = new HashMap<>();
	
	/**Extends TcpSocketHandler and implements ServerSocketHandler and therefore needs an id;
	 * @param key SelectionKey
	 * @param sl SelectorLoop object
	 * @param needsToWrite Set&lt;TcpSocketHandler&gt;
	 */
	ClientHandler(SelectionKey key, SelectorLoop sl){
		super(key, sl);
		id = ServerSocketHandler.newID();
		finalWriter = new EndOfServiceResultWriter(this){
			public synchronized void getNotified(TaskInfo ti, Message m, long duration){
				super.getNotified(ti, m, duration);
				finalizeNotification(pendingServices, m, ti);
			}
		};
	}
	
	/* (non-Javadoc)
	 * @see luci.connect.SocketHandler#processHeader(luci.connect.Message)
	 */
	@Override
	public void processHeader(Message m) throws CallException{
		Service service = processHeader(currentMsgToRead, pendingServices, finalWriter);
		setAttachmentType(service);
	}
	
	/**Removes a service from the pending services map using the callID as the key.
	 * @param callID the id of the pending service to be removed
	 */
	public void removePendingService(long callID) {
		synchronized(pendingServices){
			pendingServices.remove(callID);
		}
	}

	/* (non-Javadoc)
	 * @see luci.connect.SocketHandler#processIOException(java.io.IOException)
	 */
	@Override
	public boolean processIOException(IOException e) {
		processIOException(pendingServices);
		finalWriter.stopListening();
		return true;
	}
	
	/* (non-Javadoc)
	 * @see luci.connect.SocketHandler#processCallException(luci.connect.TcpSocketHandler.CallException)
	 */
	@Override
	public void processCallException(CallException e) {
		error(e.getClass().getSimpleName()+": "+e.getMessage());
		Set<Service> services = pendingServices.remove(e.getCallID());
		if (services != null) {
			for (Service s: services) 
				s.getFutureResult().cancel(true);
		}
	}
	
	/** 
	 * @param service if null Attachment.NETWORK is being set
	 */
	public void setAttachmentType(Service service){
		if (service != null) super.setAttachmentType(service.howToHandleAttachments());
		else super.setAttachmentType(Attachment.NETWORK);
	}
	
	/* (non-Javadoc)
	 * @see luci.core.ServerSocketHandler#getListeners()
	 */
	@Override
	public Map<Class<? extends Listener>, Listener> getListeners(){
		return writers;
	}

	/* (non-Javadoc)
	 * @see luci.connect.TcpSocketHandler#isSendingErrors()
	 */
	public boolean isSendingErrors(){
		return true;
	}

	/* (non-Javadoc)
	 * @see luci.core.ServerSocketHandler#getID()
	 */
	@Override
	public long getID() {
		return id;
	}

	/* (non-Javadoc)
	 * @see luci.core.Luci.ServerSocketHandler#getUser()
	 */
	@Override
	public User getUser() {
		return u;
	}

	/* (non-Javadoc)
	 * @see luci.core.Luci.ServerSocketHandler#setUser()
	 */
	@Override
	public void setUser(User u) {
		this.u = u;
	}

	/* (non-Javadoc)
	 * @see luci.core.ServerSocketHandler#getRemoteHandles()
	 */
	@Override
	public Map<Long, RemoteHandle> getRemoteHandles() {
		return remoteHandles;
	}
}