/** MIT License
 * @author Lukas Treyer
 * @date Aug 14, 2016
 * */
package luci.core;

import java.security.AccessControlException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.json.JSONArray;
import org.json.JSONObject;

import com.esotericsoftware.minlog.Log;

import luci.connect.Message;
import luci.connect.On.OnLong;
import luci.connect.SocketHandler;
import luci.connect.TcpSocketHandler.CallException;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;
import luci.core.validation.LcValidationException;
import luci.service.user.User;

/**Interface that distinguishes socket handlers on server side from general socket handler.
 * ServerSocketHandler must have an ID.
 */
public interface ServerSocketHandler extends SocketHandler {
	/**ServerSocketHandler must have an ID.
	 * @return int ID 
	 */
	public long getID();
	public User getUser();
	public void setUser(User u);
	public Map<Class<? extends Listener>, Listener> getListeners();
	public Map<Long, RemoteHandle> getRemoteHandles();
	
	default Listener getListener(Class<? extends Listener> key, 
			Function<Class<? extends Listener>, Listener> mappingFunction){
		return getListeners().computeIfAbsent(key, mappingFunction);
	}
	
	default Listener getServiceResultWriter(){
		return getListeners().computeIfAbsent(EndOfServiceResultWriter.class, 
				k-> new EndOfServiceResultWriter(this));
	}
	
	default Listener getServiceNotificationWriter(){
		return getListeners().computeIfAbsent(EndOfServiceNotificationWriter.class, 
				k-> new EndOfServiceNotificationWriter(this));
	}
	
	default boolean stopListeningTo(Set<Class<? extends Listener>> which, ListenerManager lm){
		Map<Class<? extends Listener>, Listener> listeners = getListeners();
		boolean stops = false;
		for (Class<? extends Listener> lc: which){
			Listener l = listeners.get(lc);
			if (l != null) {
				l.stopListeningTo(lm);
				stops = true;
			}
		}
		return stops;
	}
	
	default boolean stopListeningTo(ListenerManager slm){
		for (Listener l: getListeners().values()) l.stopListeningTo(slm);
		return getListeners().size() > 0;
	}
	
	default void stopListening(){
		for (Listener l: getListeners().values()) l.stopListening();
	}
	
	default void finalizeNotification(Map<Long, Set<Service>> pending, Message m, TaskInfo ti){
		if (m != null){
			JSONObject h = m.getHeader();
			if (h.has("result") || h.has("error")) {
				Set<Service> srv = pending.get(ti.getCallID());
				try{
					srv.remove(ti);
					if (srv.size() == 0) pending.remove(ti.getCallID());
				}
				catch (NullPointerException e) {
					System.err.println("Service/Task not available!");
				}
			}
		}
	}
	
	default void processIOException(Map<Long, Set<Service>> pending){
		stopListening();
		for (Set<Service> ss: pending.values()){
			for (Service s: ss){
				s.getFutureResult().cancel(true);
			}
		}
		pending.clear();
		for (RemoteHandle r: getRemoteHandles().values()){
			r.frs.cancelDeregister(r);
		}
		getRemoteHandles().clear();
	}
	
	default void register(RemoteHandle r){
		getRemoteHandles().put(r.getWorkerID(), r);
	}
	
	default void deregister(RemoteHandle r){
		r.deregister();
		getRemoteHandles().remove(r.getWorkerID());
	}
	
	default void deregister(long id){
		deregister(getRemoteHandles().get(id));
	}
	
	default int getNumRegistered(){
		return getRemoteHandles().size();
	}
	
	/**Increments globalCallID by 1 and returns the result.
	 * @return long a new callID
	 */
	public static long newCallID(){
		return ID.getNew("callID");
	}
	
	/**Increments globalID by 1 and returns the result;
	 * @return new socket handler ID
	 */
	public static long newID(){
		return ID.getNew("socketID");
	}
	
	/**Run a service which is identified either by its taskID or is being created by its name.
	 * @param m Message whose header contains the principal key "run".
	 * @param pendingServices the map of services associated with a callID into which the
	 * new service instance is being put.
	 * @param listener ServiceWriterComplete - provides a write method by which we can respond
	 * to the client
	 * @return the service to be run (already started if identified by instanceID)
	 * @throws Exception
	 */
	default Service runService(Message m, Map<Long, Set<Service>> pendingServices, 
			EndOfServiceResultWriter listener) throws Exception {
		JSONObject json = m.getHeader();
		Object run = json.get("run");
		long callID = m.getCallID();
		
		if (run instanceof JSONArray || run instanceof Number){
			Set<Service> services = new HashSet<>();
			if (run instanceof JSONArray){
				JSONArray taskIDs = (JSONArray) run;
				for (int i = 0; i < taskIDs.length(); i++){
					services.addAll(WorkflowElements.get(taskIDs.getLong(i)).getInitialServices(callID));
				}
			} else if (run instanceof Number){
				services.addAll(WorkflowElements.get(Long.valueOf(run.toString())).getInitialServices(callID));
			}
			
			synchronized(pendingServices) {
				Set<Service> sset = pendingServices.get(callID);
				if (sset == null) pendingServices.put(callID, (sset = new HashSet<>()));
				for (Service s: services){
					sset.add(s);
					s.run();
				}
			}
			return null;
		} else if (run instanceof String){
			final Service service;
			String serviceName = (String) run;
			Set<String> unknownHeaderKeys = new HashSet<String>();
			service = Luci.observer().createService(serviceName, m, unknownHeaderKeys);
			User u = ((ServerSocketHandler) m.getSourceSocketHandler()).getUser();
			if (!service.isPermittedFor(u)){
				String username = (u == null) ? "anonymous" : u.getEmail();
				String ip = m.getSourceSocketHandler().getRemoteSocketAddress().getAddress().getHostAddress();
				throw new AccessControlException("user '"+username+"' (ip:"+ip+") has no permission to run '"
												+service.getName()+"'"); 
			}
			service.setCallID(callID);
			service.addListener(listener);
			service.setCaller(listener);
			synchronized(pendingServices) {
				pendingServices.put(callID, new HashSet<Service>(Arrays.asList(new Service[]{service})));
			}
			m.onAttachmentsReady((length) -> startService(service, listener.sh, pendingServices));
			return service;
//			trace(serviceName+" "+callID);
		} else {
			listener.sh.write(new Message(new JSONObject().put("error", 
				"invalid type "+run.getClass().getSimpleName()+" for key run").put("callID", callID)));
			return null;
		}
	}
	
	/**Callback / method called by funtional interface once attachments of a direct service call
	 * become available. Starts the service.
	 * @param service Service to start.
	 * @param sh SocketHandler by whose write method we notify the client of an error.
	 */
	default void startService(Service service, SocketHandler sh, Map<Long, Set<Service>> pendingServices){
		try {
			service.run();
		} catch (Exception e){
			try {
				if (Log.TRACE) e.printStackTrace();
				long duration = Luci.newTimestamp() - service.startTime;
				sh.write(new Message(new JSONObject()
						.put("error", e.toString())
						.put("callID", service.getCallID())
						.put("duration", duration)));
				pendingServices.get(service.getCallID()).remove(service);
				org.pmw.tinylog.Logger.info("[error]"+","+service.getCallID()+","+service.getName()
					+","+duration+",,,\""+e.toString().replace("\"", "'")+"\"");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	
	/**
	 * @param cancelCallID long the ID by which a call is being identified
	 * @param pendingServices the map from which the service has to be removed.
	 * @return Message: either result or error
	 * @throws Exception
	 */
	default Message cancelService(long cancelCallID, Map<Long, Set<Service>> pendingServices) {
		Set<Service> services = pendingServices.get(cancelCallID);
		if (services == null) return new Message(new JSONObject().put("error", 
			"No pending call with ID '"+cancelCallID+"'!"));
		else {
			JSONArray success = new JSONArray();
			JSONArray failure = new JSONArray();
			long now = Luci.newTimestamp();
			long duration = 0;
			for (Service s: services){
				if (s.getFutureResult().cancel(true)){
					success.put(new JSONObject()
							.put("taskID", s.getTaskID())
							.put("serviceName", s.getName()));
				} else {
					failure.put(new JSONObject()
							.put("taskID", s.getTaskID())
							.put("serviceName", s.getName()));
				}
				long d = now - s.startTime;
				if (d > duration) duration = d;
			}
			return new Message(new JSONObject().put("result", 
					new JSONObject()
					.put("success", success)
					.put("failure", failure)
					.put("cancelCallID", cancelCallID)
					.put("duration", duration)));
		}
	}
	
	/**needs to be able to throw an exception so that (LcValidation-)Exceptions can be handled;
	 * TcpSocketHandler needs to be able to empty any sockets containing attachments upon exceptions 
	 * @param m Message
	 * @param pendingServices Map&lt;Long, Service&gt;
	 * @param scw ServiceWriterComplete
	 * @return Service if one has been newly created (= in a direct call)
	 * @throws Exception
	 */
	default Service processHeader(Message m, Map<Long, Set<Service>> pendingServices, 
			EndOfServiceResultWriter scw) throws CallException{
		JSONObject json = m.getHeader();
		
		final long callID;
		
		if (json.has("callID")){
			callID = json.getLong("callID");
		} else if (json.has("run")){
			callID = newCallID();
			scw.sh.write(new Message(new JSONObject().put("newCallID", callID)));
		} else if (json.has("error")){
			throw new Error("error without callID: "+json.getString("error"));
		} else {
			System.out.println(json);
			throw new LcValidationException("'callID' required");
		}
		
		// RUN
		if (json.has("run")) {
			try {
				return runService(m.setCallID(callID), pendingServices, scw);
			} catch (Exception e){
				throw new CallException(e, callID);
			}
		}
		// CANCEL
		else if (json.has("cancel")) {
			long cancelCallID = json.getLong("cancel");
			try {
				Message r = cancelService(cancelCallID, pendingServices);
				m.getHeader().put("callID", callID);
				scw.sh.write(r);
			} catch (Exception e){
				scw.sh.write(new Message(new JSONObject()
						.put("callID", callID)
						.put("error", e.toString())));
			}
		} else {
			boolean finalResponse = false;
			final RemoteHandle rh = getRemoteHandles().get(callID);
			
			if (rh == null) 
				throw new CallException("No remote handle for callID "+callID, callID);
			if (!rh.running) 
				throw new CallException("No remote service running with callID "+callID, callID);
			
			// RESULT
			if (json.has("result")){
				rh.current.setResult(m);
				rh.current.waitForResult.countDown();
				finalResponse = true;
			}
			// PROGRESS
			else if (json.has("progress")){
				rh.current.progressNotification(m);
			// ERROR
			} else if (json.has("error")) {
				rh.current.errorNotification(m);
				rh.current.waitForResult.countDown();
				finalResponse = true;
			} else {
				throw new CallException("missing main key "+JsonType.mainKeys, callID);
			}
			if (finalResponse && rh != null){
				OnLong becomeAvailable = (length) -> rh.becomeAvailable(length);
				m.onWritten(becomeAvailable);
				m.onForwardUnlock((length) -> {
					m.cancelOnWritten(becomeAvailable);
					m.onAttachmentsReady(becomeAvailable);
				});
			}
			// don't return rh.current; returning `null` makes TcpSocketHandler treat attachments
			// as network attachments
		}
		return null;
	}
}