/** MIT License
 * @author Lukas Treyer
 * @date Jan 18, 2016
 * */
package luci.core;
import org.json.JSONException;
import org.json.JSONObject;

import luci.connect.Message;

public class EndOfServiceResultWriter extends AbstractListener{
	ServerSocketHandler sh;
	
	public EndOfServiceResultWriter(ServerSocketHandler sh){
		setSocketHandler(sh);
	}
	public void setSocketHandler(ServerSocketHandler sh){
		this.sh = sh;
	}
	public ServerSocketHandler getSocketHandler(){
		return sh;
	}
	public synchronized void getNotified(TaskInfo ti, Message m, long duration) {
		String idName = ti.getCaller() == this ? "callID" : "referenceID";
		if (m != null){
			sh.write(m, h -> {
				h.remove("referenceID");
				h.remove("callID");
				h.put(idName, ti.getCallID())
				 .put("serviceName", ti.getName())
				 .put("taskID", ti.getTaskID())
				 .put("duration", duration);
				return h;
			});
		} else {
			try {
				sh.write(new Message(new JSONObject()
						.put("error", "The result/progress message of service '"+ti.getName()+"' is null")
						.put(idName, ti.getCallID())
						.put("serviceName", ti.getName())
						.putOpt("taskID", ti.getTaskID())
						.put("duration", duration)
						));
			} catch (JSONException e) {
				e.printStackTrace();
			};
		}
	}
}
