/** MIT License
 * @author Lukas Treyer
 * @date Dec 18, 2015
 * */
package luci.core.web;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Mime {
	private static Set<String> html = new HashSet<>();
	private static Map<String, String> types = new HashMap<>();
	static {
		// html
		html.add("html");
		html.add("htm");
		types.put("html", "text/html");
		types.put("htm", "text/html");
		types.put("css", "text/css");
		types.put("js", "text/javascript");
		types.put("svg", "image/svg+xml");
		types.put("svgz", "image/svg+xml");
		types.put("json", "application/json");
		
		// images
		types.put("bmp", "image/bmp");
		types.put("dwg", "image/x-dwg");
		types.put("dxf", "image/x-dxf");
		types.put("gif", "image/gif");
		types.put("ico", "image/x-icon");
		types.put("jpg", "image/jpeg");
		types.put("jpeg", "image/jpeg");
		types.put("pct", "image/x-pict");
		types.put("png", "image/png");
		types.put("tif", "image/tiff");
		types.put("tiff", "image/tiff");
		
		// audio
		types.put("aif", "audio/aiff");
		types.put("aiff", "audio/aiff");
		types.put("gsm", "audio/x-gsm");
		types.put("m2a", "audio/mpeg");
		types.put("m3u", "audio/x-mpegurl");
		types.put("mid", "audio/midi");
		types.put("midi", "audio/midi");
		types.put("mp2", "audio/mpeg");
		types.put("mp3", "audio/mpeg3");
		types.put("mpa", "audio/mpeg");
		types.put("wav", "audio/wav");
		types.put("flac", "audio/flac");
		types.put("ogg", "audio/ogg");
		types.put("oga", "audio/ogg");
		
		// video
		types.put("asf", "video/x-ms-asf");
		types.put("avi", "video/avi");
		types.put("mjpg", "video/x-motion-jpeg");
		types.put("mov", "video/quicktime");
		types.put("mp2", "video/mpeg");
		types.put("mp4", "video/mp4");
		types.put("ogv", "video/ogg");
		types.put("webm", "video/webm");
	}
	public final static Map<String, String> type = Collections.unmodifiableMap(types);
	public final static Set<String> htmlResponse = Collections.unmodifiableSet(html);
}
