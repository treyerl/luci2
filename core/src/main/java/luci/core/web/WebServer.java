package luci.core.web;
import static com.esotericsoftware.minlog.Log.trace;
import static com.esotericsoftware.minlog.Log.warn;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.json.JSONObject;

import com.esotericsoftware.minlog.Log;

import luci.connect.Attachment;
import luci.connect.GUIRun;
import luci.connect.Message;
import luci.connect.TileReader;
import luci.connect.TileReader.Tile;
import luci.core.Luci;
import luci.core.LuciGUI;
import luci.core.web.LcWebSock.WebSocketHandler.WebMessage;
 
/**
 * Simple Java non-blocking NIO webserver.
 *
 * @author c
 */
public class WebServer implements Runnable {
	public final static String webroot;
	private final static Map<String, KeyAction> keyActions = new HashMap<>();
	private final static Map<String, String> actionKeys = new HashMap<>();
	@FunctionalInterface
	interface KeyAction{void perform();}
	private static boolean shutdown = false;
	
	static {
		webroot = Luci.config.getProperty("webroot", Luci.cwd()+File.separator+"data"+File.separator+"webroot");
    	new File(webroot).mkdirs();
    	keyActions.put("/shutdown", () -> shutdown = true);
    	keyActions.put("/javaconsole", () 
    			-> {try{GUIRun.guiRun(LuciGUI.getConsoleCommand());}catch(IOException e){e.printStackTrace();}});
    	for (String key: keyActions.keySet()) actionKeys.put(key, UUID.randomUUID().toString());
	}
	
    private Charset charset = Charset.forName("UTF-8");
    private CharsetEncoder encoder = charset.newEncoder();
    private Selector selector;
    private ServerSocketChannel server = ServerSocketChannel.open();
    private boolean isRunning = true;
    private InetSocketAddress address;
    private Map<Long, Integer> attTracker = new HashMap<>();
    private TileReader tileReader;
 
    /**
     * Create a new server and immediately binds it.
     *
     * @param address the address to bind on
     * @throws IOException if there are any errors creating the server.
     */
    public WebServer(InetSocketAddress address) throws IOException {
    	String runningWith = "";
    	try {
    		String className = "luci.mbTileReader.MbTileReader";
    		@SuppressWarnings("unchecked")
    		Class<TileReader> tr = (Class<TileReader>) Class.forName(className);
    		tileReader = tr.newInstance();
    		runningWith = " - serving tiles from "+tileReader.getTileSource();
    	} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e){
    		tileReader = null;
    	} catch (Exception e){
			tileReader = null;
			if (e instanceof FileNotFoundException) runningWith = " - "+e.getMessage();
		}
    	selector = SelectorProvider.provider().openSelector();
        server.socket().bind(address);
        server.configureBlocking(false);
        server.register(selector, SelectionKey.OP_ACCEPT);
        if (address.getHostName().equals("0.0.0.0")){
        	System.out.println("WebServer: localhost:"+address.getPort()
        		+" "+java.net.InetAddress.getLocalHost()+":"+address.getPort()+" "+runningWith);
        } else System.out.println("WebServer: "+address+" "+runningWith);
        this.address = address;
    }
    
    public int getPort(){
    	return address.getPort();
    }
    
    public String getHostname(){
    	return address.getHostName();
    }
    
    public String getHost(){
    	return address.getHostString();
    }
 
    /**
     * Core run method. This is not a thread safe method, however it is non
     * blocking. If an exception is encountered it will be thrown wrapped in a
     * RuntimeException, and the server will automatically be {@link #shutDown}
     */
    @Override
    public final void run() {
        while (isRunning && !shutdown) {
            try {
                selector.select();
                if (!selector.isOpen()) return;
                Iterator<SelectionKey> i = selector.selectedKeys().iterator();
                while (i.hasNext()) {
                    SelectionKey key = i.next();
                    i.remove();
                    if (!key.isValid()) {
                        continue;
                    }
                    HTTPSession session = null;
                    try {
                        // get a new connection
                        if (key.isAcceptable()) {
                            // accept them
                            SocketChannel client = server.accept();
                            // non blocking please
                            client.configureBlocking(false);
                            // show out intentions
                            client.register(selector, SelectionKey.OP_READ);
                            // read from the connection
                        } else if (key.isReadable()) {
                            //  get the client
                            SocketChannel client = (SocketChannel) key.channel();
                            // get the session
                            session = (HTTPSession) key.attachment();
                            // create it if it doesnt exist
                            if (session == null) {
                                session = new HTTPSession(client);
                                key.attach(session);
                            }
                            
                            // determine whether to read to a buffer (GET / HEAD) or keep reading
                            // from the channel 
                            session.setMethod();
                            
                            String line;
                            while ((line = session.readLine()) != null) {
                                // check if we have got everything
                                if (line.isEmpty()) {
                                    HTTPRequest request = new HTTPRequest(session.readLines.toString());
                                    session.sendResponse(handle(session, request));
                                    session.close();
                                }
                            }
//                            trace("post while");
                        }
                    } catch (Exception ex) {
                        if (!(ex instanceof EOFException)) {
                        	if (Log.DEBUG || Log.TRACE){
                        		ex.printStackTrace();
	                        } else {
	                            System.err.println(ex);
	                            System.err.println("\tat " + ex.getStackTrace()[0]);
	                        }
                        }
                        if (key.attachment() instanceof HTTPSession) {
                            ((HTTPSession) key.attachment()).close();
                        }
                        if (session != null) session.close();
                    }
                }
            } catch (IOException ex) {
            	System.err.println(ex);
                // call it quits
                shutdown();
                // throw it as a runtime exception so that Bukkit can handle it
//                throw new RuntimeException(ex);
            }
        }
        if (shutdown) Luci.exit(true);
    }
 
    /**
     * Handle a web request.
     *
     * @param session the entire http session
     * @return the handled request
     * @throws URISyntaxException 
     */
    protected HTTPResponse handle(HTTPSession session, HTTPRequest request) 
    		throws IOException, URISyntaxException {
        if (request.method.equals("GET")) return handleGET(session, request);
        else if (request.method.equals("POST")) return handlePOST(session, request);
        else return badRequest("Unknown method '"+request.method+"'");
    }
    
    protected HTTPResponse handlePOST(HTTPSession session, HTTPRequest request) throws IOException {
    	/* handlePOST: in the selector loop read bytes bytewise from channel without a buffer;
    	 * at least the first 4 bytes (excl. whitespaces) to determine whether the request is get or
    	 * post. If GET continue using a buffer, if POST continue reading bytes byte per bytes. If 
    	 * the POST header does not contain a checksum then use a buffer as in case of a GET method.
    	 * If it does contain a checksum (and a Content-Length of course), that means we have enough
    	 * information to create an AttachmentInNetwork (using the channel of the current session).
    	 * AttachmentInNetwork allows to forward an attachment as opposed to caching it first to the
    	 * webserver (= AttachmentInNetwork are twice as fast, since other attachments need to be 
    	 * cached to the webserver first, which means we send the attachment twice: Once from the 
    	 * client to the webserver and then from the webserver to a remote service). Of course, this
    	 * applies only for remote services. Local services such as scenario / database related 
    	 * services are not affected much. And of course this is also a trade off calculation 
    	 * between MD5 calculation time in javascript and network speed. And, in case of a slow 
    	 * network connection between javascript client and webserver, (remote) services waste less
    	 * of their running time for data transmission (assuming that the network connection between
    	 * webserver and remote service is quite fast, since they most likely sit in a LAN).
    	 * 
    	 * For now we will implement it without checksum-in-header-check using just the normal buffer
    	 * to create either array or file attachments (depending on the Content-Length).*/
    	
//    	trace(request.raw);
    	
    	String contentType = request.getHeader("Content-Type");
    	if (contentType.toLowerCase().startsWith("multipart")){
    		return internalError(new Object[]{"Multipart POST request not implemented (yet)"});
    	} else {
    		String format = request.getHeader("format");
    		format = format.substring(format.lastIndexOf("/")+1);
    		
    		HTTPResponse response = request.nullCheck("Content-Length", "name", "position", "guid");
    		if (response != null) return response;
    		
    		String lenString = request.getHeader("Content-Length");
    		String name = request.getHeader("name");
    		String positionStr = request.getHeader("position");
    		String guid = request.getHeader("guid").trim();
    		String checksum = request.getHeader("checksum");
    		try{
    			int position = Integer.parseInt(positionStr); 
        		int length = Integer.parseInt(lenString);
        		JSONObject j = new JSONObject(
        				String.format("{'name':'%s','format':'%s','attachment':"
        						+ "{'position':'%s','length':'%s'}}", 
        						name, format, position, length)
        				);
        		
        		Attachment a;
        		// if we support network attachments, that could be forwarded / are not
        		// read immediately, we would need to mute the selector --> gets too complicated with multiple 
        		// attachments coming in through HTTP, possibly in parallel.
        		int type = (Attachment.tooLargeForMemory(length)) ? Attachment.FILE : Attachment.ARRAY;
        		a = Attachment.newWeb(type, name, j).setSrcChannel(session.channel);
        		if (checksum != null) a.setChecksum(checksum);
        		int numRead = 0;
        		while((numRead += a.read()) < a.getLength());
        		a.didReadCompletely(numRead);
        		
//        		trace("numRead: "+numRead+" a: "+((AttachmentAsArray) a).getByteBuffer().capacity());
            	if (a.getChecksum() == null) a.setChecksum(a.calcChecksum());
            	else a.checkTheSum();
        		LcWebSock ws = Luci.getWebsocket();
        		WebMessage webm = ws.webMessages.get(guid);
        		if (webm != null){
        			webm.post(a, request.raw.length());
            		return itWorks("POST "+a.getChecksum());
        		} else {
        			return internalError(new Object[]{"no webmessage with guid "+guid}); 
        		}
    		} catch (NumberFormatException e){
    			response = new HTTPResponse();
    			response.setResponseCode(HTTPStatusCode.BadRequest);
    			response.setResponseReason(""+e);
    			response.setContent((""+e).getBytes(Charset.forName("UTF-8")));
    			return response;
    		}
    	}
    }
    
    private HTTPResponse performKeyAction(HTTPSession session, HTTPRequest request){
    	String loc = request.getLocation();
    	HTTPResponse r = new HTTPResponse().setResponseCode(HTTPStatusCode.OK);
    	String sa = session.channel.socket().getRemoteSocketAddress().toString();
    	String host =sa.substring(0, sa.lastIndexOf(":"));
    	if (host.equals("/0:0:0:0:0:0:0:1") || host.equals("/127.0.0.1")){
    		String givenKey = request.getParams().get("key");
    		String theKey = actionKeys.get(loc);
    		if (givenKey != null) {
    			if (givenKey.equals(theKey)) {
    				keyActions.get(loc).perform();
    				return r;
    			}
    		} else {
    			return r.setContent(theKey.getBytes());
    		}
    	}
    	return badRequest("Not allowed to perform '"+loc+"'");
    }
    
	protected HTTPResponse handleGET(HTTPSession session, HTTPRequest request) throws IOException, URISyntaxException {
		String loc = request.getLocation();
		String nativeLoc = loc.replace("//", File.separator);
		
        if (loc.startsWith("/map/")) {
        	if (tileReader != null){
        		return handleMap(session, request);
        	} else {
        		warn("No MapTileReader for "+request.getLocation()
    			+"; URL's starting with '/map/' are always handled by a TileReader if present!");
        	}
        } else if (keyActions.containsKey(loc)) return performKeyAction(session, request);
        else if (loc.equals("/getConsoleCommand")) 
        	return new HTTPResponse()
        		.setResponseCode(HTTPStatusCode.OK)
        		.setContent(("cd "+Luci.cwd().getAbsolutePath()+"\n"+LuciGUI.getConsoleCommand())
        				.getBytes(Charset.forName("UTF8")));
        else if (loc.equals("/luciConnect.js")){
        	return new HTTPResponse().setInputStream(getClass().getResourceAsStream("luciConnect.js"));
        }
        	
        HTTPResponse response = new HTTPResponse();
        String format = "";
        int point = loc.lastIndexOf(".");
        if (point >= 0) format = loc.substring(point+1);
        int i = -1;
        if ((i = loc.indexOf("/", 1)) >= 0){
        	String callIDString = loc.substring(1, i);
        	try {
            	long callID = Long.parseLong(callIDString);
            	
            	String chck;
            	if (point >= 0) chck = loc.substring(i+1, point);
            	else chck = loc.substring(i+1);
            	LcWebSock ws = Luci.getWebsocket();
                Message m = ws.msgByCallID.get(callID);
                if (m != null && m.hasAttachment(chck)) {
                	response.setAttachment(m.getAttachmentByChecksum(chck), callID);
                	Integer numAtts = attTracker.get(callID);
                	attTracker.put(callID, (numAtts == null) ? (numAtts = 1) : numAtts++);
                	if (numAtts == m.getAttachmentMap().size()){
                		ws.msgByCallID.remove(callID);
                		attTracker.remove(callID);
                		m.setWritten(((String)m.unaffiliate()).length()+m.getSumAttachmentsLength());
                	}
                	return response;
                }
        	} catch (NumberFormatException e){
        		// maybe the location points to a file
        	}
        }
        
        File f = new File(Luci.getAttachmentsFolder(), nativeLoc);
        File w = new File(webroot, nativeLoc);
        String loc1 = loc.substring(1);
        URL packedFile = getClass().getResource(loc1);
        
        if (w.exists()){
        	if (w.isDirectory()) response = serveDirectory(new File(w, "index.html"), "html");
        	else response.setContentFile(w).setHeader("Content-Type", response.suffixToMime(format));
        } else if (f.exists()){
        	response.setContentFile(f);
        } else if (packedFile != null) {
        	String pk = packedFile.toString();
        	if (pk.startsWith("jar")){
				File jar = new File(pk.substring(9, pk.lastIndexOf("!")).replace("/", File.separator));
				JarFile jarFile = new JarFile(jar);
				
				String internal = pk.substring(pk.lastIndexOf("!")+2);
				JarEntry je = jarFile.getJarEntry(internal);
				JarEntry dir = jarFile.getJarEntry(internal+"/");
				
				// tell the browser to request the directory
				// otherwise all relative paths in the html won't work properly
				if (dir != null) {
					jarFile.close();
					return redirect(loc+"/");
				}
				
				if (je.isDirectory()) {
					String idx = (dir == null) ? loc1+"index.html" : loc1+"/index.html";
					if (getClass().getResource(idx) != null) 
						response.setInputStream(getClass().getResourceAsStream(idx))
								.setHeader("Content-Type", response.suffixToMime("html"));
					else {
						List<String> names = new ArrayList<>();
						Enumeration<JarEntry> entries = jarFile.entries();
						while(entries.hasMoreElements()){
							String s = entries.nextElement().getName();
							if (s.startsWith(internal)) names.add(s);
						}
					}
				} else response.setInputStream(packedFile.openStream())
							   .setHeader("Content-Type", response.suffixToMime(format));
				jarFile.close();
        	} else {
        		File l = new File(packedFile.getPath());
            	if (l.isDirectory()) {
            		if (loc.endsWith("/"))
            			response = serveDirectory(new File(pk.substring(5), "index.html"), "html");
            		else return redirect(loc+"/");
            	} else response.setContentFile(l);
        	}
        	
        } else return notFound(loc);
        return response;
    }
    
	private HTTPResponse redirect(String location){
		return new HTTPResponse()
			.setResponseCode(HTTPStatusCode.Redirect)
			.setResponseReason("Moved Permanently")
			.setHeader("Location", location);
	}
	
    /**
	 * 
	 */
	private HTTPResponse serveDirectory(File index, String format) {
		HTTPResponse response = new HTTPResponse();
		if (index.exists()) 
			response.setContentFile(index).setHeader("Content-Type", response.suffixToMime(format));
		else if (index.getParentFile().exists()){
			File p = index.getParentFile();
			response.setContent(listFilesHTML(p.toString(), p.list()).getBytes());
		} else return notFound(index.getName());
		return response;
	}
	
	private String listFilesHTML(String title, String[] names){
		StringBuilder sb = 
			new StringBuilder("<html><head><title>"+title+"/</title></head><body><h1>"+title+"</h1><hr/><ul>");
		for (String el: names){
			sb.append("<li><a href='"+el+"'>"+el+"</a></li>");
		}
		sb.append("</ul></body></html>");
		return sb.toString();
	}

	protected HTTPResponse handleMap(HTTPSession session, HTTPRequest request) throws IOException {
    	try {
    		HTTPResponse response = new HTTPResponse();
    		String query = request.getLocation().replace("/map/", "");
    		int dot = query.lastIndexOf(".");
    		String format = query.substring(dot+1);
    		if (dot > 0) query = query.substring(0, dot);
    		else trace(query);
    		String[] q = query.split("/");
    		int zoom = Integer.parseInt(q[0]);
    		int x = Integer.parseInt(q[1]);
    		int y = Integer.parseInt(q[2]);
			Tile tile = tileReader.read(zoom, x, y, format, "xyz");
			response = new HTTPResponse();
			if (tile == null) {
				response.setInputStream(getClass().getResourceAsStream("black.png"));
//				response.setContentFile(new File(getClass().getResource("black.png").getPath()));
				return response;
			}
			response.setHeader("Content-Type", tile.getMimeType());
			response.setResponseCode(HTTPStatusCode.OK);
			response.setContent(tile.getData());
			return response;
		} catch (IllegalArgumentException e) {
			return badRequest(e.toString());
		} catch (SQLException e) {
			if (Log.DEBUG || Log.TRACE) e.printStackTrace();
			return internalError(e.getStackTrace());
		}
    }
    
	private HTTPResponse itWorks(String title) {
    	HTTPResponse response = new HTTPResponse();
    	response.setResponseCode(HTTPStatusCode.OK);
    	response.setHeader("Content-Type", "text/html");
    	response.setContent(("<html><head><title>Luci Webserver</title><head>"
    			+ "<body><h1>"+title+"</h1><p>It works!</p></body></html>")
    			.getBytes(Charset.forName("UTF8")));
    	return response;
    }
    
    protected HTTPResponse notFound(String location){
    	HTTPResponse response = new HTTPResponse();
    	response.setResponseCode(HTTPStatusCode.NotFound);
    	response.setResponseReason(location+" not found");
    	response.setContentFile(new File(Luci.cwd(), "data"+File.separator+"404.html"));
    	return response;
    }
    
    protected HTTPResponse notFoundHeader(String location, String format){
    	HTTPResponse response = new HTTPResponse();
    	response.setResponseCode(HTTPStatusCode.NotFound);
//    	response.setResponseReason(location+" not found");
//    	response.setHeader("Content-Type", Mime.type.get(format));
    	return response;
    }
    
    protected static HTTPResponse badRequest(String error){
    	HTTPResponse response = new HTTPResponse();
    	response.setResponseCode(HTTPStatusCode.BadRequest);
    	response.setResponseReason(error);
    	response.setContent(("<html><head><title>WebServer</title></head>"
    			+ "<body><h1>400</h1><p>Bad Request: "+error
    			+"'</p></body></html>").getBytes(Charset.forName("UTF8")));
    	response.setHeader("Content-Type", "text/html");
    	return response;
    }
    
    protected HTTPResponse internalError(Object[] errors){
    	HTTPResponse response = new HTTPResponse();
    	response.setResponseCode(HTTPStatusCode.InternalServerError);
    	
    	StringBuilder errorsHTML = new StringBuilder("<html><head><title>"+getClass().getSimpleName()
    			+"</title></head><body><h1>Internal Server Error</h1><ul>");
    	for (Object error: errors){
    		errorsHTML.append("<li>"+error.toString()+"</li>");
    	}
    	errorsHTML.append("</ul></body></html>");
    	response.setContent(errorsHTML.toString().getBytes(Charset.forName("UTF8")));
    	response.setHeader("Content-Type", "text/html");
    	return response;
    }
 
    /**
     * Shutdown this server, preventing it from handling any more requests.
     */
    public final void shutdown() {
        isRunning = false;
        selector.wakeup();
        try {
            selector.close();
            server.close();
        } catch (IOException ex) {
            // do nothing, its game over
        }
    }
 
    public final class HTTPSession {
    	
        private final SocketChannel channel;
        private final ByteBuffer buffer = ByteBuffer.allocate(2048);
        private final StringBuilder readLines = new StringBuilder();
        private int mark = 0, channelMark = 0, method = HTTPRequest.GET;
 
        public HTTPSession(SocketChannel channel) {
            this.channel = channel;
        }
 
        /**
         * Try to read a line.
         */
        public String readLine() throws IOException {
        	if (!channel.isOpen()) return null;
            StringBuilder sb = new StringBuilder();
            int l = -1;
            char c;
            if (method == HTTPRequest.GET || method == HTTPRequest.HEAD){
            	if (mark == 0) {
            		if (method == HTTPRequest.GET) sb.append("GET");
            		if (method == HTTPRequest.HEAD) sb.append("HEAD");
            		readData();
            	}
            	while (buffer.hasRemaining()) {
                    sb.append((c = (char) buffer.get()));
                    if (c == '\n' && l == '\r') {
                        mark = buffer.position();
                        readLines.append(sb);
                        return sb.substring(0, sb.length() - 2);
                    }
                    l = c;
                }
            } else if (method == HTTPRequest.POST){
            	int i = 0;
            	ByteBuffer b1 = ByteBuffer.allocate(1);
            	if (channelMark == 0) sb.append("POST");
                while(channelMark++ < 2048){
                	b1.rewind();
                	int numRead = channel.read(b1);
                	if (numRead == 0) {
                		if (i++ < 5) {
                			try {
								Thread.sleep(1);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
                			continue;
                		}
                		trace("i: "+i);
                		return null;
                	}
                	else if (numRead < 0) throw new EOFException();
                	b1.rewind();
                	sb.append((c = (char) b1.get()));
                    if (c == '\n' && l == '\r') {
                        readLines.append(sb);
                        return sb.substring(0, sb.length() - 2);
                    }
                    l = c;
                }
                trace("channelMark"+channelMark);
            }
            return null;
        }
        
        public int setMethod() throws IOException{
        	StringBuilder sb = new StringBuilder();
        	ByteBuffer b1 = ByteBuffer.allocate(1);
        	boolean hasMethod = false;
        	while(!hasMethod){
        		b1.rewind();
        		int numRead = channel.read(b1);
            	if (numRead == 0) continue;
            	else if (numRead < 0) {
            		throw new EOFException();
            	}
            	b1.rewind();
            	char c = (char) b1.get();
            	if (c == ' ') continue;
            	sb.append(c);
            	String SB = sb.toString();
            	if (SB.equalsIgnoreCase("GET")) return (method = HTTPRequest.GET);
            	else if (SB.equalsIgnoreCase("POST")) return (method = HTTPRequest.POST);
            	else if (SB.equalsIgnoreCase("HEAD")) return (method = HTTPRequest.HEAD);
            	else if (SB.equalsIgnoreCase("PATCH")) return (method = HTTPRequest.PATCH);
        	}
        	return 0;
        }
        
        public int getMethod() throws IOException{
        	if (method == 0) setMethod();
        	return method;
        }
 
        /**
         * Get more data from the stream.
         */
        public void readData() throws IOException {
            buffer.limit(buffer.capacity());
            int read = channel.read(buffer);
            if (read == -1) {
                throw new EOFException();
            }
            buffer.flip();
            buffer.position(mark);   
        }
 
        private void writeLine(String line) throws IOException {
            channel.write(encoder.encode(CharBuffer.wrap(line + "\r\n")));
        }
 
        public void sendResponse(HTTPResponse response) {
        	if (response.responseCode == HTTPStatusCode.OK)
            response.addDefaultHeaders();
            try {
                writeLine(response.version + " " + response.responseCode + " " + response.responseReason);
                for (Map.Entry<String, String> header : response.headers.entrySet()) {
                    writeLine(header.getKey() + ": " + header.getValue());
                }
                writeLine("");
                if (response.hasContentChannel()) response.forwardFile(channel);
                else if (response.hasAttachment()) response.writeAttachment(channel);
                else if (response.in != null) response.forwardInputStream(channel);
                else if (response.content != null){
                	ByteBuffer bb = ByteBuffer.wrap(response.content);
                	int numWritten = 0;
                	while((numWritten += channel.write(bb)) < bb.capacity());
                }
                	
            } catch (IOException | InterruptedException ex) {
                // slow silently
            	if (Log.DEBUG || Log.TRACE) ex.printStackTrace();
            }
        }
 
        public void close() {
            try {
                channel.close();
            } catch (IOException ex) {
            }
        }
    }
 
    public static class HTTPStatusCode {
		public final static int OK = 200;
    	public final static int NotFound = 404;
    	public final static int BadRequest = 400;
    	public final static int PayloadTooLarge = 413;
    	public final static int InternalServerError = 500;
    	public final static int MultipleFormatChoice = 300;
    	public static final int Redirect = 301;
    	public final static int SeeOther = 303;
    }
}