/** MIT License
 * @author Lukas Treyer
 * @date Aug 23, 2016
 * */
package luci.core.web;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class HTTPRequest {
	final static int GET = 1;
	final static int POST = 2;
	final static int HEAD = 3;
	final static int PATCH = 4;
    final String raw;
    String method;
    private String location;
    @SuppressWarnings("unused")
	private String version;
    private Map<String, String> headers = new HashMap<String, String>();
    Map<String, String> params;

    public HTTPRequest(String raw) {
        this.raw = raw;
        parse();
    }

    private void parse() {
        // parse the first line
        StringTokenizer tokenizer = new StringTokenizer(raw);
        method = tokenizer.nextToken().toUpperCase();
        location = parseLocationForParameters(tokenizer.nextToken());
        version = tokenizer.nextToken();
        // parse the headers
        String[] lines = raw.split("\r\n");
        for (int i = 1; i < lines.length; i++) {
            String[] keyVal = lines[i].split(":", 2);
            headers.put(keyVal[0].trim(), keyVal[1].trim());
        }
    }

    public String getMethod() {
        return method;
    }

    public String getLocation() {
        return location;
    }
    
    public File getLocationAsFile(){
    	return new File(location.replace("/", File.separator));
    }

    public String getHeader(String key) {
        return headers.get(key);
    }
    
    public HTTPResponse nullCheck(String...str){
    	Set<String> missing = new HashSet<>();
    	for (String s: str){
    		if (!headers.containsKey(s)){
    			missing.add(s);
    		}
    	}
    	if (missing.size() > 0)
    		return WebServer.badRequest("Header(s) "+missing+" missing!");
    	return null;
    }
    
    public HTTPResponse paramNullCheck(String...str){
    	Set<String> missing = new HashSet<>();
    	for (String s: str){
    		if (!params.containsKey(s)){
    			missing.add(s);
    		}
    	}
    	if (missing.size() > 0)
    		return WebServer.badRequest("Parameter(s) "+missing+" missing!");
    	return null;
    }
    
    private String parseLocationForParameters(String location){
    	params = new TreeMap<>();
    	String[] loc = location.split("\\?");
    	if (loc.length > 1) {
    		String[] rawParams = loc[1].split("&");
    		for (String rawParam: rawParams){
    			String[] parts = rawParam.split("=");
    			if (parts.length == 1) throw new IllegalArgumentException(rawParam+
    					": URL-encoded parameters must have a key/value pair separated by a "
    					+ "'=' character.");
    			params.put(parts[0], parts[1]);
    		}
    	}
    	return loc[0];
    }
    
    public Map<String, String> getParams(){
    	return params;
    }
}