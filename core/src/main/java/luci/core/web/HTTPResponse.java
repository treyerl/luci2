/** MIT License
 * @author Lukas Treyer
 * @date Aug 23, 2016
 * */
package luci.core.web;

import static com.esotericsoftware.minlog.Log.trace;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import luci.connect.Attachment;
import luci.core.web.WebServer.HTTPStatusCode;

public class HTTPResponse {
 
        String version = "HTTP/1.1";
        int responseCode = HTTPStatusCode.OK;
        String responseReason = "OK";
        Map<String, String> headers = new LinkedHashMap<String, String>();
        byte[] content;
        private File contentFile;
        private Attachment attachment;
        InputStream in;
 
        void addDefaultHeaders() {
            headers.put("Date", getDate());
            headers.put("Server", "Java NIO Webserver by md_5");
            headers.put("Connection", "close");
            headers.put("Content-Length", getLength());
        }
        
        public String getLength(){
        	if (content != null) return Integer.toString(content.length);
        	else if (contentFile != null) return Long.toString(contentFile.length());
        	else if (attachment != null) return Long.toString(attachment.getLength());
        	else if (in != null)
				try {
					return Integer.toString(in.available());
				} catch (IOException e) {
					return "0";
				}
			else return "0";
        }
        
        public String getDate(){
        	if (contentFile != null) return new Date(contentFile.lastModified()).toString();
        	else return new Date().toString();
        }
        
        public int getResponseCode() {
            return responseCode;
        }
 
        public String getResponseReason() {
            return responseReason;
        }
 
        public String getHeader(String header) {
            return headers.get(header);
        }
 
        public byte[] getContent() {
            return content;
        }
        
        public boolean hasContentChannel(){
        	return contentFile != null && content == null && attachment == null;
        }
        
        public boolean hasAttachment(){
        	return attachment != null && contentFile == null && content == null;
        }
        
        public void forwardFile(WritableByteChannel dst) throws InterruptedException, IOException{
        	RandomAccessFile raf = new RandomAccessFile(contentFile, "r");
        	FileChannel src = raf.getChannel();
        	int contentLength = (int) contentFile.length();
        	int total = 0, numRead = 0;
        	while(total < contentLength){
        		total += numRead = (int) src.transferTo(total, contentLength - total, dst);
        		if (numRead == 0) Thread.sleep(50);
        		if (numRead == -1) break;
        	}
			raf.close();
        }
        
        public void forwardInputStream(WritableByteChannel dst) throws IOException {
        	byte[] b = new byte[16*1024];
        	int numRead = 0, numWritten = 0;
        	while((numRead = in.read(b)) >= 0){
        		numWritten = 0;
        		while((numWritten += dst.write(ByteBuffer.wrap(b, numWritten, numRead))) < numRead);
        	}
        }
        
        public void writeAttachment(WritableByteChannel dst) throws IOException, InterruptedException {
        	long numWritten = 0, wr = 0;
        	while(!attachment.writtenCompletely((numWritten += wr = attachment.write(dst)))){
        		if (wr == 0) {
        			try {
						trace("write buffer overflow");
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
        		}
        	}
        	//remove message in msgByCallID only if all attachments have been written
//        	LcWebSock ws = Luci.getWebsocket();
//            Message m = ws.msgByCallID.get(callID);
//            boolean all = true;
//            for (Attachment a: m.getAttachmentMap().values()){
//            	if (!a.didWrite()) {
//            		all = false;
//            		break;
//            	}
//            }
//            if (all) ws.msgByCallID.remove(callID);
        }
 
        public HTTPResponse setResponseCode(int responseCode) {
            this.responseCode = responseCode;
            return this;
        }
 
        public HTTPResponse setResponseReason(String responseReason) {
            this.responseReason = responseReason;
            return this;
        }
 
        public HTTPResponse setContent(byte[] content) {
            this.content = content;
            return this;
        }
        
        public HTTPResponse setInputStream(InputStream is){
        	in = is;
        	return this;
        }
 
        public HTTPResponse setHeader(String key, String value) {
            headers.put(key, value);
            return this;
        }
        
        public HTTPResponse setContentFile(File f){
        	setResponseCode(HTTPStatusCode.OK);
        	String suffix = f.getName().substring(f.getName().lastIndexOf(".")+1);
        	String mime = suffixToMime(suffix);
        	setHeader("Content-Type", mime);
        	contentFile = f;
        	return this;
        }
        
        public HTTPResponse setAttachment(Attachment a, long callID){
        	setResponseCode(HTTPStatusCode.OK);
        	String mime = suffixToMime(a.getFormat());
        	setHeader("Content-Type", mime);
        	attachment = a;
        	return this;
        }
        
        public String suffixToMime(String suffix){
        	String type = Mime.type.get(suffix);
        	if (type == null) 
        		return "application/octet-stream";
        	return type;
        }
    }