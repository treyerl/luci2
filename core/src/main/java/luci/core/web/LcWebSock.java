/** MIT License
 * @author Lukas Treyer
 * @date Dec 18, 2015
 * */
package luci.core.web;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.function.Function;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Attachment;
import luci.connect.AttachmentSpot;
import luci.connect.AttachmentSpot.JSONObjectArray;
import luci.connect.Message;
import luci.connect.On.OnVoid;
import luci.connect.SocketHandler;
import luci.connect.TcpSocketHandler.CallException;
import luci.core.EndOfServiceResultWriter;
import luci.core.Listener;
import luci.core.Luci;
import luci.core.RemoteHandle;
import luci.core.ServerSocketHandler;
import luci.core.Service;
import luci.core.TaskInfo;
import luci.core.web.LcWebSock.WebSocketHandler.WebMessage;
import luci.service.user.User;

public class LcWebSock extends WebSocketServer {
	
	public class WebSocketHandler implements ServerSocketHandler {
		class WebMessage{
			final String guid;
			JSONObject header;
			int length = 0;
			private Map<Integer, List<AttachmentSpot>> spots = new TreeMap<>();
			Map<String, Attachment> posts = new HashMap<>();
			WebMessage(){
				JSONObject json = new JSONObject(currentTextMessage);
				length = currentTextMessage.length();
				if (json.has("guid")) guid = (String) json.remove("guid");
				else guid = "noGUID";
				preparePosts((header = json));
			}
			
			public String getGUID(){
				return guid;
			}
			
			void post(Attachment a, int httpHeaderLength){
				length += httpHeaderLength + a.getLength();
				int i = a.getPosition();
				if (!spots.containsKey(i)) throw new IllegalArgumentException("Index "+i+" not registered!");
				AttachmentSpot spot0 = spots.get(i).get(0);
				JSONObject att = (JSONObject) spot0.getParent().get(spot0.getName());
				for (String key: att.keySet()){
					if (!key.equals("format") && !key.equals("POST")) 
						a.put(key, att.get(key));
				}
				posts.put(a.getChecksum(), a);
				for (AttachmentSpot as: spots.get(i)){
					as.setChecksum(a.getChecksum());
					as.getParent().put(as.getName(), a);
				}
				if (posts.size() == spots.size()) completor.react();
			}
			
			private void prepareJSONObject(JSONObject json, Object name, Object parent){
				if (json.has("format")){
					if (json.has("POST")) {
						AttachmentSpot spot = new AttachmentSpot(name, new JSONObjectArray(parent), null);
						int p = json.getInt("POST");
						List<AttachmentSpot> spotsByPosition;
						if (spots.containsKey(p)) spotsByPosition = spots.get(p);
						else spots.put(p, (spotsByPosition = new ArrayList<>()));
						spotsByPosition.add(spot);
					}
				}
				else preparePosts(json);
			}
			
			private void preparePosts(JSONObject json){
				for (String key: json.keySet()){
					Object val = json.get(key);
					if (val instanceof JSONObject) prepareJSONObject((JSONObject) val, key, json);
					else if (val instanceof JSONArray){
						JSONArray arrval = (JSONArray) val;
						for (int i = 0; i < arrval.length(); i++){
							Object o = arrval.get(i);
							if (o instanceof JSONObject) prepareJSONObject((JSONObject) o, i, arrval);
						}
					}
				}
			}
			
			boolean waitsForPosts(){
				return posts.size() < spots.size();
			}
		}
		OnVoid completor;
		WebSocket conn;
		String guid;
		Message inMsg;
		int numReadAttachmentBytes = 0;
		long id;
		Map<Long, Set<Service>> pendingServices = new HashMap<Long, Set<Service>>();
		private User u;
		final EndOfServiceResultWriter finalWriter;
		Map<Class<? extends Listener>, Listener> writers = new HashMap<>();
		private Map<Long, RemoteHandle> remoteHandles = new HashMap<>();
		
		WebSocketHandler(WebSocket conn){
			this.conn = conn;
			id = ServerSocketHandler.newID();
			finalWriter = new EndOfServiceResultWriter(this){
				public void getNotified(TaskInfo ti, Message m, long duration){
					super.getNotified(ti, m, duration);
					if (m != null){
						JSONObject h = m.getHeader();
						if (h.has("result") || h.has("error")) 
							pendingServices.remove(ti.getCallID());
					}
				}
			};
		}
		
		public InetSocketAddress getRemoteSocketAddress(){
			return conn.getRemoteSocketAddress();
		}
		
		
		public InetSocketAddress getLocalSocketAddress(){
			return conn.getLocalSocketAddress();
		}
		
		public void close() throws IOException{
			conn.close();
		}
		
		/**
		 * 
		 */
		public void cancelPendingServices() {
			for (Set<Service> ss: pendingServices.values()){
				for (Service s: ss){
					s.getFutureResult().cancel(true);
				}
			}
		}
				
		public File getDownloadFileLocation(){
			return Luci.getAttachmentsFolder();
		}
		
		/* (non-Javadoc)
		 * @see luci.core.Luci.ServerSocketHandler#getID()
		 */
		@Override
		public long getID() {
			return id;
		}

		public WebSocket getWebSocket(){
			return conn;
		}

		/* (non-Javadoc)
		 * @see luci.connect.SocketHandler#processIOException(java.io.IOException)
		 */
		@Override
		public boolean processIOException(IOException e) {
			processIOException(pendingServices);
			return true;
		}
		
		public void processCallException(CallException e){
			long callID = ((CallException) e).getCallID();
			Set<Service> services = pendingServices.remove(callID);
			if (services != null) {
				for (Service s: services) 
					s.getFutureResult().cancel(true);
			}
			write(new Message(new JSONObject()
					.put("error", e.getCause().toString())
					.put("callID", callID)));
		}

		/* (non-Javadoc)
		 * @see luci.connect.SocketHandler#processHeader(luci.connect.Message)
		 */
		@Override
		public void processHeader(Message m) throws CallException {
			processHeader(m, pendingServices, finalWriter);
			m.setAttachmentsReady((Integer) m.unaffiliate());
		}

		public void read(SocketHandler sh){
			if (currentTextMessage == null) throw new RuntimeException("Websocket: no current text message!");
			final WebMessage webm = new WebMessage();
			completor = () ->{
				try {
					sh.processHeader((inMsg = new Message(webm.header)
							.setSourceSocketHandler(this)
							.affiliate(webm.length)
							));
				} catch (CallException e) {
					sh.processCallException(e);
				}
			};
			if (webm.waitsForPosts()) {
				guid = webm.getGUID();
				if (guid == null) {
					write(new Message(new JSONObject().put("error", "missing 'guid' for attachments")));
					return;
				}
				webMessages.put(guid, webm);
			} else completor.react();
		}

		/* (non-Javadoc)
		 * @see luci.connect.SocketHandler#write()
		 */
		@Override
		public void write() throws IOException {
			// no need to implement this for LcWebSock
		}

		
		@Override
		public void write(Message m){
			write(m, msg -> msg);
		}
		
		/* (non-Javadoc)
		 * @see luci.connect.SocketHandler#write(luci.connect.Message)
		 */
		@Override
		public void write(Message m, Function<JSONObject, JSONObject> modifier) {
			String s = modifier.apply(m.getHeader()).toString();
			m.affiliate(s);
			conn.send(s);
			if (m.hasAttachments()) {
				long callID = m.getHeader().optLong("callID");
				if (callID != 0) msgByCallID.put(callID, m);
	            // schedule a task to remove the message after 30? seconds in any case
				timer.schedule(new TimerTask(){
					@Override
					public void run() {
						msgByCallID.remove(callID);
					}
				}, 30000L);
			}
		}

		/* (non-Javadoc)
		 * @see luci.core.Luci.ServerSocketHandler#getUser()
		 */
		@Override
		public User getUser() {
			return u;
		}

		/* (non-Javadoc)
		 * @see luci.core.Luci.ServerSocketHandler#setUser(luci.service.user.User)
		 */
		@Override
		public void setUser(User u) {
			this.u = u;
		}

		/* (non-Javadoc)
		 * @see luci.core.ServerSocketHandler#getListeners()
		 */
		@Override
		public Map<Class<? extends Listener>, Listener> getListeners() {
			return writers;
		}

		/* (non-Javadoc)
		 * @see luci.core.ServerSocketHandler#getRemoteHandles()
		 */
		@Override
		public Map<Long, RemoteHandle> getRemoteHandles() {
			return remoteHandles;
		}
		
	}
	
	private String currentTextMessage;
	Map<Long, Message> msgByCallID = new HashMap<>();
	Map<String, WebMessage> webMessages = new HashMap<>();
	private Map<WebSocket, ServerSocketHandler> webSocketHandlers = new HashMap<>();
	
	public LcWebSock(InetSocketAddress address) throws IOException{
		super(address);
		if (address.getHostName().equals("0.0.0.0")){
        	System.out.println("WebSocket: localhost:"+address.getPort()
        		+" "+java.net.InetAddress.getLocalHost()+":"+address.getPort());
        } else System.out.println("WebSocket: "+address);
	}
	public Map<WebSocket, ServerSocketHandler> getWebSocketHandlers() {
		return webSocketHandlers;
	}

	/* (non-Javadoc)
	 * @see org.java_websocket.server.WebSocketServer#onClose(org.java_websocket.WebSocket, int, java.lang.String, boolean)
	 */
	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		ServerSocketHandler sh = getWebSocketHandlers().get(conn);
		sh.processIOException(new IOException(reason));
		getWebSocketHandlers().remove(conn);
	}
	
	/* (non-Javadoc)
	 * @see org.java_websocket.server.WebSocketServer#onError(org.java_websocket.WebSocket, java.lang.Exception)
	 */
	@Override
	public void onError(WebSocket conn, Exception ex) {
		SocketHandler sh = getWebSocketHandlers().get(conn);
		if (sh != null){
			sh.processIOException(new IOException(ex));
//			getWebSocketHandlers().remove(conn);
			sh.write(new Message(new JSONObject().put("error", ex.toString())));
		}
	};

	/* (non-Javadoc)
	 * @see org.java_websocket.server.WebSocketServer#onMessage(org.java_websocket.WebSocket, java.nio.ByteBuffer)
	 */
	public void onMessage(WebSocket conn, ByteBuffer message) {
		SocketHandler sh = getWebSocketHandlers().get(conn);
		sh.write(new Message(new JSONObject().put("error", "binary websocket messages not supported")));
	}
	/* (non-Javadoc)
	 * @see org.java_websocket.server.WebSocketServer#onMessage(org.java_websocket.WebSocket, java.lang.String)
	 */
	@Override
	public void onMessage(WebSocket conn, String message) {
		SocketHandler sh = getWebSocketHandlers().get(conn);
		try {
			synchronized((currentTextMessage = message)){
				sh.read(sh);
			}
		} catch (IOException e) {
			sh.processIOException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.java_websocket.server.WebSocketServer#onOpen(org.java_websocket.WebSocket, org.java_websocket.handshake.ClientHandshake)
	 */
	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		getWebSocketHandlers().put(conn, new WebSocketHandler(conn));
	}
	
}
