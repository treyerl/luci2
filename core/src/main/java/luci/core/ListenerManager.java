/** MIT License
 * @author Lukas Treyer
 * @date Jan 18, 2016
 * */
package luci.core;

public interface ListenerManager {
	boolean addListener(final Listener listener);
	boolean removeListener(final Listener listener);
}