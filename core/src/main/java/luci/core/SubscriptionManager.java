/** MIT License
 * @author Lukas Treyer
 * @date Aug 15, 2016
 * */
package luci.core;

import java.util.Set;

public interface SubscriptionManager {
	Set<Class<? extends Listener>> getManagedSubscriptionTypes();
	default boolean unsubscribeFrom(ListenerManager lm, ServerSocketHandler ssh){
		return ssh.stopListeningTo(getManagedSubscriptionTypes(), lm);
	}
}
