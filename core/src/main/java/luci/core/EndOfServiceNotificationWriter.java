/** MIT License
 * @author Lukas Treyer
 * @date Dec 13, 2015
 * */
package luci.core;
import org.json.JSONObject;

import luci.connect.Message;

public class EndOfServiceNotificationWriter extends EndOfServiceResultWriter {

	public EndOfServiceNotificationWriter(ServerSocketHandler sh){
		super(sh);
	}
	
	public void getNotified(TaskInfo ti, Message m, long duration) {
		if (m != null && m.getHeader().has("result"))
			m = new Message(new JSONObject().put("result", new JSONObject().put("done", true)));
		super.getNotified(ti, m, duration);
	}
}
