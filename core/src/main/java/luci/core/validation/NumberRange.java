/** MIT License
 * @author Lukas Treyer
 * @date Aug 9, 2016
 * */
package luci.core.validation;

import java.math.BigDecimal;

import org.json.JSONObject;

class NumberRange {
	BigDecimal min, max;
	boolean inclusiveMin, inclusiveMax;
	NumberRange(String s){
		String[] minMax = s.split("-");
		if (minMax.length < 2) 
			throw new NumberFormatException("NumberRange must contain a '-' to separate min and max ");
		String min = minMax[0].trim();
		String max = minMax[1].trim();
		inclusiveMin = min.startsWith("[");
		if (inclusiveMin || min.startsWith("(")) min = min.substring(1);
		inclusiveMax = max.endsWith("]");
		if (inclusiveMax || max.endsWith(")")) max = max.substring(0,max.length()-1);
		this.min = new BigDecimal(min);
		this.max = new BigDecimal(max);
	}
	NumberRange(JSONObject j){
		min = new BigDecimal(j.get("min")+"");
		max = new BigDecimal(j.get("max")+"");
		inclusiveMin = j.getBoolean("inclusiveMin");
		inclusiveMax = j.getBoolean("inclusiveMax");
	}
	
	public boolean equals(Object other){
		Number nn = (Number) other;
		BigDecimal n = new BigDecimal(nn+"");
		if (inclusiveMin && inclusiveMax) return n.compareTo(min) >= 0 && n.compareTo(max) <= 0;
		if (inclusiveMin) return n.compareTo(min) >= 0 && n.compareTo(max) < 0;
		if (inclusiveMax) return n.compareTo(min) > 0 && n.compareTo(max) <= 0;
		return n.compareTo(min) > 0 && n.compareTo(max) < 0;
	}
	
	public String toString(){
		String open  = (inclusiveMin) ? "[":(inclusiveMax) ?"(":"";
		String close = (inclusiveMax) ? "]":(inclusiveMin) ?")":"";
		return open + min + "-" + max + close;
	}
}
