/** MIT License
 * @author Lukas Treyer
 * @date Aug 9, 2016
 * */
package luci.core.validation;

import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Attachment;

class TypeConstraint extends LcValidation {
	
	static class TypeConstraintList extends TypeConstraint{
		TypeConstraintList(Key k, int t){
			super(k, t);
		}
	}
	
	TypeConstraint(Key k, int t){
		key = k;
		type = t;
	}
	
	TypeConstraint(Key k, String t){
		key = k;
		type = string2type(t);
	}
	
	int mapClass(Object o, Key k){
    	if (o instanceof JSONObject) {
    		JSONObject j = (JSONObject) o;
    		if (j.has("attachment") && j.has("format")) {
    			unknownKeys.addAll(attachment.setKey(k).validate(o));
    			return ATTACHMENT;
    		}
    		if (j.has("geometry") && j.has("format")){
    			unknownKeys.addAll(jsonGeometry.setKey(k).validate(o));
    			return JSONGEOMETRY;
    		}
    		return JSON;
    	}
    	if (o instanceof Attachment) return ATTACHMENT;
    	if (o instanceof JSONArray) return LIST;
    	// if the service expects a json array we need a json array here, no other arrays
//    	if (o instanceof Collection) return ARRAY;
//    	if (o.getClass().isArray()) return ARRAY;
    	if (o instanceof String) return STRING;
    	if (o instanceof Number) return NUMBER;
    	if (o instanceof Boolean) return BOOLEAN;
    	if (o == JSONObject.NULL || o == null) return NULL;
    	return ANY;
    }
	
	@Override
	public Set<String> validate(Object o) {
		if (type == ANY) return unknownKeys;
		int givenType = mapClass(o, key);
		if (type != givenType) {
			// if oo is a type descriptor then it's always a string type
			if (o instanceof String && LCJSONTYPES.contains(o)) {
				if (string2type((String) o) == type)
					return unknownKeys;
			}
			throw new LcValidationException(key, type, givenType);
		}
		return unknownKeys;
	}
	
	public String toString(){
		return type2string(type);
	}
	
	public boolean equals(Object o){
		if (this == o) return true;
		if (o instanceof TypeConstraint){
			if (o.getClass() != TypeConstraint.class) return false;
			return type == ((TypeConstraint) o).type;
		}
		return false;
	}
}
