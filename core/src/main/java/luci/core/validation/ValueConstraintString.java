/** MIT License
 * @author Lukas Treyer
 * @date Aug 9, 2016
 * */
package luci.core.validation;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

 class ValueConstraintString extends TypeConstraint {
	static Number parseNumber(String s){
		try {
			// try ints
			return Integer.parseInt(s);
		} catch (NumberFormatException eInt){
			try {
				// try longs
				return Long.parseLong(s);
			} catch (NumberFormatException e){
				try {
					// try floats
					return Float.parseFloat(s);
				} catch (NumberFormatException e1){
					// try doubles
					return Double.parseDouble(s);
				}
			}
		}
	}
	
	Set<Object> constraints;
	
	ValueConstraintString(Key k, int type, Set<String> constraints){
		super(k, type);
		initConstraints(constraints);
	}
	
	public ValueConstraintString(Key k, Set<String> constraints){
		this(k, STRING, constraints);
	}
	
	void initConstraints(Set<String> constraints){
		this.constraints = new HashSet<>();
		this.constraints.addAll(constraints);
	}
	
	public Set<String> validate(Object o){
		int givenType = mapClass(o, key);
		if (type != givenType) throw new LcValidationException(key, type, givenType);
		if (constraints.size() == 0) return unknownKeys;
		if (!constraints.contains(o))
			throw new LcValidationException(key+": value '"+o+"' does not comply with constraints "+constraints+"!");
		return unknownKeys;
	}
	
	public String toString(){
		if (constraints.size() == 0) return "'"+type2string(type)+"'";
		return "['"+type2string(type)+"',"+getStringSet((o)->"\""+o+"\"").toString().substring(1);
	}
	
	Set<String> getStringSet(Function<? super Object, ? extends String> mapper){
		return constraints.stream().map(mapper).collect(Collectors.toSet());
	}
	
	public boolean equals(Object o){
		if (this == o) return true;
		if (o instanceof ValueConstraintString){
			ValueConstraintString vc = (ValueConstraintString) o;
			return type == vc.type && getStringSet((s)->s.toString()).equals(vc.getStringSet((s)->s.toString()));
		}
		return false;
	}
}
