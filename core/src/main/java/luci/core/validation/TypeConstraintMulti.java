/** MIT License
 * @author Lukas Treyer
 * @date Aug 9, 2016
 * */
package luci.core.validation;

import java.util.Set;

class TypeConstraintMulti extends TypeConstraint {
	ValidationSet<LcValidation> types = new ValidationSet<>();
	
	TypeConstraintMulti(Key k, Iterable<Object> ja){
		super(k, ANY);
		for (Object o: ja) {
			LcValidation validator = from(key, o);
			if (validator != null) types.add(validator);
		}
	}
	
	public Set<String> validate(Object o){
		if (!types.validate(o)){
			throw new LcValidationException(this, o);
		}
		return types.unknownKeys;
	}
	
	public String toString(){
		String comma = types.size() > 0 ? "," : "";
		return "[\""+type2string(type)+"Of\""+comma+types.toString().substring(1);
	}
	
	public boolean equals(Object o){
		if (o == this) return true;
		if (o instanceof TypeConstraintMulti){
			TypeConstraintMulti mt = (TypeConstraintMulti) o;
			return types.equals(mt.types);
		}
		return false;
	}
}
