/** MIT License
 * @author Lukas Treyer
 * @date Dec 14, 2015
 * */
package luci.core.validation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 */
public class JsonType extends LcValidation {
	public static final List<String> mainKeys = Collections.unmodifiableList(Arrays.asList(
			new String[]{"run","error","progress","result","cancel"}));
	
	private class JSONKey extends Key{
		
		private String valueOverride;
		private JSONKey(String k){
			this(k, true);
		}
		
		private JSONKey(String k, boolean performaAnyTest){
			super(k);
			if (getModifier().equals("ANY")){
				if (anyKey != null && performaAnyTest) {
					throw new LcValidationException(JsonType.this.key+": More than one ANY key not allowed.");
				} else anyKey = this;
				
			}
		}
		
		private JSONKey(String k, String m){
			super(k, m);
		}
		
		public final String typeIgnore(String type){
			if (ignoredStringKeys.contains(key)) {
				valueOverride = type;
				return "string";
			} else if (ignoredJSONObjectKeys.contains(key)) {
				valueOverride = type;
				return "json";
			} else if (ignoredIntKeys.contains(key)) {
				valueOverride = type;
				return "number";
			} else return type;
		}
	}

	Map<JSONKey, LcValidation> map;
	JSONKey anyKey; // --> We reinvented THE any-key! Press it only when asked to do so!
	String asString;
	
	public JsonType() {
		map = new HashMap<>();
		type = JSON;
	}
	
	public JsonType(String s){
		this(new JSONObject(s));
		asString = s;
	}
	
	public JsonType(JsonType other){
		map = new HashMap<>(other.map);
		type = JSON;
	}
	
	public JsonType(JSONObject...jsonObjects ){
		this();
		for (JSONObject j: jsonObjects) jsonInit(j);
	}
    
    public JsonType(JSONObject json){
    	this();
    	jsonInit(json);
    }
    
    JsonType(Key k, JSONObject json){
    	this();
    	key = k;
    	jsonInit(json);
    }
    
    private void jsonInit(JSONObject json){
    	if (json == null) return;
    	for (String key: json.keySet()){
    		Object o = json.get(key);
    		JSONKey k = new JSONKey(key);
    		if (o instanceof JSONObject) {
    			map.put(k, from(k, (JSONObject) o));
    		} else if (o instanceof JSONArray) {
    			JSONArray ja = (JSONArray) o;   			
    			if (ja.length() < 2) 
    				throw new LcValidationException("A LcValidation JSONArray must have a length of at least 2");
    			else map.put(k, from(k, ja));
    		}
    		else if (o instanceof String) {
    			map.put(k, new TypeConstraint(k, k.typeIgnore(o.toString())));
    		}
    		else throw new LcValidationException("A LcValidation.JsonType object cannot contain numbers or booleans");
    	}
    }
    
    public String add(String k, LcValidation o){
    	JSONKey key = new JSONKey(k);
    	int i = 1;
    	while(map.containsKey(key)){
    		key = new JSONKey(k+"."+String.format("%03d", i++));
    	}
    	map.put(key, o);
    	return key.key;
    }
    
    public String add(String key, String type){
    	return add(key, new TypeConstraint(new JSONKey(key), string2type(type)));
    }
    
    public LcValidation put(String k, LcValidation o){
    	return map.put(new JSONKey(k), o);
    }
    
    public String put(String key, String type){
    	LcValidation previous = put(key, new TypeConstraint(new JSONKey(key), string2type(type)));
    	if (previous != null) return type2string(previous.getType());
    	return null;
    }
    
    public String getType(String key){
    	return type2string(map.get(new JSONKey(key)).getType());
    }
    
    public LcValidation getTypeValidator(String key){
    	return map.get(new JSONKey(key));
    }
    
    public Object getExample(String key){
    	return LCJSONExamples.get(map.get(new JSONKey(key)).getType());
    }
    
	public List<Key> getXORKeys(){
		return map.keySet().stream().filter(k -> k.getModifier().equals("XOR")).collect(Collectors.toList());
	}
	
	public List<Key> getOPTKeys(){
		return map.keySet().stream().filter(k -> k.getModifier().equals("OPT")).collect(Collectors.toList());
	}
	
	public List<Key> getREQKeys(){
		return map.keySet().stream().filter(k -> k.getModifier().equals("")).collect(Collectors.toList());
	}
	
	public List<Key> getANYKeys(){
		return map.keySet().stream().filter(k -> k.getModifier().equals("ANY")).collect(Collectors.toList());
	}
	
	public List<Key> getNonANYKeys(){
		return map.keySet().stream().filter(k ->!k.getModifier().equals("ANY")).collect(Collectors.toList());
	}
	
	public List<String> getNonANYKeysWithoutModifier(){
		return map.keySet().stream().filter(k ->!k.getModifier().equals("ANY")).map(k -> k.key).collect(Collectors.toList());
	}
	
	public Set<String> validate(Object candidate){
		if (candidate == null && map.size() == 0) return unknownKeys;
//		if (JSONObject.NULL.equals(candidate)) return unknownKeys;
		List<Key> XOR = getXORKeys();
		List<Key> REQ = getREQKeys();
		if (!(candidate instanceof JSONObject)){
			throw new LcValidationException(key+": "+getClass().getSimpleName()+" validates only JSONObjects not "
						+candidate.getClass().getSimpleName()+"!");
		}
		JSONObject j = (JSONObject) candidate;

		// XOR
		if (XOR.size() > 0){
			List<String> XORkeys = j.keySet().stream().filter(s -> XOR.contains(new JSONKey(s)))
					.collect(Collectors.toList());
			long numXOR = XORkeys.size();
			if (numXOR > 1) 
				throw new LcValidationException(key+": Multiple exclusive (XOR) keys detected: Only one of "
					+ "the keys '"+XOR+"' allowed at a time. Given: "+XORkeys);
			if (numXOR == 0)
				throw new LcValidationException(key+": At least one of the keys '"+XOR+"' is required.");
		}
		
		
		// REQ
		REQ.removeAll(j.keySet());
		if (!j.keySet().containsAll(REQ)) {
			REQ.removeAll(j.keySet());
			throw new LcValidationException("Missing keys: "+REQ);
		}
		
		// type check
		for (String key: j.keySet()){
			// Don't perform ANY test (Key.param2 = false): if test candidate contains modifiers they 
			// are a) meaningless and b) most probably they are used to describe LcMetaJson in/output
			JSONKey k = new JSONKey(key, false);
			LcValidation v = map.get(k);
			Object o = j.get(key);
			
			if (o instanceof JSONObject ){
				JSONObject joo = (JSONObject) o;
				// if the value to be tested is a json object holding an taskID and outputKey
				// luci treats it as a subscription to another service instance
				if (joo.has("taskID") && joo.has("key")) continue;
			} 
	
			if (v == null) {
				if (anyKey == null){
					unknownKeys.add(key);
					continue;
				} else v = map.get(anyKey);
			}
			
			// transform lists that are not JSONArrays
			if (v.isRequiredType(LIST) && !(o instanceof JSONArray) &&
					(o instanceof Collection || o.getClass().isArray())){
				j.put(key, o = new JSONArray(o));
			}
			unknownKeys.addAll(v.validate(o));
		}
		return unknownKeys;
	}
	
	public String toString(){
		if (asString == null){
			StringBuilder sb = new StringBuilder("{");
			String comma = "";
			int i = 0;
			for (Entry<JSONKey, LcValidation> en: map.entrySet()){
				sb.append(comma+"\""+en.getKey()+"\":"+(en.getKey().valueOverride != null ? 
						"\"" + en.getKey().valueOverride +"\"" : toString(en.getValue())));
				if (i++ == 0) comma = ",";
			}
			sb.append("}");
			asString = sb.toString();
		}
		return asString;
	}
	
	@SuppressWarnings("unchecked")
	public String toString(Object o){
		if (o instanceof LcValidation || o instanceof NumberRange) return o.toString();
		if (o instanceof Collection) {
			String comma = "";
			int n = 0;
			StringBuilder sb = new StringBuilder("[");
			for (Integer i: ((Set<Integer>) o)){
				sb.append(comma+"\""+type2string(i)+"\"");
				if (n++ == 0) comma = ",";
			}
			sb.append("]");
			return sb.toString();
		}
		if (o instanceof Number) return "\""+type2string((int) o)+"\"";
		return "\"unknown type\"";
	}
	
	public JSONObject toJSONObject(){
		return new JSONObject(toString());
	}
	
	public boolean matches(JsonType jsonType){
		for (JSONKey key: jsonType.map.keySet()){
			Object o = map.get(key);
			if (o != null){
				Object oo = jsonType.map.get(key);
				if (!o.equals(oo)) 
					return false;
			} else return false;
		}
		return true;
	}
	
	public boolean equals(Object o){
		if (this == o) return true;
		if (o instanceof JsonType){
			JsonType jsonType = (JsonType) o;
			if (map.size() == jsonType.map.size())
				return matches(jsonType);
		}
		return false;
	}
	
	
	public JSONObject getExample(){
		List<Key> XOR = getXORKeys();
		List<Key> OPT = getOPTKeys();
		JSONObject inputs = toJSONObject();
		for (Key opt: OPT) inputs.remove(opt.toString());
		for (int i = 1; i < XOR.size(); i++) inputs.remove(XOR.get(i).toString());
		JSONObject schema = new JSONObject();
		rcrsvCreateExample(inputs, schema);
		return schema;
	}
	
	private void rcrsvCreateExample(JSONObject from, JSONObject ex){
		for (String key: from.keySet()){
			Object o = from.get(key);
			if (ignoredStringKeys.contains(key)) {
				ex.put(key, from.get(key));
			} else if(o instanceof JSONObject){
				JSONObject ex2 = new JSONObject();
				rcrsvCreateExample((JSONObject) o, ex2);
				ex.put(key, ex2);
			} else {
				int typeIndex;
				if (o instanceof JSONArray) {
					typeIndex = LCJSONTYPES.indexOf(from.getJSONArray(key).getString(0));
				} else {
					typeIndex = LCJSONTYPES.indexOf(from.getString(key));
				}
				Object example = LCJSONExamples.get(typeIndex);
				ex.put(new JsonType.JSONKey(key, false).key, example);
			}
			
		}
	}
	
	public static boolean equals(JSONObject a, JSONObject b){
		Set<String> keysA = a.keySet();
		Set<String> keysB = b.keySet();
		if (keysA.equals(keysB)){
			for (String key: keysA){
				if (!sub(a.get(key), b.get(key))) return false;
			}
			return true;
		}
		return false;
	}
	
	private static boolean sub(Object valA, Object valB){
		if (valA instanceof JSONObject){
			if (!(valB instanceof JSONObject)) return false;
			if (!equals((JSONObject)valA, (JSONObject)valB)) return false;
			return true;
		} else if (valA instanceof JSONArray){
			if (!(valB instanceof JSONArray)) return false;
			int i = 0;
			for (Object o: (JSONArray)valA){
				if (!sub(o, ((JSONArray)valB).get(i++))) return false;
			}
			return true;
		} else return valA.equals(valB);
	}

	/**
	 * @param string
	 * @return
	 */
	public LcValidation get(String string) {
		return map.get(new JSONKey(string));
	}
	
	public boolean has(String key){
		return map.containsKey(new JSONKey(key));
	}
}
