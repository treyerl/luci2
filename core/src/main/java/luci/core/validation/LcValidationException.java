/** MIT License
 * @author Lukas Treyer
 * @date Aug 9, 2016
 * */
package luci.core.validation;

import org.json.JSONException;

public class LcValidationException extends JSONException{
	private static final long serialVersionUID = 1L;
	public LcValidationException(String message) {
		super(message);
	}
	public LcValidationException(LcValidation v, Object candidate){
		super("expected type(s) "+v+" for key '"+v.getKey()+"'; given value "+candidate);
	}
	public LcValidationException(Key k, int expected, int given){
		super("expected type '"+LcValidation.type2string(expected)+"' for key '"+k+"' but got '"+LcValidation.type2string(given)+"'");
	}
}
