/** MIT License
 * @author Lukas Treyer
 * @date Jan 4, 2016
 * */
package luci.core.validation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Attachment;
import luci.connect.AttachmentAsFile;
import luci.core.Luci;

abstract class LcValidation {
	public final static int NULL = 0;
	public final static int JSON = 1;
	public final static int LIST = 2;
	public final static int STRING = 3;
	public final static int NUMBER = 4;
	public final static int BOOLEAN = 5;
	public final static int ATTACHMENT = 6;
	public final static int JSONGEOMETRY = 7;
	public final static int ANY = 8;
	
	public final static int XOR = 0;
	public final static int OPT = 1;
	
	/**keys who's type is set to string by default
	 */
	public static final List<String> ignoredStringKeys = Collections.unmodifiableList(
			Arrays.asList(new String[]{"run", "error"}));
	
	/**keys who's type is set to JSONObject by default
	 */
	public static final List<String> ignoredJSONObjectKeys = Collections.unmodifiableList(
			Arrays.asList(new String[]{"progress", "result"}));
	
	/**keys who's type is set to Integer by default
	 */
	public static final List<String> ignoredIntKeys = Collections.unmodifiableList(
			Arrays.asList(new String[]{"cancel"}));
	
	public final static List<String> LCJSONTYPES = Collections.unmodifiableList(Arrays.asList(
		new String[]{"null", "json", "list", "string", "number", "boolean", "attachment", "jsongeometry", "any"}));
	
	
	private static URL exampleFilePath = Luci.class.getResource("logo/logo256x256_b.png");
	private static File exampleFile = exampleFilePath.getFile().contains("!") ? 
			extractFile(exampleFilePath) : new File(exampleFilePath.getFile());
	public final static List<Object> LCJSONExamples = Collections.unmodifiableList(Arrays.asList(
			new Object[]{null, new JSONObject(), new JSONArray(), "a string", new Double(0.0), true, 
			new AttachmentAsFile(exampleFile), 
			new JSONObject().put("format", "geojson").put("geometry", 
				new JSONObject("{'type':'Polygon','coordinates':[[[100.0,0.0],[101.0,0.0],[101.0,1.0],[100.0,0.0]]]}")
			), "any type"}));
	
	private static File extractFile(URL url){
		File file = new File(url.getFile().substring(url.getFile().lastIndexOf("/")+1));
		if (!file.exists()){
			try {
				InputStream in = url.openStream();
			    OutputStream out = new FileOutputStream(file);
			    byte[] buffer = new byte[8 * 1024];
			    int bytesRead;
			    while ((bytesRead = in.read(buffer)) != -1) {
			        out.write(buffer, 0, bytesRead);
			    }
			    in.close();
			    out.close();
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		return file;
	}
	
	public final static JsonType getStdOutput(){
		return stdOutput;
	}
	
	private final static JsonType stdOutput = 
			new JsonType("{'XOR result':'json','XOR error':'string'}");
	
	public final static String type2string(int t){
		return LCJSONTYPES.get(t);
	}
	
	public final static int string2type(String t){
		int i = LCJSONTYPES.indexOf(t);
		if (i < 0) throw new IllegalArgumentException("Unknown json type '"+t+"'");
		return i;
	}
	
	public final static String modifierShort(String longMod){
		switch (longMod){
		case "optional": return "OPT";
		case "exclusiveKeyGroup": return "XOR";
		case "anyKey": return "ANY";
		default: return "";
		}
	}
	
	public final static String modifierLong(String shortMod){
		switch(shortMod){
		case "OPT": return "optional";
		case "XOR": return "exclusiveKeyGroup";
		case "ANY": return "anyKey";
		default: return "";
		}
	}
	
	public final static JsonType getAttachment(){
		return attachment;
	}
	
	protected final static JsonType attachment = new JsonType(
			  "{"
			+ "		'format':'string',"
			+ "		'attachment':{"
			+ "			'length':'number',"
			+ "			'checksum':'string',"
			+ "			'position':'number'"
			+ "		},"
			+ "		'OPT name':'string',"
			+ "		'ANY key':'any'"
			+ "}"); 
	
	public final static JsonType getJsonGeometry(){
		return jsonGeometry;
	}
	
	protected final static JsonType jsonGeometry = new JsonType(
			  "{"
			+ "		'format':'string',"
			+ "		'geometry':'json',"
			+ "		'ANY key':'any',"
			+ "		'OPT name':'string'"
			+ "}");
	
	
	int type;
	Key key;
	Set<String> unknownKeys = new HashSet<>();
	
	abstract public Set<String> validate(Object o);
	public LcValidation setKey(Key k){
		key = k;
		return this;
	}
	public Key getKey(){
		return key;
	}
	public int getType(){
		return type;
	}

	
	public static LcValidation from(Key k, JSONArray ja){
		int type = string2type(((String) ja.remove(0)).replace("Of", ""));
		switch(type){
		case NULL:
			throw new LcValidationException("first value in a LcValidation JSONArray cannot be 'null'");
		case JSON:
			if (ja.length() == 1) return new JsonType(k, ja.getJSONObject(0));
			else return new TypeConstraintMulti(k, ja);
		case LIST:
			return new TypeConstraintList(k, ja);
		case STRING:
			// ArrayToStringSet = make sure we get only strings
			return new ValueConstraintString(k, luci.connect.JSON.ArrayToStringSet(ja));
		case NUMBER:
			// ArrayToStringSet = make sure we get only strings
			return new ValueConstraintNumber(k, luci.connect.JSON.ArrayToStringSet(ja));
		case BOOLEAN:
			throw new LcValidationException("first value in a LcValidation JSONArray cannot be 'boolean'");
		case ATTACHMENT:
			// ArrayToStringSet = make sure we get only strings
			return new ValueConstraintJSONFormat(k, ATTACHMENT, luci.connect.JSON.ArrayToStringSet(ja));
		case JSONGEOMETRY:
			// ArrayToStringSet = make sure we get only strings
			return new ValueConstraintJSONFormat(k, JSONGEOMETRY, luci.connect.JSON.ArrayToStringSet(ja));
		case ANY:
			return new TypeConstraintMulti(k, ja);
		}
		throw new LcValidationException("Not able to match to type "+ja);
	}
	
	public static LcValidation from(Key k, Object o){
		LcValidation v;
		if (o instanceof String) v = new TypeConstraint(k, string2type((String) o));
		else if (o instanceof JSONObject) v = from(k, (JSONObject) o);
		else if (o instanceof LcValidation) v = (LcValidation) o;
		else if (o instanceof JSONArray) v = from(k, (JSONArray) o);
		else v = new TypeConstraint(k, NULL);
		return v;
	}
	
	public static LcValidation from(Key k, JSONObject jo){
		LcValidation validator;
		if (jo.has("format") && (jo.has("attachment") || jo.has("geometry"))) {
			String f = jo.getString("format");
			Set<String> constraints = new HashSet<>();
			if (!f.equals("string")) constraints.add(f);
			if (jo.has("attachment")) {
				if (!(jo instanceof Attachment)) attachment.validate(jo);
				validator = new ValueConstraintJSONFormat(k, ATTACHMENT, constraints);
			} else {
				validator = new ValueConstraintJSONFormat(k, JSONGEOMETRY, constraints);
			}
		} else {
			validator = new JsonType(jo);
		}
		return validator;
	}

	/**
	 * @param list2
	 * @return
	 */
	public boolean isRequiredType(int t) {
		if (type == ANY) return true;
		return type == t;
	}
}