/** MIT License
 * @author Lukas Treyer
 * @date Aug 9, 2016
 * */
package luci.core.validation;

import java.util.HashSet;
import java.util.Set;

class ValidationSet<E extends LcValidation> extends HashSet<E> {
	private static final long serialVersionUID = 1L;
	Set<String> unknownKeys = new HashSet<>();
	
	public boolean validate(Object o){
		for (LcValidation c: this) {
			try {
				unknownKeys.addAll(c.validate(o));
				return true;
			} catch (LcValidationException e){
				
			}
		}
		return false;
	}
	
}