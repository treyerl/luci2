package luci.core.validation;

import java.util.HashSet;
import java.util.Set;

import luci.connect.JSONFormat;

public class ValueConstraintJSONFormat extends ValueConstraintString {
	
	static int validateType(int type){
		if (!(type == ATTACHMENT || type == JSONGEOMETRY)) 
			throw new LcValidationException("expected attachment or jsongeometry type");
		return type;
	}

	public ValueConstraintJSONFormat(Key k, int type, Set<String> constraints) {
		super(k, validateType(type), constraints);
	}
	
	void initConstraints(Set<String> constraints){
		this.constraints = new HashSet<Object>(){
			private static final long serialVersionUID = 1L;
			@Override
			public boolean contains(Object o) {
				if (o instanceof String){
					String f = (String) o;
					for (Object c: this) 
						if (((String)c).equalsIgnoreCase(f)) 
							return true;
				}
				return false;
			}
		};
		this.constraints.addAll(constraints);
	}
	
	public Set<String> validate(Object o){
		int givenType = mapClass(o, key);
		if (type != givenType) throw new LcValidationException(key, type, givenType);
		if (constraints.size() == 0) return unknownKeys;
		if (!constraints.contains(((JSONFormat)o).getFormat()))
			throw new LcValidationException(key+": value '"+o+"' does not comply with constraints "+constraints+"!");
		return unknownKeys;
	}

}
