#LcValidation

In order to define in/output json structures the `luci.core.validation` package uses a subset of
json. Also look at the [API](http://treyerl.bitbucket.org/luci/api.html). All input and output 
specifications of the listed services are described with the reduced JSON syntax described here.

* `{}` - JSON for defining hierarchy/structure
* `string` - for defining data types identified by name: [`null`,`json`, `list`, `string`, `number`, `boolean`, `attachment`, `jsongeometry`]
* `[]` - JSONArrays for defining special/constraint types; examples: 

	* ['listOf','number', 'boolean'] defines a list that can only contain numbers and booleans
	* ['anyOf', 'attachment', 'jsongeometry'] declares that the value can either be of type attachment or jsongeometry (refer to [`luci.connect`](https://bitbucket.org/treyerl/luci2/raw/master/connect/) ) for the definition of attachment and json geometry types
	* ['string','optionA', 'optionB']
	* ['number', '1','2','3,4,5']
	
As shown in the examples, the JSONArray can only contain the `string`, `json` and `list` (jsonarray) 
types. The first element of a JSONArray must be a string equal to one of the allowed type names or - 
to increase readability - `listOf` and `anyOf` for the list type constraint and the multi-type 
constraint.

Let'sillustrate it with a request to register the current client as a remote service:

	{
		'run':'RemoteRegister',
		'serviceName':'example1',
		'inputs':{
			'input1':'number'
		},
		'outputs':{
			'output1':{
				'value1':'string',
				'value2':'list'
			}
		},
		'exampleCall':{
			'input1':5
		},
		'description':'This is the string that will appear in the API as the description for my service'
	}

As you see the values of the keys in `inputs` and `outputs` are always of type `string` while the 
actual result of our `example1` service would look like: 

	{
		'result':{
			'output1':{
				'value1':'this is now a string',
				'value2':['a list', 'with objects', 5, 'that can have actually', {'any':'type'}]
			}
		}
	}

###Number Constraints
As shown in the examples at the top, number constraints are defined with a list of strings like
`['number','1','2','4,5,6','80-90','(100-120]']`. The actual numbers a number value is constraint to
are given as a string. Additionally the string can contain `, ` to list numbers in a string or `-` 
to indicate a number range. Number ranges can also contain `[]` and `()` to define whether or not
the start/end number is included in the range or not. 

###Modifiers
Modifiers affect keys. Meaning whenever a key starts with `OPT ` such as `OPT meanValueX` the key is 
not required, BUT if it IS present it has the type indicated in this specification. 

* `OPT`: optional key
* `XOR`: all keys preceded with `XOR` belong to a group out of which only one key is accepted per
json hierarchy level. Note: only one group per level possible.
* `ANY`: all remaining keys (however you name them) have the type declared with this key, e.g. 
the structure `{'key1':'number','ANY key':'string'}` tells Luci's validation system to allow only 
json objects that have a `key1` whose type is `number` and that aside from this have only `string` 
types, whatever their key is.