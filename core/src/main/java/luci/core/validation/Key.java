/** MIT License
 * @author Lukas Treyer
 * @date Aug 9, 2016
 * */
package luci.core.validation;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Key implements Comparable<Key>{
	public final Collection<String> modifiers = Collections.unmodifiableList(
			Arrays.asList(new String[]{"XOR","OPT","ANY"}));
	
	final String key;
	String mod;
	
	public Key(String k){
		String[] parts = k.split("\\s");
		if (parts.length == 1 || !modifiers.contains(parts[0])) {
			key = k;
			mod = "";
		} else {
			mod = parts[0];
			key = parts[1];
		}
	}
	protected Key(String k, String m){
		if (!modifiers.contains(m))
			throw new LcValidationException("Invalid type modifier '"+m+"'");
		key = k;
		mod = m;
	}
	
	public int compareTo(Key other){
		boolean o_XOR = other.getModifier().equals("XOR");
		boolean o_ign = LcValidation.ignoredStringKeys.contains(other.key);
		boolean o_nüt = other.getModifier().equals("");
		boolean o_any = other.getModifier().equals("ANY");
		if (LcValidation.ignoredStringKeys.contains(key)){
			if (o_ign) return key.compareTo(other.key);
			else return -1;
		} else if (getModifier().equals("XOR")){
			if (o_ign) return 1;
			else if (o_XOR) return key.compareTo(other.key);
			else return -1;
		} else if (getModifier().equals("")){
			if (o_ign || o_XOR) return 1;
			else if (o_nüt) return key.compareTo(other.key);
			else return -1;
		} else if (getModifier().equals("ANY")){
			if (o_ign || o_XOR || o_nüt) return 1;
			else if (o_any) return key.compareTo(other.key);
			else return -1;
		} else if (getModifier().equals("OPT")){
			if (!other.getModifier().equals("OPT")) return 1;
			else return key.compareTo(other.key);
		}
		return 0;
	}
	
	
	public String toString(){
//		return "\""+mod+" "+key+"\"";
		String space = (getModifier().equals("")) ? "": " ";
		return getModifier()+space+key;
	}
	public boolean equals(Object other){
//		if (ignoredKeys.contains(key)) return true;
		if (other instanceof Key)
			return key.equals(((Key)other).key);
		return key.equals(other);
//		return ignoredKeys.contains(key) || key.equals(other);
	}
	public int hashCode(){
		return key.hashCode();
	}
	public String getKey(){
		return key;
	}
	public String getModifier() {
		return mod;
	}
	
}
