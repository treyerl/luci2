/** MIT License
 * @author Lukas Treyer
 * @date Aug 9, 2016
 * */
package luci.core.validation;

import java.util.Iterator;
import java.util.Set;

/**Type to test lists "with generics", denoted by a list with only one single string entry
 * such as ['attachment'] ~= List&lt;Attachment&gt;
 */
class TypeConstraintList extends TypeConstraintMulti {
	TypeConstraintList(Key k, Iterable<Object> io){
		super(k, io);
		type = LIST;
		
		// flatten erroneously nested multi-types or list-types into our type set
		Iterator<LcValidation> it = types.iterator();
		while(it.hasNext()){
			LcValidation v = it.next();
			if (v instanceof TypeConstraintMulti){
				it.remove();
				types.addAll(((TypeConstraintMulti) v).types);
			}
		}
	}
	
	public Set<String> validate(Object candidate){
		if (!(candidate instanceof Iterable<?>))
			throw new LcValidationException(this, candidate);
		for (Object o: (Iterable<?>) candidate){
			if (!types.validate(o)){
				throw new LcValidationException(this, o);
			}
		}
		return unknownKeys;
	}
}
