package luci.core.validation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ValueConstraintNumber extends ValueConstraintString {

	public ValueConstraintNumber(Key k, Set<String> constraints) {
		super(k, NUMBER, constraints);
	}

	@Override
	void initConstraints(Set<String> constraints) {
		this.constraints = new HashSet<Object>(){
			private static final long serialVersionUID = 1L;
			@Override
			public boolean contains(Object o) {
				for (Object c: this) if (c.equals(o)) return true;
				return false;
			}};
		Iterator<String> its = constraints.iterator();
		while(its.hasNext()){
			String s = its.next();
			if (s.contains(",")) {
				its.remove();
				constraints.addAll(Arrays.asList(s.split(",")));
			}
		}
		for (String s: constraints){
			try {
				this.constraints.add(parseNumber(s));
			} catch (NumberFormatException e1){
				this.constraints.add(new NumberRange(s));
			}
		}
	}

}
