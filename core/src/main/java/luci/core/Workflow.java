/** MIT License
 * @author Lukas Treyer
 * @date Mar 16, 2016
 * */
package luci.core;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.h2.mvstore.MVStore;
import org.json.JSONArray;
import org.json.JSONObject;

import com.esotericsoftware.minlog.Log;

import luci.connect.Message;
import luci.core.validation.JsonType;
import luci.service.workflow.LoopController;

public class Workflow extends WorkflowElement implements TaskInfo{

	private static MVStore workflowStore;
	public final static String taskMap = "tasks";
	public final static Map<Long, String> names = getRootWorkflowNames();
	
	static {
		if (workflowStore == null) workflowStore = MVStore.open(Luci.cwd()+"/data/workflows");
	}
	
	public static Map<Long, String> getRootWorkflowNames(){
		if (workflowStore == null) workflowStore = MVStore.open(Luci.cwd()+"/data/workflows");
		Map<Long, String> result = new HashMap<>();
		Map<Long, StoredTask> storedTasks = workflowStore.openMap(taskMap);
		List<StoredTask> roots = storedTasks.values().stream().filter((st) -> st.getParentID() == 0)
				.collect(java.util.stream.Collectors.toList());
		for (StoredTask st: roots){
			result.put(st.getTaskID(), st.getName());
		}
		return result;
	}
	
	public static long getMaxID(){
		long max = 0;
		if (workflowStore == null) workflowStore = MVStore.open(Luci.cwd()+"/data/workflows");
		Map<Long, StoredTask> storedTasks = getStoredTasks();
		for (long l: storedTasks.keySet()) if (l > max) max = l;
		return max;
	}
	
	public static Map<Long, StoredTask> getStoredTasks(){
		return workflowStore.openMap(taskMap);
	}
	
	private Map<Long, WorkflowElement> elements = new HashMap<>();
	private Map<Long, WorkflowElement> toDelete = new HashMap<>();
	private Set<WorkflowElement> subscribedElements = new HashSet<>();
	private Map<Long, Interest> interestedInElementOutputs = new HashMap<>();
	private JsonType inputDescription = new JsonType();
	private JsonType outputDescription = new JsonType();
	private JSONObject example = new JSONObject();
	private Message output;
	private WorkflowConfig wfconfig;
	// TODO: check if workflows need an outputSchema
	
	public Workflow(StoredTask ts) throws FileNotFoundException{
		super(ts);
		Map<Long, StoredTask> storedTasks = getStoredTasks();
		for (long l: ts.getChildren()){
			StoredTask subTs = storedTasks.get(l);
			if (subTs.getServiceName() == null) elements.put(l, new Workflow(subTs));
			else {
				Factory f = Luci.observer().factories.get(subTs.getServiceName());
				f.new Task(subTs);
				// new Tasks put themselves into our element set
			}
		}
		for (WorkflowElement wfe: elements.values()){
			wfe.resubscribeInterests();
		}
		wfconfig = ts.getWorkflowConfig().setWorkflow(this);
	}
	public Workflow(Message msg) {
		super(msg, new JsonType(), new JSONObject());
		names.put(getTaskID(), getName());
		wfconfig = new WorkflowConfig().setWorkflow(this);
		JSONObject h = msg.getHeader();
		if (h.has("group")){
			JSONArray jgroup = h.getJSONArray("group");
			WorkflowElement first = null;
			for (Object o: jgroup) {
				WorkflowElement wfe = WorkflowElements.get((long) o);
				if (wfe != null) {
					if (first == null) first = wfe;
					if (wfe.getParentID() == first.getParentID()) 
						elements.put(wfe.getTaskID(), wfe);
					else {
						delete();
						throw new IllegalArgumentException("Cannot group tasks from multiple workflows!"
							+ String.format("task(%s).parentID(%s) != task(%s).parentID(%s)", 
							wfe.getTaskID(), wfe.getParentID(), first.getTaskID(), first.getParentID()));
					}
				}
			}
			
			// adjust the parent
			for (WorkflowElement wfe: elements.values()){
				wfe.setParent(this);
			}
			
			// rewire inputs
			for (WorkflowElement wfe: elements.values()){
				for (Long l: wfe.getInterests().keySet()){
					Interest i = wfe.getInterests().get(l);
					WorkflowElement interestedIn = WorkflowElements.get(l);
					// if an element of this workflow is connected to an element outside of this wf
					if (interestedIn.getParentID() == this.getParentID()){
						Map<String, String> io = i.getInOutMap();
						JsonType out = interestedIn.getOutputDescription();
						Interest wfi = new Interest(l, null);
						for (String in: io.keySet()){
							String k = this.getInputDescription().add(in, out.getTypeValidator(in));
							wfi.getInOutMap().put(k, io.get(in));
						}
						this.interests.put(l, wfi);
						// TODO: subscribe tasks to workflow events
					}
				}
			}
			// TODO: rewire outputs
		}
	}
	
	public boolean addListener(Listener listener){
		// if listener is a direct child
		if (listener instanceof WorkflowElement && ((WorkflowElement) listener).getParentID() == getTaskID())
			return subscribedElements.add((WorkflowElement) listener);
		return super.addListener(listener);
	}
	
	public boolean removeListener(Listener listener){
		// if listener is a direct child
		if (listener instanceof WorkflowElement && ((WorkflowElement) listener).getParentID() == getTaskID())
			return subscribedElements.remove((WorkflowElement) listener);
		return super.removeListener(listener);
	}
	
	private void notifyListeners(long duration){
		for (Listener scl: listeners){
			synchronized(scl){
				try {
					scl.getNotified(this, output, duration);
				} catch (Exception e){
					if (Log.DEBUG) e.printStackTrace();
					// do nothing else if a notification fails
				}
			}
		}
	}
	
	private void notifySubscribedElements(){
		for (Listener scl: subscribedElements){
			synchronized(scl){
				try {
					scl.getNotified(this, input, 0);
				} catch (Exception e){
					if (Log.DEBUG) e.printStackTrace();
					// do nothing else if a notification fails
				}
			}
		}
	}
	
	@Override
	public void getNotified(TaskInfo ti, Message m, long duration){
		// if we get notified by a child of this workflow
		if (ti.getParentID() == this.getParentID()){
			JSONObject h = m.getHeader();
			if (h.has("result")){
				addTo(output, interestedInElementOutputs.get(ti.getTaskID()), m, duration);
				if (checkAllInputsReady()) {
					output.setAttachmentPositions();
					notifyListeners(duration);
					resetInterests();
				}
			} else if (h.has("error")){
				resetInterests();
			}		
		} else {
			super.getNotified(ti, m, duration);
		}
	}
	
	public void registerFor(long taskID, Map<String, String> pairs){
		WorkflowElement wfe = workflowElements.get(taskID);
		// if we are interested in the output of a child of this workflow
		if (wfe.getParentID() == this.getParentID()){
			registerFor(taskID, interestedInElementOutputs, pairs);
		} else {
			super.registerFor(taskID, interests, pairs);
		}
	}
	
	public Set<Long> updateSubscriptions(JSONArray listensToDone){
		// handle input subscriptions:
		Set<Long> listensTo = super.updateSubscriptions(listensToDone);
		// go on with output subscriptions:
		// delete all tasks from sub-interests not mentioned in the given list
		Iterator<Interest> ints = interestedInElementOutputs.values().iterator();
		while(ints.hasNext()){
			Interest i = ints.next();
			if (!listensTo.contains(i.getTaskID()) && i.listensToDone() && i.getInOutMap().size() == 0){
				ints.remove();
			}
		}
		
		// add all sub-interests mentioned in the given list
		for (long taskID: listensTo){
			if (interestedInElementOutputs.containsKey(taskID)) 
				interestedInElementOutputs.get(taskID).setListensToDone(true);
			else interestedInElementOutputs.put(taskID, new Interest(taskID, null));
		}
		return listensTo;
	}
	
	public void updateOutputSubscriptions(JSONObject outputs){
		updateSubscriptions(outputs, interestedInElementOutputs);
	}

	@Override
	public JsonType getInputDescription() {
		return inputDescription;
	}

	@Override
	public JsonType getOutputDescription() {
		return outputDescription;
	}
	
	@Override
	public JSONObject getExampleCall() {
		return example;
	}

	@Override
	public Set<Service> getInitialServices(long callID) throws Exception {
		Set<Service> services = new HashSet<>();
		for (WorkflowElement wfe: elements.values()){
			if (wfe.listensToNobody()){
				services.addAll(wfe.getInitialServices(callID));
			}
		} 
		// add loop controllers only if there is no element listening to nothing
		if (services.size() == 0){
			for (WorkflowElement wfe: elements.values()){
				if (wfe instanceof LoopController){
					services.addAll(wfe.getInitialServices(callID));
					break;
				}
			}
		}
		return services;
	}
	
	@Override
	public Set<Service> run(Listener caller, long callID, long duration) throws Exception {
		notifySubscribedElements();
		return super.run(caller, callID, duration);
	}

	public Workflow put(long id, WorkflowElement wfe){
		elements.put(id, wfe);
		return this;
	}
	
	public WorkflowElement getElement(long id){
		return elements.get(id);
	}
	
	public WorkflowElement setElement(WorkflowElement wfe){
		storeNeedsUpdate = true;
		return elements.put(wfe.getTaskID(), wfe);
	}
	
	public Collection<WorkflowElement> elements(){
		return elements.values();
	}
	
	public Set<Long> elementKeys(){
		return elements.keySet();
	}
	
	public boolean safe(){
		boolean change = false;
		Map<Long, StoredTask> storedTasks = getStoredTasks();
		for (Entry<Long, WorkflowElement> e: elements.entrySet()){
			WorkflowElement wfe = e.getValue();
			if (wfe.didChange()) 
				if (_safe(e.getKey(), wfe, storedTasks)) change = true;
		}
		if (this.didChange()) 
			if (_safe(getTaskID(), this, storedTasks)) change = true;
		if (change) workflowStore.commit();
		return change;
	}
	
	private boolean _safe(long key, WorkflowElement wfe, Map<Long, StoredTask>storedTasks){
		storedTasks.put(key, new StoredTask(wfe));
		wfe.storeNeedsUpdate = false;
		return true;
	}
	
	public JSONObject toJSONObject(){
		JSONArray JSONElements = new JSONArray();
		for (WorkflowElement wfe: elements.values()){
			JSONElements.put(wfe.toJSONObject());
		}
		return super.toJSONObject()
				.put("elements", JSONElements)
				.put("services", wfconfig.sconfigs.values().stream().map((sc)->sc.getServiceName())
						.collect(Collectors.toList()));
	}
	
	public static JSONObject getJSONDescription(){
		return WorkflowElement.getJSONDescription()
				.put("elements", "list")
				.put("services", "list");
	}
	
	public void setName(String name){
		super.setName(name);
		names.put(getTaskID(), name);
	}

	/**Deletes the whole workflow = removes it from any maps that reference to it.
	 * 
	 */
	protected Set<Long> delete() {
		Map<Long, StoredTask> storedTasks = Workflow.getStoredTasks();
		for (long id: elements.keySet()) storedTasks.remove(id);
		storedTasks.remove(this.getTaskID());
		
		names.remove(getTaskID());
		if (parent != null) parent.remove(getTaskID());
		workflowStore.commit();
		
		Set<Long> removedIDs = new HashSet<>(elements.keySet());
		removedIDs.add(getTaskID());
		return removedIDs;
	}

	/**
	 * @param id
	 */
	public void remove(long id) {
		toDelete.put(id, elements.remove(id));
		storeNeedsUpdate = true;
	}

	public Message getOutputMessage() {
		return output;
	}
	
	public WorkflowConfig getWorkflowConfig(){
		return wfconfig;
	}
	
	public static class ServiceConfig implements Serializable {
		private static final long serialVersionUID = 1L;
		private String serviceName;
		private int id;
		private String args;
		private String ip;
		private boolean isAutoID = false;
		
		public ServiceConfig(JSONObject j){
			serviceName = j.getString("serviceName");
			id = j.getInt("id");
			ip = j.getString("ip");
			args = j.getString("args");
			isAutoID = j.optBoolean("isAutoID");
		}
		
		public String getServiceName(){
			return serviceName;
		}
		public int getId(){
			return id;
		}
		public String getArgs(){
			return args;
		}
		public String getIP(){
			return ip;
		}
		public boolean isAutoID(){
			return isAutoID;
		}
		public ServiceConfig setServiceName(String name){
			serviceName = name;
			return this;
		}
		public ServiceConfig setId(int id){
			this.id = id;
			return this;
		}
		public ServiceConfig setArgs(String args){
			this.args = args;
			return this;
		}
		public ServiceConfig setIP(String ip){
			this.ip = ip;
			return this;
		}
		public ServiceConfig isAutoID(boolean is){
			isAutoID = is;
			return this;
		}
		
		public JSONObject toJSON(){
			return new JSONObject()
					.put("serviceName", serviceName)
					.put("id", id)
					.put("ip", ip)
					.put("args", args)
					.put("isAutoID", isAutoID);
		}
	}
	
	public static class WorkflowConfig implements Serializable{
		private static final long serialVersionUID = 1L;
		transient private Workflow workflow;
		private Map<Integer, ServiceConfig> sconfigs = new HashMap<>();

		public WorkflowConfig setWorkflow(Workflow wf){
			workflow = wf;
			return this;
		}
		
		private Collection<ServiceConfig> filterConfigs(Set<Integer> ids){
			return (ids == null) ? sconfigs.values() : 
				sconfigs.values().stream().filter((sc)->ids.contains(sc.getId())).collect(Collectors.toSet());
		}
		
		public Map<String, Map<Integer, ServiceConfig>> getConfigsPerIp(Set<Integer> ids){
			Map<String, Map<Integer, ServiceConfig>> perIPMap = new HashMap<>();
			for (ServiceConfig sconfig: filterConfigs(ids)){
				Map<Integer, ServiceConfig> perIP = perIPMap.get(sconfig.getIP());
				if (perIP == null) perIPMap.put(sconfig.getIP(), (perIP = new HashMap<>()));
				perIP.put(sconfig.getId(), sconfig);
			}
			return perIPMap;
		}
		
		public Map<String, Map<Integer, JSONObject>> getConfigsPerIpAsJSON(Set<Integer> ids){
			Map<String, Map<Integer, JSONObject>> perIPMap = new HashMap<>();
			for (ServiceConfig sc: filterConfigs(ids)){
				Map<Integer, JSONObject> perIP = perIPMap.get(sc.getIP());
				if (perIP == null) perIPMap.put(sc.getIP(), (perIP = new HashMap<>()));
				perIP.put(sc.getId(), new JSONObject()
						.put("serviceName", sc.serviceName)
						.put("args", sc.args)
						.put("isAutoID", sc.isAutoID));
			}
			return perIPMap;
		}
		
		public ServiceConfig get(int id){
			return sconfigs.get(id);
		}
		
		public ServiceConfig remove(int id){
			return sconfigs.remove(id);
		}
		
		public int setServiceConfig(ServiceConfig sconf){
			int id = sconf.getId();
			if (id == 0){
				id = sconfigs.keySet().stream().mapToInt((i)->i).max().orElse(0) + 1;
				sconf.isAutoID(true).setId(id);
			}
			sconfigs.put(id, sconf);
			workflow.storeNeedsUpdate = true;
			return id;
		}
		
	}
}
