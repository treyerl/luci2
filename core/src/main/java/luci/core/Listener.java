/** MIT License
 * @author Lukas Treyer
 * @date Apr 5, 2016
 * */
package luci.core;

import java.util.Set;

import luci.connect.Message;

public interface Listener {
	public void getNotified(TaskInfo ti, Message m, long duration);
	public Set<ListenerManager> listensTo();
	default boolean startListeningTo(ListenerManager slm){
		listensTo().add(slm);
		return slm.addListener(this);
	}
	default boolean stopListeningTo(ListenerManager slm){
		listensTo().remove(slm);
		if (slm != null) return slm.removeListener(this);
		return false;
	}
	default void stopListening(){
		for (ListenerManager slm: listensTo())
			slm.removeListener(this);
		listensTo().clear();
	}
}
