package luci.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

import luci.connect.AttachmentAsFile;
import luci.connect.Message;
import luci.core.Factory.Task;
import luci.core.Workflow.WorkflowConfig;
import luci.core.WorkflowElement.Interest;
import luci.core.WorkflowElement.Position;
import luci.core.validation.JsonType;

public class StoredTask implements Serializable{
	private static final long serialVersionUID = 2L;
	String name, serviceName, input, inputSchema, inputDescription, outputDescription;
	Position p;
	WorkflowConfig wfconfig;
	Map<Long, Interest> interests;
	long parentID;
	Set<Long> children;
	long taskID;
	
	public StoredTask(WorkflowElement wfe){
		setInput(wfe.getInputMessage());
		setTaskID(wfe.getTaskID());
		setParentID(wfe.getParentID());
		setInterests(wfe.getInterests());
		setPosition(wfe.getPosition());
		setName(wfe.getName());
		setInputSchema(wfe.getInputSchema());
		if (wfe instanceof Workflow){
			setChildren(((Workflow) wfe).elementKeys());
			setServiceConfigs(((Workflow) wfe).getWorkflowConfig());
			setInputDescription(wfe.getInputDescription());
			setOutputDescription(wfe.getOutputDescription());
		} else if (wfe instanceof Task){
			setServiceName(((Task) wfe).getServiceName());
		}
	}
	
	public String getName() {
		return name;
	}
	
	public String getServiceName() {
		return serviceName;
	}
	public Message getInput() throws FileNotFoundException {
		return new Message(loadAttachments(new JSONObject(input)));
	}
	public Position getPosition() {
		return p;
	}
	public Map<Long, Interest> getInterests() {
		return interests;
	}
	public long getParentID() {
		return parentID;
	}
	public Set<Long> getChildren() {
		return children;
	}
	public long getTaskID(){
		return taskID;
	}
	public JSONObject getInputSchema(){
		return new JSONObject(inputSchema);
	}
	public JsonType getInputDescription(){
		return new JsonType(inputDescription);
	}
	public JsonType getOutputDescirption(){
		return new JsonType(outputDescription);
	}
	public WorkflowConfig getWorkflowConfig(){
		return wfconfig;
	}
	
	public StoredTask setName(String n){
		name = n;
		return this;
	}
	public StoredTask setServiceName(String serviceName) {
		this.serviceName = serviceName;
		return this;
	}
	public StoredTask setInput(Message input) {
		this.input = input.getHeader().toString();
		return this;
	}
	public StoredTask setPosition(Position p) {
		this.p = p;
		return this;
	}
	public StoredTask setInterests(Map<Long, Interest> interests) {
		this.interests = interests;
		return this;
	}
	public StoredTask setParentID(long parentID) {
		this.parentID = parentID;
		return this;
	}
	public StoredTask setChildren(Set<Long> childern) {
		this.children = new HashSet<>(childern);
		return this;
	}
	public StoredTask setTaskID(long id){
		taskID = id;
		return this;
	}
	public StoredTask setInputSchema(JSONObject ins){
		inputSchema = ins.toString();
		return this;
	}
	public StoredTask setInputDescription(JsonType ind){
		inputDescription = ind.toString();
		return this;
	}
	public StoredTask setOutputDescription(JsonType outd){
		outputDescription = outd.toString();
		return this;
	}
	
	public StoredTask setServiceConfigs(WorkflowConfig wfconfig){
		this.wfconfig = wfconfig;
		return this;
	}
	
	public boolean equalsInputMessageHeader(JSONObject j){
		return new JSONObject(input).equals(j);
	}
	
	/**Scans a json object for strings that can be resolved as pathnames and replaces those strings
	 * with AttachmentAsFile objects.
	 * @param json JSONObject
	 */
	private JSONObject loadAttachments(JSONObject json) throws FileNotFoundException{
		File attachmentFolder = luci.core.Luci.getAttachmentsFolder();
		for (String key: json.keySet()){
			Object val = json.get(key);
			if (val instanceof JSONObject){
				JSONObject j = (JSONObject) val;
				if (j.has("format") && j.has("attachment")){
					JSONObject at = j.getJSONObject("attachment");
					String filename = at.getString("checksum")+
						(JSONObject.NULL.equals(j.get("format")) ? "" : "."+j.getString("format"));
					File file = new File(attachmentFolder, filename);
					if (file.exists()){
						AttachmentAsFile a = new AttachmentAsFile(file.getAbsoluteFile());
						a.copyFrom(j);
						json.put(key, a);
					} else {
						throw new FileNotFoundException(filename);
					}
				} else {
					loadAttachments((JSONObject)val);
				}
			}
		}
		return json;
	}
}