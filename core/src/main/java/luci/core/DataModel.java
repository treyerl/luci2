package luci.core;

import java.sql.SQLException;

public interface DataModel {	
	public void init(String dbPath) throws SQLException ;

}
