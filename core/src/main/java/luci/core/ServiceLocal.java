/** MIT License
 * @author Lukas Treyer
 * @date Dec 11, 2015
 * */
package luci.core;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;

import luci.connect.Attachment;
import luci.connect.Message;
import luci.core.validation.JsonType;

public abstract class ServiceLocal extends Service implements Callable<Message> {
	
	static class ThreadPool extends ThreadPoolExecutor {
		private long avgWaitingTime = 0, numCalls = 0;
		private float sigmaWait = 0;
		private long minWait = Long.MAX_VALUE, maxWait = 0;
		private ThreadPool(int corePoolSize,
                		int maximumPoolSize,
                		long keepAliveTime,
                		TimeUnit unit,
                		BlockingQueue<Runnable> workQueue){
			super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
		}
		public ThreadPool(int size){
			this(size, size, 0L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
		}
		
		public void updateWaitingTime(long waiting){
			avgWaitingTime = ((avgWaitingTime * numCalls++) + waiting) / numCalls;
			float div = Math.abs(waiting - avgWaitingTime);
			sigmaWait = ((sigmaWait * (numCalls - 1)) + div) / numCalls;
			if (waiting < minWait) minWait = waiting;
			if (waiting > maxWait) maxWait = waiting;
		}
	}
	private static ThreadPool quickThreadPool = new ThreadPool(getMaxNumWorkerThreads());
	private static ThreadPool slowThreadPool = new ThreadPool(getMaxNumWorkerThreads());
	
	private static int getMaxNumWorkerThreads(){
		try {
			return Integer.parseInt(Luci.config.getProperty("maxNumWorkerThreads", "2"));
		} catch (NumberFormatException e){
			return 5;
		}
	}
	
	private boolean isRunning = false;
	private FactoryLocalServices fls;
	private long submitTime;
	boolean quick = false;
	
	void setFactory(FactoryLocalServices f){
		fls = f;
	}
	
	public final Future<Message> start() {
		submitTime = Luci.newTimestamp();
		float d = fls.getAvgDuration();
		if (fls.getNumCalls() == 0 || d > 2000) return slowThreadPool.submit(this);
		quick = true;
		return quickThreadPool.submit(this);
	}
	
	public static ThreadPool getSlowPool(){
		return slowThreadPool;
	}
	
	public static ThreadPool getQuickPool(){
		return quickThreadPool;
	}
	
	public static JSONObject poolInfo(ThreadPool pool){
		JSONObject json = new JSONObject();
		json.put("avgWaitingTime", pool.avgWaitingTime);
		json.put("waitingInQueue", pool.getQueue().size());
		json.put("poolSize", pool.getPoolSize());
		json.put("maxPoolSize", pool.getMaximumPoolSize());
		json.put("numCalls", pool.numCalls);
		return json;
	}
	
	public Message call() throws Exception {
		synchronized(input){
			isRunning = true;
			Message m = null;
			startTime = Luci.newTimestamp();
			if (quick) quickThreadPool.updateWaitingTime(startTime - submitTime);
			else slowThreadPool.updateWaitingTime(startTime - submitTime);
			try {
				m = implementation();
				notifyListeners(m, Luci.newTimestamp() - startTime);
			} catch (Exception e) {
				e.printStackTrace();
				errorNotification(e.toString());
			} finally {
				isRunning = false;
			}
			return m;
		}
		
	}
	
	protected JsonType wrapInputDescription(String inputs){
		return wrapInputDescription(new JSONObject(inputs));
	}
	
	protected JsonType wrapInputDescription(JSONObject inputs){
		return new JsonType(inputs.put("run", getName()).put("OPT callID", "number"));
	}
	
	protected JsonType wrapResultDescription(String str){
		return wrapResultDescription(new JSONObject(str));
	}
	
	protected JsonType wrapResultDescription(JSONObject result){
		JSONObject o = new JSONObject();
		o.put("XOR result", result).put("XOR error", "string").put("XOR progress", "json");
		return new JsonType(o);
	}
	
	protected Message wrapResult(String str){
		return wrapResult(new JSONObject(str));
	}
	
	protected Message wrapResult(JSONObject result){
		return new Message(new JSONObject().put("result", result));
	}
	
	protected JSONObject wrapExampleCall(JSONObject e){
		return e.put("run", getName());
	}
	
	protected JSONObject wrapExampleCall(String s){
		return new JSONObject(s).put("run", getName());
	}
	
	public boolean isRunning(){
		return isRunning;
	}
	
	public int howToHandleAttachments(){
		return Attachment.DOWNLOAD;
	}
	
	/**Method that implements the service's logic / algorithm.
	 * @return Message containing the calculated results
	 * @throws Exception
	 */
	public abstract Message implementation() throws Exception;

}
