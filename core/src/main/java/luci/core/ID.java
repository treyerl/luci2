/** MIT License
 * @author Lukas Treyer
 * @date Aug 20, 2016
 * */
package luci.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ID {
	private static Map<String, Long> ids = new HashMap<>();
	public static long getNew(String name){
		return ids.compute(name, (k,v)-> (v==null)? 1L : (v + 1L));
	}
	public static Set<String> getIDnames(){
		return ids.keySet();
	}
	public static boolean setNewMax(String name, long newMax){
		long current = ids.compute(name, (k,v) -> (newMax > v) ? newMax : v);
		return current == newMax;
	}
}
