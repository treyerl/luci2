package luci.core;

import java.lang.reflect.Constructor;
import java.util.function.Supplier;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.validation.JsonType;

public class FactoryServiceBalancer extends FactoryRemoteServices {
	
	private Class<ServiceBalancer> serviceClass;
	public FactoryServiceBalancer(Class<ServiceBalancer> s) throws Exception {
		this(init(s));
		serviceClass = s;
	}
	private FactoryServiceBalancer(Service s){
		super(s.getName(),
				s.getGeneralDescription(),
				s.getInputDescription(), 
				s.getOutputDescription(), 
				s.getExampleCall()
				);
	}
	
	public FactoryServiceBalancer(JSONObject registration) {
		super(registration);
	}
	
	public Service newService(Message input) throws Exception{
		Constructor<ServiceBalancer> ct = serviceClass.getDeclaredConstructor();
		ServiceBalancer s = ct.newInstance();
		s.setFactory(this);
		return s;
	}
	
	public Service createWorker(Message input) throws Exception {
		return super.createService(input);
	}
	
	private static Service init(Class<ServiceBalancer> serviceClass) throws Exception{
		Constructor<ServiceBalancer> ct = serviceClass.getConstructor();
		return ct.newInstance();
	}
	
	public RemoteHandle addRemote(RemoteHandle n, Message input){
		if (allWorkers.containsKey(n.getWorkerID()))
			throw new IllegalArgumentException("Non-Unique PersistentID");
		try {
			ServiceBalancer sbal = serviceClass.newInstance();
			sbal.setFactory(this);
			JSONObject h = input.getHeader();
			JSONObject inDesc = sbal.getRemoteInputDescription();
			JSONObject outDesc = sbal.getRemoteOutputDescription();
			if (inDesc != null && !JsonType.equals(h.getJSONObject("inputs"), inDesc))
				throw new IllegalArgumentException("Expected input specification: "+inDesc);
			if (outDesc != null && !JsonType.equals(h.getJSONObject("outputs"), outDesc))
				throw new IllegalArgumentException("Expected output specification: "+outDesc);
			if (sbal.addRemote(n, input)) super.addRemote(n, input);
			else allWorkers.put(n.getWorkerID(), n);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return n;
		
	}
	
	public RemoteHandle removeRemote(RemoteHandle n){
		RemoteHandle r = super.removeRemote(n);
		try {
			ServiceBalancer sbal = serviceClass.newInstance();
			sbal.setFactory(this);
			sbal.removeRemote(n);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return r;
	}
	
	public Supplier<Boolean> removeRemoteOnceAvailable(RemoteHandle r){
		return () -> {
			allWorkers.remove(r.getWorkerID());
			return false;
		};
	}
	
}
