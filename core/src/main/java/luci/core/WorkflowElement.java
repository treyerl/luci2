/** MIT License
 * @author Lukas Treyer
 * @date Mar 15, 2016
 * */
package luci.core;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.json.JSONArray;
import org.json.JSONObject;

import luci.connect.Attachment;
import luci.connect.AttachmentSpot;
import luci.connect.AttachmentSpot.JSONObjectArray;
import luci.connect.Message;
import luci.core.Factory.Task;
import luci.core.validation.JsonType;
import luci.core.validation.LcValidationException;
import luci.core.validation.Key;
import luci.core.Listener;
import luci.service.workflow.LoopController;


/**All inputs are being initialized in the constructor using default values as defined by the value 
 * type (see LcMetaJson for how default values of types are being generated).
 */
public abstract class WorkflowElement implements Listener, TaskInfo {
	
	public static WorkflowElement loadWithEntireWorkflow(long taskID){
		Map<Long, StoredTask> storedTasks = Workflow.getStoredTasks();
		if (storedTasks.containsKey(taskID)) {
			long anchestor = taskID;
			StoredTask st = null;
			while(anchestor != 0){
				st = storedTasks.get(anchestor);
				anchestor = st.getParentID();
			}
			if (st != null){
				try {
					new Workflow(st);
					return workflowElements.get(taskID);
				} catch (FileNotFoundException e) {
					return null;
				}
			}
		}
		return null;
	}

	protected static Map<Long, WorkflowElement> workflowElements = new HashMap<>();

	public static class WorkflowElements {	
		public static WorkflowElement get(long key) {
			WorkflowElement wfe = workflowElements.get(key);
			if (wfe != null) return wfe;
			else return loadWithEntireWorkflow(key);
		}
		public static WorkflowElement strictlyGet(long key){
			WorkflowElement wfe = get(key);
			if (wfe == null) throw new IllegalArgumentException("No task with ID '"+key+"' found!");
			else return wfe;
		}
		
		public static Collection<WorkflowElement> values(){
			return workflowElements.values();
		}
		
		public static WorkflowElement remove(long id){
			WorkflowElement wfe = workflowElements.get(id);
			if (wfe instanceof Workflow) {
				((Workflow) wfe).delete();
			} else if (wfe instanceof Task){
				Workflow parent = wfe.getParent();
				if (parent != null) parent.remove(id);
			}
			return workflowElements.remove(id);
		}
	}
	
	public static class CyclicWorkflowException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public CyclicWorkflowException(){
			super("Cyclic loop with no LoopController detected!");
		}
	}
	
	protected long taskID;
	protected Message input;
	protected JSONObject inputTemplate, inputSchema;
	protected final Map<Long, Interest> interests = new HashMap<>();
	protected final Set<ListenerManager> listenerManagers = new HashSet<>();
	protected final Set<Listener> listeners = new CopyOnWriteArraySet<>();
	
	protected long timestamp;
	protected String taskName;
	protected Workflow parent;
	private Position position;
	protected boolean storeNeedsUpdate = false;
	private boolean hasController = false;
	private long callID;
	private Listener caller;
	
	private void init(){
		timestamp = Luci.newTimestamp();
		workflowElements.put(taskID, this);
	}
	
	protected WorkflowElement(StoredTask ts) throws FileNotFoundException{
		input = ts.getInput();
		taskID = ts.getTaskID();
		taskName = ts.getName();
		parent = (Workflow) WorkflowElements.get(ts.getParentID());
		if (parent != null) parent.setElement(this);
		
		inputSchema = ts.getInputSchema();
		interests.putAll(ts.getInterests());
		
		position = ts.getPosition();
		init();
	}
	
	public WorkflowElement(Message msg, JsonType inDesc, JSONObject inSchema){
		JSONObject h = msg.getHeader();
		input = msg;
		taskID = ID.getNew("taskID");
		taskName = h.optString("name", null);
		
		inputSchema = inSchema;
		updateTemplate(inDesc, initializeSubscribedMembers(inputSchema));
		updateInputSubscriptions(inputSchema);
		
		// parent is set after input subscription update so we don't have to reset the parent
		// in case an exception is being thrown because of cyclic subscriptions
		parent = (Workflow) WorkflowElements.get(h.optLong("parentID", 0));
		if (parent != null) parent.setElement(this);

		JSONObject p = h.optJSONObject("position");
		if (p == null) p = new JSONObject().put("x", 50).put("y", 50);
		position = new Position().setX(p.getInt("x")).setY(p.getInt("y"));
		storeNeedsUpdate = true; //not loaded from store
		init();
	}
	
	public Listener getCaller(){
		return caller;
	}
	
	void resubscribeInterests(){
		for (long id: interests.keySet()){
			startListeningTo(workflowElements.get(id));
		}
	}
	
	// ABSTRACTS
	
	public abstract JsonType getInputDescription();
	
	public abstract JsonType getOutputDescription();
	
	public abstract JSONObject getExampleCall();
	
	public abstract Set<Service> getInitialServices(long CallID) throws Exception;
	
	
	// GETTERS/SETTERS
	public long getTaskID(){
		return taskID;
	}
	
	public JSONObject getInputDefinition(){
		return inputTemplate;
	}

	public Message getInputMessage(){
		return input;
	}
	
	public String getName(){
		return taskName;
	}
	
	public final int getNumListeners(){
		return listeners.size();
	}
	
	public Position getPosition(){
		return position;
	}
	
	public Workflow getParent(){
		return parent;
	}
	
	public long getParentID(){
		if (parent == null) return 0;
		return parent.getTaskID();
	}
	
	public boolean didChange(){
		return storeNeedsUpdate;
	}

	public Map<Long, Interest> getInterests(){
		return interests;
	}
	
	public JSONObject getInputSchema(){
		return inputSchema;
	}
	
	public JSONObject toJSONObject(){
		return new JSONObject()
				.put("name", getName())
				.put("taskID", getTaskID())
				.put("parentID", getParentID())
				.put("inputs", getInputMessage().getHeader())
				.put("inputSchema", inputSchema)
				.put("listensToDone", listensToDone())
				.put("position", position.toJSONObject());
	}
	
	public static JSONObject getJSONDescription(){
		return new JSONObject()
				.put("name", "string")
				.put("taskID", "number")
				.put("parentID", "number")
				.put("inputs", "json")
				.put("inputSchema", "json")
				.put("listensToDone", "list")
				.put("position", "json");
	}
	
	public Set<Listener> getListeners(){
		return listeners;
	}
	
	public long getCallID(){
		return callID;
	}
	
	public void setName(String name){
		taskName = name;
		storeNeedsUpdate = true;
	}
	
	public void setPosition(Position p){
		position = p;
		storeNeedsUpdate = true;
	}
	
	public void setParent(Workflow p){
		parent = p;
		storeNeedsUpdate = true;
	}
	
	// WORKFLOW FUNCTIONALITY
	
	private Set<String> updateTemplate(JsonType inDesc, JSONObject newTemplate){
		Set<String> unknownKeys = new HashSet<String>();
		unknownKeys.addAll(inDesc.validate(newTemplate));
		input.setHeader((inputTemplate = newTemplate));
		storeNeedsUpdate = true;
		return unknownKeys;
	}
	
	protected void addTo(Message toMsg, Interest i, Message recvMsg, long duration){
		if (i == null) throw new IllegalStateException("Notified upon no interest");
		timestamp = Luci.newTimestamp();
		
		JSONObject to = toMsg.getHeader();
		JSONObject recvResult = recvMsg.getHeader().getJSONObject("result");
		Set<String> nonAnyOutputKeys = new HashSet<>();
		// first add all outputs without ANY modifier
		boolean hasANY = false;
		for (String key: i.inOutMap.keySet()){
			String out = i.inOutMap.get(key);
			Key oKey = new Key(out);
			if (oKey.getModifier().equals("ANY")) hasANY = true;
			else {
				nonAnyOutputKeys.add(oKey.getKey());
				add(toMsg, recvMsg, key, oKey.getKey());
			}
		}
		
		if (hasANY){
			// first remove all entries that were added as an example for the ANY key
			List<String> namedKeys = getInputDescription().getNonANYKeysWithoutModifier();
			Iterator<String> keyIt = to.keys();
			while (keyIt.hasNext()) if (!namedKeys.contains(keyIt.next())) keyIt.remove();
			
			// then add the remaining keys if there was a key with an ANY modifier
			for (String key: recvResult.keySet()){
				if (nonAnyOutputKeys.contains(key)) continue;
				add(toMsg, recvMsg, key, key);
			}
			
		}
		
		i.setReady();
		storeNeedsUpdate = true;
	}
	
	private void add(Message toMsg, Message recvMsg, String key, String oKey){
		JSONObject to = toMsg.getHeader();
		JSONObject recvResult = recvMsg.getHeader();
		Object outValue = recvResult.getJSONObject("result").get(oKey);
		
		if (outValue instanceof Attachment){
			Attachment a = (Attachment) outValue;
			toMsg.setAttachment(a, new AttachmentSpot(key, new JSONObjectArray(to), a.getChecksum()));
		}
		to.put(key, outValue);
	}
	
	protected boolean checkAllInputsReady(){
		for (Interest i: interests.values())
			if (!i.isReady) return false;
		return true;
	}
	
	public Set<Service> run(Listener caller, long callID, long duration) throws Exception {
		this.caller = caller;
		this.callID = callID;
		if (this instanceof LoopController){
			LoopController lc = (LoopController) this;
			if (lc.hasNext()){
				run(lc.getProgress(input), lc.getDuration());
			} else {
				notifyListeners(lc.getResult(), lc.getDuration());
			}
		} else return run(null, 0);
		return null;
	}
	
	private Set<Service> run(Message progress, long duration) throws Exception{
		Set<Service> services = getInitialServices(callID);
		for (Service s: services){
			if (progress != null) s.run(progress, duration);
			else s.run();
		}
		return services;
	}
			
	public void registerFor(long taskID, Map<String, String> pairs){
		registerFor(taskID, interests, pairs);
	}
	
	protected final void registerFor(long taskID, Map<Long, Interest> interests, Map<String, String> pairs){
		WorkflowElement wfe = workflowElements.get(taskID);
		startListeningTo(wfe);
		if (interests.containsKey(taskID)){
			if (pairs != null) interests.get(taskID).inOutMap.putAll(pairs);
			else interests.get(taskID).listensToDone = true;
		} else if (checkForSubscriptionLoop(taskID)){
			interests.put(taskID, new Interest(taskID, pairs));
		} else return;
		storeNeedsUpdate = true;
	}
	
	private boolean checkForSubscriptionLoop(long otherTaskID){
		if (otherTaskID == taskID && !hasController) throw new CyclicWorkflowException();
		WorkflowElement wfe = workflowElements.get(otherTaskID);
		if (wfe != null){
			if (wfe instanceof luci.service.workflow.LoopController) hasController = true;
			else 
				for (long id: wfe.interests.keySet()) 
					if (checkForSubscriptionLoop(id)) 
						break;
			return true;
		}
		return false;
	}
	
	public final boolean deregisterInterest(Interest i, Collection<String> inputKeys){
		if (i.inOutMap.size() == 0) {
			deregisterFrom(taskID);
			return true;
		}
		for (String inputKey: inputKeys){
			i.inOutMap.remove(inputKey);
		}
		return (i.inOutMap.size() == 0 && !i.listensToDone);
	}
	
	public boolean addListener(Listener listener) {
		return listeners.add(listener);
	}
	
	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}
	
	@Override
	public void getNotified(TaskInfo ti, Message m, long duration){
		JSONObject h = m.getHeader();
		if (h.has("result")){
			addTo(input, interests.get(ti.getTaskID()), m, duration);
			if (checkAllInputsReady()) {
				input.setAttachmentPositions();
				try {
					run(ti.getCaller(), ti.getCallID(), duration);
				} catch (Exception e) {
					e.printStackTrace();
				}
				resetInterests();
			}
		} else if (h.has("error")){
			resetInterests();
		}
	}
	
	@Override
	public Set<ListenerManager> listensTo() {
		return listenerManagers;
	}
	
	public Set<Long> listensToDone(){
		Set<Long> done = new HashSet<>();
		for (Interest i: interests.values()){
			if (i.listensToDone()) done.add(i.getTaskID());
		}
		return done;
	}
	
	public boolean listensToNobody(){
		return listenerManagers.size() == 0;
	}
	
	protected void resetInterests(){
		for (Interest i: interests.values()) i.reset();
	}
	
	public Set<String> setAndValidateInputs(JSONObject inputs){
		Set<String> unknownKeys = new HashSet<>();
		List<Key> XOR = getInputDescription().getXORKeys();
		if ((inputs = withAcompleteB(this.inputSchema, null2default(inputs), XOR)) == null) 
			return unknownKeys;
		unknownKeys = updateTemplate(getInputDescription(), initializeSubscribedMembers(inputs));
		updateInputSubscriptions((this.inputSchema = inputs));
		return unknownKeys;
	}
	
	private JSONObject null2default(JSONObject inputs){
		JsonType inDesc = getInputDescription();
		for (String key: inputs.keySet()){
			if (inputs.get(key).equals(JSONObject.NULL)) 
				inputs.put(key, inDesc.getExample(key));
		}
		return inputs;
	}
	
	private JSONObject initializeSubscribedMembers(JSONObject inputs){
		JSONObject schema = new JSONObject();
		for (String key: inputs.keySet()){
			Object value = inputs.get(key);
			if (value instanceof JSONObject){
				JSONObject json = (JSONObject) value;
				if (json.has("taskID")){
					if (!json.has("key")) 
						throwMissingKey();
					int iid = json.getInt("taskID");
					String outK = json.getString("key");
					schema.put(key, exmapleValue(iid, outK));
					continue;
				}
			}
			schema.put(key, value);
		}
		return schema;
	}
	
	private Object exmapleValue(long taskID, String key){
		Object o = workflowElements.get(taskID).getOutputDescription()
				.toJSONObject().getJSONObject("XOR result").get(key);
		String type = o.toString();
		if (o instanceof JSONObject) type = "json";
		if (o instanceof JSONArray) type = ((JSONArray) o).getString(0);
		return JsonType.LCJSONExamples.get(JsonType.LCJSONTYPES.indexOf(type));
	}
	
	private void throwMissingKey(){
		throw new LcValidationException("Missing key 'key': Task subscriptions set for an "
				+ "input must point to an ouput key of the referenced task.");
	}
	
	protected static JSONObject withAcompleteB(JSONObject from, JSONObject to, List<Key> XOR){
		if (to == null && from != null) to = new JSONObject();
		if (to != null && from == null) from = new JSONObject();
		
		// remove all illegal XOR keys from A
//		List<Key> XOR = getInputDescription().getXORKeys();
		for (String key: to.keySet()){
			Key lcKey = new Key(key);
			if (XOR.contains(lcKey)){
				XOR.remove(lcKey);
				for (Key k: XOR){
					from.remove(k.getKey());
				}
			}
		}
		
		for (String key: from.keySet()){
			if (!to.has(key) || to.get(key) == null) 
				to.put(key, from.get(key));
		}
		return to;
	}
	
	private Map<Long, Map<String, String>> getSubscriptions(JSONObject inputs){
		Map<Long, Map<String, String>> add = new HashMap<>();
		for (String key: inputs.keySet()){
			Object value = inputs.get(key);
			if (value instanceof JSONObject){
				JSONObject json = (JSONObject) value;
				if (json.has("taskID")){
					long taskID = json.getLong("taskID");
					if (json.has("key")){
						Map<String, String> pairs = add.get(taskID);
						String keyPointer = json.getString("key");
						if (pairs == null) add.put(taskID, (pairs = new HashMap<>()));
						pairs.put(key, keyPointer);
					} else throwMissingKey();
				}
			} else if (key.equals("listensToDone") && value instanceof JSONArray){
				JSONArray list = (JSONArray) value;
				for (int i = 0; i < list.length(); i++){
					add.put(list.getLong(i), null);
				}
			}
		}
		return add;
	}
	
	private void deregisterFrom(long taskID){
		interests.remove(taskID);
		stopListeningTo(workflowElements.get(taskID));
		storeNeedsUpdate = true;
	}
	
	public void updateInputSubscriptions(JSONObject inputs){
		updateSubscriptions(inputs, interests);
	}
	
	protected void updateSubscriptions(JSONObject inputs, Map<Long, Interest> interests){
		if (inputs == null || inputs.length() <= 1) return;
		Map<Long, Map<String, String>> subscriptions = getSubscriptions(inputs);
		validateTaskIDs(subscriptions);
		Set<Long> deregister = new HashSet<>();
		for (Interest i: interests.values()){
			Set<String> keys = new HashSet<>();
			for (String key: i.inOutMap.keySet()){
				if (inputs.has(key) && (!subscriptions.containsKey(i.taskID) || 
						!subscriptions.get(i.taskID).containsKey(key)))
					keys.add(key);
			}
			if (keys.size() > 0) {
				if (deregisterInterest(i, keys))
					deregister.add(i.taskID);
			}
			if (subscriptions.containsKey(i.taskID)){
				i.inOutMap.putAll(subscriptions.get(i.taskID));
			}
		}
		// remove all obsolete interests (before they might get re-added in the next for-loop)
		for (long taskID: deregister){
			deregisterFrom(taskID);
		}
		for (Entry<Long, Map<String, String>> en: subscriptions.entrySet()){
			if (!interests.containsKey(en.getKey())) registerFor(en.getKey(), en.getValue());
		}
		storeNeedsUpdate = true;
	}
		
	public Set<Long> updateSubscriptions(JSONArray listensToDone){
		// clean up
		Set<Long> listensTo = new HashSet<>();
		if (listensToDone == null) return listensTo;
		for (int i = 0; i < listensToDone.length(); i++){
			listensTo.add(listensToDone.getLong(i));
		}
		
		// delete all tasks from interests not mentioned in the given list
		Iterator<Interest> ints = interests.values().iterator();
		while(ints.hasNext()){
			Interest i = ints.next();
			if (!listensTo.contains(i.taskID) && i.listensToDone){
				if (i.inOutMap.size() == 0) ints.remove();
				else i.setListensToDone(false);
			}
		}
		
		// add all interests mentioned in the given list
		for (long taskID: listensTo){
			if (interests.containsKey(taskID)) interests.get(taskID).listensToDone = true;
			else interests.put(taskID, new Interest(taskID, null));
		}
		return listensTo;
	}
	
	/**throw an exception if one of the taskIDs does not exist
	 * @param subscriptions Map&lt;Integer, Map&lt;String, String&gt;&gt;
	 */
	private void validateTaskIDs(Map<Long, Map<String, String>> subscriptions){
		for (long taskID: subscriptions.keySet()){
			if (!workflowElements.containsKey(taskID))
				throw new IllegalArgumentException("No task for taskID "+taskID);
		}
	}
	
	public static class Position implements Serializable{
		private static final long serialVersionUID = 1L;
		int x, y;
		public Position setX(int x_){
			x = x_;
			return this;
		}
		public Position setY(int y_){
			y = y_;
			return this;
		}
		
		public boolean equals(Object o){
			if (o instanceof Position){
				Position p = (Position) o;
				return x == p.x && y == p.y;
			}
			return false;
		}
		
		public JSONObject toJSONObject(){
			return new JSONObject().put("x",x).put("y",y);
		}
	}

	/**Represents a subscription of an task to another task.
	 *
	 */
	public static class Interest implements Serializable{
		private static final long serialVersionUID = 1L;
		final Map<String, String> inOutMap;
		final long taskID;
		private boolean isReady = false;
		boolean listensToDone = false;
		public Interest(long id, Map<String, String> map){
			taskID = id;
			if (map == null) {
				inOutMap = new HashMap<>();
				listensToDone = true;
			} else {
				inOutMap = map;
			}
		}
		void reset(){
			isReady = false;
		}
		void setReady(){
			isReady = true;
		}
		public boolean equals(Object o){
			if (o instanceof Interest){
				Interest i = (Interest) o;
				return inOutMap.equals(i.inOutMap) && taskID == i.taskID;
			}
			return false;
		}
		public Map<String, String> getInOutMap() {
			return inOutMap;
		}
		public boolean listensToDone(){
			return listensToDone;
		}
		public long getTaskID(){
			return taskID;
		}
		public Interest setListensToDone(boolean b){
			listensToDone = b;
			return this;
		}
	}
}
