/** MIT License
 * @author Lukas Treyer
 * @date Aug 10, 2016
 * */
package luci.core;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractListener implements Listener {
	Set<ListenerManager> listensTo = new HashSet<>();
	public Set<ListenerManager> listensTo(){
		return listensTo;
	}
}
