/** MIT License
 * @author Lukas Treyer
 * @date Dec 4, 2015
 * */
package luci.core;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;

import luci.connect.Attachment;
import luci.connect.Message;
import luci.core.LinkedTicketQueue.Ticket;
import luci.core.validation.JsonType;

public class ServiceRemote extends Service implements Future<Message> {
	RemoteHandle rh;
	FactoryRemoteServices frs;
	boolean cancelled = false;
	Message result;
	CountDownLatch waitForResult;
	Ticket<RemoteHandle> ticket;
	
	public ServiceRemote(FactoryRemoteServices frs) {
		this.frs = frs;
	}
	
	public ServiceRemote(FactoryRemoteServices frs, RemoteHandle rh){
		this(frs);
		this.rh = rh;
	}
	
	public void setResult(Message result) {
//		System.out.println("set result");
		this.result = result;
		notifyListeners(result, Luci.newTimestamp() - startTime);
	}

	@Override
	public Future<Message> start() {
		if (waitForResult != null) throw new IllegalStateException("running already");
		if (rh == null) {
			ServiceRemote sr = this;
			ticket = new Ticket<RemoteHandle>(){
				@Override
				public void handle(RemoteHandle rh_) {
					rh = rh_;
					rh.sendRun(sr);
				}
			};
			input.onForwardUnlock((waitedForMillis)->{
				frs.availableWorkers.cancel(ticket);
				input.onAttachmentsReady((l)->{
					frs.availableWorkers.book(ticket);
				});
			});
			frs.availableWorkers.book(ticket);
		} else {
			frs.availableWorkers.remove(rh);
			rh.sendRun(this);
		}
		waitForResult = new CountDownLatch(1);
		return this;
	}
	
	@Override
	public String getGeneralDescription(){
		return frs.getGeneralDescription();
	}
	
	@Override
	public JsonType getInputDescription() {
		return frs.getInputDescription();
	}

	@Override
	public JsonType getOutputDescription() {
		return frs.getOutputDescription();
	}

	@Override
	public JSONObject getExampleCall() {
		return frs.getExampleCall();
	}

	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		frs.availableWorkers.cancel(ticket);
		// rh might be null if we didn't get a ticket yet
		if (mayInterruptIfRunning && rh != null) rh.sendCancel(getCallID());
		waitForResult.countDown();
		return cancelled = true;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public boolean isDone() {
		return waitForResult.getCount() == 0;
	}

	@Override
	public Message get() throws InterruptedException, ExecutionException {
		waitForResult.await();
		return result;
	}

	@Override
	public Message get(long timeout, TimeUnit unit) 
			throws InterruptedException, ExecutionException, TimeoutException {
		waitForResult.await(timeout, unit);
		return result;
	}

	@Override
	public boolean isRunning() {
		return waitForResult != null && waitForResult.getCount() > 0;
	}
	
	public int howToHandleAttachments(){
		synchronized(frs.availableWorkers){
			if (!frs.availableWorkers.isEmpty()) {
				return Attachment.NETWORK;
			}
		}
		return Attachment.DOWNLOAD;
	}
	
	public String getName(){
		return frs.getName();
	}
	
}
