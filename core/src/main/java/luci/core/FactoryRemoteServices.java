/** MIT License
 * @author Lukas Treyer
 * @date Dec 4, 2015
 * */
package luci.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.LongStream;

import org.json.JSONObject;

import luci.connect.Message;
import luci.core.validation.JsonType;

public class FactoryRemoteServices extends Factory{
	
	
	public static class WorkerHistoryEntry{
		long timestamp;
		int numAvailable;
		int all;
		WorkerHistoryEntry(long t, int num, int a){
			timestamp = t;
			numAvailable = num;
			all = a;
		}
		public JSONObject toJSON(){
			return new JSONObject()
					.put("tst", timestamp)
					.put("idl", numAvailable)
					.put("all", all);
		}
	}
	
	public static class CallHistoryEntry{
		long callID;
		long timestamp;
		long writingTime;
		long readingTime;
		long runningTime;
		long idleTime;
		CallHistoryEntry(long cid, long t, long w, long r, long c, long i){
			callID = cid;
			timestamp = t;
			writingTime = w;
			readingTime = r;
			runningTime = c;
			idleTime = i;
		}
		public JSONObject toJSON(){
			return new JSONObject()
					.put("tst", timestamp)
					.put("callID", callID)
					.put("wrt", writingTime)
					.put("red", readingTime)
					.put("clc", runningTime)
					.put("idl", idleTime);
		}
	}
	
	String name;
	String version;
	String description;
	JSONObject customFields;
	Map<Long, RemoteHandle> allWorkers = new HashMap<>();
	LinkedList<WorkerHistoryEntry> workerHistory = new LinkedList<>();
	LinkedList<CallHistoryEntry> callHistory = new LinkedList<>();
	LinkedTicketQueue<RemoteHandle> availableWorkers = new LinkedTicketQueue<RemoteHandle>();
	
	public FactoryRemoteServices(JSONObject registration){
		super(registration.getString("serviceName"),
				registration.getString("description"),
				new JsonType(registration.optJSONObject("inputs")),
				new JsonType(registration.optJSONObject("outputs")),
				registration.getJSONObject("exampleCall"));
		registration.remove("serviceName");
		registration.remove("description");
		registration.remove("inputs");
		registration.remove("outputs");
		registration.remove("exampleCall");
		customFields = registration;
		availableWorkers.onSizeChange((size) -> workerHistory.add(new WorkerHistoryEntry(Luci.newTimestamp(), size, allWorkers.size())));
	}
	
	/**Constructor with package visibility for FactoryServiceBalancer
	 * @param name String
	 * @param req LcMetaJson
	 * @param out LcMetaJson
	 * @param example JSONObject
	 */
	FactoryRemoteServices(String name, String descr, JsonType req, JsonType out, JSONObject example){
		super(name, descr, req, out, example);
		customFields = new JSONObject();
	}
	
	public RemoteHandle addRemote(RemoteHandle n, Message m){
		n.setIdleSince(Luci.newTimestamp());
		allWorkers.put(n.getWorkerID(), n);
		availableWorkers.add(n);
		return n;
	}
	
	public RemoteHandle removeRemote(RemoteHandle r){
		if (r.isBusy()) r.onAvailable(removeRemoteOnceAvailable(r));
		else removeRemoteOnceAvailable(r).get();
		return r;
	}
	
	/**
	 * @param r
	 * @return a Supplier function that returns true if a RemoteHandle 
	 * 		should be put back into the worker queue
	 */
	public Supplier<Boolean> removeRemoteOnceAvailable(RemoteHandle r){
		return () -> {
			allWorkers.remove(r.getWorkerID());
			if (allWorkers.size() == 0) 
				Luci.observer().deregister(getName());
			return false;
		};
	}
	
	public void removeAllRemotes(){
		for (RemoteHandle r: allWorkers.values()){
			if (r.isBusy()) r.onAvailable(() -> {
				allWorkers.remove(r.getWorkerID());
				return false;
			});
			else {
				allWorkers.remove(r.getWorkerID());
			}
		}
		availableWorkers.clear();
	}
	
	public void cancelDeregister(RemoteHandle n){
		if (n.isBusy()) {
			n.handleCancel();
		}
		deregister(n);
	}
	
	public void deregister(RemoteHandle n){
		removeRemote(n);
	}
	
	public long getMaxWaitingTime(){
		return availableWorkers.getMaxWaitingTime();
	}
	
	private LongStream getAllIdleTimes(){
		long now = Luci.newTimestamp();
		return availableWorkers.stream().mapToLong(rh -> rh.getIdleTime(now));
	}
	
	public long getSumIdleTime(){
		return getAllIdleTimes().sum();
	}
	
	public long getMaxIdleTime(){
		java.util.OptionalLong omax = getAllIdleTimes().max();
		return omax.isPresent() ? omax.getAsLong() : 0;
	}
	
	public Set<RemoteHandle> getIdleLongerThan(long duration){
		long now = Luci.newTimestamp();
		return availableWorkers.stream().filter(rh -> rh.getIdleTime(now) > duration).collect(java.util.stream.Collectors.toSet());
	}
	
	public float avgNumIdleDuringTheLast(long during){
		long now = Luci.newTimestamp();
		long moreRecent = now;
		long after = now - during;
		long sum = 0;
		Iterator<WorkerHistoryEntry> iteratorIdleEntry = workerHistory.descendingIterator();
		while(iteratorIdleEntry.hasNext()){
			WorkerHistoryEntry idleEntry = iteratorIdleEntry.next();
			if (idleEntry.timestamp > after){
				sum += (moreRecent - idleEntry.timestamp) * idleEntry.numAvailable;
				moreRecent = idleEntry.timestamp;
			} else {
				sum += (moreRecent - after) * idleEntry.numAvailable;
				break;
			}
		}
		return sum / during;
	}
	
	public float avgNumBusyDuringTheLast(long during){
		long now = Luci.newTimestamp();
		long moreRecent = now;
		long after = now - during;
		long load = 0;
		Iterator<WorkerHistoryEntry> iie = workerHistory.descendingIterator();
		while(iie.hasNext()){
			WorkerHistoryEntry ie = iie.next();
			if (ie.timestamp > after){
				load += (moreRecent - ie.timestamp) * ((ie.all - ie.numAvailable) / ie.all);
				moreRecent = ie.timestamp;
			} else {
				load += (moreRecent - after) * ((ie.all - ie.numAvailable) / ie.all);
				break;
			}
		}
		return load / during;
	}
	
	public Set<CallHistoryEntry> getCallHistory(int period){
		long after = Luci.newTimestamp() - period;
		Set<CallHistoryEntry> recent = new HashSet<>();
		Iterator<CallHistoryEntry> iter = callHistory.descendingIterator();
		while(iter.hasNext()){
			CallHistoryEntry che = iter.next();
			if (che.timestamp < after) break;
			recent.add(che);
		}
		return recent;
	}
	
	public Set<WorkerHistoryEntry> getWorkerHistory(int period){
		long after = Luci.newTimestamp() - period;
		Set<WorkerHistoryEntry> recent = new HashSet<>();
		Iterator<WorkerHistoryEntry> iter = workerHistory.descendingIterator();
		while(iter.hasNext()){
			WorkerHistoryEntry whe = iter.next();
			if (whe.timestamp < after) break;
			recent.add(whe);
		}
		return recent;
	}
	
	public Map<Long, RemoteHandle> getAllWorkers(){
		return allWorkers;
	}
	
	public Collection<RemoteHandle> getIdleWorkers(){
		return availableWorkers;
	}
	
	private List<RemoteHandle> getBusyWorkers(){
		List<RemoteHandle> running = new ArrayList<>(allWorkers.values());
		running.removeAll(availableWorkers);
		return running;
	}
	
	public int getNumBusyWorkers() {
		return getBusyWorkers().size();
	}
	
	public int getNumIdleWorkers(){
		return availableWorkers.size();
	}
	
	public double getAvgTCRatio(){
		return allWorkers.values().stream().mapToDouble((rh) -> 
			rh.getTransmissionComputationRatio()).average().orElse(0);
	}
	
	/* (non-Javadoc)
	 * @see luci.core.Factory#createService(luci.connect.Message)
	 */
	public Service newService(Message input) throws Exception {
		return new ServiceRemote(this);
	}
	
	/* (non-Javadoc)
	 * @see luci.core.Factory#getInfo(boolean)
	 */
	@Override
	public JSONObject getInfo(boolean inclDescr){
		JSONObject info = super.getInfo(inclDescr);
		info.put("numNodes", allWorkers.size());
		info.put("numBusy", getNumBusyWorkers());
//		info.put("nodes", allWorkers.values().stream()
//				.collect(Collectors.toMap(RemoteHandle::getWorkerID, RemoteHandle::getIP)));
		for (String key: customFields.keySet())
			info.put(key, customFields.get(key));
		return info;
	}
}
