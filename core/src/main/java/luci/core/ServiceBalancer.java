package luci.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import org.json.JSONObject;

import luci.connect.Attachment;
import luci.connect.Message;

public abstract class ServiceBalancer extends Service implements Future<Message>, Listener{
	protected FactoryServiceBalancer fsb;
	private boolean isRunning = false, cancelled = false;
	CountDownLatch waitForResult;
	protected Message result;
	protected Set<Service> distributedCalls = new HashSet<>();
	protected Set<Message> results = new HashSet<>();
	Set<ListenerManager> listeners = new HashSet<>();
	Map<Service, Integer> progresses = new HashMap<>();
	int progress = 0;
	
	void setFactory(FactoryServiceBalancer fsb){
		this.fsb = fsb;
	}
	
	@Override
	public Future<Message> start(){
		startTime = Luci.newTimestamp();
		distributedCalls.clear();
		waitForResult = new CountDownLatch(1);
		distribute();
		for (Service worker: distributedCalls){
			startListeningTo(worker);
			worker.start();
		}
		return this;
	}
	
	public abstract void distribute();
	
	protected abstract boolean joinResults();
	
	public abstract boolean addRemote(RemoteHandle node, Message input);
	
	public abstract boolean removeRemote(RemoteHandle node);
	
	public abstract JSONObject getRemoteInputDescription();
	
	public abstract JSONObject getRemoteOutputDescription();
	
	@Override
	public boolean isRunning() {
		return isRunning;
	}

	@Override
	public int howToHandleAttachments() {
		// download before distribute
		return Attachment.DOWNLOAD;
	}
	
	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		for (Service w: distributedCalls){
			w.getFutureResult().cancel(mayInterruptIfRunning);
		}
		waitForResult.countDown();
		return cancelled = true;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public boolean isDone() {
		return waitForResult.getCount() == 0;
	}

	@Override
	public Message get() throws InterruptedException, ExecutionException {
		waitForResult.await();
		return result;
	}

	@Override
	public Message get(long timeout, TimeUnit unit) 
			throws InterruptedException, ExecutionException, TimeoutException {
		waitForResult.await(timeout, unit);
		return result;
	}

	@Override
	public void getNotified(TaskInfo ti, Message m, long duration) {
		JSONObject h = m.getHeader();
		if (h.has("result")){
			distributedCalls.remove(ti);
			results.add(m);
		} else if (h.has("progress")){
			progresses.put((Service) ti, h.getInt("progress"));
			int sum = progresses.values().stream().collect(Collectors.summingInt((v) -> Integer.valueOf(v))) / progresses.size();
			if (sum > progress) {
				progress = sum;
				notifyListeners(new Message(new JSONObject().put("progress", progress)), duration);
			}
		} else if (h.has("error")){
			notifyListeners(m, duration);
		}
		if (hasAllAnswers()){
			if (hasFinalAnswers()){
				notifyListeners(result, duration);
			} else {
				for (Service worker: distributedCalls){
					startListeningTo(worker);
				}
			}
		}
	}
	
	protected boolean hasAllAnswers(){
		return distributedCalls.size() == 0;
	}
	
	protected boolean hasFinalAnswers(){
		if (joinResults()){
			if (waitForResult != null) waitForResult.countDown();
			return true;
		}
		return false;
	}

	@Override
	public Set<ListenerManager> listensTo() {
		return listeners;
	}

}
