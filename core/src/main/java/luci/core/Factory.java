/** MIT License
 * @author Lukas Treyer
 * @date Dec 11, 2015
 * */
package luci.core;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.json.JSONException;
import org.json.JSONObject;

import luci.connect.Message;
import luci.core.Listener;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.JsonType;
import luci.core.validation.Key;

/**Base Factory Type. Constructor only visible in package. Contains two nested classes: 
 * Instance & Interest that are used to implement the workflow / react on result message
 * of other services. 
 * 
 * @author Lukas Treyer
 *
 */
public abstract class Factory implements Listener, ListenerManager{
	private JSONObject getInputSchema(JSONObject blankExample, JSONObject inputs){
		List<Key> XOR = getInputDescription().getXORKeys();
		return WorkflowElement.withAcompleteB(blankExample, inputs, XOR);
	}
	
	/**Represents an instantiated service in terms of the workflow diagram: all inputs are
	 * stored as parameters of the Task. Tasks are being run/kicked off upon notifications
	 * which might occur upon input parameter completion.
	 */
	public class Task extends WorkflowElement {
		/**
		 * @param msg
		 * @param exampleCall
		 * @param managedSubscriptionTypes
		 */
		public Task(Message msg) {
			super(msg, Factory.this.getInputDescription(), 
					Factory.this.getInputSchema(
							Factory.this.getExampleCall(), 
							msg.getHeader().optJSONObject("inputs")));
			this.addListener(Factory.this);
			taskName = "<"+getFactory().getName()+">";
			tasks.put(getTaskID(), this);
		}

		/**
		 * @param ts
		 * @throws JSONException 
		 * @throws FileNotFoundException 
		 */
		Task(StoredTask ts) throws FileNotFoundException {
			super(ts);
			tasks.put(getTaskID(), this);
		}

		@Override
		public JsonType getInputDescription() {
			return Factory.this.getInputDescription();
		}
		
		@Override
		public JsonType getOutputDescription() {
			return Factory.this.getOutputDescription();
		}

		public Set<Service> getInitialServices(long callID) throws Exception {
			input.getHeader().put("timestamp", timestamp);
			Service s = createService(input);
			for (Listener l: listeners) l.startListeningTo(s);
			s.setParentID(getParentID());
			s.setTaskID(taskID);
			s.setCallID(callID);
			return new HashSet<Service>(Arrays.asList(new Service[]{s}));
		}
		
		public Factory getFactory(){
			return Factory.this;
		}

		@Override
		public JSONObject getExampleCall() {
			return exampleCall;
		}
		
		public JSONObject toJSONObject() {
			return super.toJSONObject().put("serviceName", getFactory().getName());
		}
		
		public String getServiceName(){
			return Factory.this.getName();
		}
		
	}
	
	private String name;
	private String descr;
	private JsonType requiredInputs;
	private JsonType deliveredOutputs;
	private JSONObject exampleCall;
	private float avgDuration = 0F, sigma = 0F;
	private long minDur = Long.MAX_VALUE, maxDur = 0;
	private int numCalls = 0;
	
	private final Set<Listener> listeners = new CopyOnWriteArraySet<>();
	private final Set<ListenerManager> lManagers = new HashSet<>();
	private final Map<Long, Task> tasks = new HashMap<>();
	
	
	Factory(String name, String descr, JsonType req, JsonType out, JSONObject example){
		this.name = name;
		this.descr = descr;
		requiredInputs = req;
		deliveredOutputs = (out != null) ? out : JsonType.getStdOutput();
		exampleCall = example;
	}
	
	public boolean addListener(Listener listener){
		if (listener.getClass().equals(this.getClass())) 
			throw new IllegalArgumentException("cannot register to itself");
		return listeners.add(listener);
	}
	public int numListeners(){
		return listeners.size();
	}
	
	public Service createService(Message input) throws Exception{
		Service s = newService(input);
		s.setInput(input);
		startListeningTo(s);
		s.setPermissions();
		return s;
	}
	
	protected abstract Service newService(Message input) throws Exception;
	
	public Task createTask(Message input){
		return new Task(input);
	}
	
	public Task createTask(StoredTask st) throws FileNotFoundException{
		return new Task(st);
	}
	
	public Task removeTask(long taskID){
		tasks.get(taskID).getParent().remove(taskID);
		return tasks.remove(taskID);
	}
	
	public void removeAllTasks(Set<Long> success, Set<Long> failure){
		for (Long t: tasks.keySet()) {
			try {
				WorkflowElements.remove(t);
				success.add(t);
			} catch (Exception e){
				failure.add(t);
			}
		}
		tasks.clear();
	}
	
	boolean equals(Factory other){
		return 	this.name.equals(other.name) &&
				this.requiredInputs.equals(other.getInputDescription()) &&
				this.deliveredOutputs.equals(other.getOutputDescription());
	}
	public JSONObject getExampleCall(){
		return exampleCall;
	}
	
	public JSONObject getInfo(boolean inclDescr){
		JSONObject info = new JSONObject();
		if (inclDescr) 
			info.put("descr", getGeneralDescription());
		info.put("inputs", getInputDescription().toJSONObject());
		info.put("outputs", getOutputDescription().toJSONObject());
		info.put("example", getExampleCall());
		info.put("type", (this instanceof FactoryLocalServices) ? "local": "remote");
		return info;
	}
	
	public JsonType getInputDescription(){
		if (requiredInputs == null) 
			requiredInputs = new JsonType();
		if (!requiredInputs.has("run"))
			requiredInputs.add("run", "string");
		if (!requiredInputs.has("OPT callID"))
			requiredInputs.add("OPT callID", "number");
		return requiredInputs;
	}
	
	public String getName(){
		return name;
	}
	
	public String getGeneralDescription(){
		return descr;
	}
	
	public JsonType getOutputDescription(){
		if (deliveredOutputs == null) return new JsonType();
		return deliveredOutputs;
	}
	
	public WorkflowElement getTask(int taskID){
		return tasks.get(taskID);
	}
	
	public Set<Long> getTaskIDs(){
		return tasks.keySet();
	}
	
	public void getNotified(TaskInfo ti, Message m, long duration){
		if (m.getHeader().has("result")) addToMovingAverage(duration);
		for (Listener listener: listeners){
			synchronized(listener){
				listener.getNotified(ti, m, duration);
			}
		}
	}
	
	private void addToMovingAverage(long duration){
//		Thread.dumpStack();
		avgDuration = ((avgDuration * numCalls++) + duration) / numCalls;
		float div = Math.abs(duration - avgDuration);
		sigma = ((sigma * (numCalls - 1)) + div) / numCalls;
		if (duration < minDur) minDur = duration;
		if (duration > maxDur) maxDur = duration;
	}
	
	public float getAvgDuration(){
		return avgDuration;
	}
	
	public float getSigma(){
		return sigma;
	}
	
	public long getMinDur(){
		return minDur;
	}
	
	public long getMaxDur(){
		return maxDur;
	}
	
	public int getNumCalls(){
		return numCalls;
	}
	
	@Override
	public Set<ListenerManager> listensTo() {
		return lManagers;
	}
	
	public boolean removeListener(Listener listener){
		return listeners.remove(listener);
	}
	
	
	/* - - FACTORY SUBSCRIPTIONS - - */
	
	void removeServiceTask(int taskID){
		tasks.remove(taskID);
	}
	
	public String toString(){
		return getInfo(false).put("name", getName()).toString();
	}
	
	Set<String> validateInputs(JSONObject input){
		return requiredInputs.validate(input);
	}
}	
