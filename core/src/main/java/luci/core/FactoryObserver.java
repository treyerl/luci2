/** MIT License
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * */

package luci.core;

import static com.esotericsoftware.minlog.Log.trace;
import static luci.connect.LcString.upto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import luci.connect.LcString;
import luci.connect.Message;
import luci.connect.On.OnLong;
import luci.core.Factory.Task;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.validation.LcValidationException;

public class FactoryObserver {
	public static class NotAServiceException extends RuntimeException {
		private static final long serialVersionUID = -274217231416926676L;
		public NotAServiceException(String serviceName){
			super(String.format("'%s' not a service!", serviceName));
		}
	}
	private class LocalServiceScanner {
		List<Class<? extends Service>> services = new ArrayList<>();
		Set<File> jars = new HashSet<File>();
		String[] nonServiceJars = new String[]{
				"jansi","jline","json","minlog","connect","Java-WebSocket","DS_Store", "thumbs",
				"jts", "proj", "slf4j", "hamcrest", "h2spatial", "java-network-analyzer",
				"poly2tri", "h2-mvstore", "h2network", "jackson", "junit", "commons-compress", 
				"math3", "cts", "HikariCP", "jgrapht", "h2", "tinylog", "spatial", "luci.core"};

		/**When Luci is run from an IDE / "in dev mode" the services reside in the classes folder. </br>
		 * All classes get loaded in order to check if they inherit from the Service class.</br>
		 * @param dir File
		 * @throws IOException 
		 */
		void findClasses(String packageName, File dir) throws IOException {
			for (File file: dir.listFiles((File f)->{return f.isFile()&&f.getName().endsWith(".class");})){
				String name = packageName+"."+upto(file.getName(), -6);
				addClass(name);
			}
			for (File f: dir.listFiles((File a) -> {return a.isDirectory();})) 
				findClasses(packageName+"."+f.getName(),f);
		}
		
		/**Loads a class and checks for inheritance from the Service class. If the class inherits from 
		 * Service it is being added to the services list.
		 * @param name String name of the class to be loaded with Class.forName(String name)
		 */
		@SuppressWarnings("unchecked")
		void addClass(String name){
			try{
				Class<? extends Service> cls = (Class<? extends Service>) Class.forName(name);
				if (ServiceLocal.class.isAssignableFrom(cls) || ServiceBalancer.class.isAssignableFrom(cls))
					services.add(cls);
			} catch (ClassNotFoundException | ClassCastException e){
				trace(e.toString());
			}
		}
		
		/**Scans all jar files that have previously been loaded into List&lt;File&gt; jars.
		 * @throws IOException
		 */
		void scanJars() throws IOException{
			for (File f: jars){
				JarFile jarFile = new JarFile(f);
				Enumeration<JarEntry> entries = jarFile.entries();
				while(entries.hasMoreElements()){
					String p = entries.nextElement().getName();
					if (p.endsWith(".class") && p.startsWith("luci/service")){
						addClass(upto(p,-6).replace('/', '.'));
					}
				}
				jarFile.close();
			}
		}
		List<Class<? extends Service>> scan() throws IOException, URISyntaxException{
			String cs = FactoryObserver.this.getAsClassFileURL().toString();
			if (cs.startsWith("jar")){
				jars.add(new File(cs.substring(9, cs.lastIndexOf("!")).replace("/", File.separator)));
			} else {
				findClasses("luci.service", new File(FactoryObserver.this.getServiceFolder()
						.getCanonicalPath().replace("test-classes", "classes")));
			}
			String servicePath = File.separator+"classes"+File.separator+"luci"+File.separator+"service";
			String submoduleTestPath = "target"+servicePath;
			String[] projectClasses = new String[]{Luci.cwd()+servicePath, submoduleTestPath};
			for (String cp: projectClasses){
				File classPath = new File(cp);
				if (classPath.exists()) 
					findClasses("luci.service", classPath);
			}
			
			File lib = new File(Luci.cwd(), "lib");
			if (lib.exists()){
				jars.addAll(Arrays.asList(lib.listFiles((File n) 
						-> {return !new LcString(n.getName()).containsAny(nonServiceJars);})));
			}
			scanJars();
			return services;
		}
	}
	Map<String, Factory> factories = new TreeMap<>();
	Set<OnLong> onloads = new HashSet<>();
	
	@SuppressWarnings("unchecked")
	public void load(){
		LocalServiceScanner lss = new LocalServiceScanner();
		try {
			for (Class<? extends Service> s: lss.scan()){
				if (ServiceLocal.class.isAssignableFrom(s) && notAbstractClass(s)){
					factories.put(getServiceName(s), new FactoryLocalServices((Class<ServiceLocal>) s));
				} else if (ServiceBalancer.class.isAssignableFrom(s) && notAbstractClass(s)){
					factories.put(getServiceName(s), new FactoryServiceBalancer((Class<ServiceBalancer>) s));
				}
			}
			long l = factories.size();
			for (OnLong on: onloads) on.react(l);
		} catch (Exception e) {
			e.printStackTrace();
			Luci.exit(true);
		}
	}
	
	public void onLoad(OnLong on){
		onloads.add(on);
	}
	
	private static boolean notAbstractClass(Class<? extends Service> s){
		return ((s.getModifiers() & Modifier.ABSTRACT) == 0);
	}
	
	public static Factory getFactoryByTaskID(long taskID) throws FileNotFoundException{
		WorkflowElement wfe = WorkflowElements.get(taskID);
		if (wfe instanceof Task)
			return ((Task) wfe).getFactory();
		return null;
	}
	
	public String getServiceName(Class<? extends Service> s){
		return s.getName().replace("luci.service.", "");
	}
	
	URL getAsClassFileURL(){
		Class<? extends FactoryObserver> C = getClass();
		return C.getResource(C.getSimpleName()+".class");
	}
	
	File getServiceFolder(){
		Class<? extends FactoryObserver> C = getClass();
		String className = C.getSimpleName()+".class";
		URL classURL = C.getResource(className);
		String serviceFolder = classURL.toString().replace("core/"+className, "service").replace("file:", "");
		return new File(serviceFolder);
	}
	
	public Set<String> getServiceNames(){
		return factories.keySet();
	}
	
	public Set<FactoryRemoteServices> getRemoteServiceFactories(){
		return factories.values().stream().filter(f -> f instanceof FactoryRemoteServices).map(f -> (FactoryRemoteServices) f).collect(Collectors.toSet());
	}
	
	public WorkflowElement loadWorkflowElement(long taskID) throws FileNotFoundException {
		StoredTask st = Workflow.getStoredTasks().get(taskID);
		if (st == null) return null;
		if (WorkflowElement.workflowElements.containsKey(st.getParentID())){
			if (st.getServiceName() == null) return new Workflow(st);
			else return factories.get(st.getServiceName()).new Task(st);
		} else return WorkflowElement.loadWithEntireWorkflow(st.getTaskID());
	}
	
	/**
	 * @param name String: the name of the service
	 * @return ServiceFactory
	 * @throws Exception if the ServiceFactory for a Service cannot be created
	 */
	public Factory getServiceFactory(String name) {
		Factory f = factories.get(name);
		if (f == null) throw new NotAServiceException(name);
		return f;
	}
	
	/**Called by ClientHandlers to create a new instance of a requested service
	 * @param name String: name of the service
	 * @param input Message storing all input parameters
	 * @param unknownKeys Set&lt;String&gt; of keys found in the input json header not being 
	 * recognized by the requested service
	 * @return Service ready to run
	 * @throws Exception; many different types of exceptions for class instantiation problems; 
	 * JSONException for invalid inputs
	 */
	public Service createService(String name, Message input, Set<String> unknownKeys) 
			throws Exception {
		Factory f = getServiceFactory(name);
		Objects.requireNonNull(unknownKeys);
		try {
			unknownKeys.addAll(f.validateInputs(input.getHeader()));
		} catch (LcValidationException e){
			LcValidationException e1 = new LcValidationException(f.getName()+": "+e.getMessage());
			e1.addSuppressed(e);
			throw e1;
		}
		return f.createService(input);
	}
	
	public Service createService(String name, Message input) throws Exception{
		return createService(name, input, new HashSet<>());
	}
	
	public void addPermissionFor(String serviceName, Set<Integer> ids) throws Exception{
		Factory f = getServiceFactory(serviceName);
		Service s = f.createService(null);
		s.permitTo(ids);
	}
	
	public void removePermissionFor(String serviceName, Set<Integer> ids) throws Exception {
		Factory f = getServiceFactory(serviceName);
		Service s = f.createService(null);
		s.cancelPermissionFor(ids);
	}
	
	Task createTask(String name, Message input){
		Factory f = getServiceFactory(name);
		return f.createTask(input);
	}
	
	public void register(FactoryRemoteServices frs){
		String name = frs.getName();
		if (!factories.containsKey(name))
			factories.put(name, frs);
		else {
			FactoryRemoteServices a = (FactoryRemoteServices) factories.get(name);
			if (!a.equals(frs)) throw new RuntimeException("Remote service "+name
					+" already registered with different in/outputs");
		}
	}
	
	public void deregister(String name){
		Factory f = factories.get(name);
		if (f != null && f instanceof FactoryRemoteServices) factories.remove(name);
	}

	/**
	 * @param name
	 * @return
	 */
	public boolean hasService(String name) {
		return factories.containsKey(name);
	}
	
	public final Map<String, Map<String, Integer>> numRemoteServices(){
		Map<String, Map<String, Integer>> nodes = new HashMap<>();
		for (Factory f: factories.values()){
			if (f instanceof FactoryRemoteServices){
				FactoryRemoteServices frs = (FactoryRemoteServices) f;
				Map<String, Integer> info = new HashMap<>();
				info.put("available", frs.getNumIdleWorkers());
				info.put("busy", frs.getNumBusyWorkers());
				nodes.put(f.getName(), info);
			}
		}
		return nodes;
	}
	
	public final Map<String, Integer> sumRemoteServices(){
		Map<String, Integer> nodes = new HashMap<>();
		int available = 0;
		int busy = 0;
		for (Factory f: factories.values()){
			if (f instanceof FactoryRemoteServices){
				FactoryRemoteServices frs = (FactoryRemoteServices) f;
				available += frs.getNumIdleWorkers();
				busy += frs.getNumBusyWorkers();
			}
		}
		nodes.put("idle", available);
		nodes.put("busy", busy);
		nodes.put("total", available + busy);
		return nodes;
	}
}
