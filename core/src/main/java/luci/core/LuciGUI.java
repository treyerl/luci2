/** MIT License
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * */
package luci.core;

import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import luci.connect.GUIRun;

public class LuciGUI extends GUIRun{
	private final String osname;
	private final Image imgON;
	private final ImageIcon alertLogo;
	private final int trayIconWidth;
	private final TrayIcon trayIcon;
	final SystemTray tray = SystemTray.getSystemTray();

	public LuciGUI() throws AWTException {
		URL logo = getClass().getResource("logo/logo256x256_b.png");
		URL alert = getClass().getResource("logo/alert64x64.png");
		imgON  = new ImageIcon(logo).getImage();
		alertLogo = new ImageIcon(alert);
		osname = System.getProperty("os.name").toLowerCase();
		
		if (!osname.contains("mac")){
    		trayIconWidth = new TrayIcon(imgON).getSize().width;
    		trayIcon = new TrayIcon(imgON.getScaledInstance(trayIconWidth, -1, Image.SCALE_SMOOTH));
    	} else {
    		trayIcon = new TrayIcon(imgON);
    		trayIconWidth = trayIcon.getSize().width;
    		trayIcon.setImageAutoSize(true);
    	}
		createMenu();
	}
	
	private PopupMenu createMenu() throws AWTException{
		final PopupMenu popup = new PopupMenu();
		
        MenuItem clientConsole = new MenuItem("Client Console");
        MenuItem localhost = new MenuItem("Open Homepage");
        MenuItem bitbucketWebsite = new MenuItem("Luci Documentation");
        MenuItem openExamples = new MenuItem("Open Examples");
        MenuItem exit = new MenuItem("Exit");
        
        clientConsole.addActionListener((ActionEvent e) 
        		-> {try{guiRun(getConsoleCommand());}catch(IOException e1){e1.printStackTrace();}});
		bitbucketWebsite.addActionListener((ActionEvent e) -> openDocumentation());
		localhost.addActionListener((ActionEvent e) -> openHomepage());
		openExamples.addActionListener((ActionEvent e) -> {
			try {
				Desktop.getDesktop().open(
						new File(getClass().getResource("console_snippets.txt").toURI()));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
		exit.addActionListener((ActionEvent e) -> Luci.exit(true));
		
		popup.add(clientConsole);
		popup.addSeparator();
		popup.add(localhost);
		popup.addSeparator();
		popup.add(bitbucketWebsite);
		popup.add(openExamples);
		popup.addSeparator();
		popup.add(exit);
		
		trayIcon.setPopupMenu(popup);
		tray.add(trayIcon);
        return popup;
	}
	
	public void showError(String message){
		JOptionPane.showMessageDialog(null, message, "Error Message", JFrame.NORMAL, alertLogo);
	}
	
	/**
	 * @return
	 */
	private void openDocumentation() {
		try {
			java.awt.Desktop.getDesktop().browse(new URI("https://bitbucket.org/treyerl/luci2"));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	private void openHomepage(){
		try{
			java.awt.Desktop.getDesktop().browse(new URI("http://localhost:"+Luci.getWebserver().getPort()));
		} catch (IOException | URISyntaxException e){
			e.printStackTrace();
		}
	}
	
	public static String getConsoleCommand(){
		String s = File.separator;
		String ps = File.pathSeparator;
		String c = Luci.observer().getAsClassFileURL().toString();
		String luci = "";
		if (c.startsWith("jar")){
			String[] split = c.split("!");
			luci = split[0].substring(split[0].lastIndexOf(s)+1);
		} else luci = "classes";
		return String.format("java -cp \"%s%s%slib%s*\" luci.console.ClientConsole", luci,s,ps,s);
	}

	
	
	public TrayIcon getTrayIcon(){
		return trayIcon;
	}
}
