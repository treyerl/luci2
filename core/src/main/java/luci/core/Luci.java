/** MIT License
 * Copyright (c) 2015 Lukas Treyer, ETH Zurich
 * @author Lukas Treyer
 * @date Dec 2, 2015
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * */
package luci.core;

import static com.esotericsoftware.minlog.Log.error;
import static com.esotericsoftware.minlog.Log.trace;
import static com.esotericsoftware.minlog.Log.warn;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.Logger;
import org.pmw.tinylog.labelers.TimestampLabeler;
import org.pmw.tinylog.policies.DailyPolicy;
import org.pmw.tinylog.writers.RollingFileWriter;

import com.esotericsoftware.minlog.Log;

import luci.connect.Attachment;
import luci.connect.AttachmentAsFile;
import luci.connect.GUIRun;
import luci.connect.LcString;
import luci.connect.SelectionLoop;
import luci.connect.TcpSocketHandler;
import luci.core.WorkflowElement.WorkflowElements;
import luci.core.web.LcWebSock;
import luci.core.web.WebServer;
import luci.service.ServiceControl;

/**
 * THE Luci SERVER </br>
 * NIO reference: http://adblogcat.com/asynchronous-java-nio-for-dummies/
 */
public class Luci extends SelectionLoop {
	public final static int OS;
	public final static int OSX = 0;
	public final static int WIN = 1;
	public final static int NIX = 2;
	
	private static Luci luci;
	private static FactoryObserver observer;
	private static long startupTime;
	private static File cwd = new File(".");
	
	public static File cwd(){
		return cwd;
	}
	
	public static FactoryObserver observer(){
		return observer;
	}
	

	public final static Properties config = new Properties();
	static {
		String n = System.getProperty("os.name").toLowerCase();
		OS = n.contains("mac") ? OSX : n.contains("win") ? WIN : 
			new LcString(n).containsAny("nix", "nux", "aix") ? NIX : -1;
	}
	private static void init() {
		try {
			config.load(new FileInputStream(new File(cwd, "Luci.conf")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		observer = new FactoryObserver();
		initDataModel();
		ServiceControl.init();
		Log.set(logLevel(config.getProperty("log", "trace")));
		File log = new File(cwd, "data"+File.separator+"log.csv");
		String logFile = "log."+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".csv";
		boolean isNew = !new File(cwd, "data"+File.separator+logFile).exists();
		Configurator.defaultConfig()
			.writer(new RollingFileWriter(log.getAbsolutePath(), 5, false, new TimestampLabeler("yyyy-MM-dd"), new DailyPolicy()))
			.level(Level.INFO)
			.formatPattern("{date:HH:mm:ss},{message}")
			.activate();
		if (isNew) Logger.info("status,callID,serviceName,duration(ms),remote-service-ip,client-ips,error msg");
		File data = new File(cwd, "data");
		data.mkdir();
		Attachment.setCwd(cwd);
		AttachmentAsFile.filenameFromChecksum = true;
		AttachmentAsFile.parentDirectory = getAttachmentsFolder();
		Attachment.allowBlindAttachments = true;
		
		observer.load();
	}

	private static DataModel dataModel;
	
	private static DataModel initDataModel() {
		String db = config.getProperty("dataModelImplementation", "luci.service.scenario.Scenario");
		try {
			@SuppressWarnings("unchecked")
			Class<DataModel> dbhandler_class = (Class<DataModel>) Class.forName(db);
			
			String workingDir = cwd.getAbsolutePath();
			String dbPath = workingDir + "/" + config.getProperty("db_path_rel", "data/database");
			System.out.println("Using database '" + new File(dbPath).getCanonicalPath() + ".db'");
			
			DataModel dbhandler = dbhandler_class.newInstance();
			dbhandler.init(dbPath);
			
			return dataModel = dbhandler;

		} catch (Exception e){
//			e.printStackTrace();
			warn("no database initialized");
			DataModel dbhandler = new DataModel(){
				@Override
				public void init(String dbPath) throws SQLException {}
			};
			return dataModel = dbhandler;
		}
	}
	
	public static DataModel getDataModel(){
		return dataModel;
	}

	public static int logLevel(String level){
		switch(level.toLowerCase()){
		case "trace": return Log.LEVEL_TRACE;
		case "debug": return Log.LEVEL_DEBUG;
		case "info": return Log.LEVEL_INFO;
		case "warn": return Log.LEVEL_WARN;
		case "error": return Log.LEVEL_ERROR;
		default: return Log.LEVEL_INFO;
		}
	}
	
	public static File getAttachmentsFolder() {
		String folderPath = config.getProperty("attachmentsFolder", null);
		if (folderPath != null) {
			File folder = new File(folderPath);
			if (folder.exists())
				return folder;
		}
		File attachmentsFolder = new File(cwd, "data"+File.separator+"attachments");
		attachmentsFolder.mkdirs();
		return attachmentsFolder;

	}

//	public static void replaceSocketHandlerWith(ServerSocketHandler newHandler) {
//		if (luci != null)
//			luci._replaceSocketHandler(newHandler);
//		else
//			throw new IllegalStateException("luci not running? no luci singleton.");
//	}

	public static LcWebSock getWebsocket() {
		if (luci != null)
			return luci.getWebSocket();
		else
			throw new IllegalStateException("luci not running? is null");
	}
	
	public static WebServer getWebserver(){
		if (luci != null)
			return luci.getWebServer();
		else
			throw new IllegalStateException("luci not running? is null");
	}

	private static WebServer startWebServer() throws IOException {
		String webHost = Luci.config.getProperty("webHost", "0.0.0.0");
		int webPort = Integer.parseInt(Luci.config.getProperty("webPort", "8080"));
		InetSocketAddress webIsa = new InetSocketAddress(resolve(webHost), webPort);
		WebServer webserver = new WebServer(webIsa);
		new Thread(webserver).start();
		return webserver;
	}

	private static LcWebSock startWebSocket() throws IOException {
		String wsHost = Luci.config.getProperty("wsHost", "0.0.0.0");
		int wsPort = Integer.parseInt(Luci.config.getProperty("wsPort", "8008"));
		InetSocketAddress wsIsa = new InetSocketAddress(resolve(wsHost), wsPort);
		LcWebSock websocket = new LcWebSock(wsIsa);
		new Thread(websocket).start();
		return websocket;
	}
	
	private static InetAddress resolve(String hostname) {
		try {
			return (hostname.equalsIgnoreCase("localhost")) ? 
					InetAddress.getLocalHost() : InetAddress.getByName(hostname);
		} catch (UnknownHostException e) {
			error(e.toString());
			return null;
		}
	}
	
	public static InetSocketAddress getCurrentInetSocketAddress(){
		return luci.getInetSocketAddress();
	}

	private LuciGUI gui;
	private WebServer webserver;
	private LcWebSock websocket;
	private InetSocketAddress isa;

	public Luci(String hostname, int port, LuciGUI gui, WebServer web, LcWebSock ws) throws IOException {
		this.gui = gui;
		this.webserver = web;
		this.websocket = ws;
		isa = bind(hostname, port);
		startupTime = newTimestamp();
		System.out.println("Luci: tcp socket bound to " + isa);
	}

//	private void _replaceSocketHandler(ServerSocketHandler newHandler) {
//		RemoteHandler n = null;
//		if (newHandler instanceof RemoteHandler) {
//			n = (RemoteHandler) newHandler;
//			newHandler = n.getSocketHandlerDelegate();
//		}
//		if (newHandler instanceof TcpSocketHandler) {
//			SelectionKey key = ((TcpSocketHandler) newHandler).getSelectionKey();
//			key.attach((n != null) ? n : newHandler);
//		} else if (newHandler instanceof WebSocketHandler) {
//			Map<WebSocket, ServerSocketHandler> websocketHandlers = websocket.getWebSocketHandlers();
//			WebSocketHandler sock = (WebSocketHandler) newHandler;
//			if (websocketHandlers.containsKey(sock.getWebSocket()))
//				websocketHandlers.put(sock.getWebSocket(), (n != null) ? n : newHandler);
//		}
//	}

	public WebServer getWebServer() {
		return webserver;
	}

	public LcWebSock getWebSocket() {
		return websocket;
	}
	
	public InetSocketAddress getInetSocketAddress(){
		return isa;
	}
	
	/* (non-Javadoc)
	 * @see luci.connect.SelectionLoop#newSocketHandler(java.nio.channels.SelectionKey, java.util.Set)
	 */
	@Override
	protected TcpSocketHandler newSocketHandler(SelectionKey key) {
		return new ClientHandler(key, this);
	}

	/* (non-Javadoc)
	 * @see luci.connect.SelectionLoop#processException(java.lang.Exception)
	 */
	@Override
	protected boolean processException(Exception e) {
		e.printStackTrace();
		if (gui != null)
			gui.showError(e.toString());
		return false;
	}
	
	/* (non-Javadoc)
	 * @see luci.connect.SelectionLoop#processIOException(java.nio.channels.SelectionKey)
	 */
	@Override
	protected boolean processIOException(SelectionKey key, IOException e) {
		trace("remove clientHandler");
		key.cancel();
		try {
			((SocketChannel) key.channel()).close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return false;
	}

	public static long newTimestamp(){
		return System.currentTimeMillis();
	}
	
	/**
	 * @return Luci's startup time in milliseconds since 1.1.1970.
	 */
	public static long getStartupTime(){
		return startupTime;
	}
	
	static String validateHostAddress(String h) throws MalformedURLException{
		new URL(h);
		return h;
	}

	/**
	 * To start Luci navigate to luci/target and enter <br>
	 * 
	 * <pre>
	 * java -cp "classes/:lib/*" org.luci.core.Luci UI
	 * </pre>
	 * 
	 * Leave out UI if you wish to start Luci without UI (UI = the small
	 * SystemTray icon). <br>
	 * 
	 * @param args
	 */
	
	public static void main(String[] args){
		start(args);
	}
	
	
	public static Luci start(String[] args){
		if (luci != null) {
			System.err.println("Luci running already!");
			return luci;
		}
		try {
			long t = newTimestamp();
			LuciGUI gui = null;
			WebServer webserver = null;
			LcWebSock websocket = null;
			String hostname = null;
			boolean web = true, browser = true;
			int port = 0;
			System.out.println("optional commandline arguments: -systemtray -noWeb -noBrowser -h 'hostname' -p 'port' -cwd 'current working directory'");
			if (args.length > 0) {
				List<String> arguments = Arrays.asList(args);
				if (arguments.contains("-systemtray"))
					gui = new LuciGUI();
				if (arguments.contains("-noWeb")) web = false;
				if (arguments.contains("-noBrowser")) browser = false;
				int h = arguments.indexOf("-h");
				int p = arguments.indexOf("-p");
				int c = arguments.indexOf("-cwd");
				try {
					if (h >= 0) hostname = validateHostAddress(arguments.get(h + 1));
					if (p >= 0) port = Integer.parseInt(arguments.get(p + 1));
					if (c >= 0) {
						File f = new File(arguments.get(c+1));
						if (!f.exists()) 
							throw new FileNotFoundException(f.getCanonicalPath());
						cwd = f;
						System.setProperty("luci.cwd", cwd.getCanonicalPath());
					}
				} catch (IndexOutOfBoundsException e){
					if (hostname == null) System.err.println("missing hostname");
					else if (port == 0) System.err.println("missing port number");
					else System.err.println("missing working dir");
				} catch (MalformedURLException | NumberFormatException | FileNotFoundException e){
					System.err.println(e.getMessage());
				}
				
			} 
			if (hostname == null)
				hostname = config.getProperty("hostname", "0.0.0.0");
			if (port == 0)
				port = Integer.valueOf(config.getProperty("port", "7654"));
			
			// set cwd and instantiate singletons [FactoryObserver, DataModel] 
			Luci.init();
			
			if (web){
				websocket = startWebSocket();
				webserver = startWebServer();
			}
			
			luci = new Luci(hostname, port, gui, webserver, websocket);
			new Thread(luci).start();
			
			Logger.info("[starting up luci],,,"+(newTimestamp() - t)+",");
			
			if (web && browser) GUIRun.openURLWithoutAWT("http://localhost:"+webserver.getPort());
			
			final Thread shutdownHook = new Thread() {
			    public void run() {
			        exit(false);
			    }
			};
			
			Runtime.getRuntime().addShutdownHook(shutdownHook);
			
		} catch (AWTException | IOException e) {
			e.printStackTrace();
		}
		return luci;
	}
	
	public static void exit(boolean exit) {
		try {
			if (luci.gui != null){
				luci.gui.tray.remove(luci.gui.getTrayIcon());
			}
			if (luci.webserver != null){
				luci.webserver.shutdown();
			}
			System.out.println("webserver stopped");
			if (luci.websocket != null){
				luci.websocket.stop();
			}
			System.out.println("websocket stopped");
			for (long id: Workflow.names.keySet()){
				((Workflow) WorkflowElements.get(id)).safe();
			}
			for (WorkflowElement wfe: WorkflowElements.values()) {
				wfe.stopListening();
			}
			System.out.println("luci stopped");
			luci = null;
//			dbHandler.close();
//			luci.selector.close();
		} catch ( IOException | InterruptedException  e){
			e.printStackTrace();
		}
		if (exit) System.exit(0);
	}
}