/**
 * 
 */


function println(str, classname, keep){
	$("#messageDisplay").fadeIn(100);
	var line = $( "#message" );
	if (typeof str == "string") line.html( str );
	else line.html( JSON.stringify(str) );
	if (classname !== undefined){
		line.removeClass("error");
		line.removeClass("green");
		line.removeClass("help");
		line.addClass(classname);
	} else line.removeAttr("class");
	if (classname != "error" && !keep){
		setTimeout(function(){
			$("#messageDisplay").fadeOut();
		}, 2500);
	} 
}

$("#messageDisplay").on("click", function(){
	$("#help").css("display", "none");
	$(this).fadeOut();
});

/**Used by e.g. shutdown; an action that needs the confirmation that it is being called from localhost
 * */
function keyAction(evt, n){
	var name = n || $(this).attr("id");
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "/"+name, false);
	xhr.onload = function(evt){
		var key = xhr.responseText;
		var xhr2 = new XMLHttpRequest();
		xhr2.open("GET", "/"+name+"?key="+key, false);
		try {
			xhr2.send();
		} catch (error){
			
		}
	};
	xhr.send();
}

function time(millis){
	var MS = pad(millis % 1000, 3);
	var s = Math.floor(millis / 1000);
	var S = pad(s % 60, 2);
	var m = Math.floor(s / 60);
	var M = pad(m % 60, 2);
	var h = Math.floor(m / 60);
	var H = pad(h % 24, 2);
	var d = Math.floor(h / 24);
	var D = d % 7;
	var w = Math.floor(d / 7);
	return {'w':w,'D':D,'d':d,'H':H,'h':h,'M':M,'m':m,'S':S,'s':s,'MS':MS};
}

function pad(number, digits){
	var d = 1;
	if (number > 0) d = Math.floor(Math.log10(number))+1;
	var n = ""+number;
	while(d++ < digits) n = "0"+n;
	return n;
}

// http://jsfiddle.net/joquery/9KYaQ/
String.format = function() {
    var theString = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }
    
    return theString;
}