
function resetDropDown(){
	$("#inputs").html($('<select />'))
}

function updateDropDown(id, ports, taskID, viewerValue){
	
	var inputs = $("#inputs");
    var data = {};
    for (var name in ports){
    	var port = ports[name]
    	var targetKey = id+"/"+name;
    	if (port.type == "in" &&!(targetKey in targets)){
    		data[name] = port.val || "";
    	}
    }
    
    var s = $('<select />');
    for(var val in data) {
        $('<option />', {value: data[val], text: val}).appendTo(s);
    }
    s.on("change", function(evt){
    	$("#input").val($(this).val() || "")
    })
    inputs.attr("taskID", taskID)
    inputs.html(s);
    
    var option = $('#inputs select').find("option:selected");
    $("#input")
		.attr("taskID", taskID)
		.attr("modelID", id)
		
	if (taskID > 0) $("#input").val(option.val());
	else if (!(viewerValue instanceof Image)) 
		$("#input").val(JSON.stringify(viewerValue, null, "\t")).css("height", "200px");
}

$("#input").on("change", function(evt){
	var $this = $(this);
	var inputs = {};
	var val = $this.val();
	try {
		val = JSON.parse($this.val());
	} catch (error){
		if (error.message.indexOf("Single quotes") >= 0)
			println(error.message, "error");
	}
	
	var option = $('#inputs select').find("option:selected");
	var taskID = parseInt($("#input").attr("taskID"));
	var key = option.text();
	option.val(val);
	if (taskID > 0){
		inputs[removeModifiers(key)] = val;
		lc.sendAndReceive(
			{'run':'task.Update','taskID':taskID,'inputs':inputs}, 
			function(message){
				var $in = $("#input");
				var cell = paper.getModelById($in.attr("modelID"));
				var portName = key;
				cell.ports[portName]["val"] = val;
				$in.blur();
			});
	} else {
		try {
			paper.getModelById($this.attr("modelID")).setValue(JSON.parse(val));
		} catch (error){
			paper.getModelById($this.attr("modelID")).setValue(val);
		}
		
	}
});

$("#run").on("click", function(){
	var startTaskIDs = [];
	var sources = graph.getSources();
	for (var i = 0; i < sources.length; i++){
		var taskID = sources[i].attr("taskID");
		tasks[taskID].attr({rect: {fill : pending }})
		startTaskIDs.push(taskID);
	}
	// turn all cells white
	var cells = graph.getCells();
	for (var i = 0; i < cells.length; i++){
		var cell = cells[i];
		if (cell instanceof joint.shapes.devs.Service){
			cell.attr({rect: {fill : white }});
		} else if (cell instanceof joint.shapes.devs.Viewer){
			cell.setValue("");
		}
	}
	
	lc.send({'run':startTaskIDs});
});

function addWorkflows(workflowMap){
	var select = $("#workflows select");
	for (var id in workflowMap){
		select.append("<option value='"+id+"'>"+workflowMap[id]+"</option>");
	}
	load(select.val());
}

function getCurrentWorkflowID(){
	return parseInt($("#workflows select option:selected").val());
}

var change = function(e, dropdown){
	var select = dropdown || this;
	var taskID = parseInt($(select).val());
	removeAllNodes();
	load(taskID);
}

$("#workflows select").change(change);

function load(taskID){
	if (typeof(taskID) != 'number') taskID = parseInt(taskID);
	if (taskID > 0){
//		console.log("loading "+taskID);
		lc.sendAndReceive({'run':'task.Get','taskID':taskID}, function(message){
			var wf = message.getHeader().result.task;
			for (var i = 0; i < wf.elements.length; i++){
				var element = wf.elements[i];
				if ("serviceName" in element){
					var p = element.position;
					createTaskAndSusbscribeToIt(element.serviceName, element.taskID, p.x, p.y);
				} else {
					// TODO: create groups
				}
			}
			for (var i = 0; i < wf.elements.length; i++){
				var element = wf.elements[i];
				for (var key in element.inputSchema){
					var value = element.inputSchema[key];
					if (Luci.typeof_o(value) == 'Object'){
						if ('key' in value && 'taskID' in value){
							createLink(element.taskID, key, value.taskID, value.key);
						}
					}
				}
				for (var j = 0; j < element.listensToDone.length; j++){
					var taskID = element.listensToDone[j];
					var link = createLink(element.taskID, mainIn, taskID, mainOut);
					var linkView = paper.findViewByModel(link.id);
					linkView.$el.find(".connection").attr("stroke", "#ea4");
				}
				tasks[element.taskID].listensTo = element.listensToDone;
			}
		});
	}
}

function createLink(taskIDTarget, keyTarget, taskIDSource, keySource){
	var targetNode = tasks[taskIDTarget];
	var sourceNode = tasks[taskIDSource];
	var targetPorts = {}
	var sourcePorts = {}
	for (var key in targetNode.ports) if (targetNode.ports[key].type == 'in') targetPorts[key.split(" ")[1] || key] = key;
	for (var key in sourceNode.ports) if (sourceNode.ports[key].type == 'out') sourcePorts[key.split(" ")[1] || key] = key;
	var link = new joint.shapes.devs.Link({
        source: { id: sourceNode.id, port: sourcePorts[keySource] || keySource},
        target: { id: targetNode.id, port: targetPorts[keyTarget] || keyTarget}
    });
	graph.addCell(link);
	return link;
}

var addedMySelf = false;

$("#add").on("click", function(e){
	addedMySelf = true;
	$("#workflows select").append("<option value='0'>New Workflow</option>");
	$("#workflows select").val("0");
	renameWorkflow(e, $("#edit")[0], function(option, newName){
		// create a workflow with the given name
		lc.sendAndReceive({'run':'workflow.Create','name':newName}, function(message){
			var taskID = message.getHeader().result.taskID;
			option.val(taskID);
			removeAllNodes();
			$("#workflows select").change(change);
		})
	});
	$("#workflows input").focus();
	$("#messageDisplay").fadeOut();
})

$("#delete").on("click", function(){
	lc.send({'run':'task.Remove','taskIDs':[getCurrentWorkflowID()]});
	removeAllNodes();
	$("#workflows select option:selected").remove();
	load(getCurrentWorkflowID());
});

function removeAllNodes(){
	delete tasks[0];
	var taskIDs = Object.keys(tasks);
	lc.send({'run':'task.UnsubscribeFrom','taskIDs':taskIDs})
	removeAll = true;
	for (var taskID in tasks){
		delete targets[tasks[taskID].id];
		tasks[taskID].remove();
		delete tasks[taskID];
	}
	var cells = graph.getElements();
	for (var i = 0; i < cells.length; i++){
		cells[i].remove();
	}
	removeAll = false;
}

$("#edit").on("click", renameWorkflowFromIcon);

function renameWorkflowFromIcon(e){
	renameWorkflow(e, this, function(option, newName){
		// update the workflow with its new name
		var taskID = parseInt(option.val());
		var update = {'run':'task.Update','taskID':taskID,'name':newName}
		lc.send(update);
	});
}

var safedMySelf = false;

$("#safe").on("click", function(){
	safedMySelf = true;
	lc.sendAndReceive({'run':'workflow.Safe','taskID':getCurrentWorkflowID()}, 
		function(message){
			if (message.getHeader().result.change) println("successfully safed", "green");
			else println("nothing to safe");
			
	});
});

$("#reload").on("click", function(e){
	change(e, $("#workflows select")[0]);
	this.src = "button/reload.svg";
});

function renameWorkflow(e, edit, renameHandler){
	var rename = function(){
		var newName = input[0].value;
		input.replaceWith(select);
		var option = $("#workflows select option:selected");
		option.text(newName);
		renameHandler(option, newName);
	}
	
	var text = $("#workflows select option:selected").text();
	var select = $("#workflows select").replaceWith("<input type='text'></input>");
	var input = $("#workflows input");
	input[0].value = text;
	edit.src = "../button/ok.svg";
	$(edit).off("click").on("click", function(){
		rename();
		edit.src = "../button/edit.svg";
		$(edit).off("click").on("click", renameWorkflowFromIcon);
	});
	input.keypress(function(e){
		if (e.which == 13){
			rename();
			edit.src = "../button/edit.svg";
			$(edit).off("click").on("click", renameWorkflowFromIcon);
		}
	});
}