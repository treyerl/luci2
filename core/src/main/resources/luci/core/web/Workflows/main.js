/** MIT License
 * @author Lukas Treyer
 * @date Jan 13, 2016
 * */

var ResponsePrinter = (function(){
	var ResponsePrinter = function(){};
	ResponsePrinter.prototype = new Luci.ResponseHandler();
	ResponsePrinter.prototype.onResult = function(message){
		var h = message.getHeader();
		if (h.serviceName == "RemoteRegister"){
			var name = h.result.registeredName;
			if (!(name in serviceInfo)){
				var ul = $("#serviceList");
				var serviceList = addServices([name], ul);
				lc.sendAndReceive({'run':'ServiceInfo','serviceNames':serviceList},
					new ServiceInfoHandler(serviceList)
				);
			}
		} else if (h.serviceName == "RemoteDeregister"){
			var deregServices = h.result.deregisteredServices;
			for (var i = 0; i < deregServices.length; i++){
				var deregService = deregServices[i];
				var name = deregService.deregisteredName;
				$("#s_"+name.replace(".", "\\.")).remove();
				delete serviceInfo[name];
			}
			
			
		} else if (h.serviceName == "workflow.Save"){
			if (savedMySelf) {
				savedMySelf = false;
				return;
			}
			if (h.result.change) {
				var taskID = parseInt(h.result.taskID);
				if (taskID == getCurrentWorkflowID()) {
					document.getElementById("reload").src = "button/reload_red.svg";
				}
			}
			
		} else if (h.serviceName == "workflow.Create"){
			if (addedMySelf) {
				addedMySelf = false;
				return;
			}
			var taskID = h.result.taskID;
			var name = h.result.name;
			$("#workflows select").append("<option value='"+taskID+"'>"+name+"</option>");
			
		} else if (h.taskID > 0){
			node = tasks[h.taskID];
			node.attr({rect: {fill : completed }});
			var successors = graph.getSuccessors(node);
			if (successors.length == 1){
				if (successors[0] instanceof joint.shapes.devs.Viewer){
					var viewer = successors[0];
					var keys = Object.keys(h.result);
					if (keys.length == 1)
						viewer.setValue(h.result[keys[0]]);
					else viewer.setValue(h.result);
				}
			}
			finishNodes();
		} else if (!("success" in h.result)){
			println(JSON.stringify(h.result).substring(0,50));
		}
	}
	ResponsePrinter.prototype.onError = function(message){
		var h = message.getHeader();
		if (!(h.serviceName == "RemoteRegister" || h.serviceName == "RemoteDeregister")){
			console.log("the error", message.getHeader().error)
			if (h.taskID > 0){
				node = tasks[h.taskID];
				node.attr({rect: {fill : error }});
				finishNodes();
			}
			println(message.getHeader().error, "error");
		}
	}
	ResponsePrinter.prototype.onProgress = function(message){
		var h = message.getHeader();
		if (h.taskID > 0){
			node = tasks[h.taskID];
			node.attr({rect: {fill : pending }});
		}
		
	}
	return ResponsePrinter;
})();


var lc = new Luci.Client(location.hostname, 8008, "", ResponsePrinter);
var serviceInfo = {};
var cellStroke = '#2b3549';
var portBody = '#5e4e78';
var selectedColor = 'red';
var completed = "#AFA";
var pending = "#FD2";
var error = "#FAA";
var white = "#ffffff";
var tasks = {};
var mainIn = '_mainIn_';
var mainOut = '_mainOut_';

function finishNodes(){
	var cells = graph.getCells();
	for (var i = 0; i < cells.length; i++){
		var cell = cells[i];
		if (cell instanceof joint.shapes.devs.Service){
			var c = cell.attr("rect").fill;
//			console.log(c);
			if (c === undefined || c == white || c == pending) return;
		}
	}
	for (var i = 0; i < cells.length; i++){
		var cell = cells[i];
		if (cell instanceof joint.shapes.devs.Service){
			if (cell.attr("rect").fill != error) 
				cell.attr({rect: {fill : white }});
		}
	}
}

var dragNode = {
	start: function(){
		$("#help").css("display", "none");
	},
	stop : function() {
		var $li = $(this);
		$li.attr("left", $li.css("left"));
		$li.attr("top", $li.css("top"));
		$li.css("left", 0).css("top", 0);
	}
}



function addViewer(){
	if (Object.keys(utilsInfo).length > 0){
		var p = $("#panel");
		var ul = $(document.createElement("ul"));
		for (var nodeName in utilsInfo){
			var li = $("<li></li>");
			li.text(nodeName);
			li.draggable(dragNode);
			ul.append(li);
		}
		p.append(ul);
	}
}

function addServices(list, ul){
	var serviceList = [];
//	list.push("elend.langer.string.der.das.ganze.layout.versaut");
	
	for (var i = 0; i < list.length; i++){
		var serviceName = list[i];
		if (serviceName.indexOf("task.") == 0 || 
			serviceName.indexOf("workflow.") == 0 ||
			serviceName.indexOf("user.") == 0) continue;
		if (serviceName == "RemoteRegister" || serviceName == "RemoteDeregister") continue;
		serviceList.push(serviceName);
		var li = $("<li></li>");
		li.attr("id", "s_"+serviceName);
		li.text(serviceName);
		li.draggable(dragNode);
		ul.append(li);
	}
	
	return serviceList;
}

var ServiceInfoHandler = (function(){
	var serviceList;
	ServiceInfoHandler = function(list){
		serviceList = list;
	};
	ServiceInfoHandler.prototype = new ResponsePrinter();
	ServiceInfoHandler.prototype.onResult = function(message){
		var infos = message.getHeader().result;
		for (var i = 0; i < serviceList.length; i++){
			var info = infos[serviceList[i]];
			delete info.inputs.run;
			info.outputs = info.outputs["XOR result"] || {};
		}
		serviceInfo = _.merge(serviceInfo, infos);
	};
	return ServiceInfoHandler;
})();

$("#paper").droppable({
	drop : function(event, ui) {
		resetDropDown();
		$("#input").blur().val("").css("height", "17px");
		var serviceName = ui.draggable.text();
		var x = ui.offset.left - 5;
		var y = ui.offset.top - 35;
		if (Object.keys(utilsInfo).indexOf(serviceName) >= 0){
			createTask(serviceName, 0, x, y);
		} else {
			var parentID = getCurrentWorkflowID();
			if (parentID > 0){
				lc.sendAndReceive(
					{'run':'task.Create','serviceName':serviceName,'parentID': parentID, 
						'position':{'x':x,'y':y}},
					function(message){
						var result = message.getHeader().result;
						createTaskAndSusbscribeToIt(serviceName, result.taskID, x, y);
					}
				);
			} else {
				println("No worklfow; create one first with the (+) icon.", "error");
			}
		}
	}
});

function createTaskAndSusbscribeToIt(serviceName, taskID, x, y){
	createTask(serviceName, taskID, x, y);
	var subscribe = {'run':'task.SubscribeTo','taskIDs':[taskID], 'inclResults':true};
	lc.send(subscribe);
}

function createTask(serviceName, id, _x, _y){
	var info = serviceInfo[serviceName] || utilsInfo[serviceName];
	var inputKeys = Object.keys(info.inputs);
	var outputKeys = Object.keys(info.outputs);
	var ox = 25 + (inputKeys.length * 10);
	var w = Math.max(120, serviceName.length*7 + 20);
	var h = (inputKeys.length + outputKeys.length) * 10 + 40;
	var attributes = {
		".label": {"text":serviceName}, 
		'.outPorts':{ 'ref-y': ox, ref: '.body'},
		'.body': { 'rx': 6, 'ry': 6  },
		rect : {'stroke': cellStroke},
		taskID: id
	};
	
	// input types
	for (var i = 0; i < inputKeys.length; i++){
		attributes['.inPorts>.port'+i+'>.port-body'] = {type:info.inputs[inputKeys[i]]};
	}
	// output types
	for (var i = 0; i < outputKeys.length; i++){
		attributes['.outPorts>.port'+i+'>.port-body'] = {type:info.outputs[outputKeys[i]]};
	}
	var inits = {
		attrs: attributes,
	    position: { x: _x - 155, y: _y },
	    inPorts: inputKeys,
	    outPorts: outputKeys,
	    mainPorts: [mainIn,mainOut]
	};
	var node;
	if (Object.keys(utilsInfo).indexOf(serviceName) >= 0){
		inits.size = { width: 160, height: 190 };
		node = new joint.shapes.devs[serviceName](inits);
	} else {
		inits.size = {width: w, height: h};
		node = new joint.shapes.devs.Service(inits);
	}
	node.on("change:position", function(cellView, pos, ui){
		movedID = id;
		movedTo = pos;
	});
	
	graph.addCells([node]);
	tasks[id] = node;
}

var movedID = null;
var movedTo = {};
var li;
var green = false;
var orange = false;
var sourceType;
var targets = {};
var graph = new joint.dia.Graph();
graph.on('change:source change:target', function(link) {
//	console.log(link);
	var sourcePort = link.get('source').port;
	var serviceName = link.getSourceElement().attributes.attrs[".label"].text;
	var sourceType = serviceInfo[serviceName].outputs[sourcePort];
	// TODO: make source type labels work
	link.set({labels:{text:sourceType}});
	var target = link.get('target');
	var source = link.get("source");
	if (sourcePort == mainOut && !orange) {
		var linkView = paper.findViewByModel(link.id);
		var el = linkView.$el;
		orange = true;
		el.find(".connection").attr("stroke", "#ea4");
	}
	if (Object.keys(target).indexOf("id") >= 0) {
		orange = false;
		resetPorts(sourceType);
		targets[target.id+"/"+target.port] = {
				"sourceID": link.get("source").id, 
				"targetID": target.id,
				"targetPort": target.port
		};
		var ti = paper.getModelById(target.id);
		if (ti instanceof joint.shapes.devs.Service){
			
			var update = {'run':'task.Update','taskID':ti.attr("taskID")};
			var si = paper.getModelById(source.id);
			var to = ti.listensTo;
			if (source.port != mainOut && target.port != mainIn){
				var inputs = {}
				inputs[removeModifiers(target.port)] = {'key':source.port, 'taskID':si.attr("taskID")};
				update['inputs'] = inputs;
				if ($("#inputs").attr("taskID") == ti.attr("taskID")){
					updateDropDown(ti.id, ti.ports, ti.attr("taskID"));
				}
			} else {
				to = to || [];
				to.push(si.attr("taskID"));
			}
			if (to !== undefined){
				ti.listensTo = to;
				update["listensToDone"] = to;
			}
			console.log(update);
			lc.sendAndReceive(update, new ConnectionValidator(link));
		}
	} else if (!green){
		greenPorts(sourceType);
	}
});


function removeModifiers(key){
	var modifiers = ["XOR ","OPT ","ANY "];
	for (var i = 0; i < modifiers.length; i++){
		if (key.indexOf(modifiers[i]) == 0) return key.split(" ")[1];
	}
	return key;
}

var ConnectionValidator = (function(){
	var link
	var ConnectionValidator = function(l){
		link = l;
	}
	ConnectionValidator.prototype = new ResponsePrinter();
	ConnectionValidator.prototype.onError = function(message){
		link.remove();
	}
	return ConnectionValidator;
})();

var removeAll = false;

graph.on('remove', function(el){
	if (el.isLink()){
		var t = el._previousAttributes.target;
		var s = el._previousAttributes.source;
		var keyname = removeModifiers(t.port);
		var target = graph.getCell(t.id);
		var source = graph.getCell(s.id);
		var taskID = parseInt(target.attr("taskID"));
		var sourceID = parseInt(source.attr("taskID"));
		var inputs = {};
		inputs[keyname] = null;
		if (taskID > 0 && !removeAll){
			if (keyname == mainIn){
				var toDone = target.listensTo;
				toDone.pop(toDone.indexOf(sourceID));
				lc.sendAndReceive({'run':'task.Update','taskID':taskID,'listensToDone':toDone},
					function(message){
						console.log(message.getHeader());
				});
			} else {
				lc.sendAndReceive({'run':'task.Update','taskID':taskID,'inputs':inputs}, 
					function(message){
						delete targets[t.id+"/"+t.port];	
				});
			}
			
		}
	}
});

function greenPorts(srcType){
	console.log(srcType);
	sourceType = srcType;
	if (srcType === undefined){
		$("circle[port="+mainIn+"]").attr("fill", "#ea4");
	} else {
		$("circle[type='"+srcType+"']").attr("fill", "#0f0");
		$("circle[type='any']").attr("fill", "#0f0");
	}
	green = true;
}

function resetPorts(srcType){
	sourceType = srcType || sourceType;
	$("circle[port="+mainIn+"]").attr("fill", "#fff");
	$("circle[type='"+sourceType+"']").attr("fill", portBody);
	$("circle[type='any']").attr("fill", portBody);
	green = false;
}

var tKey;
var paper = new joint.dia.Paper({
    el: $('#paper'),
    width: $(document).width() - 155,
    height: $(document).height() - 35,
    gridSize: 1,
    model: graph,
    snapLinks: true,
    linkPinning: false,
    embeddingMode: true,
    validateEmbedding: function(childView, parentView) {
        return parentView.model instanceof joint.shapes.devs.Coupled;
    },
    validateConnection: function(sourceView, sourceMagnet, targetView, targetMagnet) {
    	var s = V(sourceMagnet), t = V(targetMagnet);
    	var sType = sourceMagnet.parentNode.parentNode.classList[0];
    	var tType = targetMagnet.parentNode.parentNode.classList[0];
    	tKey = targetView.model.id+"/"+t.attr("port");
    	if (sourceView.id != targetView.id && s.attr("id") != t.attr("id")) {
    		if (s.attr("port") == mainOut && t.attr("port") == mainIn){
    			return true;
    		} else {
    			return (s.attr("type") == t.attr("type") || t.attr("type") == "any") 
    		        	&& sType != tType && (Object.keys(targets).indexOf(tKey) < 0 || 
    		        			targets[tKey].sourceID == sourceView.model.id); 
    		}
    	}
    	return false;
        
    },
    defaultLink: new joint.shapes.devs.Link
});


/* custom highlighting */
var selected = {};
var shift = false;
var alt = false;
var highlighter = V('circle', {
    'r': 6,
    'stroke': '#a32a6e',
    'stroke-width': '2px',
    'fill': 'transparent',
    'pointer-events': 'none'
});

//var node;
paper.off('cell:highlight cell:unhighlight').on({
    
    'cell:highlight': function(cellView, el, opt) {

        if (opt.embedding) {
            V(el).addClass('highlighted-parent');
        }

        if (opt.connecting) {
            var bbox = V(el).bbox(false, paper.viewport);
            highlighter.translate(bbox.x + 4, bbox.y + 4, { absolute: true });
            V(paper.viewport).append(highlighter);
        }
    },
    
    'cell:unhighlight': function(cellView, el, opt) {

        if (opt.embedding) {
            V(el).removeClass('highlighted-parent');
        }

        if (opt.connecting) {
            highlighter.remove();
        }
    },
    'cell:pointerclick': function(cellView) {
    	
        var s = cellView.model;
        var c = s.attributes.attrs.rect.stroke;
        if (c == cellStroke) {
        	if (!shift) {
        		for (var sel in selected){
        			selected[sel].attr({rect: {stroke : cellStroke }});
        		}
        		selected = {};
        	}
        	s.attr({rect: {stroke : selectedColor }});
        	selected[s.cid] = s;
        	updateDropDown(s.id, s.ports, s.attr("taskID"), s.attr("value"));
//        	CopyHTMLToClipboard($(cellView.el).find(".svgbody>p"));
        	$("#input").focus();
        	if (s.attr("taskID") == 0) $("#input").css("height", "200px");
        } else {
        	if (!shift && Object.keys(selected).length > 1){
        		delete selected[s.cid];
        		for (var sel in selected) {
        			selected[sel].attr({rect: {stroke: cellStroke }});
        		}
        		selected = {};
        		selected[s.cid] = s;
        		updateDropDown(s.id, s.ports, s.attr("taskID"), s.attr("value"));
        		$("#input").focus();
        	} else if ($("#input").is(":focus")){
        		$("#input").blur();
        	} else {
        		s.attr({rect: {stroke : cellStroke }});
            	delete selected[s.cid];
            	$("#input").blur().val("").css("height", "17px");
            	resetDropDown();
        	}
        }
        
    },
    'blank:pointerclick': function(evt,x,y){
    	for (var sel in selected){
			selected[sel].attr({rect: {stroke : cellStroke }});
		}
		selected = {};
		$("#input").blur().val("").css("height", "17px");
		resetDropDown();
    },
    "cell:pointerup": function (cellView, evt, x, y){
    	if (movedID != null && movedID > 0){
    		var m = {x:movedTo.x +155,y:movedTo.y}
    		var update = {'run':'task.Update','taskID':movedID,'position':m};
    		lc.send(update);
    		movedID = null;
    	}
    }
    
});

$(document).keyup(function(event){
	var k = event.which;
	var $in = $("#input");
	if (!$in.is(":focus") && (k == 46 || k == 8)){
		var taskIDs = [];
		for (var key in selected) {
			var sel = selected[key];
			var i = 0;
			while (i < Object.keys(targets).length){
				var k = Object.keys(targets)[i];
				if (targets[k] == sel.id) delete targets[k];
				i++;
			}
			var taskID = sel.attr("taskID");
			taskIDs.push(taskID);
			delete tasks[taskID];
			delete targets[sel.id];
			sel.remove();
			delete selected[key];
		}
		lc.send({'run':'task.Remove','taskIDs':taskIDs});
	}
	if (k == 16) shift = false;
	if (k == 18) alt = false;
});

$(document).keydown(function(event){
	var k = event.which;
	if (k == 16) shift = true;
	if (k == 18) alt = true;
});

$(document).on("mouseup", function(e){
	if (green) resetPorts();
});

/* DOWNLOADING THE SVG TO FILE
 * 
 * first set all style properties of the svg directly in the svg and not in the css 
$("#topRight").on("click", function () {
	var svg = btoa($("svg").parent().html());
    var fileURL = "data:image/octed-stream;base64,\n"+svg;
    var save = document.createElement('a');
    save.href = fileURL;
    save.target = '_blank';
    save.download = 'luci.svg';
    
    console.log($("svg").parent().html());

    var event = document.createEvent('Event');
    event.initEvent('click', true, true);
    save.dispatchEvent(event);
    (window.URL || window.webkitURL).revokeObjectURL(save.href);
});
*/

