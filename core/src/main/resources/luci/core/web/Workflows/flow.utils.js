var utilsInfo = {
	Viewer : {
		inputs: { value: "any"},
		outputs: {}
	}
}
var jView;

joint.shapes.devs.Viewer = joint.shapes.devs.Model.extend({
//			'<g class="rotatable"><g class="scalable"><rect class="body"/></g><text class="label"/><g class="inPorts"/><g class="outPorts"/></g>'
	markup: '<g class="rotatable"><g class="scalable"><rect class="body"/></g><text class="label"/><g class="inPorts"/><g class="outPorts"/><g class="html"/><foreignObject x="0" y="30" width="150px" height="150px"><body xmlns="http://www.w3.org/1999/xhtml"><div class="svgbody" style="padding: 4px; margin: 5px; overflow: hidden; font-family: monaco; font-size: 10px; border: 1px black dotted; max-height: 160px; "><p></p></div></body></foreignObject></g>',
    defaults: joint.util.deepSupplement({

        type: 'devs.Viewer',
        size: { width: 160, height: 190 },
        attrs: {
            '.label': { text: 'Viewer' },
            '.inPorts>.port0>.port-body': {type:"any"},
            inputs: ['value']
            
        }
    }, joint.shapes.devs.Model.prototype.defaults),
    
    setValue : function(v){
    	this.attr("value", v);
    	var p = $("<p></p>");
    	var string;
    	if (v instanceof Image){
    		var html;
    		var format = v.getFormat();
    		html = $("<img>");
    		html.attr("src", v.getSource());
    		html.attr("width", 130);
    		html.attr("height", 130);
    		p.append(html);
    	} else if (v instanceof Object){
    		string = JSON.stringify(v, null, '\t').replace(/\n/g, "</br>").replace(/\t/g, "&nbsp;&nbsp;&nbsp;");
    	} else string = v;
    	if (string !== undefined){
    		p.append(string);
    	}
    	var view = this.findView(paper);
    	jView = $(view.el);
    	jView.find(".svgbody>p").replaceWith(p);
    }

});
joint.shapes.devs.ViewerView = joint.shapes.devs.ModelView; 