9/*! JointJS v0.9.6 - JavaScript diagramming library  2015-12-19 


This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
//      JointJS library.
//      (c) 2011-2013 client IO

joint.shapes.devs = {};

joint.shapes.devs.Model = joint.shapes.basic.Generic.extend(_.extend({}, joint.shapes.basic.PortsModelInterface, {

    markup: '<g class="rotatable"><g class="scalable"><rect class="body"/></g><text class="label"/><g class="mainPorts"/><g class="inPorts"/><g class="outPorts"/></g>',
    portMarkup: '<g class="port port<%= id %>"><circle class="port-body"/><text class="port-label"/></g>',
    mainMarkup: '<g class="port port<%= id %>"><circle class="port-body"/></g>',

    defaults: joint.util.deepSupplement({

        type: 'devs.Model',
        size: { width: 1, height: 1 },

        inPorts: [],
        outPorts: [],
        mainPorts: [],

        attrs: {
            '.': { magnet: false },
            '.body': {
                width: 150, height: 250,
                stroke: '#2b3549',
                fill: '#ffffff',
            	'stroke-width': '1px',
            },
            '.port-body': {
                r: 4,
                magnet: true,
                stroke: '#ffffff',
                'stroke-width': '2px',
                fill: '#5e4e78'
            },
            text: {
            	'font-family': 'Helvetica',
            	'font-size': '10px',
                'pointer-events': 'none'
            },
            '.label': { text: 'Model', 'ref-x': .5, 'ref-y': 1, ref: '.body', 
            	'text-anchor': 'middle', fill: '#2b3549', 'font-weight': 800, 'font-size': '16px' },
            '.outPorts .port-label': { x:-10, dy: 4, y: 2, 'text-anchor': 'end', fill: '#6070a5' },
            '.inPorts .port-label':{ x: 10, dy: 4, y: 2, fill: '#6070a5' },
            '.inPorts':{ 'ref-y': 20 , ref: '.body' },
            '.outPorts':{ 'ref-y': 20 , ref: '.body'},
//            '.outPorts .port-body':{magnet: true},
            '.mainPorts .port-body':{fill:'white', stroke:'#546895', 'stroke-width': '1px', r:3},
            
        }

    }, joint.shapes.basic.Generic.prototype.defaults),

    getPortAttrs: function(portName, index, total, selector, type) {
        var attrs = {};

        var portClass = 'port' + index;
        var portSelector = selector + '>.' + portClass;
        var portLabelSelector = portSelector + '>.port-label';
        var portBodySelector = portSelector + '>.port-body';

        attrs[portLabelSelector] = { text: portName };
        attrs[portBodySelector] = { port: { id: portName || _.uniqueId(type) , type: type } };
        if (portName == "_mainOut_") {
        	attrs[portSelector] = { ref: '.body', 'ref-x': 120 , 'ref-y': 10 };
        	attrs[portBodySelector] = _.extend(attrs[portBodySelector], { magnet:true });
        } else attrs[portSelector] = { ref: '.body', 'ref-y': 10 + (index * 10) };

        if (selector === '.outPorts') { attrs[portSelector]['ref-dx'] = 0; }
        return attrs;
    },
    updatePortsAttrs: function(eventName) {

        // Delete previously set attributes for ports.
        var currAttrs = this.get('attrs');
        _.each(this._portSelectors, function(selector) {
            if (currAttrs[selector]) delete currAttrs[selector];
        });

        // This holds keys to the `attrs` object for all the port specific attribute that
        // we set in this method. This is necessary in order to remove previously set
        // attributes for previous ports.
        this._portSelectors = [];

        var attrs = {};

        _.each(this.get('inPorts'), function(portName, index, ports) {
            var portAttributes = this.getPortAttrs(portName, index, ports.length, '.inPorts', 'in');
            this._portSelectors = this._portSelectors.concat(_.keys(portAttributes));
            _.extend(attrs, portAttributes);
        }, this);

        _.each(this.get('outPorts'), function(portName, index, ports) {
            var portAttributes = this.getPortAttrs(portName, index, ports.length, '.outPorts', 'out');
            this._portSelectors = this._portSelectors.concat(_.keys(portAttributes));
            _.extend(attrs, portAttributes);
        }, this);
        
        _.each(this.get('mainPorts'), function(portName, index, ports) {
            var portAttributes = this.getPortAttrs(portName, index, ports.length, '.mainPorts', 'main');
            this._portSelectors = this._portSelectors.concat(_.keys(portAttributes));
            _.extend(attrs, portAttributes);
        }, this);

        // Silently set `attrs` on the cell so that noone knows the attrs have changed. This makes sure
        // that, for example, command manager does not register `change:attrs` command but only
        // the important `change:inPorts`/`change:outPorts` command.
        this.attr(attrs, { silent: true });
        // Manually call the `processPorts()` method that is normally called on `change:attrs` (that we just made silent).
        this.processPorts();
        // Let the outside world (mainly the `ModelView`) know that we're done configuring the `attrs` object.
        this.trigger('process:ports');
    }
}));


joint.shapes.devs.Service = joint.shapes.devs.Model.extend({

    defaults: joint.util.deepSupplement({

        type: 'devs.Service',
        size: { width: 120, height: 120 },
        attrs: {
            '.label': { text: 'Service' }
        }

    }, joint.shapes.devs.Model.prototype.defaults)

});

joint.shapes.devs.Coupled = joint.shapes.devs.Model.extend({

    defaults: joint.util.deepSupplement({

        type: 'devs.Coupled',
        size: { width: 200, height: 300 },
        attrs: {
            '.label': { text: 'Coupled' }
        }

    }, joint.shapes.devs.Model.prototype.defaults)
});

joint.shapes.devs.Link = joint.dia.Link.extend({

    defaults: {
        type: 'devs.Link',
        attrs: { '.connection' : { stroke: '#4B4F6A', 'stroke-width' :  2 }},
        smooth: true
    }
});

joint.shapes.devs.ModelView = joint.dia.ElementView.extend(_.extend({}, joint.shapes.basic.PortsViewInterface, {
	renderPorts: function() {

        var $inPorts = this.$('.inPorts').empty();
        var $outPorts = this.$('.outPorts').empty();
        var $mainPorts = this.$('.mainPorts').empty();

        var portTemplate = _.template(this.model.portMarkup);
        var mainTemplate = _.template(this.model.mainMarkup);

        _.each(_.filter(this.model.ports, function(p) { return p.type === 'in'; }), function(port, index) {

            $inPorts.append(V(portTemplate({ id: index, port: port })).node);
        });
        _.each(_.filter(this.model.ports, function(p) { return p.type === 'out'; }), function(port, index) {

            $outPorts.append(V(portTemplate({ id: index, port: port })).node);
        });
        _.each(_.filter(this.model.ports, function(p) { return p.type === 'main'; }), function(port, index) {

            $mainPorts.append(V(mainTemplate({ id: index, port: port })).node);
        });
    }
}));
joint.shapes.devs.ServiceView = joint.shapes.devs.ModelView;
joint.shapes.devs.CoupledView = joint.shapes.devs.ModelView;
