
function resetDropDown(){
	$("#inputs").html($('<select />'))
}

function updateDropDown(id, ports, taskID, viewerValue){
	
	var inputs = $("#inputs");
    var data = {};
    for (var name in ports){
    	var port = ports[name]
    	var targetKey = id+"/"+name;
    	if (port.type == "in" &&!(targetKey in targets)){
    		data[name] = port.val || "";
    	}
    }
    
    var s = $('<select />');
    for(var val in data) {
        $('<option />', {value: data[val], text: val}).appendTo(s);
    }
    s.on("change", function(evt){
    	$("#input").val($(this).val() || "")
    })
    inputs.attr("taskID", taskID)
    inputs.html(s);
    
    var option = $('#inputs select').find("option:selected");
    $("#input")
		.attr("taskID", taskID)
		.attr("modelID", id)
		
	if (taskID > 0) $("#input").val(option.val());
	else if (!(viewerValue instanceof Image)) 
		$("#input").val(JSON.stringify(viewerValue, null, "\t")).css("height", "200px");
}

$("#input").on("change", function(evt){
	var $this = $(this);
	var inputs = {};
	var val = $this.val();
	try {
		val = JSON.parse($this.val());
	} catch (error){
		if (error.message.indexOf("Single quotes") >= 0)
			println(error.message, "error");
	}
	
	var option = $('#inputs select').find("option:selected");
	var taskID = parseInt($("#input").attr("taskID"));
	var key = option.text();
	option.val(val);
	if (taskID > 0){
		inputs[removeModifiers(key)] = val;
		lc.sendAndReceive(
			{'run':'task.Update','taskID':taskID,'inputs':inputs}, 
			function(message){
				var $in = $("#input");
				var cell = paper.getModelById($in.attr("modelID"));
				var portName = key;
				cell.ports[portName]["val"] = val;
				$in.blur();
			});
	} else {
		try {
			paper.getModelById($this.attr("modelID")).setValue(JSON.parse(val));
		} catch (error){
			paper.getModelById($this.attr("modelID")).setValue(val);
		}
		
	}
});

$("#run").on("click", function(){
	console.log("TODO: 1. shutdown all servcies, 2. implement serviceID/persistendID in libs, 3. adapt RemoteRegister subscriptions to actually replace rows and turn the grey dot blue");
	lc.send({'run':'workflow.ServiceStart', 'taskID':loadedWorkflow.taskID}, function(message){
		var h = message.getHeader();
		console.log(h);
	});
});

function addWorkflows(workflowMap){
	var select = $("#workflows select");
	for (var id in workflowMap){
		select.append("<option value='"+id+"' running='false'>"+workflowMap[id]+"</option>");
	}
	load(select.val());
}

function getCurrentWorkflowID(){
	return parseInt($("#workflows select option:selected").val());
}

var change = function(e, dropdown){
	var select = dropdown || this;
	var taskID = $(select).val();
	clearRunningServices();
	if (taskID == "no") {
		loadedWorkflow = undefined;
		showRunningServices($("#runningPerNodeTable tbody"));
		$("#edit").css("opacity", 0.5);
		
	} else {
		load(parseInt(taskID));
		$("#edit").css("opacity", 1);
	}
	hideRunningInfo();
}

$("#workflows select").change(change);
if ($("#workflows select").val() == "no"){
	$("#edit").css("opacity", 0.5);
}

var loadedWorkflow;

function load(taskID, runningTaskID){
	if (typeof(taskID) != 'number') taskID = parseInt(taskID);
	if (taskID > 0){
//		console.log("loading "+taskID);
		lc.sendAndReceive({'run':'workflow.ServiceGet','taskID':taskID}, function(message){
			var h = message.getHeader();
			loadedWorkflow = h.result;
			loadedWorkflow['taskID'] = taskID;
			showWorkflowServices();
		});
	}
}

function showWorkflowServices(){
	var runningWorkflow = $("#workflows option[running=true]").prop("value");
	var services = loadedWorkflow[shownNode.ip];
	var runningServices = $("#runningPerNode tbody");
	for (var id in services){
		var connectedById = {}
		var running = false;
		if (runningWorkflow == loadedWorkflow.taskID && shownNode.services.connected.hasOwnProperty(id)){
			connectedById = shownNode.services.connected[id];
			running = true;
		} else {
			connectedById = {
				"serviceName": services[id].serviceName,
				"avgIdleTimeRatio": "",
				"bytesRead": "",
				"bytesSent": "",
				"transmissionComputationRatio": "" 
			};
		}
		
		runningServices.append(runningService(id, connectedById, running));
	}
}

var addedMySelf = false;

$("#add").on("click", function(e){
	addedMySelf = true;
	$("#workflows select").append("<option value='0' running='false'>New Workflow</option>");
	$("#workflows select").val("0");
	$("#edit").css("opacity", 1);
	renameWorkflow(e, $("#edit")[0], function(option, newName){
		// create a workflow with the given name
		lc.sendAndReceive({'run':'workflow.Create','name':newName}, function(message){
			var taskID = message.getHeader().result.taskID;
			option.val(taskID);
			clearRunningServices();
			$("#workflows select").change(change);
		})
	});
	$("#workflows input").focus();
	$("#messageDisplay").fadeOut();
})

$("#delete").on("click", function(){
	lc.send({'run':'task.Remove','taskIDs':[getCurrentWorkflowID()]});
	clearRunningServices();
	$("#workflows select option:selected").remove();
	load(getCurrentWorkflowID());
});

function clearRunningServices(){
	$("#runningPerNodeTable tbody").empty();
}

$("#edit").on("click", renameWorkflowFromIcon);

function renameWorkflowFromIcon(e){
	renameWorkflow(e, this, function(option, newName){
		// update the workflow with its new name
		var taskID = parseInt(option.val());
		var update = {'run':'task.Update','taskID':taskID,'name':newName}
		lc.send(update);
	});
}

var safedMySelf = false;

$("#safe").on("click", function(){
	safedMySelf = true;
	lc.sendAndReceive({'run':'workflow.Safe','taskID':getCurrentWorkflowID()}, 
		function(message){
			if (message.getHeader().result.change) println("successfully safed", "green");
			else println("nothing to safe");
			
	});
});

$("#reload").on("click", function(e){
	change(e, $("#workflows select")[0]);
	this.src = "button/reload.svg";
});

function renameWorkflow(e, edit, renameHandler){
	if ($("#workflows select").val() == "no") return;
	var rename = function(){
		var newName = input[0].value;
		input.replaceWith(select);
		var option = $("#workflows select option:selected");
		option.text(newName);
		renameHandler(option, newName);
	}
	
	var text = $("#workflows select option:selected").text();
	var select = $("#workflows select").replaceWith("<input type='text'></input>");
	var input = $("#workflows input");
	input[0].value = text;
	edit.src = "button/ok.svg";
	$(edit).off("click").on("click", function(){
		rename();
		edit.src = "button/edit.svg";
		$(edit).off("click").on("click", renameWorkflowFromIcon);
	});
	input.keypress(function(e){
		if (e.which == 13){
			rename();
			edit.src = "button/edit.svg";
			$(edit).off("click").on("click", renameWorkflowFromIcon);
		}
	});
}