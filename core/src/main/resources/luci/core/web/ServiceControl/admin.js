/** MIT License
 * @author Lukas Treyer
 * @date Apr 22, 2016
 * */

var ResponsePrinter = (function(){
	var ResponsePrinter = function(){};
	ResponsePrinter.prototype = new Luci.ResponseHandler();
	ResponsePrinter.prototype.onResult = function(message){
		var h = message.getHeader();
		if (h.serviceName == "RemoteRegister"){
			var name = h.result.registeredName;
			var id = h.result.id;
			var ip = h.result.nodeIP;
			if (ip == shownNode.ip && !(id in shownNode.services.connected)){
				var connectedById = {
					"serviceName": name,
					"avgIdleTimeRatio": 0,
					"bytesRead": 0,
					"bytesSent": 0,
					"transmissionComputationRatio": 0 
				};
				shownNode.services.connected[id] = connectedById;
				$("#runningPerNodeTable .placeholder:last-of-type").remove();
				$("#runningPerNodeTable tbody").append(runningService(id, connectedById));
			}
		} else if (h.serviceName == "RemoteDeregister"){
			// TODO: test stopping of remote services --> RemoteDeregister event in admin page
			console.log(h);
			var name = h.result.deregisteredName;
			var id = h.result.id;
			var ip = h.result.nodeIP;
			if (ip == shownNode.ip && id in shownNode.services.connected){
				delete shownNode.services.connected[id];
				$("#runningPerNodeTable tbody tr[id="+id+"]").remove();
			}			
		} else if (h.serviceName == "workflow.Safe"){
			if (safedMySelf) {
				safedMySelf = false;
				return;
			}
			if (h.result.change) {
				var taskID = parseInt(h.result.taskID);
				if (taskID == getCurrentWorkflowID()) {
					document.getElementById("reload").src = "button/reload_red.svg";
				}
			}
			
		} else if (h.serviceName == "workflow.Create"){
			if (addedMySelf) {
				addedMySelf = false;
				return;
			}
			var taskID = h.result.taskID;
			var name = h.result.name;
			$("#workflows select").append("<option value='"+taskID+"'>"+name+"</option>");
			
		} else if (h.taskID > 0){
			node = tasks[h.taskID];
			node.attr({rect: {fill : completed }});
			var successors = graph.getSuccessors(node);
			if (successors.length == 1){
				if (successors[0] instanceof joint.shapes.devs.Viewer){
					var viewer = successors[0];
					var keys = Object.keys(h.result);
					if (keys.length == 1)
						viewer.setValue(h.result[keys[0]]);
					else viewer.setValue(h.result);
				}
			}
			finishNodes();
		} else if (!("success" in h.result)){
			println(JSON.stringify(h.result).substring(0,50));
		}
	}
	ResponsePrinter.prototype.onError = function(message){
		var h = message.getHeader();
		if (!(h.serviceName == "RemoteRegister" || h.serviceName == "RemoteDeregister")){
			console.log("the error", message.getHeader().error)
			if (h.taskID > 0){
				node = tasks[h.taskID];
				node.attr({rect: {fill : error }});
				finishNodes();
			}
			println(message.getHeader().error, "error");
		}
	}
	ResponsePrinter.prototype.onProgress = function(message){
		var h = message.getHeader();
		if (h.taskID > 0){
			node = tasks[h.taskID];
			node.attr({rect: {fill : pending }});
		}
		
	}
	return ResponsePrinter;
})();




var lc = new Luci.Client(location.hostname, 8008, "", ResponsePrinter);