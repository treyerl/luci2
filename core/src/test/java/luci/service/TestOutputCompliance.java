/** MIT License
 * @author Lukas Treyer
 * @date Aug 8, 2016
 * */
package luci.service;

import static luci.console.TestConsole.setExampleInputAndValidateOutput;
import static luci.console.TestConsole.validateOutput;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import luci.service.workflow.Save;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import luci.connect.AttachmentAsFile;
import luci.connect.JSON;
import luci.connect.Message;
import luci.console.TestConsole;
import luci.console.TestConsole.BlockingCallHandler;
import luci.core.AbstractListener;
import luci.core.Luci;
import luci.core.TaskInfo;
import luci.core.validation.LcValidationException;
import luci.service.test.RemoteFileEcho;
import luci.service.test.Validation;
import luci.service.user.User.EmailAddressTakenException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestOutputCompliance {
	
	Validation valid = new Validation();
	static Luci luci;
	static long taskID;
	static int userID;
	static File permissions;
	static File users;
	static File workflows;
	static TestConsole console;
	static BlockingCallHandler stdHandler;
	static RemoteFileEcho remoteFileEcho;
	static Map<String, String> testFiles1;
	static Map<String, String> testFiles3;
	
	@BeforeClass
	public static void startupLuci(){
		luci = Luci.start(new String[]{"-noBrowser","-cwd", "target"});
		permissions = new File(Luci.cwd(), "data"+File.separator+"servicePermissions.json");
		users = new File(Luci.cwd(), "data"+File.separator+"users");
		workflows = new File(Luci.cwd(), "data"+File.separator+"workflows");
		try {
			console = new TestConsole();
			console.connect("localhost","7654");
			console.awaitConnected();
			stdHandler = console.getCurrentHandler();
			remoteFileEcho = new RemoteFileEcho();
			new Thread(remoteFileEcho).start();
			remoteFileEcho.connect("localhost", 7654);
			remoteFileEcho.awaitConnected();
		} catch (IOException | InterruptedException e) {}
		testFiles1 = new TreeMap<>();
		testFiles1.put("file1", "external-resources/test.jpg");
		testFiles3 = new TreeMap<>(testFiles1);
	}
	
	@AfterClass
	public static void cleanup(){
		permissions.delete();
		users.delete();
		workflows.delete();
	}
	
	// :::::::: BASIC services
	
	@Test
	public void t01attachmentJSON() throws Exception {
		setExampleInputAndValidateOutput(new JsonTypes());
	}
	
	@Test
	public void t02download() throws Exception {
		Download d = new Download();
		d.setInput(new Message(d.getExampleCall()));
		d.getInputDescription().validate(d.getInput().getHeader());
		JSONObject jsonOutput = d.testImplementation().getHeader();
		d.getOutputDescription().validate(jsonOutput);
	}
	
	@Test
	public void t03exists() throws Exception {
		setExampleInputAndValidateOutput(new Exists());
	}
	
	@Test
	public void t04filterServices() throws Exception{
		setExampleInputAndValidateOutput(new FilterServices());
	}
	
	@Test
	public void t05getStartupTime() throws Exception{
		setExampleInputAndValidateOutput(new GetStartupTime());
	}
	
	@Test
	public void t06serviceHistory() throws Exception{
		setExampleInputAndValidateOutput(new ServiceHistory());
	}

	@Test
	public void t07serviceInfo() throws Exception{
		setExampleInputAndValidateOutput(new ServiceInfo());
	}
	
	@Test
	public void t08serviceList() throws Exception{
		setExampleInputAndValidateOutput(new ServiceList());
	}
	
	@Test
	public void t09serviceControl() throws Exception{
		setExampleInputAndValidateOutput(new ServiceControl());
	}
	
	// :::::::: TASK services
	
	@Test
	public void t10createTask() throws Exception{
		JSONObject output = setExampleInputAndValidateOutput(new luci.service.task.Create());
		taskID = output.getJSONObject("result").getLong("taskID");
		assertTrue(workflows.exists());
//		System.out.println("taskID "+taskID);
	}
	
	@Test
	public void t11getTask() throws Exception{
		luci.service.task.Get get = new luci.service.task.Get();
		get.setInput(new Message(new JSONObject().put("run", "task.Get").put("taskID", taskID)));
		validateOutput(get);
	}
	
	@Test
	public void t12update() throws Exception{
		luci.service.task.Update u = new luci.service.task.Update();
		u.setInput(new Message(new JSONObject()
				.put("run", "task.Update")
				.put("taskID", taskID)
				.put("inputs", new JSONObject("{'in1':25}"))));
		validateOutput(u);
	}
	
	// call workflow.Save first so a task can be reverted
	@Test(expected=IllegalStateException.class)
	public void t13revertTask() throws Exception{
		luci.service.task.Revert revert = new luci.service.task.Revert();
		revert.setInput(new Message(new JSONObject().put("run", "task.Revert").put("taskID", taskID)));
		validateOutput(revert);
	}
	
	@Test
	public void t14removeTask() throws Exception{
		luci.service.task.Remove remove = new luci.service.task.Remove();
		remove.setInput(new Message(new JSONObject().put("run", "task.Remove").put("taskIDs", new long[]{taskID})));
		validateOutput(remove);
	}
	
	// :::::::: TEST services
	
	@Test
	public void t15delay() throws Exception {
		luci.service.test.Delay d = new luci.service.test.Delay();
		d.setInput(new Message(new JSONObject().put("seconds", 1).put("run", "test.Delay")));
		validateOutput(d);
	}
	
	@Test(expected=RuntimeException.class)
	public void t16error() throws Exception {
		setExampleInputAndValidateOutput(new luci.service.test.Error());
	}
	
	@Test
	public void t17fibonacci() throws Exception {
		setExampleInputAndValidateOutput(new luci.service.test.Fibonacci());
	}
	
	@Test
	public void t18randomly() throws Exception {
		setExampleInputAndValidateOutput(new luci.service.test.Randomly());
	}
	
	@Test
	public void t19validation() throws Exception {
		luci.service.test.Validation v = new luci.service.test.Validation();
		JSONObject jsonOutput = setExampleInputAndValidateOutput(v);
		List<String> errorTestStrings = JSON.ArrayToStringList(jsonOutput.getJSONObject("result")
				.getJSONArray("errorTestStrings"));
		try {
			v.setInput(new Message(new JSONObject(errorTestStrings.get(0))));
			validateOutput(v);
		} catch (LcValidationException e){
//			System.out.println(e.getMessage());
			try {
				v.setInput(new Message(new JSONObject(errorTestStrings.get(1))));
				validateOutput(v);
			} catch (LcValidationException e1){
//				System.out.println(e1.getMessage());
				try {
					v.setInput(new Message(new JSONObject(errorTestStrings.get(2))));
					validateOutput(v);
				} catch (LcValidationException e2){
//					System.out.println(e2.getMessage());
					try {
						v.setInput(new Message(new JSONObject(errorTestStrings.get(3))));
						validateOutput(v);
					} catch (LcValidationException e3){
//						System.out.println(e3.getMessage());
						try {
							v.setInput(new Message(new JSONObject(errorTestStrings.get(4))));
							validateOutput(v);
						} catch (LcValidationException e4){
//							System.out.println(e3.getMessage());
						}
					}
				}
			}
		}
	}
	
	// :::::::: USER services
	
	@Test
	public void t20user() throws Exception {
		try {
			setExampleInputAndValidateOutput(new luci.service.user.Create());
		} catch (EmailAddressTakenException e){
			// that's good enough
		}
		assertTrue(users.exists());
	}

	@Test
	public void t21auth() throws Exception {
		luci.service.user.Authenticate auth = new luci.service.user.Authenticate();
		auth.setInput(new Message(new JSONObject()
				.put("run", "user.Authenticate")
				.put("email", "abc@domain.com")
				.put("password", "5413EE24723BBA2C5A6BA2D0196C78B3EE4628D1")));
		validateOutput(auth);
	}
	
	@Test
	public void t22userlist() throws Exception {
		setExampleInputAndValidateOutput(new luci.service.user.List());
	}
	
	@Test
	public void t23permission() throws Exception {
		setExampleInputAndValidateOutput(new luci.service.user.Permission());
		assertTrue(permissions.exists());
	}
	
	@Test
	public void t24userupdate() throws Exception {
		luci.service.user.Update update = new luci.service.user.Update();
		update.setInput(new Message(new JSONObject("{'id':1,'email':'mynew@email.address','run':'user.Update'}")));
		validateOutput(update);
	}
	
	@Test
	public void t25deleteUser() throws Exception {
		luci.service.user.Delete delete = new luci.service.user.Delete();
		delete.setInput(new Message(new JSONObject("{'id':1,'run':'user.Delete'}")));
		validateOutput(delete);
	}
	
	
	// :::::::: WORKFLOW services
	
	@Test
	public void t26createworkflow() throws Exception {
		taskID = setExampleInputAndValidateOutput(new luci.service.workflow.Create())
				.getJSONObject("result").getLong("taskID");
//		System.out.println("taskID "+taskID);
	}
	
	@Test
	public void t27listworkflow() throws Exception {
		setExampleInputAndValidateOutput(new luci.service.workflow.List());
	}
	
	@Test
	public void t28safeworkflow() throws Exception {
		Save save = new Save();
		save.setInput(new Message(new JSONObject().put("run", "workflow.Save").put("taskID", taskID)));
		validateOutput(save);
	}
	
	@Test
	public void t29updateIO() throws Exception {
		luci.service.workflow.UpdateIO update = new luci.service.workflow.UpdateIO();
		update.setInput(new Message(new JSONObject()
				.put("run", "workflow.UpdateIO")
				.put("taskID", taskID)
				.put("inputs", new JSONObject("{'amount':{'type':'number','default':'11'}}"))
				));
		validateOutput(update);
	}
	
	@Test
	public void t30serviceGet() throws Exception {
		luci.service.workflow.ServiceGet serviceGet = new luci.service.workflow.ServiceGet();
		serviceGet.setInput(new Message(serviceGet.getExampleCall().put("taskID", taskID)));
		validateOutput(serviceGet);
	}
	
	@Test
	public void t31serviceStart() throws Exception {
		luci.service.workflow.ServiceStart serviceStart = new luci.service.workflow.ServiceStart();
		AbstractListener sl = new AbstractListener(){
			public void getNotified(TaskInfo ti, Message m, long duration) {
				serviceStart.getOutputDescription().validate(m.getHeader());
			}
		};
		serviceStart.setInput(new Message(serviceStart.getExampleCall().put("taskID", taskID)));
		serviceStart.addListener(sl);
		serviceStart.implementation();
	}
	
	@Test
	public void t32serviceUpdate() throws Exception {
		luci.service.workflow.ServiceUpdate serviceUpdate = new luci.service.workflow.ServiceUpdate();
		serviceUpdate.setInput(new Message(serviceUpdate.getExampleCall().put("taskID", taskID)));
		validateOutput(serviceUpdate);
	}
	
	@Test
	public void t33serviceRemove() throws Exception {
		luci.service.workflow.ServiceRemove serviceRemove = new luci.service.workflow.ServiceRemove();
		serviceRemove.setInput(new Message(serviceRemove.getExampleCall().put("taskID", taskID)));
		validateOutput(serviceRemove);
	}
	
	
	// :::::::: ONLINE services (require the TestConsole)
	
	@Test
	public void t34subscribeTo() throws Exception {
		console.run("task.SubscribeTo", "serviceName=RemoteRegister", "inclResults=true");
		Message m  = console.getCurrentHandler().await();
		new luci.service.task.SubscribeTo().getOutputDescription().validate(m.getHeader());
	}
	
	@Test
	public void t35remoteRegister() throws Exception {
//		stdHandler.reset();
		console.run("RemoteRegister","serviceName=test", "exampleCall={'run':'test'}", 
				"description=\"A RemoteService for testing service registration.\"");
		Message m = console.getCurrentHandler().await();
		new RemoteRegister().getOutputDescription().validate(m.getHeader());
		m = stdHandler.await();
		new RemoteRegister().getOutputDescription().validate(m.getHeader());
	}
	
	@Test
	public void t36unsubscribeFrom() throws Exception {
		console.run("task.UnsubscribeFrom", "serviceName=RemoteRegister");
		Message m = console.getCurrentHandler().await();
		new luci.service.task.UnsubscribeFrom().getOutputDescription().validate(m.getHeader());
	}
	
	@Test
	public void t37remoteDeregister() throws Exception {
		console.run("RemoteDeregister");
		Message m = console.getCurrentHandler().await();
		new RemoteDeregister().getOutputDescription().validate(m.getHeader());
	}
	
	@Test
	public void t38getRandomFile1() throws Exception {
		AttachmentAsFile.parentDirectory = new File("external-resources");
		console.run("test.RandomBytes", "size=3MB");
		AttachmentAsFile a = (AttachmentAsFile) console.getCurrentHandler().await().getAttachment(0);
		testFiles3.put("file2", a.getFile().toString());
	}
	
	@Test
	public void t38getRandomFile2() throws Exception {
		AttachmentAsFile.parentDirectory = new File("external-resources");
		console.run("test.RandomBytes", "size=30MB");
		AttachmentAsFile a = (AttachmentAsFile) console.getCurrentHandler().await().getAttachment(0);
		testFiles3.put("file3", a.getFile().toString());
	}
	
	private void testFileEchoes(Map<String, String> files) throws InterruptedException{
		Message m = console.getCurrentHandler().await();
		JSONObject h = m.getHeader();
		for (Entry<String, String> e: files.entrySet()){
			String r_chck = h.getJSONObject("result")
					.getJSONObject(e.getKey())
					.getJSONObject("attachment")
					.getString("checksum");
			assertTrue(new AttachmentAsFile(new File(e.getValue())).getChecksum().equals(r_chck));
			new File(new File("target"), r_chck+".jpg").delete();
		}
	}
	
	private String[] mapParameters(Map<String, String> map){
		String[] params = new String[map.size()];
		int i = 0;
		for (Entry<String, String> e: map.entrySet()){
			params[i++] = e.getKey()+"="+e.getValue();
		}
		return params;
	}
	
	@Test
	public void t39FileEcho1() throws Exception {
		AttachmentAsFile.parentDirectory = new File("target");
		console.run("test.FileEcho", mapParameters(testFiles1));
		testFileEchoes(testFiles1);
	}
	
	@Test
	public void t39FileEcho3() throws Exception {
		AttachmentAsFile.parentDirectory = new File("target");
		String[] p = mapParameters(testFiles3);
		System.out.println(Arrays.toString(p));
		console.run("test.FileEcho", p);
		testFileEchoes(testFiles3);
	}
	
	@Test
	public void t40RemoteFileEcho1() throws Exception {
		AttachmentAsFile.parentDirectory = new File("target");
		console.run("test.RemoteFileEcho", mapParameters(testFiles1));
		testFileEchoes(testFiles1);
	}
	
	@Test
	public void t40RemoteFileEcho3() throws Exception {
		AttachmentAsFile.parentDirectory = new File("target");
		console.run("test.RemoteFileEcho", mapParameters(testFiles3));
		testFileEchoes(testFiles3);
		new File(testFiles3.get("file2")).delete();
		new File(testFiles3.get("file3")).delete();
		new File(testFiles3.get("file2").replace("external-resources", "target")).delete();
		new File(testFiles3.get("file3").replace("external-resources", "target")).delete();
	}
	
}
